#include "hal_gpio.h"
#include "hal.h"
#include "hal_types.h"
#include "ev_poll.h"
#include "tools.h"

#include <glib/gprintf.h>
#include <linux/input.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <syslog.h>
#include <glib.h>
#include <poll.h>

#define MODULE_NAME         "[gpio] "
#define BUF_SIZE            1024
#define MAX_GPIOS           3

typedef struct
{
    char * path_value;
    char * path_state;
    int fd_value;
    int fd_state;
    hal_gpio_state state;
    hal_gpio_state current_value;
} gpio_node;

static gpio_node gpio_data[MAX_GPIOS] =
{
        {
          .path_value = "/var/links/gpio/spare1-value",
          .path_state = "/var/links/gpio/spare1-state"
        },
        {
          .path_value = "/var/links/gpio/spare2-value",
          .path_state = "/var/links/gpio/spare2-state"
        },
        {
          .path_value = "/var/links/gpio/spare3-value",
          .path_state = "/var/links/gpio/spare3-state"
        },
};

static gboolean initialized = FALSE;
static GMutex mutex;
static guint cap_num_gpios = 0;

static gpointer ev_poll_callback ( gpointer data )
{
    hal_error err;
    int idx = (int)data;
    int val;
    struct input_event ev;
    gpio_node *node;

    if ( idx >= cap_num_gpios || gpio_data[idx].fd_state == -1 )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Invalid callback parameters: %d", idx );
        return NULL;
    }

    g_mutex_lock( &mutex );

    node =  &gpio_data[idx];
    err = file_get_value_int( node->fd_state, &val );

    if ( err != HAL_E_OK )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not read state (idx: %d)", idx );
        g_mutex_unlock( &mutex );
        return NULL;
    }
    else
    {
        if ( val == 0 )
        {
            node->state = HAL_GPIO_STATE_LOW;
        }
        else
        {
            node->state = HAL_GPIO_STATE_HIGH;
        }
    }

    g_mutex_unlock( &mutex );

    return NULL;
}

static gboolean check_gpio( hal_gpio c )
{
    return !( ( cap_num_gpios == 0 ) || ( cap_num_gpios <= (guint)c ) );
}

static hal_error check_gpio_caps()
{
    hal_error err = HAL_E_OK;
    uint32_t cap;

    cap_num_gpios = 0;
    err = hal_syscfg_get_capability( HAL_SYSCAP_GPIO, &cap );
    if ( err != HAL_E_OK )
    {
        return HAL_E_FAIL;
    }
    
    switch ( cap )
    {
        case SYSCFG_GPIO_3x:
        case SYSCFG_ALL_FEATURES:
            cap_num_gpios = 3;
            break;
        default:
            err = HAL_E_NOFEATURE;
            break;
    }
    
    return err;
}

hal_error hal_gpio_init()
{
    hal_error err;
    char tmp[BUF_SIZE];
    int tmp_fd;
    CHECK_HAL_INITIALIZED();
    CHECK_MODULE_ALREADY_INITIALIZED( initialized );
    
    err = check_gpio_caps();
    if (err == HAL_E_FAIL)
    {
        return HAL_E_FAIL;
    }

    for ( int i = 0; i < cap_num_gpios; i++ )
    {
        g_sprintf( tmp, "%s/value", gpio_data[i].path_value );
        gpio_data[i].fd_value = open( tmp, O_WRONLY );
        if ( gpio_data[i].fd_value == -1 )
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Could not open \"value\" path %s: %d", tmp, errno );
            return HAL_E_FAIL;
        }

        g_sprintf( tmp, "%s/value", gpio_data[i].path_state );
        gpio_data[i].fd_state = open( tmp, O_RDONLY );
        if ( gpio_data[i].fd_state == -1 )
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Could not open \"state\" path %s: %d", tmp, errno );
            return HAL_E_FAIL;
        }

        g_sprintf( tmp, "%s/edge", gpio_data[i].path_state );
        tmp_fd = open( tmp, O_WRONLY );
        if ( tmp_fd != -1 )
        {
            err = file_set_value_str( tmp_fd, "both" );
            if ( err != HAL_E_OK )
            {
                hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                    "Could not open \"edge\" file %s: %d", tmp, errno );
                return err;
            }
            else
            {
                close( tmp_fd );
            }
        }

        ev_poll_callback( (gpointer)i );
        ev_poll_add( gpio_data[i].fd_state, POLLPRI, &ev_poll_callback, (gpointer)i );
    }
    
    initialized = TRUE;

    for ( int i = 0; i < cap_num_gpios; i++ )
    {
        // Set to "high", so that set_state really sets state;
        gpio_data[i].current_value = HAL_GPIO_STATE_HIGH;
        hal_gpio_set_state(i, HAL_GPIO_STATE_LOW);
    }
    return HAL_E_OK;
}

hal_error hal_gpio_deinit()
{
    gpio_node *n;
    CHECK_MODULE_INITIALIZED( initialized );

    for ( int i = 0; i < cap_num_gpios; i++ )
    {
        n = &(gpio_data[i]);
        if ( n->fd_state != -1 )
        {
            ev_poll_remove( n->fd_state );

            close(n->fd_state);
            n->fd_state = -1;
        }

        if ( n->fd_value != -1 )
        {
            close(n->fd_value);
            n->fd_value = -1;
        }
    }

    initialized = FALSE;

    return HAL_E_OK;
}

hal_error hal_gpio_set_state( hal_gpio gpio, hal_gpio_state state )
{
    hal_error err;
    guint idx;

    CHECK_MODULE_INITIALIZED( initialized );

    if ( !check_gpio( gpio ) )
    {
        return HAL_E_NOFEATURE;
    }
    
    idx = (guint) gpio;

    if ( state == gpio_data[idx].current_value )
    {
        return HAL_E_OK;
    }

    g_mutex_lock( &mutex );

    switch ( state )
    {
        case HAL_GPIO_STATE_LOW:
            err = file_set_value_int( gpio_data[idx].fd_value, 0);
            break;
        case HAL_GPIO_STATE_HIGH:
            err = file_set_value_int( gpio_data[idx].fd_value, 1);
            break;
        default:
            g_mutex_unlock( &mutex );
            return HAL_E_INVALID_PARAMETER;
    }

    if ( err != HAL_E_OK )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not set GPIO%u state", idx );
    }
    else
    {
        gpio_data[idx].current_value = state;
    }

    g_mutex_unlock( &mutex );

    return err;
}

hal_error hal_gpio_get_state( hal_gpio gpio, hal_gpio_state * const state )
{
    hal_error err;
    guint idx;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( state );

    if ( !check_gpio( gpio ) )
    {
        return HAL_E_NOFEATURE;
    }
    
    idx = (guint) gpio;

    *state = gpio_data[idx].state;

    return HAL_E_OK;
}
