#ifndef EV_POLL_H_
#define EV_POLL_H_

#include "hal_types.h"
#include <poll.h>
#include <glib.h>

hal_error ev_poll_init();
hal_error ev_poll_add( int fd
                     , short events
                     , GThreadFunc cb
                     , gpointer data );
void ev_poll_remove( int fd );
hal_error ev_poll_deinit();

#endif /* EV_POLL_H_ */
