#include <glib.h>
#include <linux/input.h>
#include <sys/types.h>
#include <fcntl.h>
#include <syslog.h>
#include <errno.h>
#include <poll.h>
#include <unistd.h>
#include <stdio.h>

#include "hal.h"
#include "hal_encoder.h"
#include "hal_types.h"
#include "tools.h"
#include "ev_poll.h"

#define MODULE_NAME         "[encoder] "
#define DEV_ENCODER         "/dev/input/encoder"
#define MAX_KEYS            3

static int fd_encoder_input = -1;
static GMutex mutex;
static key_state_t key_state[MAX_KEYS];

static gboolean initialized = FALSE;

static gboolean cap_has_encoder = FALSE;

static gpointer ev_poll_callback ( gpointer data )
{
    int bytes_read;
    struct input_event ev;
    int key_idx;

    if ( fd_encoder_input == -1 )
    {
        return NULL;
    }

    if ( read(fd_encoder_input, &ev, sizeof(ev)) == sizeof(ev) )
    {
        if ( ev.type == EV_KEY )
        {
            switch ( ev.code )
            {
                case KEY_DOWN:
                    key_idx = HAL_ENCODER_LEFT;
                    break;
                case KEY_UP:
                    key_idx = HAL_ENCODER_RIGHT;
                    break;
                case KEY_ENTER:
                    key_idx = HAL_ENCODER_ENTER;
                    break;
                default:
                    return NULL;
            }

            g_mutex_lock( &mutex );

            if ( ev.value == 1 )
            {
                key_state[key_idx].pressed = TRUE;
            }
            else
            {
                key_state[key_idx].pressed = FALSE;
                if ( key_state[key_idx].count < UINT16_MAX )
                {
                    key_state[key_idx].count++;
                }
            }

            g_mutex_unlock( &mutex );
        }

    }

    return NULL;
}

hal_error hal_encoder_init()
{
    hal_error err = HAL_E_OK;
    uint32_t cap = 0;
    
    CHECK_HAL_INITIALIZED();
    CHECK_MODULE_ALREADY_INITIALIZED( initialized );
    
    err = hal_syscfg_get_capability( HAL_SYSCAP_ENCODER, &cap );
    if ( err != HAL_E_OK )
    {
        return HAL_E_FAIL;
    }
    
    cap_has_encoder = ( cap != SYSCFG_FIELD_NONE );
    
    if ( cap_has_encoder )
    {
        fd_encoder_input = open( DEV_ENCODER, O_RDONLY );
        if ( fd_encoder_input == -1 )
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Could not open encoder input device node " DEV_ENCODER ": %d",
                errno );
            return HAL_E_FAIL;
        }

        for ( int i = 0; i < MAX_KEYS; i++ )
        {
            key_state[i].count = 0;
            key_state[i].pressed = FALSE;
        }

        ev_poll_add( fd_encoder_input
                   , POLLIN
                   , &ev_poll_callback
                   , NULL);
        
        initialized = TRUE;
        return HAL_E_OK;
    }
    else
    {
        initialized = TRUE;
        return HAL_E_NOFEATURE;
    }
}

hal_error hal_encoder_deinit()
{
    CHECK_MODULE_INITIALIZED( initialized );

    if ( fd_encoder_input != -1 )
    {
        ev_poll_remove( fd_encoder_input );
        close( fd_encoder_input );
        fd_encoder_input = -1;
    }

    initialized = FALSE;
    return HAL_E_OK;
}

hal_error hal_encoder_get_device(uint8_t const ** device)
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_encoder );
    
    if (device == NULL)
    {
        return HAL_E_NULL_POINTER;
    }
    
    *device = DEV_ENCODER;
    return HAL_E_OK;
}

hal_error hal_encoder_get_key_state( hal_encoder_key key, bool * const pressed, uint16_t * const count )
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_encoder );

    if ( key >= MAX_KEYS )
    {
        return HAL_E_INVALID_PARAMETER;
    }

    g_mutex_lock( &mutex );
    SET_IF_NOT_NULL( pressed, key_state[key].pressed );
    SET_IF_NOT_NULL( count, key_state[key].count );

    key_state[key].count = 0;

    g_mutex_unlock( &mutex );

    return HAL_E_OK;
}
