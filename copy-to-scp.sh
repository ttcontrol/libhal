#!/bin/sh

if [ -z $1 ]; then
        echo "Please specify target IP."
        exit
fi

sshpass -p dude scp src/.libs/libhal.so.* root@$1:/usr/lib
sshpass -p dude scp tools/scom/.libs/scom_tool root@$1:/usr/bin
sshpass -p dude scp `find tests/.libs/ -executable -type f` root@$1:/usr/bin
#	data/keymaps/* root@$1:/lib/udev/keymaps \
#	tools/update-sysconfig root@$1:/usr/bin \

