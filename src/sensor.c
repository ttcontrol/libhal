#include "hal_sensor.h"
#include "hal.h"
#include "hal_types.h"
#include "tools.h"

#include <linux/input.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <syslog.h>
#include <glib.h>
#include <stdio.h>

#define MODULE_NAME         "[sensor] "
#define DEV_AMBIENT_LUX     "/sys/class/hwmon/hwmon0/device/lux"
#define DEV_AMBIENT_TEMP    "/sys/devices/platform/vision2_sensors/ambient_temp"
#define DEV_CPU_TEMP        "/sys/devices/platform/vision2_sensors/cpu_temp"
#define DEV_PMIC_TEMP       "/sys/devices/platform/vision2_sensors/pmic_temp"
#define DEV_K30_VOLT        "/sys/devices/platform/vision2_sensors/k30_voltage"
#define DEV_BATT_VOLT       "/sys/devices/platform/vision2_sensors/licell_voltage"
#define DEV_PMIC_VOLT       "/sys/devices/platform/vision2_sensors/main_voltage"
#define DEV_PMIC_CURR       "/sys/devices/platform/vision2_sensors/main_current"

static int fd_amb_lux = -1;
static int fd_amb_temp = -1;
static int fd_cpu_temp = -1;
static int fd_pmic_temp = -1;
static int fd_k30_volt = -1;
static int fd_batt_volt = -1;
static int fd_pmic_volt = -1;
static int fd_pmic_curr = -1;

static gboolean cap_has_ambient = FALSE;

static gboolean initialized = FALSE;
static GMutex mutex;

static hal_error check_sensor_caps()
{
    hal_error err;
    uint32_t cap;

    cap_has_ambient = FALSE;
    err = hal_syscfg_get_capability( HAL_SYSCAP_AMBIENT_SENSOR, &cap );
    if ( err != HAL_E_OK )
    {
        return HAL_E_FAIL;
    }

    switch ( cap )
    {
        case SYSCFG_AMBSENS_BH1840:
        case SYSCFG_ALL_FEATURES:
            cap_has_ambient = TRUE;
            break;
    }
    
    return HAL_E_OK;
}

hal_error hal_sensor_init()
{
    hal_error err;
    CHECK_HAL_INITIALIZED();
    CHECK_MODULE_ALREADY_INITIALIZED( initialized );
    
    err = check_sensor_caps();
    if ( err == HAL_E_FAIL)
    {
        return HAL_E_FAIL;
    }

    err = HAL_E_OK;

    if ( cap_has_ambient )
    {
        fd_amb_lux = open( DEV_AMBIENT_LUX, O_RDONLY );
        if ( fd_amb_lux == -1 )
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Could not open ambient brightness device node "
                DEV_AMBIENT_LUX ": %d", errno );
            err = HAL_E_SENSOR_INIT_FAILED;
        }
    }

    fd_amb_temp = open( DEV_AMBIENT_TEMP, O_RDONLY );
    if ( fd_amb_temp == -1 )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not open ambient temperature device node "
            DEV_AMBIENT_TEMP ": %d", errno );
        err = HAL_E_SENSOR_INIT_FAILED;
    }

    fd_cpu_temp = open( DEV_CPU_TEMP, O_RDONLY );
    if ( fd_cpu_temp == -1 )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not open CPU temperature device node "
            DEV_CPU_TEMP ": %d", errno );
        err = HAL_E_SENSOR_INIT_FAILED;
    }

    fd_pmic_temp = open( DEV_PMIC_TEMP, O_RDONLY );
    if ( fd_pmic_temp == -1 )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not open PMIC temperature device node "
            DEV_PMIC_TEMP ": %d", errno );
        err = HAL_E_SENSOR_INIT_FAILED;
    }

    fd_k30_volt = open( DEV_K30_VOLT, O_RDONLY );
    if ( fd_k30_volt == -1 )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not open k30 voltage device node " DEV_K30_VOLT ": %d",
            errno );
        err = HAL_E_SENSOR_INIT_FAILED;
    }

    fd_batt_volt = open( DEV_BATT_VOLT, O_RDONLY );
    if ( fd_batt_volt == -1 )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not open RTC battery voltage device node " DEV_BATT_VOLT ": %d",
            errno );
        err = HAL_E_SENSOR_INIT_FAILED;
    }

    fd_pmic_volt = open( DEV_PMIC_VOLT, O_RDONLY );
    if ( fd_pmic_volt == -1 )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not open PMIC voltage device node " DEV_PMIC_VOLT ": %d",
            errno );
        err = HAL_E_SENSOR_INIT_FAILED;
    }

    fd_pmic_curr = open( DEV_PMIC_CURR, O_RDONLY );
    if ( fd_pmic_curr == -1 )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not open PMIC current device node " DEV_PMIC_CURR ": %d",
            errno );
        err = HAL_E_SENSOR_INIT_FAILED;
    }

    initialized = TRUE;
    return err;
}

hal_error hal_sensor_deinit()
{
    CHECK_MODULE_INITIALIZED( initialized );

    if ( fd_amb_lux != -1 )
    {
        close( fd_amb_lux );
        fd_amb_lux = -1;
    }

    if ( fd_amb_temp != -1 )
    {
        close( fd_amb_temp );
        fd_amb_temp = -1;
    }

    if ( fd_cpu_temp != -1 )
    {
        close( fd_cpu_temp );
        fd_cpu_temp = -1;
    }

    if ( fd_pmic_temp != -1 )
    {
        close( fd_pmic_temp );
        fd_pmic_temp = -1;
    }

    if ( fd_k30_volt != -1 )
    {
        close( fd_k30_volt );
        fd_k30_volt = -1;
    }

    if ( fd_batt_volt != -1 )
    {
        close( fd_batt_volt );
        fd_batt_volt = -1;
    }

    if ( fd_pmic_volt != -1 )
    {
        close( fd_pmic_volt );
        fd_pmic_volt = -1;
    }

    if ( fd_pmic_curr != -1 )
    {
        close( fd_pmic_curr );
        fd_pmic_curr = -1;
    }

    initialized = FALSE;

    return HAL_E_OK;
}


hal_error hal_sensor_get_value( hal_sensor_type type, int32_t * const value,
    bool * const fresh )
{
    hal_error err = HAL_E_OK;
    int res;
    int32_t val;
    char buf[8];

    CHECK_POINTER_PARAMETER( value );
    CHECK_MODULE_INITIALIZED( initialized );

    switch ( type )
    {
        case HAL_SENS_TYPE_AMBIENT_LUX:

            CHECK_FEATURE( cap_has_ambient );
            if ( fd_amb_lux == -1 )
            {
                err = HAL_E_SENSOR_ERROR;
            }
            else
            {
                g_mutex_lock( &mutex );
                res = pread( fd_amb_lux, buf, sizeof(buf), 0 );
                g_mutex_unlock( &mutex );

                if ( res == -1 )
                {
                    hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                        "Could not read from ambient lux device node: %d",
                        errno );
                    err = HAL_E_FAIL;
                }
                else if ( res == 0 )
                {
                    err = HAL_E_OK;
                    SET_IF_NOT_NULL( fresh, FALSE );
                }
                else if ( res > 0)
                {
                    res = sscanf(buf, "%d", &val);
                    if ( res == 1 )
                    {
                        *value = val;
                        SET_IF_NOT_NULL( fresh, TRUE );
                    }
                    else
                    {
                        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                            "Could not parse value from ambient lux device node: %d",
                            errno );
                        err = HAL_E_FAIL;
                    }
                }
            }
            break;

        case HAL_SENS_TYPE_AMBIENT_TEMP:
            if ( fd_amb_temp == -1 )
            {
                err = HAL_E_SENSOR_ERROR;
            }
            else
            {
                g_mutex_lock( &mutex );
                err = file_get_value_int( fd_amb_temp, &val );
                g_mutex_unlock( &mutex );

                if ( err == HAL_E_OK )
                {
                    *value = val;
                    SET_IF_NOT_NULL( fresh, TRUE );
                }
            }
            break;

        case HAL_SENS_TYPE_CPU_TEMP:
            if ( fd_cpu_temp == -1 )
            {
                err = HAL_E_SENSOR_ERROR;
            }
                else
                {
                g_mutex_lock( &mutex );
                err = file_get_value_int( fd_cpu_temp, &val );
                g_mutex_unlock( &mutex );
                if ( err == HAL_E_OK )
                {
                    *value = val;
                    SET_IF_NOT_NULL( fresh, TRUE );
                }
            }
            break;

        case HAL_SENS_TYPE_PMIC_TEMP:
            if ( fd_pmic_temp == -1 )
            {
                err = HAL_E_SENSOR_ERROR;
            }
            else
            {
                g_mutex_lock( &mutex );
                err = file_get_value_int( fd_pmic_temp, &val );
                g_mutex_unlock( &mutex );

                if ( err == HAL_E_OK )
                {
                    *value = val;
                    SET_IF_NOT_NULL( fresh, TRUE );
                }
            }
            break;
        case HAL_SENS_TYPE_TERMINAL30_VOLT:
            if ( fd_k30_volt == -1 )
            {
                err = HAL_E_SENSOR_ERROR;
            }
            else
            {
                g_mutex_lock( &mutex );
                err = file_get_value_int( fd_k30_volt, &val );
                g_mutex_unlock( &mutex );

                if ( err == HAL_E_OK )
                {
                    *value = val;
                    SET_IF_NOT_NULL( fresh, TRUE );
                }
            }
            break;
        case HAL_SENS_TYPE_RTCBATT_VOLT:
            if ( fd_batt_volt == -1 )
            {
                err = HAL_E_SENSOR_ERROR;
            }
            else
            {
                g_mutex_lock( &mutex );
                err = file_get_value_int( fd_batt_volt, &val );
                g_mutex_unlock( &mutex );

                if ( err == HAL_E_OK )
                {
                    *value = val;
                    SET_IF_NOT_NULL( fresh, TRUE );
                }
            }
            break;
        case HAL_SENS_TYPE_PMIC_VOLT:
            if ( fd_pmic_volt == -1 )
            {
                err = HAL_E_SENSOR_ERROR;
            }
            else
            {
                g_mutex_lock( &mutex );
                err = file_get_value_int( fd_pmic_volt, &val );
                g_mutex_unlock( &mutex );

                if ( err == HAL_E_OK )
                {
                    *value = val;
                    SET_IF_NOT_NULL( fresh, TRUE );
                }
            }
            break;
        case HAL_SENS_TYPE_PMIC_CURR:
            if ( fd_pmic_curr == -1 )
            {
                err = HAL_E_SENSOR_ERROR;
            }
            else
            {
                g_mutex_lock( &mutex );
                err = file_get_value_int( fd_pmic_curr, &val );
                g_mutex_unlock( &mutex );

                if ( err == HAL_E_OK )
                {
                    *value = val;
                    SET_IF_NOT_NULL( fresh, TRUE );
                }
            }
            break;
        default:
            err = HAL_E_INVALID_PARAMETER;
            break;
    }

    if ( err != HAL_E_INVALID_PARAMETER && err != HAL_E_OK )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not read sensor type %u", type );
    }

    return err;
}


