/**************************************************************************
 * Copyright (c) 2013 TTTControl. All rights reserved. Confidential proprietary
 * Schoenbrunnerstrasse 7, A-1040 Wien, Austria. office[at]ttcontrol.com
 **************************************************************************/
/**********************************************************************//**
 * \file hal_buzzer.h
 *
 * \brief Hardware Abstraction Layer for buzzer device.
 *
 **************************************************************************/

#ifndef _HAL_BUZZER_H_
#define _HAL_BUZZER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "hal_types.h"
#include <glib.h>
#include <stdint.h>

/** \defgroup buzzer Buzzer
 *  \brief Functions and types for Buzzer
 *  \{
 */

/** Available states for the buzzer */
typedef enum
{
    HAL_BUZ_STATE_OFF, /**< Buzzer off */
    HAL_BUZ_STATE_ON,  /**< Buzzer on */
} hal_buzzer_state;

/** \brief Initialize buzzer module
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_FAIL               Could not initialize module
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 *
 *  \remarks
 *  This function must be called before any other function of this module is called.
 *
 */
hal_error hal_buzzer_init( );

/** \internal
 */
hal_error hal_buzzer_init_internal( uint8_t scale );

/** \brief Deinitialize buzzer module.
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 *
 *  \remarks
 *  The buzzer is disabled automatically when #hal_buzzer_deinit is called.
 */
hal_error hal_buzzer_deinit();

/** Enable changeable buzzer volume also for hardware that does not support it */
#define HAL_COMPAT_BUZZER_VOLUME 1

/** \brief Set compatibility flags for buzzer module.
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                  Everything fine
 *  \retval #HAL_E_INVALID_PARAMETER   Invalid compatibility flag
 */
hal_error hal_buzzer_set_compatibility( uint32_t flags );

/**
 * \brief Sets the state (on/off) of the buzzer
 *
 * \param[in] state One of:
 *              - \c #HAL_BUZ_STATE_OFF: Switch the buzzer off
 *              - \c #HAL_BUZ_STATE_ON:  Switch the buzzer on
 *
 * \return #hal_error
 * \retval #HAL_E_OK                 Everything fine
 * \retval #HAL_E_FAIL               Could not set state
 * \retval #HAL_E_INVALID_PARAMETER  Invalid parameter passed to the function
 * \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
hal_error hal_buzzer_set_state( hal_buzzer_state const state);

/**
 * \brief Get the current state (on/off) of the buzzer
 *
 * \param[out] state Returned value; one of:
 *                   - \c #HAL_BUZ_STATE_OFF: buzzer is off
 *                   - \c #HAL_BUZ_STATE_ON:  buzzer is on
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_buzzer_get_state( hal_buzzer_state * const state);

/**
 * \brief Sets the buzzer volume
 *
 * \param[in] volume Volume ( 0 .. 255);\n
 *               maximum volume: 255\n
 *               minimum volume: 0
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Could not set volume
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 */
hal_error hal_buzzer_set_volume( uint8_t const volume );

/**
 * \brief Get the current volume of the buzzer
 *
 * \param[out] volume Volume ( Range: 0..255)
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_buzzer_get_volume( uint8_t * const volume );

/** \} */

#ifdef __cplusplus
}  /* End of the 'extern "C"' block */
#endif

#endif /* _HAL_BUZZER_H_ */
