#!/bin/sh

if [ -z $1 ]; then
	echo "Please specify rootfs folder."
	exit
fi

sudo -u vbuilder mkdir -pv /data/rootfs/$1/home/root/hal-tests
sudo -u vbuilder cp -Pv src/.libs/libhal.so.* /data/rootfs/$1/usr/lib
sudo -u vbuilder cp -v data/keymaps/* /data/rootfs/$1/lib/udev/keymaps
sudo -u vbuilder cp -v tools/update-sysconfig \
	/data/rootfs/$1/usr/bin
sudo -u vbuilder cp -v `find tests/.libs/ -executable -type f` \
	/data/rootfs/$1/home/root/hal-tests
