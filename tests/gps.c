#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>

#include <glib.h>
#include <gps.h>

#include "libhal.h"
#include "hal_gps.h"

static struct option options[] =
{
    {"help", no_argument, 0, '?'},
    {"led0", required_argument, 0, '0'},
    {"led1", required_argument, 0, '1'},
    {0, 0, 0, 0}
};

void print_help()
{
    printf("HAL GPS test\n");
}

void leave(int sig)
{
    if (sig != 0)
    {
        printf("SIGINT caught. Exiting.\n");
    }
    hal_gps_deinit();
    hal_deinit();
    exit(sig);
}


int main(int argc, char **argv)
{
    hal_gps_fix fix;
    hal_error err;
    double lon, lat, alt;
    double speed_ground, speed_climb;
    int ret;
    uint8_t iso8601[512];
    uint8_t sat_used;
    uint8_t sat_vis;

    signal(SIGINT, leave);

    hal_init();

    err = hal_gps_init( );

    printf("Error: %u\n", err ); 

    while ( 1 )
    {

        err = hal_gps_get_state( &fix, &sat_used, &sat_vis );
        if ( err == HAL_E_OK )
        {
            printf("Fix state: %u Satellites used: %u Satellites in view: %u\n", fix, sat_used, sat_vis );
        }

        err = hal_gps_get_utc_iso8601( iso8601, sizeof(iso8601) );
        if ( err == HAL_E_OK )
        {
            printf("UTC: %s\n", iso8601 );
        }


        err = hal_gps_get_location( &lon, &lat, &alt );
        if ( err == HAL_E_OK )
        {
            printf("Location: longitude: %f latitude: %f altitude: %f\n", lon, lat, alt );
        }
        else if ( err == HAL_E_GPS_NO_FIX )
        {
            printf("Location: NO FIX\n" );
        }
        else
        {
            printf("ERROR: %u\n", err );
        }

        err = hal_gps_get_speed( &speed_ground, &speed_climb );
        if ( err == HAL_E_OK )
        {
            printf("Speed over ground: %f Climb speed: %f\n", speed_ground, speed_climb );
        }

        g_usleep( 1* G_USEC_PER_SEC );
    }

    leave(0);
}
