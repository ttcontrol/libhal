/*
 * ----------------------------------------------------------------------------
 */
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <syslog.h>
#include <poll.h>
#include <termios.h>
#include <string.h>
#include <glib.h>
#include <glib/gprintf.h>
#include <stdint.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>


#include "scom.h"
#include "hal.h"
#include "hal_types.h"
#include "tools.h"
#include "hal_modem.h"

/* #define DEBUG */

#define MODULE_NAME             "[SCOM]"
#define DEV_SERIAL0             "/dev/ttyModem"
#define DEV_LOCKFILE0           "/var/lock/LCK..ttyModem"
#define DEV_SERIAL1             "/dev/ttyGPS"
#define DEV_LOCKFILE1           "/var/lock/LCK..ttyGPS"
#define HAL_INIT_FILE           HAL_VAR_RUN_DIR"/modem_init"
#define NEWLINE                 "\r\n"

// Specifies how long to wait (Unit: microseconds) after a command has been processed
// (either successfully or unsuccessfully).
// This timeout is used to wait an appropriate time after each AT command to allow
// unsolicited responsecodes to be sent by the modem.
#define TIMEOUT_AFTER_COMMAND   100000LL

static uint8_t file_lock;
static int  ref_count = 0;
static int fd_lockfile = -1;
static int fd_tty = -1;
static int fd_initfile = -1;
static gboolean initialized = FALSE;

static pthread_mutex_t m1;

static struct termios tty1;
static gint64 max_command_duration = 0;

// Caution: Order must match enum hal_modem_result_code
static char *result_text[HAL_MODEM_RES_MAX] =
{
    "OK",
    "ERROR",
    "CONNECT",
    "RING",
    "NO_CARRIER",
    "NO_DIALTONE",
    "BUSY",
    "NO_ANSWER",
};

static const char * URC[5] = {"^SYSLOADING", "^SYSSTART", "+PBREADY", "^SYSINFO", "^SBC"};

/*  functions for showing AT
 * ----------------------------------------------------------------------------
 */

void scom_print_at(char * message)
{
    char str[1024] = "";
    char *msg = message;

    if (message == NULL)
    {
        return;
    }

    while (*msg != '\0')
    {
        if (*msg == '\r')
        {
            strcat(str, "<CR>");
        }
        else if (*msg == '\n')
        {
            strcat(str, "<LF>");
        }
        else if (*msg < 32)
        {
            // print control characters other than \r and \n as hex-number.
            char *tmp = g_strdup_printf("<0x%02X>", *msg);
            strcat(str, tmp);
            g_free(tmp);
        }
        else
        {
            // printable character; just append it (and null-terminate).
            int len = strlen(str);
            str[len] = *msg;
            str[len + 1] = '\0';
        }
        msg++;
    }

    hal_log(HAL_LOG_SEVERITY_INFO, MODULE_NAME "-%s-", str);
}

/*      serial_write
 * ----------------------------------------------------------------------------
 */

static int serial_write(char * const str, char termination)
{
    int i;
    int val;

    i = strlen(str);
    val = write( fd_tty, str, i );
    if (termination > 0)
    {
        write( fd_tty, &termination, 1 );
    }
    return val;
}

/*      serial_read
 * ----------------------------------------------------------------------------
 */

static int serial_read(char * const str, int size, int timeout)
{
    int val;
    struct pollfd poll_fd;

    poll_fd.fd = fd_tty;
    poll_fd.events = POLLIN | POLLPRI; /* all the read event types, as defined for poll*/
    poll_fd.revents = 0;
    val = poll( &poll_fd, 1, timeout );
    /* printf("val:%d, fd: %d \n", val, fd_tty); */
    if ( val > 0) {
        val = read( fd_tty, (void *)str, size );
        /* printf("val:%d, fd: %d \n", val, fd_tty); */
        str[val] = '\0';
        /* printf("\n Serial read output: - %s - \n", str); */
    }
    return val;
}

/*      module_init - prepare GPS/GSM module for work based on the model type
 *      HW architecture dependent function - based on the sysconfig 
 * ----------------------------------------------------------------------------
 */

static hal_error module_init(void) 
{
/* HAL interface function */

    int val;

    int i;
    char buf[SCOM_BUF_SIZE] = "";
    char temp[SCOM_BUF_SIZE] = "";
    char * buf_adr;
    hal_error err; 
    uint32_t cap;
    uint8_t mod_ready[2];
    uint8_t ehs8_ready;
    uint8_t ees3_ready;

    gint64 tval_start; 
    gint64 tval_loop; 
    gint64 tval_result;
    gint64 tval_timeout;
    
    tval_result = 0;

    ehs8_ready = 0;
    ees3_ready = 0;
    mod_ready[0] = 0;
    mod_ready[1] = 0;
    mod_ready[2] = 0;
    cap = 0;        
    buf_adr = &buf[0];
    val = 0;

    /* check from HAL on module type - HAL should already been initialized */
    err = hal_syscfg_get_capability(HAL_SYSCAP_MODEM, &cap);
    if ( err != HAL_E_OK ) {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "HAL sysconfig get capability failed!" );
        cap = 0;
    }
    switch (cap) {
        /*Cinterion EES3 - requires 1 URC - and is much faster */
        case SYSCFG_MODEM_INTERNAL_GPRS:
            tval_timeout = SCOM_INIT_TIMEOUT_EES3 * 1000; 
            /* pass from millisecs like the mods to us like this module */
            /* hal_log( HAL_LOG_SEVERITY_DEBUG, MODULE_NAME "modem is Cinterion EES3" ); */
            pthread_mutex_lock(&m1);
            /* critical section - start */
            /*loop: check URC1 */
            while ( (tval_timeout > tval_result) && (ees3_ready == 0) ) {
                tval_start = g_get_monotonic_time();  /* start timeout */
                memset(temp, 0, sizeof(temp));
                memset(buf, 0, sizeof(buf));
                val = serial_read(buf_adr, SCOM_BUF_SIZE, SCOM_TIMEOUT);
                if (val > 0) {
                    sprintf(temp, "%s", URC[1]);
                    if ( strstr(buf_adr, temp) != NULL ) {
                        /* hal_log( HAL_LOG_SEVERITY_DEBUG, MODULE_NAME "Module loaded..." ); */
                        mod_ready[1] = 1;
                    } 
                    else {
                        mod_ready[1] = 0;
                    }
                }
                else {
                    ees3_ready = 0;
                }
                if (mod_ready[1] == 1) {
                    ees3_ready = 1;
                }
                else {
                    ees3_ready = 0;
                }
                tval_loop = g_get_monotonic_time();  /* get loop time */
                tval_result = tval_result + (tval_loop - tval_start); /*compute timout */
            }
            if (mod_ready[1] == 1) {
                val = 0;
                initialized = TRUE;
            }
            else {
                /* special case of the Cinterion EES3, since there are bad problems with receiving URC[1] 
                 * at this point because of the large init timeout URC[1] should be received
                 * if we got here everything else is fine, and since the modem is used in a different process
                 * it is safe for us to continue, at some point we could recheck the URC with a call from 
                 * a different module of the HAL, but this is not need since SCOM has the capability to ignore
                 *  URC's
                 */
                hal_log( HAL_LOG_SEVERITY_INFO, MODULE_NAME "Modem is Cinterion EES3 - special case on URC[1]" );
                val = 0;
                initialized = TRUE;
            }
            /* critical section - end */
            pthread_mutex_unlock(&m1);
            return val;
        /* Cinterion EHS8 - required 2 URCs*/
        case SYSCFG_MODEM_INTERNAL_EHS8:
            /* hal_log( HAL_LOG_SEVERITY_DEBUG, MODULE_NAME "modem is Cinterion EHS8" ); */
            tval_timeout = SCOM_INIT_TIMEOUT_EHS8 * 1000; 
            /* pass from millisecs like the mods to us like this module */
            pthread_mutex_lock(&m1);
            /* critical section - start */
            /* loop: check URC1 & URC2 */
            while ( (tval_timeout > tval_result) && (ehs8_ready == 0) ) {
                tval_start = g_get_monotonic_time(); /* start timeout */
                memset(temp, 0, sizeof(temp));
                memset(buf, 0, sizeof(buf));
                val = serial_read(buf_adr, SCOM_BUF_SIZE, SCOM_TIMEOUT);
                if (val > 0) {
                    if (mod_ready[0] == 0) {
                        sprintf(temp, "%s%s", URC[0], NEWLINE);
                        if ( strstr(buf_adr, temp) != NULL ) {
                        /* hal_log( HAL_LOG_SEVERITY_DEBUG, MODULE_NAME "Module loading..." ); */
                            mod_ready[0] = 1;
                        } 
                        else {
                            mod_ready[0] = 0;
                        }
                    }
                    if (mod_ready[1] == 0) {
                        sprintf(temp, "%s%s", URC[1], NEWLINE);
                        if ( strstr(buf_adr, temp) != NULL ) {
                        /* hal_log( HAL_LOG_SEVERITY_DEBUG, MODULE_NAME "Module loaded..." ); */
                            mod_ready[1] = 1;
                        } 
                        else {
                            mod_ready[1] = 0;
                        }
                    }
                }
                else {
                    ehs8_ready = 0;
                }
                if (mod_ready[0] == 1 && mod_ready[1] == 1) {
                    ehs8_ready = 1;
                }
                else {
                    ehs8_ready = 0;
                }
                tval_loop = g_get_monotonic_time();  /* get loop time */
                tval_result = tval_result + (tval_loop - tval_start); /*compute timout */
            }
            if (mod_ready[0] == 1 && mod_ready[1] == 1) {
                val = 0;
                initialized = TRUE;
            }
            else {
                val = -1;
                initialized = FALSE;
            }
            /* critical section - end */
            pthread_mutex_unlock(&m1);
            return val;
        /*Unknown modem - or not present*/
        default:
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Unknown modem capability: %u - not supported", cap);
            /* Unknown modem type */
            /* critical section - end */
            pthread_mutex_unlock(&m1);
            return val;
    }
    
    /* critical section - end */
    pthread_mutex_unlock(&m1);
    return val;
}

/*      hal_scom_init
 * ----------------------------------------------------------------------------
 */
hal_error hal_scom_init(void) 
{
/* we need to initialize the serial communication with the /dev/tty's this 
 * function should deal with waiting for the modules to be ready
 * and all the other thread initializations such that the HAL can
 * make use of the tty's
 */
    int val;
    int i;
    struct sched_param param;
    char buf[SCOM_BUF_SIZE] = "";
    char temp[SCOM_BUF_SIZE] = "";
    char * buf_adr;
    int baud_speed;
    uint8_t mod_ready[3];
    hal_error err;

    baud_speed = 115200;

    buf_adr = &buf[0];

    val = 0;
    file_lock = 0;

    pthread_mutex_init(&m1, NULL);
    ref_count++;
    CHECK_MODULE_ALREADY_INITIALIZED(initialized);
    ref_count--;
    fd_lockfile = open( DEV_LOCKFILE0, O_CREAT | O_WRONLY | O_TRUNC | O_EXCL, 0644 );
    if ( fd_lockfile < 0 ) {
        if ( errno == EEXIST ) {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Can't create lock file" DEV_LOCKFILE0 );
            err = HAL_E_FAIL;
        }
    } 
    else {
        dprintf( fd_lockfile, "%4d\n", getpid() );
        close( fd_lockfile );
        file_lock = 1;
    }
    fd_tty = open(DEV_SERIAL0, O_RDWR | O_EXCL); 
    if ( fd_tty == -1 ) {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not open modem device " DEV_SERIAL0 ": %d", errno);
        err = HAL_E_FAIL;
        return err;
    }
    /* fcntl( fd_tty, F_SETFL, O_RDWR ); /* /* this does not work on Linux, all access mode and creation flags are ignored as arguments to fcntl F_SETFL*/
    tcgetattr( fd_tty, &tty1 );
    cfmakeraw(&tty1);
    cfsetspeed(&tty1, B115200); 
    tty1.c_iflag = IGNPAR;
    tty1.c_oflag = 0; 
    tty1.c_lflag = 0; 
    tty1.c_cc[VEOL] = 0; 
    tcsetattr( fd_tty, TCSANOW, &tty1 );
    
    fd_initfile = open( HAL_INIT_FILE, O_CREAT | O_WRONLY | O_TRUNC | O_EXCL, 0644 );
    if ( fd_initfile < 0 ) {
        if ( errno == EEXIST ) {
            fd_initfile = open( HAL_INIT_FILE, O_RDWR, 0644 );
            if ( fd_initfile < 0 ) {
                err = HAL_E_FAIL;
            }
            else {
                val = read( fd_initfile, (void *)buf_adr, sizeof(char) ); 
                if (val > 0) {
                    sprintf(temp, "1");
                    if ( strstr(buf_adr, temp) != NULL ) {
                            hal_log( HAL_LOG_SEVERITY_INFO, MODULE_NAME "Module already initialized...");
                            ref_count++;
                            initialized = TRUE;
                            err = HAL_E_ALREADY_INITIALIZED;
                    }        
                    else {
                        val = module_init(); 
                        if (val == 0) {
                            hal_log( HAL_LOG_SEVERITY_INFO, MODULE_NAME "Init completed successfully"); 
                            ref_count = 1;
                            pwrite( fd_initfile, "1", 1, 0);
                            close( fd_initfile ); 
                            err = HAL_E_OK;
                        }
                        else {
                            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Init failed" );
                            pwrite( fd_initfile, "0", 1, 0);
                            close( fd_initfile ); 
                            err = HAL_E_FAIL;
                        }
                    }
                }
                else {
                    hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Init failed - cannot read init file");
                    err = HAL_E_FAIL;
                }
            }       
        }
    }
    else {
        val = module_init(); 
        if (val == 0) {
            hal_log( HAL_LOG_SEVERITY_INFO, MODULE_NAME "Init completed successfully" ); 
            ref_count = 1;
            pwrite( fd_initfile, "1", 1, 0);
            close( fd_initfile ); 
            err = HAL_E_OK;
        }
        else {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Init failed" );
            pwrite( fd_initfile, "0", 1, 0);
            close( fd_initfile ); 
            err = HAL_E_FAIL;
        }
    } 
    hal_log( HAL_LOG_SEVERITY_INFO, MODULE_NAME "Reference counter init - is at: %d", ref_count );
    return err;
}

/*      hal_scom_deinit
 * ----------------------------------------------------------------------------
 */
hal_error hal_scom_deinit(void) 
{
/* we need to deinitalize the SCOM module in a clean way as to maintain
 * full re-entrance on the code
 */

#ifdef DEBUG
    hal_log(HAL_LOG_SEVERITY_INFO, MODULE_NAME "Maximal command duration: %"G_GINT64_FORMAT"us",
                        max_command_duration);
#endif
    
    hal_log( HAL_LOG_SEVERITY_INFO, MODULE_NAME "Reference counter deinit - is at: %d", ref_count );
    int val;
    hal_error err;
    if (ref_count > 1) { 
        err = HAL_E_OK;
        ref_count--;
        return err; 
    }
    if (ref_count == 0) {
        hal_log( HAL_LOG_SEVERITY_DEBUG, MODULE_NAME "Deinit called - Module not initialized" );
        return HAL_E_NOT_INITIALIZED;
    }
    
    initialized  = FALSE;
    ref_count = 0;
    
    pthread_mutex_destroy(&m1);
        
    if ( fd_tty != -1 ) {
        val = close( fd_tty );
        fd_tty = -1;
    }
    if ( fd_lockfile != -1 ) {
        val = close( fd_lockfile );
        fd_tty = -1;
    }
    if ( file_lock == 1 ) {
        val = unlink( DEV_LOCKFILE0 );
    }
    if (val == 0) {
        /* hal_log( HAL_LOG_SEVERITY_DEBUG, MODULE_NAME "DeInit completed successfully\n\n" ); */
        err = HAL_E_OK;
    }
    else {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Deinit failed" );
        err = HAL_E_FAIL;
    }
    return err;
}

static hal_error parse_response(char * const cmd,
        char * const text,
        char * const response, int size,
        hal_modem_result_code * const result_code)
{
    int ret;
    char str_ops[SCOM_BUF_SIZE];
    char str_buf[SCOM_BUF_SIZE];
    char *str_ops_p;
    char *tmp;

    char * resp;
    char * res;
    gboolean found = FALSE;
    hal_error err;

    // Copy complete response to internal variable for further operations/processing.
    g_strlcpy(str_ops, text, sizeof(str_ops));
    str_ops_p = str_ops;

#ifdef DEBUG
    hal_log(HAL_LOG_SEVERITY_INFO, MODULE_NAME "Command: %s", cmd);
#endif

//    g_snprintf(str_buf, sizeof(str_buf), "%s%c", cmd, cmd_termination);

    // According to https://embeddedfreak.wordpress.com/2008/08/19/handling-urc-unsolicited-result-code-in-hayes-at-command/ ,
    // if the echo is turned on, the modem won't interrupt the response with an URC.
    // This means, we can just search for the echoed command in the complete response (which may include one or more URCs),
    // and start parsing from the found command and we won't run into any URCs until a result code (e.g. "OK").

    // Example content of str_ops_p:
    // "^SYSSTART<CR><LF>AT+CREG?<CR><CR><LF>+CREG: 0,3<CR><LF><CR><LF>OK<CR><LF><NULL>"
    tmp = strstr (str_ops_p, cmd);

#ifdef DEBUG
    hal_log(HAL_LOG_SEVERITY_INFO, MODULE_NAME "parse_response:");
    scom_print_at(str_ops_p);
#endif
    if (tmp == NULL)
    {
        hal_log(HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Echoed command (%s) not found", str_buf);
        return HAL_E_FAIL;
    }

    // Remove trailing echoed command, terminator (thus the "+1") and all data before it..
    // str_ops_p after this operation: "<CR><LF>+CREG: 0,3<CR><LF><CR><LF>OK<CR><LF><NULL>"
    str_ops_p = tmp + strlen(cmd) + 1;

    // search last newline
    tmp = g_strrstr(str_ops_p, NEWLINE);
    if (tmp == NULL)
    {
        hal_log(HAL_LOG_SEVERITY_ERROR,  MODULE_NAME "No <CR><LF> found in response (1)!");
        return HAL_E_FAIL;
    }

    // cut string after last newline
    // str_ops_p after this operation: "<CR><LF>+CREG: 0,3<CR><LF><CR><LF>OK<NULL>"
    *tmp = '\0';

    res = g_strrstr(str_ops_p, NEWLINE); // search last newline;
    if (res == NULL)
    {
        hal_log(HAL_LOG_SEVERITY_ERROR, MODULE_NAME "No <CR><LF> found in response (2)!");
        return HAL_E_FAIL;
    }

    // cut string before last newline
    // str_ops after this operation: "<CR><LF>+CREG: 0,3<CR><LF><NULL><LF>OK<NULL>"
    *res = '\0';

    // skip <NULL><LF>
    // res now contains points to result code text: "OK"
    res += 2;
#ifdef DEBUG
    hal_log(HAL_LOG_SEVERITY_INFO, MODULE_NAME "Resultcode text: -%s-", res);
#endif

    *result_code = HAL_MODEM_RES_UNKNOWN;
    // Search for a valid result code
    for (int i = 0; i < HAL_MODEM_RES_MAX - 1; i++)
    {
        if (g_strcmp0(res, result_text[i]) == 0)
        {
            *result_code = i;
            break;
        }
    }

    // No valid result code found
    if (*result_code == HAL_MODEM_RES_UNKNOWN)
    {
        hal_log(HAL_LOG_SEVERITY_ERROR,
                MODULE_NAME "No valid result code found: -%s-", res);
        return HAL_E_FAIL;
    }
#ifdef DEBUG
    hal_log(HAL_LOG_SEVERITY_INFO, MODULE_NAME "Parsed result code: %u",
            *result_code);
#endif
    // If there are still 2 newlines left (start & ending), command has a information response:
    if (g_str_has_prefix(str_ops_p, NEWLINE) && g_str_has_suffix(str_ops_p, NEWLINE))
    {
        // skip leading <CR><LF>
        // contents of str_ops_p afterwards: "+CREG: 0,3<CR><LF>"
        str_ops_p += 2;

        // cut last <CR><LF>
        // contents of str_ops_p afterwards: "+CREG: 0,3"
        tmp = g_strrstr(str_ops_p, NEWLINE);
        *tmp = '\0';

        if (response != NULL)
        {
            // Copy calculated response information to passed parameter.
            g_strlcpy(response, str_ops_p, size);
        }
    }
    else
    {
        if (response != NULL)
        {
            // No response information; return empty string.
            g_stpcpy(response, "");
        }
    }
#ifdef DEBUG
    hal_log(HAL_LOG_SEVERITY_INFO, MODULE_NAME "Response information: -%s-", response);
#endif

    return HAL_E_OK;
}

static gboolean contains_result(char * const response)
{
    char temp[SCOM_BUF_SIZE];
    int ret;

    // Search for valid result code
    for (int i = 0; i < HAL_MODEM_RES_MAX - 1; i++)
    {
        // Valid resultcodes always contain a <CR><LF> as pre- and postfix.
        g_sprintf(temp, "%s%s%s", NEWLINE, result_text[i], NEWLINE);
        if (g_strrstr(response, temp) != NULL)
        {
            // A valid result code has been found.
            return TRUE;
        }
    }
    // No result code found.
    return FALSE;
}

/**
 * Sends a string (e.g. AT command) with the specified terminator (e.g. '\r'\ to the modem,
 * and returns the response information (if any) and the result code.
 *
 * \param[in]   cmd                 String/command to send (e.g. AT+CREG?)
 * \param[in]   cmd_termination     Terminator to send after the command (e.g. '\r')
 * \param[in]   timeout             Time in ms after which the response must be received
 *                                  completely. If this timeout is exceeded, the function fails.
 * \param[out]  response            Response information of the command (e.g. CREG: 0,2).
 *                                  If "needle != NULL", no response is returned as none is received.
 * \param[in]   size                Size to "response" parameter
 * \param[in]   needle              If not NULL, the function waits until this string is received as
 *                                  response and returns afterwards.
 * \param[out]  result              Result code, if available.
 *                                  If "needle != NULL", no response is returned as none is received.
 * */
static hal_error transfer(char * const cmd,
                   char cmd_termination,
                   int timeout,
                   char * const response,
                   int size,
                   char * const needle,
                   hal_modem_result_code * const result)
{
    gint64 tval_start;
    gint64 tval_current;
    gint64 tval_diff;
    gint64 tval_timeout;

    static gint64 tval_last_cmd = 0;

    char buf[SCOM_BUF_SIZE] = "";
    int buf_pos = 0;
    int val;
    gboolean done = FALSE;
    hal_error err;

    tval_timeout = timeout * 1000;



    tval_diff = g_get_monotonic_time() - tval_last_cmd;
    if (tval_diff < TIMEOUT_AFTER_COMMAND)
    {
        // If shortly before an AT command was sent, wait so long that at least
        // TIMEOUT_AFTER_COMMAND microseconds have passed before sending another command.
        /* printf("WAIT FOR %d\n", (int)(TIMEOUT_AFTER_COMMAND - tval_diff)); */
        g_usleep(TIMEOUT_AFTER_COMMAND - tval_diff);
    }

    /* just in case there was a bad read or some
     other issues we flush the FD clean, not sure how well this actually works!
     * in a kernel bug report this is also mentioned and unsupported by the driver
     * see here: https://bugzilla.kernel.org/show_bug.cgi?id=5730
     */
    tcflush(fd_tty, TCIOFLUSH);

    serial_write(cmd, cmd_termination);
    tval_start = g_get_monotonic_time(); /* start timeout */
    while ( done == FALSE ) /* done is timeout based, see below */
    {
        // read from serial-device and "paste" the string directly into
        // the current position inside the buffer "buf". This saves a "strconcat" operation.
        val = serial_read((buf + buf_pos), sizeof(buf) - buf_pos, timeout);

        tval_current = g_get_monotonic_time();  /* get loop time */
        tval_diff = tval_current - tval_start; /*compute timeout */

#ifdef DEBUG
        scom_print_at(buf);
#endif

        if (val > 0)
        {
            buf_pos += val;
            if (needle != NULL)
            {
                // Looking for the needle in the response
                if (g_strrstr(buf, needle) != NULL )
                {
#ifdef DEBUG
                    hal_log(HAL_LOG_SEVERITY_INFO, MODULE_NAME "Needle (%s) found in response after %" G_GINT64_FORMAT"us",
                                    needle, tval_diff);
#endif
                    err = HAL_E_OK;
                    done = TRUE;
                }
            }
            else if (contains_result(buf) == TRUE)
            {
                // We got a result code; the complete response has been received.
                err = parse_response(cmd, buf, response, size, result);
                done = TRUE;
                // store timestamp when last AT command was processed.
                tval_last_cmd = g_get_monotonic_time();
#ifdef DEBUG
                if (tval_diff > max_command_duration)
                {
                    max_command_duration = tval_diff;
                }
                hal_log(HAL_LOG_SEVERITY_INFO, MODULE_NAME "Response received after %" G_GINT64_FORMAT"us",
                                    tval_diff);
#endif
            }
        }

        if (tval_diff > tval_timeout && done == FALSE)
        {
            hal_log(HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Timeout reached:%"G_GINT64_FORMAT ">%"G_GINT64_FORMAT" us",
                    tval_diff, tval_timeout);
            err = HAL_E_FAIL;
            done = TRUE;

            // store timestamp when last AT command was processed.
            tval_last_cmd = g_get_monotonic_time();
        }
    }
    return err;
}

hal_error hal_scom_transfer(char * const cmd,
                             char cmd_termination,
                             int timeout,
                             char * const response,
                             int size,
                             hal_modem_result_code * const result)
{
    hal_error err;
    
    CHECK_MODULE_INITIALIZED(initialized);
    hal_scom_lock();
    err = transfer(cmd, cmd_termination, timeout, response, size, NULL, result);
    hal_scom_unlock();
    return err;
}

hal_error hal_scom_transfer_no_lock(char * const cmd,
                             char cmd_termination,
                             int timeout,
                             char * const response,
                             int size,
                             hal_modem_result_code * const result)
{
    hal_error err;
    
    CHECK_MODULE_INITIALIZED(initialized);
    err = transfer(cmd, cmd_termination, timeout, response, size, NULL, result);
    return err;
}

hal_error hal_scom_transfer_and_wait_no_lock(char * const cmd,
                                            char cmd_termination,
                                            int timeout,
                                            char *str)
{
    hal_error err;

    CHECK_MODULE_INITIALIZED(initialized);
    err = transfer(cmd, cmd_termination, timeout, NULL, 0, str, NULL);
    return err;
}

hal_error hal_scom_lock()
{
    int ret;
    hal_error err;

    CHECK_MODULE_INITIALIZED(initialized);
    ret = pthread_mutex_lock(&m1);
    err = (ret == 0) ? HAL_E_OK : HAL_E_FAIL;
    return err;
}

hal_error hal_scom_unlock()
{
    int ret;
    hal_error err;
    
    CHECK_MODULE_INITIALIZED(initialized);
    ret = pthread_mutex_unlock(&m1);
    err = (ret == 0) ? HAL_E_OK : HAL_E_FAIL;
    return err;
}

hal_error hal_scom_reset()
{
    hal_modem_result_code result;
    hal_error err;

    CHECK_MODULE_INITIALIZED(initialized);
    err = hal_scom_transfer("AT+CFUN=1,1", '\r', 500, NULL, 0, &result);
    if (err == HAL_E_OK && result == HAL_MODEM_RES_OK)
    {
        hal_scom_deinit();
        pwrite(fd_initfile, "0", 1, 0);

        return HAL_E_OK;
    }
    else
    {
        return HAL_E_FAIL;
    }
}

int get_scom_ref_counter (void)
{
    return ref_count;    
}


