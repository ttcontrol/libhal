#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <syslog.h>
#include <glib.h>
#include <linux/input.h>

#include "hal.h"
#include "hal_types.h"
#include "hal_rtc.h"
#include "tools.h"
#include "hal_power.h"
#include "ev_poll.h"

#define MODULE_NAME           "[power] "
#define DEV_POWER_CAMERA      "/var/links/gpio/camera-power/value"
#define DEV_POWER_PMIC        "/dev/input/pmic"
#define DEV_POWER_RTC         "/dev/rtc-backup"
#define MXC_KEYPAD_WAKEUP     "/sys/bus/platform/drivers/mxc_keypad/mxc_keypad.0/power/wakeup"
#define SUSPEND_FILE          "suspend"
#define RESUME_FILE           "resume"

static int fd_camera_power = -1;
static int fd_pmic_dev = -1;
static int fd_rtc = -1;

static gboolean cap_has_wakeup_rtc = FALSE;

static gboolean initialized = FALSE;

static hal_power_state terminal15_state = HAL_PWR_STATE_HIGH;
static hal_power_state wakeup_state = HAL_PWR_STATE_HIGH;

static GMutex mutex = G_STATIC_MUTEX_INIT;
static GPtrArray *terminal15_callbacks = NULL;
static GPtrArray *wakeup_callbacks = NULL;
static hal_power_state current_camera_state;

static gboolean powerOnWakeAlarm = FALSE;
static gboolean suspendWakeAlarm = FALSE;

static hal_error hal_power_open_rtc(void);
static void hal_power_close_rtc(void);


static void do_power_callback(gpointer data, gpointer user_data)
{
    hal_power_state state = (hal_power_state)user_data;

    (*(hal_power_callback)(data))(state);
}

static gpointer ev_poll_callback ( gpointer data )
{
    int bytes_read;
    struct input_event ev;

    if ( fd_pmic_dev == -1 )
    {
        return NULL;
    }

    if ( read(fd_pmic_dev, &ev, sizeof(ev)) == sizeof(ev) )
    {
        if ( ev.type == EV_SW )
        {
            switch ( ev.code )
            {
                /* power-on button */
                case SW_DOCK:
                    /* not implemented */
                    break;
                /* wakeup */
                case SW_LID:
                    g_mutex_lock( &mutex );
                    wakeup_state = ev.value;
                    g_ptr_array_foreach( wakeup_callbacks, &do_power_callback, (gpointer)wakeup_state );
                    g_mutex_unlock( &mutex );
                    break;
                /* K15 */
                case SW_RFKILL_ALL:
                    g_mutex_lock( &mutex );
                    terminal15_state = ev.value;
                    g_ptr_array_foreach( terminal15_callbacks, &do_power_callback, (gpointer)terminal15_state );
                    g_mutex_unlock( &mutex );
                    break;
            }
        }
    }
}

static hal_error hal_power_check_alarm_time_difference(struct rtc_wkalrm * alarm, gboolean * result)
{
    hal_error error;
    struct rtc_time rtcTime;

    *result = FALSE;

    // get rtc time
    error = hal_power_open_rtc();

    if (error == HAL_E_OK)
    {
        /* Read the RTC time/date */
        if (ioctl(fd_rtc, RTC_RD_TIME, &rtcTime) == -1) 
        {
            error = HAL_E_FAIL;
        }

        hal_power_close_rtc();

        if (error == HAL_E_OK)
        {
            // assume that the time difference between the alarm and 
            // the current time can not be more than 60 seconds
            if ((alarm->enabled == (unsigned char)1) && (alarm->pending == (unsigned char)1))
            {
                if ((rtcTime.tm_min == alarm->time.tm_min) && (rtcTime.tm_hour == alarm->time.tm_hour) && (rtcTime.tm_mday == alarm->time.tm_mday))
                {
                    *result = TRUE;
                }

                /* remove any alarm which already expired. this is done to avoid that device is 
                 * powered on in the future at the same time the alarm expired
                 */
                error = hal_power_clear_wakeup_alarm();
            }
        }
    }

    return error;
}

static hal_error check_power_caps()
{
    hal_error err = HAL_E_OK;
    uint32_t cap;

    cap_has_wakeup_rtc = FALSE;
    err = hal_syscfg_get_capability( HAL_SYSCAP_WAKEUP_RTC, &cap );
    if ( err != HAL_E_OK )
    {
        return HAL_E_FAIL;
    }
    
    switch ( cap )
    {
        case SYSCFG_WAKEUP_RTC_EXT:
        case SYSCFG_ALL_FEATURES:
            cap_has_wakeup_rtc = TRUE;
            break;
        default:
            break;
    }
    
    return err;
}

hal_error hal_power_init()
{
    hal_error err;
    int ret;
    uint32_t sw;
    struct rtc_wkalrm alarm;

    CHECK_HAL_INITIALIZED();
    CHECK_MODULE_ALREADY_INITIALIZED( initialized );

    err = check_power_caps();
    if (err == HAL_E_FAIL)
    {
        return HAL_E_FAIL;
    }

    terminal15_callbacks = g_ptr_array_new();
    wakeup_callbacks = g_ptr_array_new();

    fd_camera_power = open(DEV_POWER_CAMERA, O_RDWR);
    if (fd_camera_power == -1)
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not open camera power device node " DEV_POWER_CAMERA ": %d",
            errno );
        return HAL_E_FAIL;
    }

    fd_pmic_dev = open(DEV_POWER_PMIC, O_RDONLY);
    if (fd_pmic_dev == -1)
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not open pmic device node " DEV_POWER_PMIC ": %d", errno );
        return HAL_E_FAIL;
    }

    ret = ioctl(fd_pmic_dev, EVIOCGSW(sizeof(sw)), &sw);
    if (ret == -1)
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not get initial power button state: %d", errno );
        return HAL_E_FAIL;
    }

    terminal15_state = (sw & (1 << SW_RFKILL_ALL)) > 0 ? 1 : 0;
    wakeup_state = (sw & (1 << SW_LID)) > 0  ? 1 : 0;

    err = hal_power_set_keyboard_wakeup(FALSE);

    if (err != HAL_E_OK)
    {
        return err;
    }

    ev_poll_add( fd_pmic_dev
               , POLLIN
               , &ev_poll_callback
               , NULL);

    initialized = TRUE;

    current_camera_state = HAL_PWR_STATE_HIGH;
    hal_power_set_camera(HAL_PWR_STATE_LOW);

    powerOnWakeAlarm = FALSE;
    suspendWakeAlarm = FALSE;

    if (cap_has_wakeup_rtc)
    {
        //try to find out if power on was due to an alarm
        err = hal_power_get_wakeup_alarm(&alarm);
        if (err != HAL_E_OK)
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Could not get alarm time" );
            return err;
        }

        err = hal_power_check_alarm_time_difference(&alarm, &powerOnWakeAlarm);
        if (err != HAL_E_OK)
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Could not calculate alarm time diff" );
            return err;
        }
    }

    return HAL_E_OK;
}

hal_error hal_power_deinit()
{
    CHECK_MODULE_INITIALIZED( initialized );

    /* Switch off camera power before deinit */
    hal_power_set_camera( HAL_PWR_STATE_LOW );

    if ( fd_camera_power != -1 )
    {
        close(fd_camera_power);
        fd_camera_power = -1;
    }

    if ( fd_pmic_dev != -1 )
    {
        ev_poll_remove( fd_pmic_dev );

        close(fd_pmic_dev);
        fd_pmic_dev = -1;
    }

    g_ptr_array_free(terminal15_callbacks, TRUE);
    g_ptr_array_free(wakeup_callbacks, TRUE);

    initialized = FALSE;

    return HAL_E_OK;
}

hal_error hal_power_set_camera(hal_power_state const state)
{
    hal_error err;
    int val;

    if (current_camera_state == state)
    {
        return HAL_E_OK;
    }

    CHECK_MODULE_INITIALIZED( initialized );

    switch ( state )
    {
        case HAL_PWR_STATE_LOW:
            val = 0;
            break;
        case HAL_PWR_STATE_HIGH:
            val = 1;
            break;
        default:
            return HAL_E_INVALID_PARAMETER;
    }

    g_mutex_lock( &mutex );
    err = file_set_value_int( fd_camera_power, val );
    g_mutex_unlock( &mutex );

    if (err == HAL_E_OK)
    {
        current_camera_state = state;
    }
    return err;
}

hal_error hal_power_get_camera( hal_power_state * const state )
{
    hal_error err;

    CHECK_POINTER_PARAMETER( state );
    CHECK_MODULE_INITIALIZED( initialized );

    *state = current_camera_state;

//    g_mutex_lock( &mutex );
//    err = file_get_value_int( fd_camera_power, (int*) state);
//    g_mutex_unlock( &mutex );
//
//    return err;
}

hal_error hal_power_get_terminal15( hal_power_state * const state )
{
    CHECK_POINTER_PARAMETER( state );
    CHECK_MODULE_INITIALIZED( initialized );

    g_mutex_lock( &mutex );
    *state = terminal15_state;
    g_mutex_unlock( &mutex );

    return HAL_E_OK;
}

hal_error hal_power_register_callback_terminal15( hal_power_callback cb )
{
    CHECK_MODULE_INITIALIZED( initialized );

    g_mutex_lock( &mutex );
    g_ptr_array_add( terminal15_callbacks, cb );
    g_mutex_unlock( &mutex );

    return HAL_E_OK;
}

hal_error hal_power_unregister_callback_terminal15( hal_power_callback cb )
{
    hal_error err;

    CHECK_MODULE_INITIALIZED(initialized);

    g_mutex_lock(&mutex);
    err = g_ptr_array_remove( terminal15_callbacks, cb) ? HAL_E_OK : HAL_E_FAIL;
    g_mutex_unlock(&mutex);

    return err;
}

hal_error hal_power_get_wakeup( hal_power_state * const state )
{
    CHECK_POINTER_PARAMETER( state );
    CHECK_MODULE_INITIALIZED( initialized );

    g_mutex_lock( &mutex );
    *state = wakeup_state;
    g_mutex_unlock( &mutex );

    return HAL_E_OK;
}

hal_error hal_power_register_callback_wakeup( hal_power_callback cb )
{
    CHECK_MODULE_INITIALIZED( initialized );

    g_mutex_lock( &mutex );
    g_ptr_array_add( wakeup_callbacks, cb );
    g_mutex_unlock( &mutex );

    return HAL_E_OK;
}

hal_error hal_power_unregister_callback_wakeup( hal_power_callback cb )
{
    hal_error err;

    CHECK_MODULE_INITIALIZED(initialized);

    g_mutex_lock(&mutex);
    err = g_ptr_array_remove( wakeup_callbacks, cb) ? HAL_E_OK : HAL_E_FAIL;
    g_mutex_unlock(&mutex);

    return err;
}

void hal_power_off()
{
    fakesystem("poweroff");
}

hal_error hal_power_set_keyboard_wakeup( bool enable )
{
    hal_error err;
    int fd;

    fd = open( MXC_KEYPAD_WAKEUP, O_RDWR );
    if ( fd == -1 )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not open keypad wakeup file " MXC_KEYPAD_WAKEUP ": %d",
            errno );
        err = HAL_E_FAIL;
    }
    else
    {
        if ( enable )
        {
            err = file_set_value_str(fd, "enabled");
        }
        else
        {
            err = file_set_value_str(fd, "disabled");
        }

        if ( err != HAL_E_OK )
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Could not write to keypad wakeup file" MXC_KEYPAD_WAKEUP ": %d",
                errno );
        }

        close(fd);
    }

    return err;
}


hal_error hal_power_suspend()
{
    GTimeVal time;
    GError *error = NULL;
    char *str = NULL;
    hal_error err;
    hal_error err2;
    struct rtc_wkalrm alarm;

    g_get_current_time(&time);

    str = g_strdup_printf("%d", time.tv_sec);
    g_file_set_contents(HAL_VAR_RUN_DIR"/"SUSPEND_FILE
                      , str
                      , -1
                      , &error );

    g_free(str);
    if ( error != NULL )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not write suspend timestamp " SUSPEND_FILE ": %s",
            error->message );
        err = HAL_E_FAIL;
    }
    else
    {
        powerOnWakeAlarm = FALSE;
        suspendWakeAlarm = FALSE;

        // clear any alarm which already expired so it is not detected as the reason for resume
        if (HAL_E_OK == hal_power_get_wakeup_alarm(&alarm))
        {
            //pending has inverted logic! 1 means alarm was fired!
            if ((alarm.enabled == (unsigned char)1) && (alarm.pending == (unsigned char)1))
            {
                (void)hal_power_clear_wakeup_alarm();
            }
        }

        hal_log( HAL_LOG_SEVERITY_INFO, MODULE_NAME "Entering suspend..." );
        fakesystem("echo mem > /sys/power/state");
        
        // Wait for 50ms after resume. This gives the system the chance
        // to be fully operational again. important for T15; see issue69374.
        // NB: Sleep only affects the calling thread!
        usleep(50000);
        
        hal_log( HAL_LOG_SEVERITY_INFO, MODULE_NAME "... Resumed!" );
        g_get_current_time(&time);

        str = g_strdup_printf("%d", time.tv_sec);
        g_file_set_contents(HAL_VAR_RUN_DIR"/"RESUME_FILE
                          , str
                          , -1
                          , &error );

        g_free(str);
        if ( error != NULL )
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Could not write resume timestamp " RESUME_FILE ": %s",
                error->message );
            err = HAL_E_FAIL;
        }
        else
        {
            err = HAL_E_OK;
        }

        // check whether resume was produced by a RTC alarm
        err2 = hal_power_get_wakeup_alarm(&alarm);
        if (HAL_E_OK == err2)
        {
            //pending has inverted logic! 1 means alarm was fired!
            if ((alarm.enabled == (unsigned char)1) && (alarm.pending == (unsigned char)1))
            {
                suspendWakeAlarm = TRUE;
                (void)hal_power_clear_wakeup_alarm();
            }
        }

        if (err2 != HAL_E_OK || err2 != HAL_E_NOFEATURE)
        {
            if (err == HAL_E_OK)
            {
                err = err2;
            }
        }
    }

    return err;
}

hal_error hal_power_get_wakeup_source( hal_power_wakeup_source * const source)
{
    gboolean ret;

    CHECK_MODULE_INITIALIZED(initialized);
    CHECK_POINTER_PARAMETER(source);

    ret = g_file_test(HAL_VAR_RUN_DIR"/"RESUME_FILE, G_FILE_TEST_EXISTS);

    if ( ret == TRUE )
    {
        if (suspendWakeAlarm == TRUE)
        {
            *source = HAL_POWER_WAKEUP_ALARM_RESUME;
        }
        else
        {
            *source = HAL_POWER_WAKEUP_RESUME;
        }
    }
    else
    {
        if (powerOnWakeAlarm == TRUE)
        {
            *source = HAL_POWER_WAKEUP_ALARM_POWERON;
        }
        else
        {
            *source = HAL_POWER_WAKEUP_POWERON;
        }
    }

    return HAL_E_OK;
}

/* RTC device can be opened only once (until it is closed) and it is read-only
 * This is why  this device is only opened when needed.
 */
static hal_error hal_power_open_rtc(void)
{
    fd_rtc = open (DEV_POWER_RTC, O_RDONLY);
    if (fd_rtc == -1)
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not open rtc device node " DEV_POWER_RTC ": %d", errno );
        return HAL_E_FAIL;
    }

    return HAL_E_OK;
}

static void hal_power_close_rtc(void)
{
    if ( fd_rtc != -1 )
    {
        close(fd_rtc);
        fd_rtc = -1;
    }
}

static gboolean hal_power_check_max_time_diff(struct rtc_time * alarm_time, struct rtc_time * current_time)
{
    if (alarm_time->tm_year != current_time->tm_year)
    {
        if ((0 != alarm_time->tm_mon) || (11 != current_time->tm_mon) || ((current_time->tm_year + 1) != alarm_time->tm_year))
        {
            return FALSE; //either it is not next year or not in january (month 0) or current month 
                          // not december (month 11) so alarm is wrong for sure; more than a month
        }
    }
    else
    {
        if (current_time->tm_mon == alarm_time->tm_mon)
        {
            return TRUE;  //same year and same month, so alarm is for sure correct.
        }
        if ((current_time->tm_mon + 1) != alarm_time->tm_mon)
        {
            return FALSE; //same year, different month but alarm month is not next month so alarm is for sure wrong
        }
    }

    // if this point is reached alarm is next month (either in the same or next year).
    if (current_time->tm_mday > alarm_time->tm_mday)
    {
        return TRUE;  //next month but earlier day so no problem
    }
    if (current_time->tm_mday < alarm_time->tm_mday)
    {
        return FALSE;  //next month and later day so more than a month for sure. Alarm is wrong.
    }

    //if this point is reached alarm is next month at the same day.
    if (current_time->tm_hour > alarm_time->tm_hour)
    {
        return TRUE;  //earlier hour so no problem
    }
    if (current_time->tm_hour < alarm_time->tm_hour)
    {
        return FALSE;  //later hour alarm is wrong.
    }

    //if this point is reached alarm is next month at the same day a the same hour.
    if (current_time->tm_min > alarm_time->tm_min)
    {
        return TRUE;  //earlier minute so no problem
    }
    else
    {
        return FALSE;  //later or equal minute alarm is wrong.
    }
}

static hal_error hal_power_add_wakeup_alarm(struct rtc_wkalrm * alarm, struct rtc_time * current_time)
{
    struct tm timeDate;
    struct tm currentTimeDate;
    time_t alarmTime;
    time_t currentTime;
    int month;

    /* Call mktime to ensure that time is in correct format. This will allow users to, for example, add 120
     * seconds to seconds fields and mktime will calculate correctly all minutes, seconds, hours, etc.
     *
     * Users of this function rely on that.
     */
    /* WARNING: for some reason it is not possible to use next commented line and it is needed to copy
     * all fields from a rtc_time struct to a tm struct. When the cast is done some memory is corrupted
     * and for example codesys function returns an error
     */
    //(void)mktime((struct tm *)&(alarm->time));
    timeDate.tm_sec = alarm->time.tm_sec;
    timeDate.tm_min = alarm->time.tm_min;
    timeDate.tm_hour = alarm->time.tm_hour;
    timeDate.tm_mday = alarm->time.tm_mday;
    timeDate.tm_mon = alarm->time.tm_mon;
    timeDate.tm_year = alarm->time.tm_year;
    timeDate.tm_wday = alarm->time.tm_wday;
    timeDate.tm_yday = alarm->time.tm_yday;
    timeDate.tm_isdst = alarm->time.tm_isdst;
    alarmTime = mktime(&timeDate);
    timeDate.tm_sec = 0;
    alarmTime = mktime(&timeDate);

    alarm->time.tm_sec = 0;
    alarm->time.tm_min = timeDate.tm_min;
    alarm->time.tm_hour = timeDate.tm_hour;
    alarm->time.tm_mday = timeDate.tm_mday;
    alarm->time.tm_mon = timeDate.tm_mon;
    alarm->time.tm_year = timeDate.tm_year;
    alarm->time.tm_wday = timeDate.tm_wday;
    alarm->time.tm_yday = timeDate.tm_yday;
    alarm->time.tm_isdst = timeDate.tm_isdst;

    if (alarm->enabled == (unsigned char)0x1U)
    {
        /* Verify that the time difference between the current time and the 
         * alarm time is not more than a month. This is because RTC only 
         * considers day, hour and minute. This is necessary only in case
         * that the alarm is being set (when cleared it is not needed)
         */
        currentTimeDate.tm_sec = current_time->tm_sec;
        currentTimeDate.tm_min = current_time->tm_min;
        currentTimeDate.tm_hour = current_time->tm_hour;
        currentTimeDate.tm_mday = current_time->tm_mday;
        currentTimeDate.tm_mon = current_time->tm_mon;
        currentTimeDate.tm_year = current_time->tm_year;
        currentTimeDate.tm_wday = current_time->tm_wday;
        currentTimeDate.tm_yday = current_time->tm_yday;
        currentTimeDate.tm_isdst = current_time->tm_isdst;
        currentTime = mktime(&currentTimeDate);

        if ((hal_power_check_max_time_diff(&alarm->time, current_time) == FALSE) ||
            (difftime(alarmTime, currentTime) < 60))
        {
            return HAL_E_INVALID_PARAMETER;
        }
    }

    /* Set wakeup alarm */
    if (ioctl(fd_rtc, RTC_WKALM_SET, alarm) == -1) 
    {
        return HAL_E_FAIL;
    }

    return HAL_E_OK;
}

static hal_error hal_power_set_wakeup_alarm_from_now_internal( uint32_t seconds, unsigned char enabled, unsigned char pending, struct rtc_wkalrm * rtc_alarm )
{
    hal_error         error;
    struct rtc_wkalrm alarm;
    struct rtc_time   currentTime;

    // in case user does not want to have the alarm time, use a local variable
    if (rtc_alarm == NULL)
    {
        rtc_alarm = &alarm;
    }

    error = hal_power_open_rtc();

    if (error == HAL_E_OK)
    {
        /* Read the RTC time/date */
        if (ioctl(fd_rtc, RTC_RD_TIME, &currentTime) == -1) 
        {
            error = HAL_E_FAIL;
        }

        if (error == HAL_E_OK)
        {
            rtc_alarm->time = currentTime;

            /* Function hal_power_add_wakeup_alarm() calls mktime() which will 
             * correctly modify minutes, hours, etc, in case of an overflow in seconds
             */
            rtc_alarm->time.tm_sec += seconds;

            rtc_alarm->enabled = enabled;
            rtc_alarm->pending = pending;

            error = hal_power_add_wakeup_alarm( rtc_alarm, &currentTime );
        }

        hal_power_close_rtc();
    }

    return error;
}

hal_error hal_power_set_wakeup_alarm_from_now( uint32_t seconds, struct rtc_wkalrm * alarm )
{
    CHECK_MODULE_INITIALIZED(initialized);
    CHECK_FEATURE(cap_has_wakeup_rtc);

    return hal_power_set_wakeup_alarm_from_now_internal( seconds, (unsigned char)0x1U, (unsigned char)0x1U, alarm );
}

hal_error hal_power_set_wakeup_alarm( struct rtc_wkalrm * alarm )
{
    hal_error         error;
    struct rtc_time   currentTime;

    CHECK_MODULE_INITIALIZED(initialized);
    CHECK_FEATURE(cap_has_wakeup_rtc);
    CHECK_POINTER_PARAMETER(alarm);

    error = hal_power_open_rtc();

    if (error == HAL_E_OK)
    {
        /* Read the RTC time/date */
        if (ioctl(fd_rtc, RTC_RD_TIME, &currentTime) == -1) 
        {
            error = HAL_E_FAIL;
        }
        else
        {
            error = hal_power_add_wakeup_alarm(alarm, &currentTime);
        }

        hal_power_close_rtc();
    }

    return error;
}

hal_error hal_power_get_wakeup_alarm(struct rtc_wkalrm * alarm)
{
    hal_error         error;

    CHECK_MODULE_INITIALIZED(initialized);
    CHECK_FEATURE(cap_has_wakeup_rtc);
    CHECK_POINTER_PARAMETER(alarm);

    error = hal_power_open_rtc();

    if (error == HAL_E_OK)
    {
        if (ioctl(fd_rtc, RTC_WKALM_RD, alarm) == -1)
        {
            error = HAL_E_FAIL;
        }

        hal_power_close_rtc();
    }

    return error;
}

hal_error hal_power_clear_wakeup_alarm(void)
{
    CHECK_MODULE_INITIALIZED(initialized);
    CHECK_FEATURE(cap_has_wakeup_rtc);

    return hal_power_set_wakeup_alarm_from_now_internal( 0, (unsigned char)0x0U, (unsigned char)0x1U, NULL );
}
