#include "hal_led.h"
#include "hal.h"
#include "hal_types.h"
#include "tools.h"

#include <linux/input.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <syslog.h>
#include <glib.h>

#define MODULE_NAME         "[led] "
#define MAX_LEDS            2
#define LEDS_MAX_VALUE      100

static char *dev_nodes[MAX_LEDS] =
{
        "/proc/spi_leds/a",
        "/proc/spi_leds/b",
};

/* This fd array is initialized to -1 by hal_led_init() */
static int fd_leds[MAX_LEDS];
/* This fd array is defined as static, so initialized to 0 automatically */
static int leds_value[MAX_LEDS];

static gboolean initialized = FALSE;
static GMutex mutex;

static gboolean cap_has_leds = FALSE;

static hal_error check_led_caps()
{
    hal_error err = HAL_E_OK;
    uint32_t cap;

    cap_has_leds = FALSE;
    err = hal_syscfg_get_capability( HAL_SYSCAP_LEDS, &cap );
    if ( err != HAL_E_OK )
    {
        return HAL_E_FAIL;
    }
    
    switch ( cap )
    {
        case SYSCFG_LEDS_2xPWM:
        case SYSCFG_ALL_FEATURES:
            cap_has_leds = TRUE;
            break;
        default:
            err = HAL_E_NOFEATURE;
            break;
    }
    
    return err;
}

hal_error hal_led_init()
{
    int i;
    hal_error err = HAL_E_OK;

    CHECK_HAL_INITIALIZED();
    CHECK_MODULE_ALREADY_INITIALIZED( initialized );

    /* initialize the file descriptor array members to -1 */
    for (i = 0; i < MAX_LEDS; i++)
    {
        fd_leds[i] = -1;
    }

    err = check_led_caps();
    if (err == HAL_E_FAIL)
    {
        return HAL_E_FAIL;
    }
    else if (err == HAL_E_NOFEATURE)
    {
        initialized = TRUE;
        return HAL_E_NOFEATURE;
    }

    for ( i = 0; i < MAX_LEDS; i++ )
    {
        fd_leds[i] = open( dev_nodes[i], O_RDWR );
        if ( fd_leds[i] == -1 )
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Could not open led device node %s: %d", dev_nodes[i], errno );
            return HAL_E_FAIL;
        }

    }


    initialized = TRUE;
    return HAL_E_OK;
}

hal_error hal_led_deinit()
{
    CHECK_MODULE_INITIALIZED( initialized );

    for ( int i = 0; i < MAX_LEDS; i++ )
    {
        /* Switch off LEDs before deinit */
        hal_led_set_value( i, 0 );
        if ( fd_leds[i] != -1 )
        {
            close( fd_leds[i] );
            fd_leds[i] = -1;
        }
    }
    initialized = FALSE;

    return HAL_E_OK;
}

hal_error hal_led_set_value( hal_led led, uint8_t value)
{
    hal_error err;
    int scaled;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_leds );

    if ( (int)led >= MAX_LEDS )
    {
        return HAL_E_INVALID_PARAMETER;
    }

    if (leds_value[led] == value)
    {
        return HAL_E_OK;
    }

    scaled = ((uint16_t)value * LEDS_MAX_VALUE) / 0xFF;

    g_mutex_lock( &mutex );
    err = file_set_value_int( fd_leds[(int)led], scaled);

    if ( err == HAL_E_OK )
    {
        leds_value[led] = value;
    }
    else
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not set LED %u to scaled value %u", led, scaled );
    }

    g_mutex_unlock( &mutex );

    return err;
}

hal_error hal_led_get_value( hal_led led, uint8_t * const value )
{
    hal_error err;
    int val;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( value );
    CHECK_FEATURE( cap_has_leds );

    if ( (int)led >= MAX_LEDS )
    {
        return HAL_E_INVALID_PARAMETER;
    }

    *value = leds_value[led];

    return HAL_E_OK;
}
