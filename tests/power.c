#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/stat.h> 
#include <fcntl.h>

#include <glib.h>

#include "libhal.h"

static struct option options[] =
{
    {"help", no_argument, 0, '?'},
    {"test", required_argument, 0, 't'},
    {0, 0, 0, 0}
};

#define TEST_CAMERA_POWER            0
#define TEST_TERMINAL15              1
#define TEST_WAKEUP                  2
#define TEST_EVENT_POWEROFF          3
#define TEST_EVENT_WAKEUP            4
#define TEST_SUSPEND                 5
#define TEST_WAKE_ALARM              6
#define TEST_WAKE_ALARM_FROM_NOW     7
#define TEST_WAKE_ALARM_MAX          8
#define TEST_SET_RTC_TIME            9
#define TEST_GET_RTC_TIME           10
#define TEST_RD_WAKE_ALARM          11
#define TEST_CLEAR_WAKE_ALARM       12
#define TEST_GET_WAKEUP_SOURCE      13

void print_help()
{
    printf("HAL power test\n" \
           "  -? --help       This message\n" \
           "  -t --test=<n>   Test program to run\n" \
           "                  0 ... Toggle camera power\n" \
           "                  1 ... Read terminal15 state in a loop\n" \
           "                  2 ... Read wakeup state in a loop\n" \
           "                  3 ... Do a K15 event-triggered poweroff\n" \
           "                  4 ... wakeup callback register\n" \
           "                  5 ... suspend\n" \
           "                  6 ... set wake alarm in 120 seconds\n" \
           "                  7 ... set wake alarm in 120 seconds from now\n" \
           "                  8 ... set wake alarm to more than 1 month (will fail)\n" \
           "                  9 ... set RTC time (use option 9 to get time, this time will be set)\n" \
           "                 10 ... get RTC time (use option 8 to set time, this time will be set)\n" \
           "                 11 ... read wake alarm information in a loop\n" \
           "                 12 ... clear wake alarm\n" \
           "                 13 ... get wakeup source\n" \
           "\n");
}

void leave(int sig)
{
    if (sig != 0)
    {
        printf("SIGINT caught. Exiting.\n");
    }
    hal_power_deinit();
    hal_deinit();
    exit(sig);
}

static void print_wakeup_source()
{
    hal_error err;
    hal_power_wakeup_source source;
    char * source_name;

    err = hal_power_get_wakeup_source(&source);
    if (err == HAL_E_OK)
    {
        switch (source)
        {
            case HAL_POWER_WAKEUP_POWERON:
                source_name = "Poweron";
                break;
            case HAL_POWER_WAKEUP_RESUME:
                source_name = "Resume";
                break;
            case HAL_POWER_WAKEUP_ALARM_POWERON:
                source_name = "Poweron (Alarm)";
                break;
            case HAL_POWER_WAKEUP_ALARM_RESUME:
                source_name = "Resume (Alarm)";
                break;
            default:
                source_name = "UNKNOWN";
                break;
        }
        fprintf(stderr, "Wakeup source: %s\n", source_name);
    }
    else
    {
        printf( "Could not get wakeup source : %d\n", err );
    }
}

static void test_camera_power()
{
    hal_power_state state;

    printf("Test: camera power\n\n");

    printf("Set camera power to 1\n");
    hal_power_set_camera( HAL_PWR_STATE_HIGH );
    hal_power_get_camera( &state );
    printf("Read camera power: %u\n", state );

    printf("Set camera power to 0\n");
    hal_power_set_camera( HAL_PWR_STATE_LOW );
    hal_power_get_camera( &state );
    printf("Read camera power: %u\n", state );
}

static void test_terminal15()
{
    hal_power_state state;

    printf("Test: terminal 15 read\n\n");

    while (1)
    {
        hal_power_get_terminal15( &state );
        printf("Terminal15: %u\n", state );
        g_usleep( G_USEC_PER_SEC / 2 );
    }
}

static void test_wakeup()
{
    hal_power_state state;

    printf("Test: wakeup read\n\n");

    while (1)
    {
        hal_power_get_wakeup( &state );
        printf("Wakeup: %u\n", state );
        g_usleep( G_USEC_PER_SEC / 2 );
    }
}

static void test_suspend()
{
    hal_power_suspend();
    print_wakeup_source();
}

void terminal15_cb_1( hal_power_state state )
{
    printf("Callback function 1 called with state: %u...\n", state);
}

void terminal15_cb_2( hal_power_state state )
{
    printf("Callback function 2 called with state: %u...\n", state);
    g_usleep( G_USEC_PER_SEC );
    printf("Execute poweroff\n");
    hal_power_off();
}

static void test_event_poweroff()
{
    printf("Test: power off on terminal 15 event\n\n");
    hal_power_register_callback_terminal15( &terminal15_cb_1 );
    hal_power_register_callback_terminal15( &terminal15_cb_2 );
    printf("Disconnect Terminal15 to shutdown\n");
    while (1)
    {
    }
}

void wakeup_cb( hal_power_state state )
{
    printf("Wakeup Callback function called with state: %u...\n", state);
}

static void test_event_wakeup()
{
    printf("Test: wakeup event\n\n");
    hal_power_register_callback_wakeup( &wakeup_cb );
    while (1)
    {
    }
}

static int test_get_rtc_time_internal2(struct rtc_wkalrm * alarm, const uint8_t * device, int fd_rtc)
{
    if (ioctl(fd_rtc, RTC_RD_TIME, &alarm->time) == -1) 
    {
        printf( "Could not get rtc current time : %d\n", errno );
        close(fd_rtc);
        return -1;
    }

    return 0;
}

static void printTime(char * format, struct rtc_wkalrm * alarm)
{
    int weekDay = (alarm->time.tm_wday == 0) ? 7 : alarm->time.tm_wday;
    int year = (alarm->time.tm_year != 255) ? (alarm->time.tm_year + 1900) : alarm->time.tm_year;
    int month = (alarm->time.tm_mon != 255) ? (alarm->time.tm_mon + 1) : alarm->time.tm_mon;
    int yearDay = (alarm->time.tm_year != 255) ? (alarm->time.tm_yday + 1) : alarm->time.tm_yday;

    fprintf(stderr, format, 
            year, month, alarm->time.tm_mday, alarm->time.tm_hour, alarm->time.tm_min, alarm->time.tm_sec, 
            weekDay, yearDay, alarm->time.tm_isdst);
}

static void printTime2(char * format, struct rtc_wkalrm * alarm)
{
    int weekDay = (alarm->time.tm_wday == 0) ? 7 : alarm->time.tm_wday;
    int year = (alarm->time.tm_year != 255) ? (alarm->time.tm_year + 1900) : alarm->time.tm_year;
    int month = (alarm->time.tm_mon != 255) ? (alarm->time.tm_mon + 1) : alarm->time.tm_mon;
    int yearDay = (alarm->time.tm_year != 255) ? (alarm->time.tm_yday + 1) : alarm->time.tm_yday;

    fprintf(stderr, format, 
            year, month, alarm->time.tm_mday, alarm->time.tm_hour, alarm->time.tm_min, alarm->time.tm_sec, 
            weekDay, yearDay, alarm->time.tm_isdst, (unsigned int)alarm->enabled, (unsigned int)alarm->pending);
}

static int test_get_rtc_time_internal(struct rtc_wkalrm * alarm)
{
    const uint8_t * device;
    int             fd_rtc = -1;

    // get device name
    if (hal_rtc_get_device(&device) != HAL_E_OK)
    {
        printf( "Could not get rtc device node name\n" );
        return -1;
    }

    fd_rtc = open (device, O_RDONLY);
    if (fd_rtc == -1)
    {
        printf( "Could not open rtc device node : %d\n", errno );
        return -1;
    }

    if (test_get_rtc_time_internal2(alarm, device, fd_rtc) != 0)
    {
        close(fd_rtc);
        return -1;
    }

    close(fd_rtc);

    return 0;
}

static void test_set_wake_alarm_from_now()
{
    struct rtc_wkalrm   alarm;
    static int          fd_rtc = -1;
    const uint8_t     * device;

    printf("Test: set WAKE alarm from now (in 120 seconds)\n\n");

    // print current time so tester knows when alarm is being set.
    if (test_get_rtc_time_internal(&alarm) == 0)
    {
        printTime("Current RTC time %d-%d-%d %d:%d:%d %d (Week day) %d (year day) %d (Daylight saving time?)\n", &alarm);

        // call function under test using a null pointer
        if (hal_power_set_wakeup_alarm_from_now( 120U, NULL ) != HAL_E_OK)
        {
            fprintf(stderr, "Function hal_power_set_wakeup_alarm_from_now(120, NULL) has returned ERROR!\n");
        }
        else
        {
            fprintf(stderr, "Function hal_power_set_wakeup_alarm_from_now(120, NULL) OK (NULL as second parameter)\n");
        }

        // call function under test.
        if (hal_power_set_wakeup_alarm_from_now( 120U, &alarm ) != HAL_E_OK)
        {
            fprintf(stderr, "Function hal_power_set_wakeup_alarm_from_now() has returned ERROR!\n");
        }
        else
        {
            printTime("Alarm was set at %d-%d-%d %d:%d:%d %d (Week day) %d (year day) %d (Daylight saving time?)\n", &alarm);
        }
    }
}

static void test_set_wake_alarm()
{
    struct rtc_wkalrm   alarm;
    static int          fd_rtc = -1;
    const uint8_t     * device;

    printf("Test: set WAKE alarm (2 minutes from now)\n\n");

    // find out the current time
    if (hal_rtc_get_device(&device) != HAL_E_OK)
    {
        printf( "Could not get rtc device node name\n" );
        return;
    }

    fd_rtc = open (device, O_RDONLY);
    if (fd_rtc == -1)
    {
        printf( "Could not open rtc device node : %d\n", errno );
        return;
    }

    if (test_get_rtc_time_internal2(&alarm, device, fd_rtc) != 0)
    {
        printf( "Could not get rtc current time : %d\n", errno );
        close(fd_rtc);
        return;
    }

    printTime("Current RTC time %d-%d-%d %d:%d:%d %d (Week day) %d (year day) %d (Daylight saving time?)\n", &alarm);

    close(fd_rtc);

    alarm.time.tm_min += 2U;
    alarm.enabled = (unsigned char)0x1U;
    alarm.pending = (unsigned char)0x1U;

    if (hal_power_set_wakeup_alarm( &alarm ) != HAL_E_OK)
    {
        fprintf(stderr, "Function hal_power_set_wakeup_alarm_from_now() has returned ERROR!\n");
    }
    else
    {
        printTime("Alarm was set at %d-%d-%d %d:%d:%d %d (Week day) %d (year day) %d (Daylight saving time?)\n", &alarm);
    }
}

static void test_wake_max_alarm()
{
    struct rtc_wkalrm   alarm;
    static int          fd_rtc = -1;
    const uint8_t     * device;

    printf("Test: set WAKE alarm (2 minutes from now)\n\n");

    // find out the current time
    if (hal_rtc_get_device(&device) != HAL_E_OK)
    {
        printf( "Could not get rtc device node name\n" );
        return;
    }

    fd_rtc = open (device, O_RDONLY);
    if (fd_rtc == -1)
    {
        printf( "Could not open rtc device node : %d\n", errno );
        return;
    }

    if (test_get_rtc_time_internal2(&alarm, device, fd_rtc) != 0)
    {
        printf( "Could not get rtc current time : %d\n", errno );
        close(fd_rtc);
        return;
    }

    printTime("Current RTC time %d-%d-%d %d:%d:%d %d (Week day) %d (year day) %d (Daylight saving time?)\n", &alarm);

    close(fd_rtc);

    alarm.time.tm_mday += 31U;
    alarm.enabled = (unsigned char)0x1U;
    alarm.pending = (unsigned char)0x1U;

    if (hal_power_set_wakeup_alarm( &alarm ) != HAL_E_OK)
    {
        fprintf(stderr, "Function hal_power_set_wakeup_alarm_from_now() has returned ERROR!\n");
        printTime("Alarm was set at %d-%d-%d %d:%d:%d %d (Week day) %d (year day) %d (Daylight saving time?)\n", &alarm);
    }
    else
    {
        fprintf(stderr, "HUH??\n");
    }
}

static void test_set_rtc_time()
{
    const uint8_t * device;
    int             fd_rtc = -1;
    struct rtc_wkalrm rtcAlarm;

    printf("Test: set RTC time\n\n");

    // get device name
    if (hal_rtc_get_device(&device) != HAL_E_OK)
    {
        printf( "Could not get rtc device node name\n" );
        return;
    }

    fd_rtc = open (device, O_RDONLY);
    if (fd_rtc == -1)
    {
        printf( "Could not open rtc device node : %d\n", errno );
        return;
    }

    if (test_get_rtc_time_internal2(&rtcAlarm, device, fd_rtc) != 0)
    {
        close(fd_rtc);
        return;
    }

    printTime("Current RTC time is %d-%d-%d %d:%d:%d %d (Week day) %d (year day) %d (Daylight saving time?)\n", &rtcAlarm);

    if (rtcAlarm.time.tm_min > 0)
    {
        rtcAlarm.time.tm_min--;
    }
    else
    {
        rtcAlarm.time.tm_min = 59;
        rtcAlarm.time.tm_hour--;
    }
    rtcAlarm.time.tm_sec = 0;

    if (ioctl(fd_rtc, RTC_SET_TIME, &rtcAlarm.time) == -1) 
    {
        printf( "Could not set rtc time : %d\n", errno );
        close(fd_rtc);
        return;
    }

    printTime("RTC time set to %d-%d-%d %d:%d:%d %d (Week day) %d (year day) %d (Daylight saving time?)\n", &rtcAlarm);

    close(fd_rtc);
}

static void test_get_rtc_time()
{
    struct rtc_wkalrm rtcAlarm;

    printf("Test: get RTC time\n\n");

    if (test_get_rtc_time_internal(&rtcAlarm) == 0)
    {
        printTime("Current RTC time %d-%d-%d %d:%d:%d %d (Week day) %d (year day) %d (Daylight saving time?)\n", &rtcAlarm);
    }
}

static void test_read_wake_alarm()
{
    const uint8_t *   device;
    int               fd_rtc = -1;
    struct rtc_wkalrm rtcAlarm;

    printf("Test: read wake alarm\n\n");
    
    while (1)
    {
        if (hal_power_get_wakeup_alarm(&rtcAlarm) != HAL_E_OK)
        {
            printf( "Could not get rtc alarm. Function hal_power_get_wakeup_alarm() returned error.\n" );
        }
        else
        {
            printTime2("Current RTC alarm is %d-%d-%d %d:%d:%d %d (Week day) %d (year day) %d (Daylight saving time?) %d (enabled) %d (pending)\n", &rtcAlarm);
        }
        g_usleep( G_USEC_PER_SEC / 2 );
    }
}

static void test_clear_wake_alarm()
{
    hal_error err;

    printf("Test: clear wake alarm\n\n");

    err = hal_power_clear_wakeup_alarm();
    if (err == HAL_E_OK)
    {
        fprintf(stderr, "Alarm cleared correctly\n");
    }
    else
    {
        printf( "Could not clear wake alarm : %d\n", err );
    }
}

static void test_get_wakeup_source()
{
    printf("Test: get wakeup source\n\n");

    print_wakeup_source();
}

int main(int argc, char **argv)
{
    int option_index = 0;
    int c = 0;
    int test_program = 0;
    hal_error err;

    while (c != -1)
    {
        option_index = 0;
        c = getopt_long(argc, argv, "?t:", options,
                        &option_index);

        switch (c)
        {
            case '?':
                print_help();
                exit(0);
                break;
            case 't':
                if (sscanf(optarg, "%d", &test_program) == 0)
                {
                    printf("Missing: Test program missing.");
                    exit(-1);
                }
                break;
        }
    }

    signal(SIGINT, leave);


    err = hal_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize HAL (%d)\n", err);
        exit(-1);
    }

    err = hal_power_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize power module (%d)\n", err);
        exit(-1);
    }

    switch (test_program)
    {
        case TEST_CAMERA_POWER:
            test_camera_power();
            break;
        case TEST_TERMINAL15:
            test_terminal15();
            break;
        case TEST_WAKEUP:
            test_wakeup();
            break;
        case TEST_EVENT_WAKEUP:
            test_event_wakeup();
            break;
        case TEST_SUSPEND:
            test_suspend();
            break;
        case TEST_EVENT_POWEROFF:
            test_event_poweroff();
            break;
        case TEST_WAKE_ALARM:
            test_set_wake_alarm();
            break;
        case TEST_WAKE_ALARM_FROM_NOW:
            test_set_wake_alarm_from_now();
            break;
        case TEST_WAKE_ALARM_MAX:
            test_wake_max_alarm();
            break;
        case TEST_SET_RTC_TIME:
            test_set_rtc_time();
            break;
        case TEST_GET_RTC_TIME:
            test_get_rtc_time();
            break;
        case TEST_RD_WAKE_ALARM:
            test_read_wake_alarm();
            break;
        case TEST_CLEAR_WAKE_ALARM:
            test_clear_wake_alarm();
            break;
        case TEST_GET_WAKEUP_SOURCE:
            test_get_wakeup_source();
            break;
        default:
            printf("Undefined test program! Abort.");
            break;
    }

    leave(0);
}
