#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>

#include <glib.h>

#include "libhal.h"
#include "libudev.h"

static struct option options[] =
{
    {"help", no_argument, 0, '?'},
    {0, 0, 0, 0}
};

void print_help()
{
    printf("HAL UDEV test\n" \
           "  -? --help       This message\n" \
           "\n");
}

void leave(int sig)
{
    if (sig != 0)
    {
        printf("SIGINT caught. Exiting.\n");
    }
    hal_udev_deinit();
    hal_deinit();
    exit(sig);
}

void device_callback(struct udev_device * const dev)
{
    struct udev_list_entry * e = NULL;

    printf("\nGot Device:\n  Device Node: %s\n  Subsystem: %s\n" \
        "  Device Type: %s\n  Driver: %s\n  Action: %s\n  ",
        udev_device_get_devnode(dev), udev_device_get_subsystem(dev),
        udev_device_get_devtype(dev), udev_device_get_driver(dev),
        udev_device_get_action(dev));
        
    printf("  Properties:\n");
    udev_list_entry_foreach(e, udev_device_get_properties_list_entry(dev))
    {
        printf("    %s = \"%s\"\n", udev_list_entry_get_name(e),
            udev_list_entry_get_value(e));
    }
    
    printf("  Tags:\n");
    udev_list_entry_foreach(e, udev_device_get_tags_list_entry(dev))
    {
        printf("    %s = \"%s\"\n", udev_list_entry_get_name(e),
            udev_list_entry_get_value(e));
    }

}

int main(int argc, char **argv)
{
    hal_error err;
    int option_index = 0;
    int c = 0;
    uint8_t val;
    int led0;
    int led1;

    while (c != -1)
    {
        option_index = 0;
        c = getopt_long(argc, argv, "?", options,
                        &option_index);

        switch (c)
        {
            case '?':
                print_help();
                exit(0);
                break;
        }
    }

    signal(SIGINT, leave);

    err = hal_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize HAL (%d)\n", err);
        exit(-1);
    }

    err = hal_udev_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize UDEV module (%d)\n", err);
        exit(-1);
    }
    
    hal_udev_register_callback(device_callback);
    hal_udev_start(TRUE);
    
    while (1)
    {
        usleep(100*1000);
    }

    leave(0);
}
