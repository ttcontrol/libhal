/**************************************************************************
 * Copyright (c) 2013 TTTControl. All rights reserved. Confidential proprietary
 * Schoenbrunnerstrasse 7, A-1040 Wien, Austria. office[at]ttcontrol.com
 **************************************************************************/
/**********************************************************************//**
 * \file hal_types.h
 *
 * \brief Definition of some global types
 *
 **************************************************************************/

#ifndef _HAL_TYPES_H_
#define _HAL_TYPES_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdarg.h>

/** General error type */
typedef enum
{
    HAL_E_OK,                     /**< Everything OK */
    HAL_E_FAIL,                   /**< Generic Error */
    HAL_E_INVALID_PARAMETER,      /**< A invalid parameter has been passed to the function */
    HAL_E_NULL_POINTER,           /**< A NULL pointer has been passed to the function */
    HAL_E_NOT_INITIALIZED,        /**< The requested module or HAL in general is not initialized */
    HAL_E_ALREADY_INITIALIZED,    /**< The requested module or HAL in general is already initialized */
    HAL_E_NOFEATURE,              /**< The requested feature is not available on the current hardware */
    HAL_E_NVRAM_INCOMPLETE_WRITE, /**< The requested write operation to NVRAM was only completed partially */
    HAL_E_NVRAM_INCOMPLETE_READ,  /**< The requested read operation to NVRAM was only completed partially */
    HAL_E_GPS_NO_FIX,             /**< Requested GPS information not available due to no fix */
    HAL_E_SENSOR_INIT_FAILED,     /**< One of the sensors could not be initialized */
    HAL_E_SENSOR_ERROR,           /**< The requested sensor produced an error */
    HAL_E_PENDING,                /**< Operation currently in progress */
    HAL_E_DEVICE_ALREADY_MOUNTED, /**< Block device has already been mounted and device has not been ejected */
    HAL_E_DBUS_NOREPLY,           /**< No reply to function call */
    HAL_E_WLAN_NOCARRIER,         /**< No carrier */
    HAL_E_WLAN_PASSPHRASE,        /**< Wrong passphrase */
    HAL_E_WLAN_ALREADY_CONNECTED, /**< Already connected to network */
    HAL_E_UNSUPPORTED = -1,       /**< Unsupported by library */
} hal_error;

/**
 * \brief Type for versions with 3 components
 */
typedef struct
{
    uint16_t major;                 /**< Major version number */
    uint16_t minor;                 /**< Minor version number */
    uint16_t patch;                 /**< Patch version number*/
} hal_version3;

typedef enum
{
    HAL_LOG_SEVERITY_DEBUG,
    HAL_LOG_SEVERITY_INFO,
    HAL_LOG_SEVERITY_WARNING,
    HAL_LOG_SEVERITY_ERROR,
} hal_log_severity;

/**
 * \brief Function that is called on a logging event
 */
typedef void ( *hal_log_func )( hal_log_severity const severity,
    uint8_t const * const message, va_list args );

void hal_log_default_func( hal_log_severity const severity,
    uint8_t const * const message, va_list args );

#ifdef __cplusplus
}  /* End of the 'extern "C"' block */
#endif

#endif /* _HAL_TYPES_H_ */
