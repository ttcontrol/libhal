#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>

#include "libhal.h"

static struct option options[] =
{
    {"help", no_argument, 0, '?'},
    {"left", required_argument, 0, 'x'},
    {"top", required_argument, 0, 'y'},
    {"width", required_argument, 0, 'w'},
    {"height", required_argument, 0, 'h'},
    {"channel", required_argument, 0, 'c'},
    {"rotation", required_argument, 0, 'r'},
    {"test", required_argument, 0, 't'},
    {"delay", required_argument, 0, 'd'},
    {0, 0, 0, 0}
};

#define TEST_SHOW            0
#define TEST_CYCLE_PIPELINE  1
#define TEST_CYCLE_CHANNEL   2
#define TEST_CYCLE_POWER     3
//#define TEST_ROAMING         4

void print_help()
{
    printf("HAL camera test\n" \
           "  -? --help       This message\n" \
           "  -x --left       X coordinate of camera window\n" \
           "  -y --top        Y coordinate of camera window\n" \
           "  -w --width      Width of camera window\n" \
           "  -h --height     Height of camera window\n" \
           "  -c --channel    Camera channel to display\n" \
           "  -r --rotation   Camera rotation\n" \
           "                  0 ... None\n" \
           "                  1 ... Vertical Flip\n" \
           "                  2 ... Horizontal Flip\n" \
           "                  3 ... 180�\n" \
           "                  4 ... 90� CW\n" \
           "                  5 ... 90� CW VFlip\n" \
           "                  6 ... 90� CW HFlip\n" \
           "                  7 ... 90� CCW\n" \
           "  -t --test=<n>   Test program to run\n" \
           "                  0 ... Show camera image (DEFAULT)\n" \
           "                  1 ... Cycle pipeline state PLAYING->NULL\n" \
           "                  2 ... Cycle camera channels\n" \
           "                  3 ... Cycle camera power\n" \
           "  -d --delay=<n>  Delay between tests in milliseconds\n" \
           "                  (DEFAULT 1000)\n\n");
}

void test_show(uint32_t udelay)
{
    printf("Test: show camera image\n\n");
    hal_camera_play(HAL_CAMERA_VIEWPORT_1);
    usleep(udelay);
}

void test_cycle_pipeline(uint32_t udelay)
{
    uint32_t cycle = 0;
    
    printf("Test: cycle pipeline\n\n");    
    while (1)
    {
        printf(">>Cycle #%d<<\nStarting\n", cycle++);
        hal_camera_play(HAL_CAMERA_VIEWPORT_1);
        usleep(udelay);
        
        printf("Stopping\n\n");
        hal_camera_stop(HAL_CAMERA_VIEWPORT_1);
        usleep(udelay);
    }
}

void test_cycle_channel(uint32_t udelay)
{
    hal_camera_channel ch = HAL_CAMERA_CHANNEL_1;
    
    printf("Test: cycle channel\n\n");    
    hal_camera_play(HAL_CAMERA_VIEWPORT_1);
    while (1)
    {
        printf("Channel %d\n", ch);
        hal_camera_set_channel(HAL_CAMERA_VIEWPORT_1, ch);
        usleep(udelay);
        ch = (ch == HAL_CAMERA_VIEWPORT_1) ? HAL_CAMERA_VIEWPORT_2
                                           : HAL_CAMERA_VIEWPORT_1;
    }
    hal_camera_stop(HAL_CAMERA_VIEWPORT_1);
}

void test_cycle_power(uint32_t udelay)
{
    uint8_t pwr = 1;
    
    printf("Test: cycle power\n\n");    
    hal_camera_play(HAL_CAMERA_VIEWPORT_1);
    while (1)
    {
        printf("Power %d\n", pwr);
        hal_power_set_camera(pwr);
        usleep(udelay);
        pwr = !pwr;
    }
    hal_camera_stop(HAL_CAMERA_VIEWPORT_1);
}

/*void test_roaming(uint32_t udelay)
{
    uint32_t x = 0, y = 0;

    printf("Test: roaming image\n\n");
    
    hal_camera_play(HAL_CAMERA_VIEWPORT_1);
    while (udelay > 0)
    {
        hal_camera_set_x(HAL_CAMERA_VIEWPORT_1, x++);
        hal_camera_set_y(HAL_CAMERA_VIEWPORT_1, y++);
        usleep(10000);
        udelay -= 10000;
    }
    hal_camera_stop(HAL_CAMERA_VIEWPORT_1);
}*/

void leave(int sig)
{
    if (signal != 0)
    {
        printf("SIGINT caught. Exiting.\n");
    }
    hal_camera_stop(HAL_CAMERA_VIEWPORT_1);
    hal_power_set_camera(0);
    hal_camera_deinit();
    hal_power_deinit();
    hal_deinit();
    exit(sig);
}

int main(int argc, char **argv)
{
    uint32_t dummy;
    uint32_t res;
    int option_index = 0;
    int c = 0;
    int test_program = 0;
    
    uint32_t x = 10, y = 10, width = 300, height = 300, channel = 0,
             rotation = 0;
    uint32_t udelay = 1000000;
    
    while (c != -1)
    {
        option_index = 0;
        c = getopt_long(argc, argv, "?x:y:w:h:c:r:t:d:", options,
                        &option_index);
        
        switch (c)
        {
            case '?':
                print_help();
                exit(0);
                break;
            case 'x':
                if (sscanf(optarg, "%d", &x) == 0)
                {
                    printf("Missing: X coordinate value missing.");
                    exit(-1);
                }
                break;
            case 'y':
                if (sscanf(optarg, "%d", &y) == 0)
                {
                    printf("Missing: Y coordinate value missing.");
                    exit(-1);
                }
                break;
            case 'w':
                if (sscanf(optarg, "%d", &width) == 0)
                {
                    printf("Missing: Width value missing.");
                    exit(-1);
                }
                break;
            case 'h':
                if (sscanf(optarg, "%d", &height) == 0)
                {
                    printf("Missing: Height value missing.");
                    exit(-1);
                }
                break;
            case 'c':
                if (sscanf(optarg, "%d", &channel) == 0)
                {
                    printf("Missing: Channel value missing.");
                    exit(-1);
                }
                else
                {
                    if ((channel < 0) || (channel > 1))
                    {
                        printf("Channel value must be between 0 and 1.");
                        exit(-1);
                    }
                }
                break;
            case 'r':
                if (sscanf(optarg, "%d", &rotation) == 0)
                {
                    printf("Missing: Rotation value missing.");
                    exit(-1);
                }
                else
                {
                    if ((rotation < 0) || (rotation > 7))
                    {
                        printf("Rotation value must be between 0 and 7.");
                        exit(-1);
                    }
                }
                break;
            case 't':
                if (sscanf(optarg, "%d", &test_program) == 0)
                {
                    printf("Missing: Test program missing.");
                    exit(-1);
                }
                break;
            case 'd':
                if (sscanf(optarg, "%d", &udelay) == 0)
                {
                    printf("Missing: Delay value missing.");
                    exit(-1);
                }
                udelay *= 1000;
                break;
        }
    }
    
    signal(SIGINT, leave);

    hal_init();
    
    res = hal_camera_init();
    if (res != HAL_E_OK)
    {
        printf("hal_camera_init: %d\n", res);
        exit(-1);
    }
    
    //res = hal_power_init();
    if (res != HAL_E_OK)
    {
        printf("hal_power_init: %d\n", res);
        exit(-1);
    }
    
    //hal_power_set_camera(1);
    
    hal_camera_set_x(HAL_CAMERA_VIEWPORT_1, x);
    hal_camera_get_x(HAL_CAMERA_VIEWPORT_1, &dummy);
    printf("X: %d\n", dummy);

    hal_camera_set_y(HAL_CAMERA_VIEWPORT_1, y);
    hal_camera_get_y(HAL_CAMERA_VIEWPORT_1, &dummy);
    printf("Y: %d\n", dummy);

    hal_camera_set_width(HAL_CAMERA_VIEWPORT_1, width);
    hal_camera_get_width(HAL_CAMERA_VIEWPORT_1, &dummy);
    printf("Width: %d\n", dummy);

    hal_camera_set_height(HAL_CAMERA_VIEWPORT_1, height);
    hal_camera_get_height(HAL_CAMERA_VIEWPORT_1, &dummy);
    printf("Height: %d\n", dummy);
    
    hal_camera_set_channel(HAL_CAMERA_VIEWPORT_1, channel);
    hal_camera_get_channel(HAL_CAMERA_VIEWPORT_1, &dummy);
    printf("Channel: %d\n", dummy);
    
/*    hal_camera_set_rotation(HAL_CAMERA_VIEWPORT_1, rotation);
    hal_camera_get_rotation(HAL_CAMERA_VIEWPORT_1, &dummy);
    printf("Rotation: %d\n\n", dummy);*/
    
    switch (test_program)
    {
        case TEST_SHOW:
            test_show(udelay);
            break;
        case TEST_CYCLE_PIPELINE:
            test_cycle_pipeline(udelay);
            break;
        case TEST_CYCLE_CHANNEL:
            test_cycle_channel(udelay);
            break;
        case TEST_CYCLE_POWER:
            test_cycle_power(udelay);
            break;
/*        case TEST_ROAMING:
            test_roaming(udelay);
            break;*/
        default:
            printf("Undefined test program! Abort.");
            break;
    }
    
    leave(0);
}
