#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>
#include <glib.h>

#include "libhal.h"

void leave(int sig)
{
    if (sig != 0)
    {
        printf("SIGINT caught. Exiting.\n");
    }
    hal_power_deinit();
    hal_deinit();
    exit(sig);
}

static void test_buzzer_on()
{
    hal_buzzer_set_state( HAL_BUZ_STATE_ON );
}

static void test_buzzer_off()
{
    hal_buzzer_set_state( HAL_BUZ_STATE_OFF );
}

int main(int argc, char **argv)
{
    hal_error err;
    int option_index = 0;
    int c = 0;
    int test_program = 0;
    int32_t amb_lux;
    int32_t amb_temp;
    int32_t cpu_temp;
    int32_t pmic_temp;
    int32_t term30_volt;
    int32_t batt_volt;
    int32_t pmic_volt;
    int32_t pmic_curr;
    bool fresh;

    signal(SIGINT, leave);

    err = hal_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize HAL (%d)\n", err);
        exit(-1);
    }

    err = hal_sensor_init();
    if ( err == HAL_E_OK )
    {
        // NOP
    }
    else if ( err == HAL_E_SENSOR_INIT_FAILED )
    {
        printf("Could not fully initialize sensor module (%d)\n", err);
    }
    else
    {
        printf("Could not initialize sensor module (%d)\n", err);
        exit(-1);
    }

    while ( TRUE )
    {

        err = hal_sensor_get_value( HAL_SENS_TYPE_AMBIENT_TEMP, &amb_temp, NULL );
        if ( err != HAL_E_OK )
        {
            printf("Could not get ambient temperature (%d)\n", err);
            exit(-1);
        }

        err = hal_sensor_get_value( HAL_SENS_TYPE_CPU_TEMP, &cpu_temp, NULL );
        if ( err != HAL_E_OK )
        {
            printf("Could not get CPU temperature (%d)\n", err);
            exit(-1);
        }

        err = hal_sensor_get_value( HAL_SENS_TYPE_PMIC_TEMP, &pmic_temp, NULL );
        if ( err != HAL_E_OK )
        {
            printf("Could not get PMIC temperature (%d)\n", err);
            exit(-1);
        }

        err = hal_sensor_get_value( HAL_SENS_TYPE_TERMINAL30_VOLT, &term30_volt, NULL );
        if ( err != HAL_E_OK )
        {
            printf("Could not get Terminal30 voltage (%d)\n", err);
            exit(-1);
        }

        err = hal_sensor_get_value( HAL_SENS_TYPE_RTCBATT_VOLT, &batt_volt, NULL );
        if ( err != HAL_E_OK )
        {
            printf("Could not get RTC battery voltage (%d)\n", err);
            exit(-1);
        }

        err = hal_sensor_get_value( HAL_SENS_TYPE_PMIC_VOLT, &pmic_volt, NULL );
        if ( err != HAL_E_OK )
        {
            printf("Could not get PMIC voltage (%d)\n", err);
            exit(-1);
        }

        err = hal_sensor_get_value( HAL_SENS_TYPE_PMIC_CURR, &pmic_curr, NULL );
        if ( err != HAL_E_OK )
        {
            printf("Could not get PMIC current (%d)\n", err);
            exit(-1);
        }

        printf( "Ambient temperature: %d\n", amb_temp );
        printf( "CPU temperature:     %d\n", cpu_temp );
        printf( "PMIC temperature:    %d\n", pmic_temp );
        printf( "Terminal 30 voltage: %d\n", term30_volt );
        printf( "RTC battery voltage: %d\n", batt_volt );
        printf( "PMIC voltage:        %d\n", pmic_volt );
        printf( "PMIC current:        %d\n", pmic_curr );

        err = hal_sensor_get_value( HAL_SENS_TYPE_AMBIENT_LUX, &amb_lux, &fresh );
        if ( err != HAL_E_OK )
        {
            printf("Could not get ambient light brightness (%d)\n", err);
            exit(-1);
        }
        if ( fresh == FALSE )
        {
            printf("Lux value not fresh!\n");
        }
        else
        {
            printf("Ambient lux: %d\n", amb_lux );
        }

        printf("\n");

        g_usleep( G_USEC_PER_SEC );
    }
    leave(0);
}
