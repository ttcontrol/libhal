#ifdef HAVE_CONFIG_H
    #include "config.h"
#endif

#include "hal_gps.h"
#include "hal.h"
#include "hal_types.h"
#include "tools.h"
#include "scom.h"

#ifdef HAVE_GPS

#include <linux/input.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <syslog.h>
#include <glib.h>
#include <gps.h>
#include <math.h>

#define MODULE_NAME             "[gps] "
#define DEV_GPS                 "/dev/ttyGPS"
#define GPSD_START_TIMEOUT      500000      /* unit: microseconds */
#define GPS_DATA_TIMEOUT        50000000    /* unit: microseconds */
#define GPS_INIT_BUF_SIZE       256
#define GPS_INIT_TIMEOUT        5000 /* ms */
#define GPS_BUF_SIZE            1024
#define GPS_SHORT_TIMEOUT       500
#define GPS_MEDIUM_TIMEOUT      700
#define GPS_LONG_TIMEOUT        900

typedef struct
{
    double lon;
    double lat;
    double alt;
    double speed_ground;
    double speed_climb;
    double time;
    int mode;
    int sat_visible;
    int sat_used;

} gps_info;

static gboolean initialized = FALSE;
static GMutex mutex;

static struct gps_data_t my_gps_data;
static pthread_t worker_thread = 0;

static gboolean exit_request = FALSE;
static uint32_t rate = 1000;

static gps_info info;

static void* gps_thread( void *data )
{
    int res;
    gps_mask_t set;

    while ( exit_request == FALSE )
    {
        if ( gps_waiting ( &my_gps_data, GPS_DATA_TIMEOUT ) )
        {
            g_mutex_lock( &mutex );
            res = gps_read( &my_gps_data );
            g_mutex_unlock( &mutex );

            if ( res == -1 )
            {
                hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                    "gps_waiting returned with: %d", res );
            }
            else
            {
                if ( my_gps_data.set > 0 )
                {
                    set = my_gps_data.set;
                    if ( set & MODE_SET )
                    {
                        info.mode = my_gps_data.fix.mode;
                    }

                    if ( set & LATLON_SET )
                    {
                        info.lat = my_gps_data.fix.latitude;
                        info.lon = my_gps_data.fix.longitude;
                    }

                    if ( set & ALTITUDE_SET )
                    {
                        info.alt = my_gps_data.fix.altitude;
                    }

                    if ( set & SPEED_SET )
                    {
                        info.speed_ground = my_gps_data.fix.speed;
                        info.speed_climb = my_gps_data.fix.climb;
                    }

                    if ( set & SATELLITE_SET )
                    {
                        info.sat_used = my_gps_data.satellites_used;
                        info.sat_visible = my_gps_data.satellites_visible;
                    }

                    if ( set & TIME_SET )
                    {
                        info.time = my_gps_data.fix.time;
                    }
                }
            }
        }
        else
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Timeout on gps_waiting" );
        }
    }
}

hal_error hal_gps_init( )
{
    gint64  start, curr;
    int ret;
    hal_error err;
    hal_modem_result_code res;
    char buf[GPS_BUF_SIZE];
    uint32_t cap;

    CHECK_HAL_INITIALIZED();
    CHECK_MODULE_ALREADY_INITIALIZED( initialized );

    /* check from HAL on module type - HAL should already been initialized */
    err = hal_syscfg_get_capability(HAL_SYSCAP_GPS, &cap);
    if (err != HAL_E_OK)
    {
        /* printf("SCOM - HAL sysconfig get capability failed! %d", err); */
        cap = 0;
    }
    switch (cap)
    {
    /*Cinterion EES3 - */
    case SYSCFG_GPS_INTERNAL:
        /* printf("GPS  - GSM Modem is Cinterion EES3 - GPS is u-Bloxx\n"); */
        if ( (err == HAL_E_OK) || (err == HAL_E_ALREADY_INITIALIZED)  ) {
            hal_log(HAL_LOG_SEVERITY_INFO, MODULE_NAME "GPS does not requires special settings");
        }
        else {
            err = HAL_E_FAIL;
            hal_log(HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not initialize SCOM");
            initialized = FALSE;
        }
        break;
        /* Cinterion EHS8 - */
    case SYSCFG_GPS_EHS8:
        err = hal_modem_scom_init();
        if ( (err == HAL_E_OK) || (err == HAL_E_ALREADY_INITIALIZED)  )
        {
            if ( (get_scom_ref_counter() == 1) && (err == HAL_E_OK) ) {
                // set factory defaults
                hal_scom_transfer("AT&F", '\r', GPS_LONG_TIMEOUT, buf, GPS_BUF_SIZE, &res);
                if (res != HAL_MODEM_RES_OK || err != HAL_E_OK)
                {
                    err = HAL_E_FAIL;
                    hal_log(HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not switch to factory settings");
                    initialized = FALSE;
                    return err;
                }
                else
                {
                    err = HAL_E_OK;
                }
            }
            err = hal_scom_transfer("AT^SGPSC=\"Engine\",\"3\"", '\r', GPS_INIT_TIMEOUT, buf, GPS_BUF_SIZE, &res);
            if (res != HAL_MODEM_RES_OK && err != HAL_E_OK)
            {
                err = HAL_E_FAIL;
                hal_log(HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not turn on GPS");
                initialized = FALSE;
                return err;
            }
            else
            {
                err = HAL_E_OK;
            }
        }
        else
        {
            err = HAL_E_FAIL;
            hal_log(HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not initialize SCOM");
            initialized = FALSE;
        }
        err = hal_modem_scom_deinit();
        break;
    default:
        /* Unknown module - */
        hal_log(HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not initialize SCOM - Unknown module");
        err = HAL_E_FAIL;
        initialized = FALSE;
        break;
    }
    if ( (err == HAL_E_OK) || (err == HAL_E_ALREADY_INITIALIZED)  )
    {
        start = g_get_monotonic_time();
        do
        {
            ret = gps_open("localhost", DEFAULT_GPSD_PORT, &my_gps_data);
            if ( ret == 0 )
            {
                break;
            }
        } while ( curr - start > GPSD_START_TIMEOUT );

        if ( ret != 0 )
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Could not open GPS device: %s", gps_errstr( ret ) );
            return HAL_E_FAIL;
        }

        (void) gps_stream( &my_gps_data, WATCH_ENABLE, NULL );

        // Create worker thread
        ret = pthread_create( &worker_thread, NULL, &gps_thread, NULL );
        if ( ret != 0 )
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Could not open GPS device: %s", gps_errstr( ret ) );
            return HAL_E_FAIL;
        }
        initialized = TRUE;
    }
    return err;
}

hal_error hal_gps_deinit()
{
    hal_error err;

    CHECK_MODULE_INITIALIZED( initialized );

    exit_request = TRUE;
    pthread_join( worker_thread, NULL );

    (void) gps_stream( &my_gps_data, WATCH_DISABLE, NULL );
    gps_close( &my_gps_data );

    err = hal_modem_scom_deinit();

    if ( err != HAL_E_OK && err != HAL_E_NOT_INITIALIZED && err != HAL_E_NOFEATURE)
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "gps - SCOM deinitialization failed: %d", err );
    }
    else {
        hal_log( HAL_LOG_SEVERITY_INFO, MODULE_NAME "gps - SCOM sucessfully deinitialized: %d", err );
    }
    return err;
}

hal_error hal_gps_get_state( hal_gps_fix * const fix, uint8_t * const sat_used, uint8_t * const sat_vis )
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( fix );
    CHECK_POINTER_PARAMETER( sat_vis );
    CHECK_POINTER_PARAMETER( sat_used );

    *fix = info.mode;
    *sat_vis = info.sat_visible;
    *sat_used = info.sat_used;

    return HAL_E_OK;
}

hal_error hal_gps_get_location( double * const lat, double * const lon, double * const alt )
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( lat );
    CHECK_POINTER_PARAMETER( lon );

    if ( info.mode >= MODE_2D )
    {
        *lat = info.lat;
        *lon = info.lon;

        if ( info.mode >= MODE_3D )
        {
            SET_IF_NOT_NULL( alt, info.alt );
        }
        else
        {
            SET_IF_NOT_NULL( alt, NAN );
        }
    }
    else
    {
        return HAL_E_GPS_NO_FIX;
    }

    return HAL_E_OK;
}

hal_error hal_gps_get_speed( double * const ground, double * const climb )
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( ground );
    CHECK_POINTER_PARAMETER( climb );

    if ( info.mode >= MODE_2D )
    {
        *ground= info.speed_ground;

        if ( info.mode >= MODE_3D )
        {
            *climb = info.speed_climb;
        }
        else
        {
            *climb = NAN;
        }

    }
    else
    {
        return HAL_E_GPS_NO_FIX;
    }

    return HAL_E_OK;
}

hal_error hal_gps_get_utc_unix ( double * const time )
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( time );

    if ( info.mode >= MODE_NO_FIX )
    {
        *time = info.time;
    }
    else
    {
        return HAL_E_GPS_NO_FIX;
    }

    return HAL_E_OK;

}

hal_error hal_gps_get_utc_iso8601 ( uint8_t * const str, uint16_t size )
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( str );

    if ( info.mode >= MODE_NO_FIX )
    {
        unix_to_iso8601( info.time, str, size );
    }
    else
    {
        return HAL_E_GPS_NO_FIX;
    }

    return HAL_E_OK;
}

#else

hal_error hal_gps_init( ) { return HAL_E_UNSUPPORTED; }
hal_error hal_gps_deinit() { return HAL_E_UNSUPPORTED; }
hal_error hal_gps_get_state( hal_gps_fix * const fix, uint8_t * const sat_used, uint8_t * const sat_vis ) { return HAL_E_UNSUPPORTED; }
hal_error hal_gps_get_location( double * const lat, double * const lon, double * const alt ) { return HAL_E_UNSUPPORTED; }
hal_error hal_gps_get_speed( double * const ground, double * const climb ) { return HAL_E_UNSUPPORTED; }
hal_error hal_gps_get_utc_unix ( double * const time ) { return HAL_E_UNSUPPORTED; }
hal_error hal_gps_get_utc_iso8601 ( uint8_t * const str, uint16_t size ) { return HAL_E_UNSUPPORTED; }

#endif
