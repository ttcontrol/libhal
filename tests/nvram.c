#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>

#include <glib.h>

#include "libhal.h"

#define BUF_SIZE 1024

#define TEST_WRITE            0
#define TEST_READ             1

static struct option options[] =
{
    {"help", no_argument, 0, '?'},
    {"prot", required_argument, 0, 'p'},
    {"offset", required_argument, 0, 'o'},
    {"length", required_argument, 0, 'l'},
    {"data", required_argument, 0, 'd'},
    {"test", required_argument, 0, 't'},
    {0, 0, 0, 0}
};

void print_help()
{
    printf("HAL LEDs test\n" \
           "  -? --help        This message\n" \
           "  -o --offset=<n>  offset\n" \
           "  -l --length=<n>  bytes to read (ignored for -t 0)\n" \
           "  -p --prot=<n>    enable/disable write protection\n" \
           "  -d --data=<n>    data to write\n" \
           "  -t --test=<n>   Test program to run\n" \
           "                  0 ... Write data (DEFAULT)\n" \
           "                  1 ... Read data with length specified by \"l\"\n" \
           "\n");
}

void leave(int sig)
{
    if (sig != 0)
    {
        printf("SIGINT caught. Exiting.\n");
    }
    hal_deinit();
    exit(sig);
}

void test_write( uint32_t offset, char * data )
{
    uint32_t    bytes_written;
    hal_error   err;

    printf("Writing %u bytes with offset %u...\n", strlen(data), offset );
    err = hal_nvram_write( offset, strlen(data), data, &bytes_written );
    if ( err == HAL_E_OK )
    {
        printf("Successfully written %u bytes\n", bytes_written );
    }
    else if ( err == HAL_E_NVRAM_INCOMPLETE_WRITE )
    {
        printf("Only written %u bytes\n", bytes_written);
    }
    else
    {
        printf("Could not write data to NVRAM\n");
        exit(-1);
    }
}

void test_read( uint32_t offset, uint32_t length )
{
    char buf[BUF_SIZE];
    hal_error err;
    uint32_t bytes_read;

    printf("Reading %u bytes with offset %u...\n", length, offset );
    err = hal_nvram_read( offset, length, buf, &bytes_read );
    if ( err == HAL_E_OK )
    {
        printf("Successfully read %u bytes:\n%s\n", bytes_read, buf );
    }
    else if ( err == HAL_E_NVRAM_INCOMPLETE_READ )
    {
        printf("Only read %u bytes\n", bytes_read);
    }
    else
    {
        printf("Could not read data from NVRAM\n");
        exit(-1);
    }
}

int main(int argc, char **argv)
{
    hal_error err;
    int option_index = 0;
    int c = 0;
    int write_prot;
    int length;
    int offset;
    int test_program = 0;
    uint8_t val;
    uint32_t nvram_size;
    char buf[BUF_SIZE];

    uint32_t bytes_read;

    while (c != -1)
    {
        option_index = 0;
        c = getopt_long(argc, argv, "?d:o:l:p:d:t:", options,
                        &option_index);

        switch (c)
        {
            case '?':
                print_help();
                exit(0);
                break;
            case 'p':
                if (sscanf(optarg, "%p", &write_prot) == 0)
                {
                    printf("Missing: Write protection.");
                    exit(-1);
                }
                break;
            case 'o':
                if (sscanf(optarg, "%d", &offset) == 0)
                {
                    printf("Missing: Offset.");
                    exit(-1);
                }
                break;
            case 'l':
                if (sscanf(optarg, "%d", &length) == 0)
                {
                    printf("Missing: Length");
                    exit(-1);
                }
                break;
            case 'd':
                if (sscanf(optarg, "%s", buf) == 0)
                {
                    printf("Missing: data");
                    exit(-1);
                }
                break;
            case 't':
                if (sscanf(optarg, "%d", &test_program) == 0)
                {
                    printf("Missing: Test program missing.");
                    exit(-1);
                }
                break;
        }
    }

    signal(SIGINT, leave);

    err = hal_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize HAL (%d)\n", err);
        exit(-1);
    }

    err = hal_nvram_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize NVRAM module (%d)\n", err);
        exit(-1);
    }

    err = hal_nvram_get_size( &nvram_size );
    if ( err != HAL_E_OK )
    {
        printf("Could not get NVRAM size(%d)\n", err);
        exit(-1);
    }
    printf("NVRAM Size: %u Bytes\n", nvram_size );


    err = hal_nvram_set_writeprotection( ((write_prot == 0) ? FALSE : TRUE) );
    if ( err != HAL_E_OK )
    {
        printf("Could not set write protection (%d)\n", err);
        exit(-1);
    }

    switch (test_program)
    {
        case TEST_WRITE:
            test_write( offset, buf );
            break;
        case TEST_READ:
            test_read(  offset, length );
            break;
        default:
            printf("Undefined test program! Abort.");
            break;
    }



    leave(0);
}
