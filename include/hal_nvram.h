/**************************************************************************
 * Copyright (c) 2013 TTTControl. All rights reserved. Confidential proprietary
 * Schoenbrunnerstrasse 7, A-1040 Wien, Austria. office[at]ttcontrol.com
 **************************************************************************/
/**********************************************************************//**
 * \file hal_nvram.h
 *
 * \brief Hardware Abstraction Layer for Non-volatile RAM.
 *
 **************************************************************************/

#ifndef _HAL_NVRAM_H_
#define _HAL_NVRAM_H_

#ifdef __cplusplus
extern "C" {
#endif

/** \defgroup nvram NVRAM
 *  \brief Functions and datastructures for Non-Volatile Random Access Memory
 *  \{
 */

#include "hal_types.h"
#include <glib.h>
#include <stdint.h>
#include <stdbool.h>

/** \brief Initialize nvram module
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_FAIL               Could not initialize module
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 *
 *  \remarks
 *  This function must be called before any other function of this module is called.
 */
hal_error hal_nvram_init();

/** \brief Deinitialize nvram module.
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_NOT_INITIALIZED    Module was not initialized
 *
 *  \remarks
 *  The write-protection of the NVRAM gets activated automatically when
 *  #hal_nvram_deinit is called.
 */
hal_error hal_nvram_deinit();

/**
 * \brief Gets the size of the NVRAM in bytes
 *
 * \param[out] size Size in bytes:
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 *
 */
hal_error hal_nvram_get_size( uint32_t * const length);

/**
 * \brief Sets the write-protection of the NVRAM. By default,
 *        the write-protection is enabled.
 *
 * \param prot TRUE to enable the write-protection, FALSE to disable
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 */
hal_error hal_nvram_set_writeprotection( bool prot );


/**
 * \brief Write data to the NVRAM
 *
 * \param[in] offset           Written data will start at offset
 * \param[in] data             Pointer to data which shall be written
 * \param[in] size             Number of bytes to write
 * \param[out] bytes_written   Number of bytes which were actually written
 *
 * \return #hal_error
 * \retval #HAL_E_OK                      Everything fine
 * \retval #HAL_E_FAIL                    Requested write operation failed
 * \retval #HAL_E_NOT_INITIALIZED         Module not initialized
 * \retval #HAL_E_NULL_POINTER            A NULL pointer has been passed
 * \retval #HAL_E_INVALID_PARAMETER       A invalid parameter has been passed to the function
 * \retval #HAL_E_NVRAM_INCOMPLETE_WRITE
 *          Only part of the requested data was written. The number of bytes actually written
 *          is stored in parameter \c bytes_written.
 *
 * \remarks
 * If the write protection was not disabled before calling #hal_nvram_write, the function
 * will return with #HAL_E_FAIL
 *
 * \remarks
 * If the requested write exceeds the size of the NVRAM (i.e. offset + size > nvram_size), the function
 * returns #HAL_E_INVALID_PARAMETER.
 */
hal_error hal_nvram_write( uint32_t offset
                         , uint32_t length
                         , const uint8_t * const data
                         , uint32_t * const bytes_written );

/**
 * \brief Read data from the NVRAM
 *
 * \param[in] offset           Read will start with this offset
 * \param[out] data            Pointer to data where read data shall be stored
 * \param[in] size             Number of bytes to read
 * \param[out] bytes_read      Number of bytes which were actually read
 *
 * \return #hal_error
 * \retval #HAL_E_OK                      Everything fine
 * \retval #HAL_E_FAIL                    Requested write operation failed
 * \retval #HAL_E_NOT_INITIALIZED         Module not initialized
 * \retval #HAL_E_NULL_POINTER            A NULL pointer has been passed
 * \retval #HAL_E_NVRAM_INCOMPLETE_READ
 *          Only part of the requested data was read. The number of bytes actually read
 *          is stored in parameter \c bytes_read.
 *
 * \remarks
 * If the requested read exceeds the size of the NVRAM (i.e. offset + size > nvram_size), the function
 * returns #HAL_E_INVALID_PARAMETER.
 */
hal_error hal_nvram_read ( uint32_t offset
                         , uint32_t length
                         , uint8_t * const data
                         , uint32_t * const bytes_read );

/** \} */

#ifdef __cplusplus
}  /* End of the 'extern "C"' block */
#endif

#endif /* _HAL_NVRAM_H_ */
