/**************************************************************************
 * Copyright (c) 2013 TTTControl. All rights reserved. Confidential proprietary
 * Schoenbrunnerstrasse 7, A-1040 Wien, Austria. office[at]ttcontrol.com
 **************************************************************************/
/**********************************************************************//**
 * \file hal_device.h
 *
 * \brief Functions to retrieve various device information.
 *
 **************************************************************************/

#ifndef _HAL_DEVICE_H_
#define _HAL_DEVICE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "hal_types.h"
#include <glib.h>
#include <stdint.h>

/** \defgroup device Device
 *  \brief Functions and types for the device
 *  \{
 */

/** Size of IP address string */
#define HAL_DEVICE_IP_SIZE 16
/** Size of MAC address string */
#define HAL_DEVICE_MAC_SIZE 18

/** \brief Initialize device module
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_FAIL               Could not initialize module
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 *
 *  \remarks
 *  This function must be called before any other function of this module is called.
 */
hal_error hal_device_init();

/** \brief Deinitialize device module.
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 *
 */
hal_error hal_device_deinit();

/**
 * \brief Gets the total available and currently free RAM
 *
 * \param[out] capacity Size of RAM in Bytes
 * \param[out] free     Currently free RAM in Bytes
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Could not retrieve RAM status
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_device_get_ram_info( uint64_t * const capacity, uint64_t * const free );

/**
 * \brief Gets the total available and currently free flash memory
 *
 * \param[out] capacity Size of flash memory available to user in Bytes
 * \param[out] free     Currently free flash memory in Bytes
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Could not retrieve flash status
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_device_get_flash_info( uint64_t * const capacity, uint64_t * const free );

/**
 * \brief Gets the device serial number
 *
 * \param[out] sn       Device serial number as string
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Could not retrieve serial number
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 *
 * \caution
 * Do not free the returned string in \c sn
 */
hal_error hal_device_get_serial_number( uint8_t ** sn );

/**
 * \brief Gets the device software release version
 *
 * \param[out] version             Device software release version
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Could not retrieve release version
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_device_get_release_version( uint32_t * version );

/**
 * \brief Gets the device ID value.
 *
 * The device ID identifies device families
 * | ID | Device family |
 * | 0x201 | HY-eVision� 7.0 |
 * | 0x202 | HY-eVision� 10.4 |
 * | 0x204 | HY-eVision� 7.0 Touch |
 *
 * \param[out] id             Device software release version
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Could not retrieve device ID
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_device_get_device_id( uint16_t * id );

/**
 * \brief Gets information on the ethernet network interface
 *
 * \param[out] mac_addr             MAC address string. Must point to buffer of at
 *                                  least #HAL_DEVICE_MAC_SIZE bytes.
 * \param[out] ip_addr              IP address string. Must point to a buffer of
 *                                  at least #HAL_DEVICE_IP_SIZE bytes.
 * \param[out] netmask              Netmask string. Must point to a buffer of
 *                                  at least #HAL_DEVICE_IP_SIZE bytes.
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Could not retrieve information
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_device_get_eth_details( uint8_t * mac_addr, uint8_t * ip_addr,
    uint8_t * netmask );

/**
 * \brief Gets information on the WLAN network interface.
 *
 * \param[out] mac_addr             MAC address string. Must point to buffer of at
 *                                  least #HAL_DEVICE_MAC_SIZE bytes.
 * \param[out] ip_addr              IP address string. Must point to a buffer of
 *                                  at least #HAL_DEVICE_IP_SIZE bytes.
 * \param[out] netmask              Netmask string. Must point to a buffer of
 *                                  at least #HAL_DEVICE_IP_SIZE bytes.
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Could not retrieve information or non-wlan device
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_device_get_wlan_details( uint8_t * mac_addr, uint8_t * ip_addr,
    uint8_t * netmask );
/** \} */

#ifdef __cplusplus
}  /* End of the 'extern "C"' block */
#endif

#endif /* _HAL_DEVICE_H_ */
