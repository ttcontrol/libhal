/**************************************************************************
 * Copyright (c) 2013 TTTControl. All rights reserved. Confidential proprietary
 * Schoenbrunnerstrasse 7, A-1040 Wien, Austria. office[at]ttcontrol.com
 **************************************************************************/
/**********************************************************************//**
 * \file hal_display.h
 *
 * \brief Hardware Abstraction Layer for display control.
 *
 **************************************************************************/

#ifndef _HAL_DISPLAY_H_
#define _HAL_DISPLAY_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "hal_types.h"
#include <glib.h>
#include <stdint.h>

/** \defgroup display Display control
 *  \brief Functions and datastructures for display control
 *  \{
 */

/** Available states for the display backlight */
typedef enum
{
    HAL_DISP_BL_STATE_OFF, /**< Display backlight off */
    HAL_DISP_BL_STATE_ON,  /**< Display backlight on */
} hal_display_bl_state;

/** \brief Initialize display control module
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_FAIL               Could not initialize module
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 *
 *  \remarks
 *  This function must be called before any other function of this module is called.
 */
hal_error hal_display_init();

/** \internal
 */
hal_error hal_display_init_internal(uint8_t set_default);

/** \brief Deinitialize display control module.
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
hal_error hal_display_deinit();

/**
 * \brief Sets the state (on/off) of the display backlight
 *
 * \param state One of:
 *              - \c #HAL_DISP_BL_STATE_OFF: Switch the display backlight off
 *              - \c #HAL_DISP_BL_STATE_ON:  Switch the display backlight on
 *
 * \return #hal_error
 * \retval #HAL_E_OK                 Everything fine
 * \retval #HAL_E_FAIL               Could not set state
 * \retval #HAL_E_INVALID_PARAMETER  Invalid parameter passed to the function
 * \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
hal_error hal_display_set_bl_state( hal_display_bl_state const state );

/**
 * \brief Get the current state of the display backlight
 *
 * \param[out] state Current state
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Could not read display backlight state
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_display_get_bl_state( hal_display_bl_state * const state );

/**
 * \brief Sets the the brightness of the display backlight
 *
 * \param[in] value Backlight brightness ( 0 .. 255);\n
 *               maximum brightness: 255\n
 *               minimum brightness: 0
 *
 * \return #hal_error
 * \retval #HAL_E_OK                 Everything fine
 * \retval #HAL_E_FAIL               Could not set display backlight brightness
 * \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
hal_error hal_display_set_brightness( uint8_t value );

/**
 * \brief Get the current display backlight brightness
 *
 * \param[out] value Current display backlight brightness ( Range: 0..255)
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Could not read display backlight brightness
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_display_get_brightness( uint8_t * const value );

/** \} */

#ifdef __cplusplus
}  /* End of the 'extern "C"' block */
#endif

#endif /* _HAL_DISPLAY_H_ */
