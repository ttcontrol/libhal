/**************************************************************************
 * Copyright (c) 2015 TTTControl. All rights reserved. Confidential proprietary
 * Schoenbrunnerstrasse 7, A-1040 Wien, Austria. office[at]ttcontrol.com
 **************************************************************************/
/**********************************************************************//**
 * \file hal_wlan.h
 *
 *  \brief Functions and datastructures for WLAN support
 *
 **************************************************************************/

#ifndef HAL_WLAN_H
#define HAL_WLAN_H

#ifdef __cplusplus
extern "C" {
#endif

#include "hal_types.h"

/** \defgroup wlan WLAN support
 *  \brief Functions and types for WLAN integration
 *  \{
 * 
 */

/** States of a WLAN network */
typedef enum
{
    /** Network detected and unknown. A connection can be set up. */
    HAL_WLAN_STATE_IDLE,
    /** Error during connection to network. */
    HAL_WLAN_STATE_FAILURE,
    /** Associating to network. */
    HAL_WLAN_STATE_ASSOCIATION,
    /** Exchanging configuration data. */
    HAL_WLAN_STATE_CONFIGURATION,
    /** Connected to network. No connection to internet or check not possible. */
    HAL_WLAN_STATE_READY,
    /** Connected to network and live internet connection. */
    HAL_WLAN_STATE_ONLINE,
    /** Disconnecting from network. */
    HAL_WLAN_STATE_DISCONNECT,
    /** Unknown error */
    HAL_WLAN_STATE_0,
} hal_wlan_state;

/** Error states of a WLAN network */
typedef enum
{
    /** No error */
    HAL_WLAN_ERROR_OK,
    /** Network is out of range. */
    HAL_WLAN_ERROR_OUT_OF_RANGE,
    /** Unused */
    HAL_WLAN_ERROR_PIN_MISSING,
    /** DHCP setup of connection failed. */
    HAL_WLAN_ERROR_DHCP_FAILED,
    /** Connection failed. */
    HAL_WLAN_ERROR_CONNECT_FAILED,
    /** Login to network failed. */
    HAL_WLAN_ERROR_LOGIN_FAILED,
    /** Authentication with network failed. */
    HAL_WLAN_ERROR_AUTH_FAILED,
    /** Invalid passphrase. */
    HAL_WLAN_ERROR_INVALID_KEY,
    /** Unknown error */
    HAL_WLAN_ERROR_0,
} hal_wlan_error;

/** Network encryption types */
typedef enum
{
    /** No encryption */
    HAL_WLAN_SECURITY_NONE,
    /** Wire equivalent privacy */
    HAL_WLAN_SECURITY_WEP,
    /** Pre-shared key */
    HAL_WLAN_SECURITY_PSK,
    /** Not supported */
    HAL_WLAN_SECURITY_IEEE8021x,
    /** Not supported */
    HAL_WLAN_SECURITY_WPS,
    /** Unknown encryption */
    HAL_WLAN_SECURITY_0,
} hal_wlan_security;

/** Network configuration method */
typedef enum
{
    /** Network off */
    HAL_WLAN_CONFIG_OFF,
    /** Configured with DHCP */
    HAL_WLAN_CONFIG_DHCP,
    /** Configured statically */
    HAL_WLAN_CONFIG_STATIC,
    /** Fixed network configuration */
    HAL_WLAN_CONFIG_FIXED,
    /** Unknown configuration */
    HAL_WLAN_CONFIG_0,
} hal_wlan_config;

/** Length of SSID */
#define SSID_LEN 128
/** Length of DBus path */
#define PATH_LEN 255
/** Length of IP */
#define IP_LEN 16

/** This structure represents a WLAN network */
struct hal_wlan_network
{
    /** SSID (network name) */
    char ssid[SSID_LEN];
    /** Signal strength (0..100) */
    uint8_t strength;
    /** Connection state. */
    hal_wlan_state state;
    /** If TRUE a connection to the network is automatically set up if in
      * reach using the passphrase used for the last connection. */
    uint8_t auto_connect;
    /** Error code */
    hal_wlan_error error;
    /** Token used to identify the network internally. */
    char dbus_path[PATH_LEN];
};

/** This structure represents additional network infos */
struct hal_wlan_network_data
{
    /** Encryption used in network */
    hal_wlan_security security;
    /** Network configuration method */
    hal_wlan_config config_method;
    /** IP address */
    char ip[IP_LEN];
    /** Netmask */
    char netmask[IP_LEN];
    /** Gateway server */
    char gateway[IP_LEN];
};

/** \brief Initialize WLAN support module
 *
 *  Initializes WLAN support. #hal_dbus_init() must be called before calling
 *  this function.
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_NOT_INITIALIZED    HAL or DBUS module not initialized
 */
hal_error hal_wlan_init();

/** \brief Deinitialize WLAN support module
 *
 *  Deinitializes WLAN support. #hal_dbus_deinit() shall be called afterwards.
 */
void hal_wlan_deinit();

/** \brief Enable or disable WLAN module
 *
 *  If enable is set to TRUE the WLAN module is powered, scans for network, and
 *  connects to known networks if configured to do so.
 *  If enable is set to FALSE the WLAN module is not powered. No connections
 *  are possible.
 *  The module will remember the state and restore it after the next call to
 *  #hal_wlan_init.
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_FAIL               An error occured
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
hal_error hal_wlan_enable( uint8_t enable );

/** \brief Get available networks
 *
 *  Return a list of known or currently available networks. If there is no active
 *  connection the module will automatically scan for networks. Otherwise only
 *  the active connection is shown. Use #hal_wlan_scan in that case to check
 *  for available networks.
 *  If count is set to zero the function will assume an array size of one and
 *  will update the state of the network passed in (e.g. used for a call to
 *  #hal_wlan_connect).
 *  Do not call this function with an interval of more than 2 seconds.
 *
 *  \param[inout] networks   An array of available networks.
 *  \param[inout] count      Pass in number of entries available in array.
 *                           Returns number of networks detected.
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_FAIL               An error occured
 *  \retval #HAL_E_NULL_POINTER       A NULL pointer has been passed
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
hal_error hal_wlan_get_networks( struct hal_wlan_network * networks,
    uint8_t * count );

/** \brief Get available networks with configuration data
 *
 *  Return a list of known or currently available networks and configuration 
 *  data. If there is no active
 *  connection the module will automatically scan for networks. Otherwise only
 *  the active connection is shown. Use #hal_wlan_scan in that case to check
 *  for available networks.
 *  If count is set to zero the function will assume an array size of one and
 *  will update the state of the network passed in (e.g. used for a call to
 *  #hal_wlan_connect).
 *  Do not call this function with an interval of more than 2 seconds.
 *
 *  \param[inout] networks   An array of available networks.
 *  \param[inout] count      Pass in number of entries available in array.
 *                           Returns number of networks detected.
 *  \param[inout] data       An array receiving configuration data for each network.
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_FAIL               An error occured
 *  \retval #HAL_E_NULL_POINTER       A NULL pointer has been passed
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
hal_error hal_wlan_get_networks2( struct hal_wlan_network * networks,
    uint8_t * count, struct hal_wlan_network_data * data );

/** \brief Scan for networks
 *
 *  Scans for available networks. The list of networks detected can then
 *  be obtained using #hal_wlan_net_networks.
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_FAIL               An error occured
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
hal_error hal_wlan_scan( );
/** \brief Scan for networks
 * 
 *  Non-blocking version of the function.
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_FAIL               An error occured
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 *  \retval #HAL_E_PENDING            Operation currently in progress
 *  \retval #HAL_E_DBUS_NOREPLY       Scan did not return a result
 *  \retval #HAL_E_WLAN_NOCARRIER     No carrier
 */
hal_error hal_wlan_scan_async();

/** \brief Connect to network
 *
 *  Connect to a network returned by #hal_wlan_get_networks. If it is protected
 *  by a passphrase it must be supplied. If the network requires no passphrase
 *  an empty string must be passed.
 *  After a successfull connection the credentials will be stored. The next
 *  call to connect won't require a password (it can be set to an empty string)
 *  if it has not changed. If not desired, use #hal_wlan_set_auto_connect
 *  to disable the feature for this connection.
 *
 *  \param[in]  network               Network to connect to
 *  \param[in]  passphrase            Passphrase required to access network
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_FAIL               An error occured
 *  \retval #HAL_E_NULL_POINTER       A NULL pointer has been passed
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
hal_error hal_wlan_connect( struct hal_wlan_network * network,
    char * passphrase );

/** \brief Connect to network
 * 
 * 	Non-blocking version of the function.
 *  
 *	\param[in]  network               Network to connect to
 *  \param[in]  passphrase            Passphrase required to access network
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                       Everything fine
 *  \retval #HAL_E_FAIL                     An error occured
 *  \retval #HAL_E_NULL_POINTER             A NULL pointer has been passed
 *  \retval #HAL_E_NOT_INITIALIZED          Module not initialized
 *  \retval #HAL_E_PENDING                  Operation currently in progress
 *  \retval #HAL_E_DBUS_NOREPLY             Connection attempt timeout
 *  \retval #HAL_E_WLAN_PASSPHRASE          Wrong passphrase
 *  \retval #HAL_E_WLAN_ALREADY_CONNECTED   Already connected to network
 */
hal_error hal_wlan_connect_async( struct hal_wlan_network * network,
    char * passphrase );

/** \brief Disconnect from current network
 *
 *  Disconnect from currently connected network. The connection credentials
 *  will keep stored. To forget the network call #hal_wlan_remove.
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_FAIL               An error occured
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
hal_error hal_wlan_disconnect( );

/** \brief Forget network data
 *
 *  Forget stored network credentails. No automatic connection can be made upon
 *  calling #hal_wlan_connect and providing the passphrase again.
 *
 *  \param[in]  network               Network to remove
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_FAIL               An error occured
 *  \retval #HAL_E_NULL_POINTER       A NULL pointer has been passed
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
hal_error hal_wlan_remove( struct hal_wlan_network * network );

/** \brief Set automatic connection for network
 *
 *  If enable is TRUE connection will be automatically made if network gets
 *  in reach. If FALSE #hal_wlan_connect must be called explicitly.
 *
 *  \param[in]  network               Network to modify
 *  \param[in]  enable                Auconnect state
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_FAIL               An error occured
 *  \retval #HAL_E_NULL_POINTER       A NULL pointer has been passed
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
hal_error hal_wlan_set_autoconnect( struct hal_wlan_network * network,
    uint8_t enable );

/** \} */

#ifdef __cplusplus
}  /* End of the 'extern "C"' block */
#endif

#endif /* HAL_WLAN_H */
