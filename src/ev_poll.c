/*
 * dev_poll.c
 *
 *  Created on: 27.11.2013
 *      Author: lfauster
 */

#include <glib.h>
#include <poll.h>
#include <pthread.h>
#include <signal.h>
#include <errno.h>
#include <syslog.h>
#include <stdio.h>
#include <time.h>

#include "hal.h"
#include "ev_poll.h"
#include "hal_types.h"
#include "tools.h"

#define MODULE_NAME "[ev_poll] "
#define MAX_EVENTS 10

static struct pollfd fds[MAX_EVENTS];
static GThreadFunc cbs[MAX_EVENTS];
static gpointer user_data[MAX_EVENTS];
static int ev_count = 0;

static pthread_t worker_thread = 0;
static gboolean exit_request;

static gboolean initialized = FALSE;

static GMutex mutex;

static void worker_thread_continue()
{
    // Send SIGUSR1 to worker thread; this will "unblock" ppoll call
    pthread_kill( worker_thread, SIGUSR1 );
}

static void worker_thread_exit()
{
    // signal an request to exit.
    exit_request = TRUE;
    worker_thread_continue();
}

void sigusr1_handler(int sig)
{
    // Do nothing
}

gpointer ev_poll_thread( gpointer data )
{
    sigset_t sigmask;

    int ret;

    // Set up a sigset which blocks all signals except SIGUSR1
    sigfillset( &sigmask );
    sigdelset( &sigmask, SIGUSR1 );
    sigprocmask( SIG_BLOCK, &sigmask, NULL );

    while ( exit_request == FALSE )
    {
        // Call ppoll and mask all signals except SIGUSR1
        ret = ppoll(fds, ev_count, NULL, &sigmask );
        if ( ret > 0 )
        {
            for ( int i = 0; i < ev_count; i++ )
            {
                if ( fds[i].revents != 0 )
                {
                    (cbs[i])(user_data[i]);
                }
            }
        }
        else if ( errno == EINTR )
        {
            // Got a signal; continue with loop
            continue;
        }
    }
}

hal_error ev_poll_add( int fd
                     , short events
                     , GThreadFunc cb
                     , gpointer data )
{
    hal_error err;

    if ( ev_count < MAX_EVENTS )
    {
        g_mutex_lock( &mutex );
        fds[ev_count].fd = fd;
        fds[ev_count].events = events;
        cbs[ev_count] = cb;
        user_data[ev_count] = data;
        ev_count++;
        g_mutex_unlock( &mutex );
        worker_thread_continue();

        err = HAL_E_OK;
    }
    else
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Max events (%u) reached",
            MAX_EVENTS );
        err = HAL_E_FAIL;
    }

    return err;
}

void ev_poll_remove( int fd )
{
    gboolean found = FALSE;

    for ( int i = 0; i < ev_count; i++ )
    {
        if ( fds[i].fd == fd )
        {
            found = TRUE;
        }

        if ( found == TRUE && i < (ev_count - 1) )
        {
            fds[i] = fds[i+1];
            cbs[i] = cbs[i+1];
            user_data[i] = user_data[i+1];
        }
    }

    if ( found == TRUE )
    {
        fds[ev_count].fd = -1;
        cbs[ev_count] = NULL;
        user_data[ev_count] = NULL;
        ev_count--;

        worker_thread_continue();
    }
    else
    {
        hal_log( HAL_LOG_SEVERITY_WARNING, MODULE_NAME
            "File descriptor %d not found in poll-list", fd );
    }
}

hal_error ev_poll_init()
{
    int ret;
    struct sigaction sa;
    sigset_t sigmask;

    CHECK_MODULE_ALREADY_INITIALIZED( initialized );

    // Create worker thread
    ret = pthread_create( &worker_thread, NULL, &ev_poll_thread, NULL );
    if ( ret != 0 )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Could not open create event worker thread: %d", errno );
        return HAL_E_FAIL;
    }

    // Set up a sigset which blocks SIGUSR1; SIGUSR1 will be "captured" by worker_thread
    sigemptyset( &sigmask );
    sigaddset( &sigmask, SIGUSR1 );
    pthread_sigmask ( SIG_BLOCK, &sigmask, NULL );

    // Set signal handler for SIGUSR1
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    sa.sa_handler = sigusr1_handler;
    sigaction( SIGUSR1, &sa ,NULL );

    for ( int i = 0; i < MAX_EVENTS; i++ )
    {
        fds[i].fd = -1;
        cbs[i] = NULL;
    }

    initialized = TRUE;
    exit_request = FALSE;
    return HAL_E_OK;
}

hal_error ev_poll_deinit()
{
    int ret;
    hal_error err;
    struct timespec timeout;

    CHECK_MODULE_INITIALIZED( initialized );

    worker_thread_exit();

    ret = clock_gettime(CLOCK_REALTIME, &timeout);
    if (ret == -1)
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "clock_gettime failed (%d)!", errno);
    }
    else
    {
        // Wait up to 2 seconds for the worker thread to terminate.
        timeout.tv_sec += 2;
    }

    if (ret != -1)
    {
        // Wait for worker thread to terminate.
        hal_log( HAL_LOG_SEVERITY_DEBUG, MODULE_NAME "Waiting for worker thread to terminate...");
        ret = pthread_timedjoin_np(worker_thread, NULL, &timeout);
    }
    else
    {
        // Don't use the timeout as apparently it was not possible to get a timestamp.
        hal_log( HAL_LOG_SEVERITY_INFO, MODULE_NAME "Terminate worker thread without timeout...");
        ret = pthread_join(worker_thread, NULL);
    }

    if (ret == 0)
    {
        hal_log( HAL_LOG_SEVERITY_DEBUG, MODULE_NAME "Done!");
        err = HAL_E_OK;
    }
    else
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "pthread_join failed (%d)!", ret);
        err = HAL_E_FAIL;
    }

    initialized = FALSE;
    return err;
}

