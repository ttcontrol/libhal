/**************************************************************************
 * Copyright (c) 2013 TTTControl. All rights reserved. Confidential proprietary
 * Schoenbrunnerstrasse 7, A-1040 Wien, Austria. office[at]ttcontrol.com
 **************************************************************************/
/**********************************************************************//**
 * \file hal_udev.h
 *
 *  \brief Functions and datastructures for Udev support
 *
 *  To use these functions, \c libudev.h must be included and
 *  possibly \c libudev linked, as calling Udev functions might be
 *  necessary.
 *
 *  Subsystems listened to are block and usb.
 *
 **************************************************************************/

#ifndef HAL_UDEV_H
#define HAL_UDEV_H

#ifdef __cplusplus
extern "C" {
#endif

#include "hal_types.h"
#include "libudev.h"

/** \defgroup block Udev support
 *
 *  \{
 */

/** \brief Callback prototype for device actions
 *
 *  Called if a device is added or removed. Use
 *  #hal_udev_register_callback() to subscribe to event.
 *
 *  \param[in] dev Pointer to Udev device structure
 */
typedef void (*hal_device_event_func)(struct udev_device * const dev);

/** \brief Initialize Udev support module
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_NOT_INITIALIZED    HAL not initialized
 */
hal_error hal_udev_init();

/** \brief Deinitialize Udev support module
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
void hal_udev_deinit();

/** \brief Starts listening to events
 *
 *  Before calling this function, all listeners must be registered
 *  using #hal_udev_register_callback().
 * 
 *  \param[in] replay Also call callback for devices already present
 *                    to the system.
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
hal_error hal_udev_start(uint8_t const replay);

/** \brief Registers a callback if a device is added or removed
 *
 *  \param[in] device Pointer to the callback function
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                  Everything fine
 *  \retval #HAL_E_NOT_INITIALIZED     Module not initialized 
 */
hal_error hal_udev_register_callback(hal_device_event_func func);

/**
 * \brief Unregisters a callback if a new block device is added
 *
 * \param[in] device Pointer to the registered callback function
 *
 * \return #hal_error
 * \retval #HAL_E_OK                  Everything fine
 * \retval #HAL_E_NOT_INITIALIZED     Module not initialized 
 * \retval #HAL_E_FAIL                Could not unregister
 */
hal_error hal_udev_unregister_callback(hal_device_event_func func);

/** \} */

#ifdef __cplusplus
}  /* End of the 'extern "C"' block */
#endif

#endif /* HAL_UDEV_H */
