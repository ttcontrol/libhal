/*
 * cplusplus.cpp
 *
 *  Created on: 19.02.2014
 *      Author: lfauster
 */


using namespace std;

#include "libhal.h"
#include <iostream>

int main()
{
    cout << "Hello World" << endl;
    hal_init();
    hal_touch_init();

    return 0;
}

