/**************************************************************************
 * Copyright (c) 2013 TTTControl. All rights reserved. Confidential proprietary
 * Schoenbrunnerstrasse 7, A-1040 Wien, Austria. office[at]ttcontrol.com
 **************************************************************************/
/**********************************************************************//**
 * \file hal_dbus.h
 *
 *  \brief Functions and datastructures for Dbus support
 *
 **************************************************************************/

#ifndef HAL_DBUS_H
#define HAL_DBUS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <dbus/dbus.h>
#include "hal_types.h"

/** \defgroup block DBUS support
 *
 *  \{
 */

/** \brief Initialize DBUS support module
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_FAIL               Could not initialize module
 *  \retval #HAL_E_NOT_INITIALIZED    HAL not initialized
 */
hal_error hal_dbus_init();

/** \brief Deinitialize DBUS support module
 *
 */
void hal_dbus_deinit();

/** \brief Deinitialize DBUS support module
 *
 *  Get system bus connections. Needed as parameter for external modules using
 *  DBus features.
 *
 *  \param[out] conn                  Returns a pointer to the system bus connection
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_FAIL               Could not get system bus connection
 *  \retval #HAL_E_NULL_POINTER       A NULL pointer has been passed
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
hal_error hal_dbus_get_system_bus(DBusConnection ** conn);

/** \} */

#ifdef __cplusplus
}  /* End of the 'extern "C"' block */
#endif

#endif /* HAL_DBUS_H */
