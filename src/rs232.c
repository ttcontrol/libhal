#include <glib.h>

#include "hal.h"
#include "hal_types.h"
#include "tools.h"

#define DEV_RS232         "/dev/ttyS0"

hal_error hal_rs232_get_device( uint8_t const ** device )
{
    uint32_t cap = 0;

    // HAL must be initialized as otherwise "syscfg" module has not been initialized.
    CHECK_HAL_INITIALIZED();
    CHECK_POINTER_PARAMETER(device);

    hal_error err = HAL_E_OK;
    
    err = hal_syscfg_get_capability( HAL_SYSCAP_RS232, &cap );
    if (err == HAL_E_OK)
    {
        if (( cap == SYSCFG_RS232 ) || ( cap == SYSCFG_ALL_FEATURES ))
        {
            *device = DEV_RS232;
        }
        else
        {
            err = HAL_E_NOFEATURE;
        }
    }

    return err;
}
