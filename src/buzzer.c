/*
 * buzzer.c
 *
 *  Created on: 27.11.2013
 *      Author: lfauster
 */

#include "hal_buzzer.h"
#include "hal.h"
#include "hal_types.h"
#include "tools.h"

#include <linux/input.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <syslog.h>
#include <glib.h>

#define MODULE_NAME "[buzzer] "

#define DEV_BUZZER_INPUT    "/dev/input/buzzer"
#define DEV_BUZZER_VOLUME   "/var/links/buzzer-volume"

/* changed BUZZER_MAX_VOLUME to 63 and BUZZER_VOLUME_OFFSET to 64 as discussed with LFA */
#define BUZZER_MAX_VOLUME      63
#define BUZZER_VOLUME_OFFSET   64

static int fd_buzzer_input = -1;
static int fd_buzzer_vol = -1;
static hal_buzzer_state current_state;
static uint8_t current_vol = 0;

static gboolean cap_has_buzzer = FALSE;
static gboolean cap_can_change_volume = FALSE;

static gboolean initialized = FALSE;

static GMutex mutex;

static hal_error set_volume( uint8_t const volume )
{
    uint8_t scaled = volume;
    hal_error err;
    CHECK_MODULE_INITIALIZED( initialized );

    /* scale range of volume (0..255) to supported range (64..127) */
    scaled = (uint8_t)((uint16_t)volume * (uint16_t)BUZZER_MAX_VOLUME / 0xFF + BUZZER_VOLUME_OFFSET);

    g_mutex_lock( &mutex );
    err = file_set_value_int( fd_buzzer_vol, (int)scaled );
    if ( err == HAL_E_OK )
    {
        current_vol = volume;
    }
    g_mutex_unlock( &mutex );
    return err;
}

/* input parameter scale is not used */
hal_error hal_buzzer_init_internal( uint8_t scale )
{
    hal_error err;
    uint32_t cap;
    
    CHECK_HAL_INITIALIZED();
    CHECK_MODULE_ALREADY_INITIALIZED( initialized );
    
    err = hal_syscfg_get_capability( HAL_SYSCAP_BUZZER, &cap );
    if ( err != HAL_E_OK )
    {
        return HAL_E_FAIL;
    }

    cap_has_buzzer = (( cap == SYSCFG_BUZZER_RESONANCE ) || ( cap == SYSCFG_ALL_FEATURES ));

    if ( cap_has_buzzer )
    {
        fd_buzzer_input = open( DEV_BUZZER_INPUT, O_WRONLY );
        if ( fd_buzzer_input == -1 )
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not open buzzer input device node "
                DEV_BUZZER_INPUT ": %d", errno );
            return HAL_E_FAIL;
        }

        fd_buzzer_vol = open( DEV_BUZZER_VOLUME, O_WRONLY );
        if ( fd_buzzer_vol == -1 )
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not open buzzer volume device node "
                DEV_BUZZER_VOLUME ": %d", errno );
            return HAL_E_FAIL;
        }
        initialized = TRUE;
        /* Set initial buzzer state */
        current_state = HAL_BUZ_STATE_ON;
        hal_buzzer_set_state( HAL_BUZ_STATE_OFF );

        if ( cap_can_change_volume )
        {
            set_volume( 0 );
        }
        else
        {
            set_volume( 255 );
        }
        return HAL_E_OK;
    }
    else
    {
        initialized = TRUE;
        return HAL_E_NOFEATURE;
    }
}

hal_error hal_buzzer_init()
{
    return hal_buzzer_init_internal( TRUE );
}

hal_error hal_buzzer_deinit()
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE ( cap_has_buzzer );

    /* Switch off buzzer before deinit */
    hal_buzzer_set_state( HAL_BUZ_STATE_OFF );

    if ( fd_buzzer_input != -1 )
    {
        close( fd_buzzer_input );
        fd_buzzer_input = -1;
    }

    if ( fd_buzzer_vol != -1 )
    {
        close( fd_buzzer_vol);
        fd_buzzer_vol = -1;
    }

    initialized = FALSE;

    return HAL_E_OK;
}

hal_error hal_buzzer_set_compatibility( uint32_t flags )
{
    switch ( flags )
    {
        case HAL_COMPAT_BUZZER_VOLUME:
            cap_can_change_volume = TRUE;
            return HAL_E_OK;
        default:
            return HAL_E_INVALID_PARAMETER;
    }
}

hal_error hal_buzzer_set_state( hal_buzzer_state const state )
{
    struct input_event ev;
    int result;
    hal_error err;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_buzzer );
    
    if (current_state == state)
    {
        // State has not changed; do nothing and return OK;
        return HAL_E_OK;
    }

    switch ( state )
    {
        case HAL_BUZ_STATE_ON:
            ev.value = 100;
            break;
        case HAL_BUZ_STATE_OFF:
            ev.value = 0;
            break;
        default:
            return HAL_E_INVALID_PARAMETER;
    }

    ev.type = EV_SND;
    ev.code = SND_TONE;

    g_mutex_lock( &mutex );
    if ( write( fd_buzzer_input, &ev, sizeof(ev) ) == sizeof(ev) )
    {
        current_state = state;
        err = HAL_E_OK;
    }
    else
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not write to buzzer input device "
            DEV_BUZZER_INPUT ": %d", errno );
        err = HAL_E_FAIL;
    }

    g_mutex_unlock( &mutex );

    return err;
}

hal_error hal_buzzer_get_state( hal_buzzer_state * const state)
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_buzzer );
    CHECK_POINTER_PARAMETER( state );

    *state = current_state;

    return HAL_E_OK;
}

hal_error hal_buzzer_set_volume( uint8_t const volume )
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_can_change_volume );
    if (volume == current_vol)
    {
        // Volume has not changed; do nothing and return OK;
        return HAL_E_OK;
    }
    return set_volume( volume );
}

hal_error hal_buzzer_get_volume( uint8_t * const volume )
{
    hal_error err;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_can_change_volume );
    CHECK_POINTER_PARAMETER( volume );

    *volume = current_vol;

    return HAL_E_OK;
}
