#include "libhal.h"
#include "hal.h"
#include "tools.h"
#include "udev.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mount.h>
#include <sys/vfs.h>
#include <fcntl.h>
#include <syslog.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <libudev.h>
#include <blkid/blkid.h>
#include <uuid/uuid.h>


#define MODULE_NAME      "[block] "
#define MMC_DEVICE_NODE  "/dev/mmcblk"

typedef struct
{
    char * device_node;
    char * fs_type;
    char * mountpoint;
    char * uuid;
} hal_blockdevice;

static gboolean initialized = FALSE;
static GRecMutex mutex;
static blkid_cache cache = NULL;
static GHashTable * device_list = NULL;
static GPtrArray * device_add_callbacks = NULL;
static GPtrArray * device_remove_callbacks = NULL;
static gchar const * mmc_uuid = NULL;

static void do_device_add_callback(gpointer data, gpointer user_data)
{
    uint8_t * uuid = g_strdup(user_data);
    (*(hal_blockdevice_device_add_func)(data))(uuid);
    g_free(uuid);
}

static void do_device_remove_callback(gpointer data, gpointer user_data)
{
    uint8_t * uuid = g_strdup(user_data);
    (*(hal_blockdevice_device_remove_func)(data))(uuid);
    g_free(uuid);
}

static void delete_device(gpointer p)
{
    hal_blockdevice * d = (hal_blockdevice *)p;

    g_free(d->device_node);
    g_free(d->fs_type);
    g_free(d->mountpoint);
    g_free(d);
}

static void uninit_device(gpointer key, gpointer value, gpointer data)
{
    uint8_t * uuid = (uint8_t *) key;
    hal_blockdevice_eject(uuid);
}

static gboolean find_device_by_node(gpointer key, gpointer value, gpointer node)
{
    hal_blockdevice * d = (hal_blockdevice *)value;

    return strcmp(d->device_node, (char *)node) == 0;
}

static void blockdevice_udev_callback(struct udev_device * const dev)
{
    char const * subsystem = udev_device_get_subsystem(dev);
    char const * action = udev_device_get_action(dev);
    char const * device_node = udev_device_get_devnode(dev);

    if ((subsystem != NULL) && (strcmp(subsystem, "block") == 0))
    {
        char const * fs_type = blkid_get_tag_value(cache, "TYPE",
            device_node);
        char const * uuid = blkid_get_tag_value(cache, "UUID", device_node);
        int result;
        hal_blockdevice * block_device;

        if (fs_type != NULL)
        {
            if ((action == NULL) || (strcmp(action, "add") == 0))
            {
                if (uuid == NULL)
                {
                    uuid_t fake_uuid;

                    uuid_generate(fake_uuid);
                    uuid = g_new(char, 48);
                    uuid_unparse(fake_uuid, (char *)uuid);
                }
                else
                {
                    uuid = g_strdup(uuid);
                }

                block_device = g_malloc(sizeof(hal_blockdevice));
                block_device->device_node = g_strdup(device_node);
                block_device->fs_type = g_strdup(fs_type);
                block_device->mountpoint = NULL;
                block_device->uuid = (char *)uuid;
                g_hash_table_insert(device_list, (gpointer *)uuid, block_device);

                hal_log(HAL_LOG_SEVERITY_INFO, MODULE_NAME "Add device: %s",
                    block_device->device_node);

                if (strncmp(block_device->device_node, MMC_DEVICE_NODE,
                    sizeof(MMC_DEVICE_NODE) - 1) == 0)
                {
                    hal_log(HAL_LOG_SEVERITY_INFO, MODULE_NAME "MMC found",
                        block_device->device_node);
                    if (mmc_uuid == NULL)
                    {
                        mmc_uuid = uuid;
                    }
                    else
                    {
                        hal_log(HAL_LOG_SEVERITY_INFO, MODULE_NAME
                            "MMC found, but only one device supported",
                            block_device->device_node);
                    }
                }

                g_ptr_array_foreach(device_add_callbacks, &do_device_add_callback,
                    (gpointer)uuid);
            }
            else if (strcmp(action, "remove") == 0)
            {
                if (uuid == NULL)
                {
                    block_device = g_hash_table_find(device_list,
                        find_device_by_node, (gpointer *)device_node);
                    uuid = block_device->uuid;
                }
                else
                {
                    block_device = g_hash_table_lookup(device_list, uuid);
                }

                if (block_device != NULL)
                {
                    hal_log(HAL_LOG_SEVERITY_INFO, MODULE_NAME "Remove device: %s",
                        block_device->device_node);

                    g_ptr_array_foreach(device_remove_callbacks,
                        &do_device_remove_callback, (gpointer)uuid);

                    if (mmc_uuid != NULL && strcmp(mmc_uuid, uuid) == 0)
                    {
                        mmc_uuid = NULL;
                    }

                    hal_blockdevice_eject(uuid);
                    g_hash_table_remove(device_list, uuid);
                }
            }
        }
    }
}

hal_error hal_blockdevice_init()
{
    hal_error err = HAL_E_OK;

    CHECK_HAL_INITIALIZED();
    CHECK_FOREIGN_MODULE_INITIALIZED(udev);
    CHECK_MODULE_ALREADY_INITIALIZED(initialized);

    g_rec_mutex_init(&mutex);
    g_rec_mutex_lock(&mutex);

    blkid_get_cache(&cache, NULL);
    device_list = g_hash_table_new_full(g_str_hash, g_str_equal, g_free,
        delete_device);
    device_add_callbacks = g_ptr_array_new();
    device_remove_callbacks = g_ptr_array_new();
    err = hal_udev_register_callback(blockdevice_udev_callback);

    initialized = TRUE;
    g_rec_mutex_unlock(&mutex);
    return err;
}

hal_error hal_blockdevice_deinit()
{
    hal_error err = HAL_E_OK;

    CHECK_MODULE_INITIALIZED(initialized);

    g_rec_mutex_lock(&mutex);

    g_hash_table_foreach(device_list, uninit_device, NULL);

    blkid_put_cache(cache);
    cache = NULL;

    g_hash_table_destroy(device_list);
    g_ptr_array_free(device_add_callbacks, TRUE);
    g_ptr_array_free(device_remove_callbacks, TRUE);
    device_list = NULL;
    device_add_callbacks = NULL;
    device_remove_callbacks = NULL;

    err = hal_udev_unregister_callback(blockdevice_udev_callback);

    mmc_uuid = NULL;

    initialized = FALSE;
    g_rec_mutex_unlock(&mutex);
    return err;
}

gboolean hal_blockdevice_is_initialized()
{
    return initialized;
}

hal_error hal_blockdevice_register_callback_device_new(
    hal_blockdevice_device_add_func func)
{
    return hal_blockdevice_register_callback_device_add(func);
}

hal_error hal_blockdevice_register_callback_device_add(
    hal_blockdevice_device_add_func func)
{
    CHECK_MODULE_INITIALIZED(initialized);

    g_rec_mutex_lock(&mutex);
    g_ptr_array_add(device_add_callbacks, func);
    g_rec_mutex_unlock(&mutex);

    return HAL_E_OK;
}

hal_error hal_blockdevice_register_callback_device_remove(
    hal_blockdevice_device_remove_func func)
{
    CHECK_MODULE_INITIALIZED(initialized);

    g_rec_mutex_lock(&mutex);
    g_ptr_array_add(device_remove_callbacks, func);
    g_rec_mutex_unlock(&mutex);

    return HAL_E_OK;
}

hal_error hal_blockdevice_unregister_callback_device_new(
    hal_blockdevice_device_add_func func)
{
    return hal_blockdevice_unregister_callback_device_add(func);
}

hal_error hal_blockdevice_unregister_callback_device_add(
    hal_blockdevice_device_add_func func)
{
    hal_error err = HAL_E_OK;

    CHECK_MODULE_INITIALIZED(initialized);

    g_rec_mutex_lock(&mutex);
    err = (g_ptr_array_remove(device_add_callbacks, func))
        ? HAL_E_OK : HAL_E_FAIL;
    g_rec_mutex_unlock(&mutex);

    return err;
}

hal_error hal_blockdevice_unregister_callback_device_remove(
    hal_blockdevice_device_remove_func func)
{
    hal_error err = HAL_E_OK;

    CHECK_MODULE_INITIALIZED(initialized);

    g_rec_mutex_lock(&mutex);
    err = (g_ptr_array_remove(device_remove_callbacks, func))
        ? HAL_E_OK : HAL_E_FAIL;
    g_rec_mutex_unlock(&mutex);

    return err;
}

//--------------------------------------------------------------------------------------------------------------------------------------------------------------
/* mount functions for block devices 
*
*  call stack: 
*           
*           mount  -> mount_core
*           mount2 -> mount   
*           mount3 -> mount_core
* 
*/
hal_error hal_blockdevice_mount_core(uint8_t const * const uuid, uint8_t const ** mountpoint, unsigned long int mount_flags)
{
    hal_blockdevice * block_device = NULL;
    GRand * mountpoint_uid_generator;
    guint32 r_dir_uid;
    char buf[255];

    CHECK_MODULE_INITIALIZED(initialized);

    g_rec_mutex_lock(&mutex);
    if (uuid == NULL)
    {
        g_rec_mutex_unlock(&mutex);
        return HAL_E_INVALID_PARAMETER; 
    }

    block_device = g_hash_table_lookup(device_list, uuid);
    if (block_device == NULL)
    {
        g_rec_mutex_unlock(&mutex);
        return HAL_E_INVALID_PARAMETER;
    }
    
    if (block_device->mountpoint != NULL)
    {
        g_rec_mutex_unlock(&mutex);
        return HAL_E_DEVICE_ALREADY_MOUNTED;
    }


    mountpoint_uid_generator = g_rand_new();
    r_dir_uid = g_rand_int(mountpoint_uid_generator);
    snprintf(buf, 255, "/mnt/%s-%u", uuid, r_dir_uid);
    
    //snprintf(buf, 255, "/mnt/test%u", -G_MAXINT32); //test for clashing names1
    //snprintf(buf, 255, "/mnt/test"); //test for clashing names2
    
    while (g_file_test((const gchar*)buf, G_FILE_TEST_EXISTS) == TRUE)
    { 
        r_dir_uid = g_rand_int(mountpoint_uid_generator);
        snprintf(buf, 255, "/mnt/%s-%u", uuid, r_dir_uid);
    }

    if (mkdir(buf, 0777))
    {
        hal_log(HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not create mountpoint (%d)",
            errno);
        g_rec_mutex_unlock(&mutex);
        return HAL_E_FAIL;
    }
    if (mount(block_device->device_node, buf, block_device->fs_type, mount_flags, NULL)) { 
	hal_log(HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not mount device %s type %s (%d)", 
                block_device->device_node, block_device->fs_type, errno);
        g_rec_mutex_unlock(&mutex);
	return HAL_E_FAIL;
    }

    block_device->mountpoint = g_strdup(buf);
    *mountpoint = block_device->mountpoint;
    g_rec_mutex_unlock(&mutex);

    return HAL_E_OK;    
    
}

//--------------------------------------------------------------------------------------------------------------------------------------------------------------

hal_error hal_blockdevice_mount(uint8_t const * const uuid, uint8_t const ** mountpoint)
{
    hal_error err = HAL_E_OK;
    unsigned long int mount_flags = 0;
    if (mountpoint == NULL)
    {
        err = HAL_E_FAIL;
    }
    else
    {
        err = hal_blockdevice_mount_core(uuid, mountpoint, mount_flags);
    }
    
    return err;
}

//--------------------------------------------------------------------------------------------------------------------------------------------------------------

hal_error hal_blockdevice_mount2(uint8_t const * const uuid, uint8_t * const mountpoint, uint32_t * const mountpoint_size)
{
    hal_error err = HAL_E_OK;
    uint8_t const * mp = NULL;

    err = hal_blockdevice_mount(uuid, &mp);
    if (err == HAL_E_OK)
    {
        if (mountpoint != NULL && mountpoint_size != NULL)
        {
            *mountpoint_size = g_strlcpy(mountpoint, mp, *mountpoint_size);
        }
        else
        {
            err = HAL_E_FAIL; 
        }
    }

    return err;
}

//--------------------------------------------------------------------------------------------------------------------------------------------------------------

hal_error hal_blockdevice_mount3(uint8_t const * const uuid, uint8_t * const mountpoint, uint32_t * const mountpoint_size, unsigned long int mount_flags)
{
    hal_error err = HAL_E_OK;
    uint8_t const * mp = NULL;
    
    err = hal_blockdevice_mount_core(uuid, &mp, mount_flags); //mount_flags_test for testing
    if (err == HAL_E_OK)
    {
        if (mountpoint != NULL && mountpoint_size != NULL)
        {
            *mountpoint_size = g_strlcpy(mountpoint, mp, *mountpoint_size);
        }
        else
        {
            err = HAL_E_INVALID_PARAMETER; 
        }
    }

    return err;
}

// end of mount functions
//--------------------------------------------------------------------------------------------------------------------------------------------------------------

hal_error hal_blockdevice_eject(uint8_t const * const uuid)
{
    hal_error err = HAL_E_OK;
    hal_blockdevice * block_device = NULL;

    CHECK_MODULE_INITIALIZED(initialized);

    g_rec_mutex_lock(&mutex);

    block_device = g_hash_table_lookup(device_list, uuid);
    if (block_device == NULL)
    {
        g_rec_mutex_unlock(&mutex);
        return HAL_E_INVALID_PARAMETER;
    }

    if (block_device->mountpoint != NULL)
    {
        if (umount(block_device->mountpoint))
        {
            if (umount2(block_device->mountpoint, MNT_DETACH))
            {
                hal_log(HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not unmount mountpoint %s (%d)", block_device->mountpoint, errno);
                err = HAL_E_FAIL;
            }
        }
        
        if (rmdir(block_device->mountpoint))
        {
            hal_log(HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not delete mountpoint (%d)", errno);
            err = HAL_E_FAIL;
        }
    
        g_free(block_device->mountpoint);
        block_device->mountpoint = NULL;
    
    }

    g_rec_mutex_unlock(&mutex);
    return err;
}

hal_error hal_blockdevice_get_capacity(uint8_t const * const uuid,
    uint64_t * const capacity, uint64_t * const free)
{
    struct statfs s;
    hal_blockdevice * block_device = NULL;
    uint64_t a,b;

    CHECK_MODULE_INITIALIZED(initialized);
    CHECK_POINTER_PARAMETER(capacity);
    CHECK_POINTER_PARAMETER(free);

    g_rec_mutex_lock(&mutex);

    *capacity = 0;
    *free = 0;

    block_device = g_hash_table_lookup(device_list, uuid);
    if (block_device == NULL)
    {
        g_rec_mutex_unlock(&mutex);
        return HAL_E_INVALID_PARAMETER;
    }

    if (statfs(block_device->mountpoint, &s) == 0)
    {
        *capacity = (uint64_t)s.f_blocks * (uint64_t)s.f_bsize;
        *free = (uint64_t)s.f_bfree * (uint64_t)s.f_bsize;
    }
    else
    {
        hal_log(HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not get device capacity (%d)",
            errno);
        g_rec_mutex_unlock(&mutex);
        return HAL_E_FAIL;
    }

    g_rec_mutex_unlock(&mutex);
    return HAL_E_OK;
}

hal_error hal_blockdevice_query(uint8_t const * const uuid,
    uint8_t * const device_node, uint32_t * const device_node_size,
    uint8_t * const fs_type, uint32_t * const fs_type_size,
    uint8_t * const mountpoint, uint32_t * const mountpoint_size)
{
    hal_blockdevice * block_device = NULL;

    CHECK_MODULE_INITIALIZED(initialized);

    g_rec_mutex_lock(&mutex);

    block_device = g_hash_table_lookup(device_list, uuid);
    if (block_device == NULL)
    {
        g_rec_mutex_unlock(&mutex);
        return HAL_E_INVALID_PARAMETER;
    }

    if (device_node != NULL && device_node != NULL )
    {
        *device_node_size = g_strlcpy(device_node, block_device->device_node,
            *device_node_size);
    }

    if (fs_type != NULL && fs_type_size != NULL)
    {
        *fs_type_size = g_strlcpy(fs_type, block_device->fs_type,
            *fs_type_size);
    }

    if (mountpoint != NULL && mountpoint_size != NULL)
    {
            *mountpoint_size = g_strlcpy(mountpoint, block_device->mountpoint, *mountpoint_size);
    }

    g_rec_mutex_unlock(&mutex);
    return HAL_E_OK;
}

hal_error hal_blockdevice_get_mmc_uuid(uint8_t * const uuid,
    uint32_t * const uuid_size)
{
    CHECK_MODULE_INITIALIZED(initialized);
    CHECK_POINTER_PARAMETER(uuid);

    g_rec_mutex_lock(&mutex);

    if (mmc_uuid == NULL)
    {
        *uuid_size = 0;
        *uuid = '\0';
    }
    else
    {
        *uuid_size = g_strlcpy(uuid, mmc_uuid, *uuid_size);
    }

    g_rec_mutex_unlock(&mutex);
    return HAL_E_OK;
}
