#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>
#include <string.h>

#include <glib.h>

#include "libhal.h"

static struct option options[] =
{
    {"help", no_argument, 0, '?'},
    {0, 0, 0, 0}
};

void print_help()
{
    printf("HAL UDEV test\n" \
           "  -? --help       This message\n" \
           "\n");
}

void leave(int sig)
{
    if (sig != 0)
    {
        printf("SIGINT caught. Exiting.\n");
    }
    hal_blockdevice_deinit();
    hal_udev_deinit();
    hal_deinit();
    exit(sig);
}

void remove_device(uint8_t const * const uuid)
{
    printf("Device removed (%s)\n", uuid);
    hal_blockdevice_eject(uuid);
}


void new_device(uint8_t const * const uuid)
{
    uint8_t mountpoint[255]; //test mount2, mount3
    uint8_t const * mountpoint2[255]; //test mount
    //uint8_t const * const uuid2 = NULL; //uuid pointer test
    //uint8_t * mountpoint2 = NULL; //mountpoint pointer test - mount2, mount3
    //uint8_t const *mountpoint2[0] = NULL; //mountpoint pointer test - mount1
    unsigned long int const * mount_flags = NULL; //test mount3
    uint32_t mountpoint_size = 255;
    uint8_t fs_type[255];
    uint32_t fs_type_size = 255;
    uint8_t device_node[255];
    uint32_t device_node_size = 255;
    uint8_t mmc_uuid[255];
    uint32_t mmc_uuid_size = 255;
    uint64_t total_space;
    uint64_t free_space;

    printf("Device added (%s)\n", uuid);
    hal_blockdevice_query(uuid, device_node, &device_node_size, fs_type,
        &fs_type_size, NULL, NULL);
    printf("    Device node: %s\n    Filesystem:  %s\n", device_node, fs_type);
    hal_blockdevice_get_mmc_uuid(mmc_uuid, &mmc_uuid_size);
    if (strcmp(uuid, mmc_uuid) == 0)
    {
        printf("This is the SD card!\n");
    }
    
    if (hal_blockdevice_mount2(uuid, mountpoint, &mountpoint_size) == HAL_E_OK) // test mount2
    //if (hal_blockdevice_mount3(uuid, mountpoint, &mountpoint_size, mount_flags) == HAL_E_OK) // test mount3
    //if (hal_blockdevice_mount(uuid, mountpoint) == HAL_E_OK) // test mount
    {
        printf("Device mounted: %s\n", mountpoint); //test mount2, mount3
        //printf("Device mounted: %s\n", *mountpoint); //test mount
        
        hal_blockdevice_get_capacity(uuid, &total_space, &free_space);
        printf("Device total: %llu Bytes\nDevice free: %llu Bytes\n", total_space, free_space);
    }
    else
    {
        printf("Could not mount.\n");
    }
}


int main(int argc, char **argv)
{
    hal_error err;
    int option_index = 0;
    int c = 0;
    uint8_t val;
    int led0;
    int led1;

    while (c != -1)
    {
        option_index = 0;
        c = getopt_long(argc, argv, "?", options,
                        &option_index);

        switch (c)
        {
            case '?':
                print_help();
                exit(0);
                break;
        }
    }

    signal(SIGINT, leave);

    err = hal_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize HAL (%d)\n", err);
        exit(-1);
    }

    err = hal_udev_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize UDEV module (%d)\n", err);
        exit(-1);
    }

    err = hal_blockdevice_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize blockdevice module (%d)\n", err);
        exit(-1);
    }
    
    hal_blockdevice_register_callback_device_new(new_device);
    hal_blockdevice_register_callback_device_remove(remove_device);
    hal_udev_start(TRUE);
    
    while (1)
    {
        usleep(100*1000);
    }

    leave(0);
}
