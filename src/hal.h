/*
 * hal.h
 *
 *  Created on: 27.11.2013
 *      Author: lfauster
 */

#ifndef HAL_H_
#define HAL_H_

#include "hal_types.h"
#include <glib.h>

#define HAL_VAR_RUN_DIR "/var/run/hal"

gboolean hal_is_initialized();

#endif /* HAL_H_ */
