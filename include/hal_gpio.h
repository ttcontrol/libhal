/**************************************************************************
 * Copyright (c) 2013 TTTControl. All rights reserved. Confidential proprietary
 * Schoenbrunnerstrasse 7, A-1040 Wien, Austria. office[at]ttcontrol.com
 **************************************************************************/
/**********************************************************************//**
 * \file hal_gpio.h
 *
 * \brief Hardware Abstraction Layer for spare GPIOs.
 *
 * Please note, that GPIOs are not available on HY-eVision� 7.0 and
 * HY-eVision� 10.4 devices.
 *
 **************************************************************************/

#ifndef _HAL_GPIOS_H_
#define _HAL_GPIOS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "hal_types.h"
#include <glib.h>
#include <stdint.h>

/** \defgroup gpios GPIOs
 *  \brief Functions and datastructures for GPIOs
 *  \{
 */

/** Available GPIOs. Refer to the hardware manual to find out if the device 
  * has GPIOs and to which pin they are mapped.
  */
typedef enum
{
    HAL_GPIO_0,  /**< GPIO0 */
    HAL_GPIO_1,  /**< GPIO1 */
    HAL_GPIO_2,  /**< GPIO2 */
} hal_gpio;


/** Available states of a GPIO */
typedef enum
{
    HAL_GPIO_STATE_LOW,   /**< low-level */
    HAL_GPIO_STATE_HIGH,  /**< high-level */
} hal_gpio_state;

/** \brief Initialize GPIO module
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_FAIL               Could not initialize module
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 *
 *  \remarks
 *  This function must be called before any other function of this module is called.
 */
hal_error hal_gpio_init();

/** \brief Deinitialize GPIO module.
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 *
 *  \remarks
 *  All GPIOs are disabled automatically when #hal_gpio_deinit is called.
 */
hal_error hal_gpio_deinit();

/**
 * \brief Sets the state of a GPIO
 *
 * \param[in] gpio  specifies the GPIO of interest
 * \param[in] state One of:
 *              - \c #HAL_GPIO_STATE_LOW:  low-level
 *              - \c #HAL_GPIO_STATE_HIGH: high-level
 *
 * \return #hal_error
 * \retval #HAL_E_OK                 Everything fine
 * \retval #HAL_E_FAIL               Could not set state
 * \retval #HAL_E_INVALID_PARAMETER  Invalid parameter passed to the function
 * \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
hal_error hal_gpio_set_state( hal_gpio gpio, hal_gpio_state state );

/**
 * \brief Get the current state of a GPIO
 *
 * \param[in]  gpio  specifies the GPIO of interest
 * \param[out] state Returned state; one of :
 *              - \c #HAL_GPIO_STATE_LOW:  low-level
 *              - \c #HAL_GPIO_STATE_HIGH: high-level
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_gpio_get_state( hal_gpio gpio, hal_gpio_state * const state );

#ifdef __cplusplus
}  /* End of the 'extern "C"' block */
#endif

#endif /* _HAL_GPIOS_H_ */
