#include "libhal.h"
#include "tools.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <syslog.h>
#include <glib/gprintf.h>

hal_error file_set_value_int(int fd, int val)
{
    char buf[8];
    int result;
    
    result = snprintf(buf, 8, "%d", val);
    if ((result < 0) || (result >= 8))
    {
        return HAL_E_FAIL;
    }
    
    result = write(fd, buf, result);
    if (result == -1)
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, "Could not write to device node: %d",
            errno );
        return HAL_E_FAIL;
    }
    
    return HAL_E_OK;
}

hal_error file_set_value_str( int fd, const char * const str )
{
    int size;
    int result;

    size = strlen(str);
    result = write( fd, str, size );
    if (result == -1)
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, "Could not write to device node: %d",
            errno );
        return HAL_E_FAIL;
    }

    return HAL_E_OK;
}

hal_error file_get_value_int(int fd, int * const val)
{
    char buf[8];
    int result;
    
    result = pread(fd, buf, sizeof(buf), 0);

    if (result == - 1)
    {
        return HAL_E_FAIL;
    }

    result = sscanf(buf, "%u", val);

    if ( result != 1 )
    {
        return HAL_E_FAIL;
    }
    
    if (result == -1)
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, "Could not read from device node: %d",
            errno );
        return HAL_E_FAIL;
    }
    
    return HAL_E_OK;
}

gchar* ver3_to_str( gchar *str, hal_version3 ver )
{
    g_sprintf(str, "%"G_GUINT16_FORMAT".%"G_GUINT16_FORMAT".%"G_GUINT16_FORMAT, ver.major, ver.minor, ver.patch);
    return str;
}

static void close_handles()
{
    int i;
    
    for (i = getdtablesize(); i > 3; i--)
    {
        close(i);
    }
}

int fakesystem(const char *command)
{
  pid_t return_pid;
  pid_t child_pid;
  int child_status = -1;

  child_pid = vfork();

  if (child_pid == -1)
  {
    return -1;
  }
  else if (child_pid == 0)
  {
    close_handles();
    execl("/bin/sh", "sh", "-c", command, NULL);
    _exit(127);
  }
  else
  {
    return_pid = waitpid(child_pid, &child_status, 0);
    if (return_pid != child_pid)
    {
      return -1;
    }
    else
    {
      return child_status;
    }
  }
}
