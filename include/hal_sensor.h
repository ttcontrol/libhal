/**************************************************************************
 * Copyright (c) 2013 TTTControl. All rights reserved. Confidential proprietary
 * Schoenbrunnerstrasse 7, A-1040 Wien, Austria. office[at]ttcontrol.com
 **************************************************************************/
/**********************************************************************//**
 * \file hal_sensor.h
 *
 * \brief Hardware Abstraction Layer for sensors.
 *
 **************************************************************************/

#ifndef _HAL_SENSORS_H_
#define _HAL_SENSORS_H_

#ifdef __cplusplus
extern "C" {
#endif

/** \defgroup sensors Sensors
 *  \brief Functions and datastructures for various sensors
 *  \{
 */

#include "hal_types.h"
#include <glib.h>
#include <stdint.h>
#include <stdbool.h>

/** Available sensor types */
typedef enum
{
    HAL_SENS_TYPE_AMBIENT_LUX,      /**< Ambient light sensor */
    HAL_SENS_TYPE_AMBIENT_TEMP,     /**< Sensor for ambient temperature*/
    HAL_SENS_TYPE_CPU_TEMP,         /**< Sensor for CPU temperature */
    HAL_SENS_TYPE_PMIC_TEMP,        /**< Sensor for Power Manager temperature */
    HAL_SENS_TYPE_TERMINAL30_VOLT,  /**< Sensor for Terminal30 voltage */
    HAL_SENS_TYPE_RTCBATT_VOLT,     /**< Sensor for RTC voltage */
    HAL_SENS_TYPE_PMIC_VOLT,        /**< Sensor for Power Manager voltage */
    HAL_SENS_TYPE_PMIC_CURR,        /**< Sensor for Power Manager current */
} hal_sensor_type;


/** \brief Initialize sensors module
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_FAIL               Could not initialize module
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 *  \retval #HAL_E_SENSOR_INIT_FAILED At least one sensor could not be initialized.
 *
 *  \remarks
 *  This function must be called before any other function of this module is called.
 *
 *  \remarks
 *  If #HAL_E_SENSOR_INIT_FAILED is returned, the module was initialized nonetheless
 *  and working sensors can be used. Calling #hal_sensor_get_value on the erroneous
 *  sensor will return #HAL_E_SENSOR_ERROR.
 */
hal_error hal_sensor_init();

/** \brief Deinitialize sensors module.
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
hal_error hal_sensor_deinit();

/**
 * \brief Read a sensor value
 *
 * \param[in]  type   Specifies which sensors shall be read.
 * \param[out] value  Returned value; physical unit and scaling depends on sensor type:
 *                   - \c #HAL_SENS_TYPE_AMBIENT_LUX:      [lux]
 *                   - \c #HAL_SENS_TYPE_AMBIENT_TEMP:     [°mC]
 *                   - \c #HAL_SENS_TYPE_CPU_TEMP:         [°mC]
 *                   - \c #HAL_SENS_TYPE_PMIC_TEMP:        [°mC]
 *                   - \c #HAL_SENS_TYPE_TERMINAL30_VOLT:  [mV]
 *                   - \c #HAL_SENS_TYPE_RTCBATT_VOLT:     [mV]
 *                   - \c #HAL_SENS_TYPE_PMIC_VOLT:        [mV]
 *                   - \c #HAL_SENS_TYPE_PMIC_CURR:        [mA]
 * \param[out] fresh Specifies the returned value is fresh, i.e. a new value was available.
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 * \retval #HAL_E_SENSOR_ERROR     Requested sensor was not correctly initialized
 *
 * \remarks
 * Sensor types #HAL_SENS_TYPE_AMBIENT_TEMP, #HAL_SENS_TYPE_CPU_TEMP, #HAL_SENS_TYPE_PMIC_TEMP,
 * #HAL_SENS_TYPE_RTCBATT_VOLT, #HAL_SENS_TYPE_PMIC_VOLT, and #HAL_SENS_TYPE_PMIC_CURR always
 * return a fresh value.\n
 * Sensor type #HAL_SENS_TYPE_AMBIENT_LUX only returns a fresh value after every second
 * call of the function. The first call of #hal_sensor_get_value triggers the reading of
 * the ambient light sensor and the second call of #hal_sensor_get_value returns a new value.
 *
 * <p>#HAL_SENS_TYPE_PMIC_VOLT and #HAL_SENS_TYPE_PMIC_CURR are only provided as for
 * legacy appliations; do not use those values, they provide no meaningful data.
 *
 */
hal_error hal_sensor_get_value( hal_sensor_type type, int32_t * const value, bool * const fresh );

/** \} */

#ifdef __cplusplus
}  /* End of the 'extern "C"' block */
#endif

#endif /* _HAL_SENSORS_H_ */
