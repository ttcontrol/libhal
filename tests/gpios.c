#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>

#include <glib.h>

#include "libhal.h"

#define BYTETOBINARYPATTERN "%d%d%d%d%d%d%d%d"
#define BYTETOBINARY(byte)  \
  (byte & 0x80 ? 1 : 0), \
  (byte & 0x40 ? 1 : 0), \
  (byte & 0x20 ? 1 : 0), \
  (byte & 0x10 ? 1 : 0), \
  (byte & 0x08 ? 1 : 0), \
  (byte & 0x04 ? 1 : 0), \
  (byte & 0x02 ? 1 : 0), \
  (byte & 0x01 ? 1 : 0)

static struct option options[] =
{
    {"help", no_argument, 0, '?'},
    {"set", required_argument, 0, '0'},
    {0, 0, 0, 0}
};

void print_help()
{
    printf("HAL LEDs test\n" \
           "  -? --help       This message\n" \
           "  -s --set=<n>    switch LED0 off/on\n" \
           "\n");
}

void leave(int sig)
{
    if (sig != 0)
    {
        printf("SIGINT caught. Exiting.\n");
    }
    hal_gpio_deinit();
    hal_deinit();
    exit(sig);
}



int main(int argc, char **argv)
{
    hal_error err;
    int option_index = 0;
    int c = 0;
    uint8_t val;
    int tmp;

    while (c != -1)
    {
        option_index = 0;
        c = getopt_long(argc, argv, "?s:", options,
                        &option_index);

        switch (c)
        {
            case 's':
                if (sscanf(optarg, "%d", &tmp) == 0)
                {
                    printf("Missing: Set state.");
                    exit(-1);
                }
                else
                {
                    val = (uint8_t)tmp;
                }
                break;
            case '?':
                print_help();
                exit(0);
                break;
                break;
        }
    }

    signal(SIGINT, leave);

    err = hal_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize HAL (%d)\n", err);
        exit(-1);
    }

    err = hal_gpio_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize GPIO module (%d)\n", err);
        exit(-1);
    }

    printf("Setting value to 0b"BYTETOBINARYPATTERN"\n", BYTETOBINARY(val));
    for ( int i = 0; i < 3; i++ )
    {
        if ( (val & (uint8_t)(1 << i)) > 0 )
        {
            hal_gpio_set_state( i, HAL_GPIO_STATE_HIGH );
        }
        else
        {
            hal_gpio_set_state( i, HAL_GPIO_STATE_LOW );
        }
    }

    printf("Reading GPIO state...\n");
    while ( 1 )
    {
        for ( int i = 0; i < 3; i++ )
        {
            hal_gpio_state state;
            err = hal_gpio_get_state( i, &state );

            if ( err != HAL_E_OK )
            {
                printf("Error reading GPIO%u state\n", i);
            }
            else
            {
                printf("GPIO%u state: %u\n", i, state );
            }
        }
        g_usleep( G_USEC_PER_SEC / 2 );
    }

    leave(0);
}
