#ifndef _TOOLS_H_
#define _TOOLS_H_

#include "libhal.h"
#include <glib.h>

typedef enum
{
    HAL_ASYNC_STATE_IDLE,
    HAL_ASYNC_STATE_PENDING,
    HAL_ASYNC_STATE_FINISHED
} hal_async_state;

struct hal_async_result
{
    hal_async_state state;
    hal_error result;
};

typedef struct
{
    gboolean pressed;
    uint16_t count;
} key_state_t;

#define DEBUG_PRINT_LINE \
    printf("%s.%u\n", __FILE__, __LINE__ )

#define CHECK_POINTER_PARAMETER(p) \
    do { \
        if (p == NULL) { return HAL_E_NULL_POINTER; } \
    } while (0)

#define CHECK_HAL_INITIALIZED() \
    do { \
        if ( hal_is_initialized() == FALSE ) { return HAL_E_NOT_INITIALIZED; } \
    } while (0)

#define CHECK_MODULE_ALREADY_INITIALIZED(init) \
    do { \
        if ( init == TRUE ) { return HAL_E_ALREADY_INITIALIZED; } \
    } while (0)

#define CHECK_MODULE_INITIALIZED(init) \
    do { \
        if ( init == FALSE ) { return HAL_E_NOT_INITIALIZED; } \
    } while (0)

#define CHECK_MODULE_INITIALIZED_RETURN_VOID(init) \
    do { \
        if ( init == FALSE ) { return; } \
    } while (0)


#define CHECK_FOREIGN_MODULE_INITIALIZED(module) \
    do { \
        if (!hal_##module##_is_initialized()) { return HAL_E_NOT_INITIALIZED; } \
    } while (0)

#define SET_IF_NOT_NULL(dest, val) \
    do { \
        if ( (dest) != NULL ) { *(dest) = (val); } \
    } while (0)


hal_error file_set_value_int(int fd, int val);
hal_error file_set_value_str( int fd, const char * const str );
hal_error file_get_value_int(int fd, int * const val);
gchar* ver3_to_str( gchar *str, hal_version3 ver );
int fakesystem(const char *command);

#endif
