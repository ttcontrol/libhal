#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>

#include <glib.h>

#include "libhal.h"

#define TEST_KEYBOARD_POLL          0

static struct option options[] =
{
    {"help", no_argument, 0, '?'},
    {"back", required_argument, 0, 'b'},
    {"test", required_argument, 0, 't'},
    {0, 0, 0, 0}
};

void print_help()
{
    printf("HAL keyboard test\n" \
           "  -? --help       This message\n" \
           "  -b --back=<n>   set backlight off/on\n" \
           "  -t --test=<n>   Test program to run\n" \
           "                  0 ... Poll key pressed\n" \

           "\n");
}

void leave(int sig)
{
    if (sig != 0)
    {
        printf("SIGINT caught. Exiting.\n");
    }
    hal_keyboard_deinit();
    hal_deinit();
    exit(sig);
}

static void test_keyboard_poll()
{
    while( TRUE )
    {
        bool pressed;
        uint16_t count;
        for ( int i = 0; i <= HAL_KEYB_KEY_F10; i++ )
        {
            hal_keyboard_get_key_state( i, &pressed, &count );
            printf("Key%u: pressed: %u count: %u\n", i, pressed, count );
        }
        printf("\n");
        g_usleep( 1 * G_USEC_PER_SEC );
    }

}

int main(int argc, char **argv)
{
    hal_error err;
    int option_index = 0;
    int c = 0;
    int bl;
    int test_program = 0;
    hal_keyboard_bl_state state;
    uint8_t const * keyboard_device;

    while (c != -1)
    {
        option_index = 0;
        c = getopt_long(argc, argv, "?b:t:", options,
                        &option_index);

        switch (c)
        {
            case '?':
                print_help();
                exit(0);
                break;
            case 't':
                if (sscanf(optarg, "%d", &test_program) == 0)
                {
                    printf("Missing: Test program missing.");
                    exit(-1);
                }
                break;
            case 'b':
                if (sscanf(optarg, "%d", &bl) == 0)
                {
                    printf("Missing: enable backlight.");
                    exit(-1);
                }
                break;
        }
    }

    signal(SIGINT, leave);
    
    err = hal_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize HAL (%d)\n", err);
        exit(-1);
    }

    err = hal_keyboard_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize keyboard module (%d)\n", err);
        exit(-1);
    }

    err = hal_keyboard_get_device(&keyboard_device);
    if ( err != HAL_E_OK )
    {
        printf("Could not get keyboard device (%d)\n", err);
        exit(-1);
    }

    printf("Using keyboard: %s\n", keyboard_device);

    err = hal_keyboard_set_bl_state( bl );
    if ( err != HAL_E_OK )
    {
        printf("Could not set keyboard backlight state (%d)\n", err);
        exit(-1);
    }

    err = hal_keyboard_get_bl_state( &state );
    if ( err != HAL_E_OK )
    {
        printf("Could not get keyboard backlight state (%d)\n", err);
        exit(-1);
    }
    printf( "Keyboard backlight state: %u\n", state );

    switch (test_program)
    {
        case TEST_KEYBOARD_POLL:
            test_keyboard_poll();
            break;
        default:
            printf("Undefined test program! Abort.");
            break;
    }

    leave(0);
}
