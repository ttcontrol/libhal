#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>

#include <glib.h>

#include "libhal.h"


static struct option options[] =
{
    {"help", no_argument, 0, '?'},
    {"on", required_argument, 0, 'o'},
    {"bright", required_argument, 0, 'b'},
    {0, 0, 0, 0}
};

#define TEST_BUZZER_OFF            0
#define TEST_BUZZER_ON             1


void print_help()
{
    printf("HAL display test\n" \
           "  -? --help       This message\n" \
           "  -o --on=<n>     switch backlight off/on\n" \
           "  -b --bright=<n> display backlight brightness (0..255)\n" \
           "\n");
}

void leave(int sig)
{
    if (sig != 0)
    {
        printf("SIGINT caught. Exiting.\n");
    }
    hal_power_deinit();
    hal_deinit();
    exit(sig);
}

static void test_buzzer_on()
{
    hal_buzzer_set_state( HAL_BUZ_STATE_ON );
}

static void test_buzzer_off()
{
    hal_buzzer_set_state( HAL_BUZ_STATE_OFF );
}

int main(int argc, char **argv)
{
    hal_error err;
    int option_index = 0;
    int c = 0;
    int test_program = 0;
    int bl;
    int bright;
    uint8_t bright_read;
    hal_display_bl_state state;

    while (c != -1)
    {
        option_index = 0;
        c = getopt_long(argc, argv, "?b:o:", options,
                        &option_index);

        switch (c)
        {
            case '?':
                print_help();
                exit(0);
                break;
            case 'o':
                if (sscanf(optarg, "%d", &bl) == 0)
                {
                    printf("Missing: on/off.");
                    exit(-1);
                }
                break;
            case 'b':
                if (sscanf(optarg, "%d", &bright) == 0)
                {
                    printf("Missing: display brightness.");
                    exit(-1);
                }
                break;
            case 't':
                if (sscanf(optarg, "%d", &test_program) == 0)
                {
                    printf("Missing: Test program missing.");
                    exit(-1);
                }
                break;
        }
    }

    signal(SIGINT, leave);

    err = hal_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize HAL (%d)\n", err);
        exit(-1);
    }

    err = hal_display_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize display module (%d)\n", err);
        exit(-1);
    }

    err = hal_display_set_bl_state( bl );
    if ( err != HAL_E_OK )
    {
        printf("Could not set display backlight state (%d)\n", err);
        exit(-1);
    }

    err = hal_display_set_brightness( bright );
    if ( err != HAL_E_OK )
    {
        printf("Could not set display backlight brightness (%d)\n", err);
        exit(-1);
    }

    err = hal_display_get_brightness( &bright_read );
    if ( err != HAL_E_OK )
    {
        printf("Could not get display backlight brightness (%d)\n", err);
        exit(-1);
    }

    err = hal_display_get_bl_state( &state );
    if ( err != HAL_E_OK )
    {
        printf("Could not get display state (%d)\n", err);
        exit(-1);
    }

    printf( "Read display brightness: %u\n", bright_read );
    printf( "Read display state: %u\n", state );

    leave(0);
}
