#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>

#include <glib.h>

#include "libhal.h"


static struct option options[] =
{
    {"help", no_argument, 0, '?'},
    {"ignore-syscfg", no_argument, 0, 'S'},
    {"volume", required_argument, 0, 'v'},
    {"test", required_argument, 0, 't'},
    {0, 0, 0, 0}
};

#define TEST_BUZZER_OFF            0
#define TEST_BUZZER_ON             1

void print_help()
{
    printf("HAL buzzer test\n" \
           "  -? --help           This message\n" \
           "  -S --ignore-syscfg  Ignore system config (act as test device)\n" \
           "  -v --volume=<n>     Set volume [0..255] \n" \
           "  -c --compat         Enable compatibility mode" \
           "\n");
}

void leave(int sig)
{
    if (sig != 0)
    {
        printf("SIGINT caught. Exiting.\n");
    }
//  Don't deinit buzzer as buzzer is turned off automatically
//    hal_buzzer_deinit();
    hal_deinit();
    exit(sig);
}

int main(int argc, char **argv)
{
    hal_error err;
    int option_index = 0;
    int c = 0;
    int test_program = 0;
    int vol;
    bool ignore_syscfg = FALSE;
    bool use_compat = FALSE;

    while (c != -1)
    {
        option_index = 0;
        c = getopt_long(argc, argv, "?St:v:c", options,
                        &option_index);

        switch (c)
        {
            case '?':
                print_help();
                exit(0);
                break;
            case 'S':
                ignore_syscfg = TRUE;
                break;
            case 'c':
                use_compat = TRUE;
                break;
            case 'v':
                if (sscanf(optarg, "%d", &vol) == 0)
                {
                    printf("Missing: Volume.");
                    exit(-1);
                }
                break;
            case 't':
                if (sscanf(optarg, "%d", &test_program) == 0)
                {
                    printf("Missing: Test program missing.");
                    exit(-1);
                }
                break;
        }
    }

    signal( SIGINT, leave );

    err = hal_init_internal( ignore_syscfg );
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize HAL (%d)\n", err);
        exit(-1);
    }

    err = hal_buzzer_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize buzzer module (%d)\n", err);
        exit(-1);
    }
    
    if ( use_compat )
    {
        hal_buzzer_set_compatibility( HAL_COMPAT_BUZZER_VOLUME );
    }

    hal_buzzer_set_volume( vol );
    hal_buzzer_set_state( HAL_BUZ_STATE_ON );
    sleep(3);

    leave(0);
}
