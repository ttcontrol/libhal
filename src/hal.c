#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <syslog.h>

#include "config.h"
// all the include files for the modules are in the libhal.h
#include "libhal.h"
#include "hal.h"
#include "tools.h"
#include "ev_poll.h"
#include "scom.h"

#define MODULE_NAME "[hal] "

static gboolean initialized = FALSE;

static hal_log_func logger = hal_log_default_func;

hal_error hal_init()
{
    return hal_init_internal( FALSE );
}

hal_error hal_init_internal( bool const ignore_config )
{
    hal_error err;
    gchar ver_str[14];

    CHECK_MODULE_ALREADY_INITIALIZED( initialized );

    openlog( "hal", 0, LOG_USER );
    
    mkdir( HAL_VAR_RUN_DIR, 01777 );

    err = hal_syscfg_init_internal( ignore_config );

    if (err == HAL_E_OK)
    {
        err = ev_poll_init();
        if (err == HAL_E_OK)
        {
            hal_log( HAL_LOG_SEVERITY_INFO, MODULE_NAME "libhal %s initialized", VERSION );
            initialized = TRUE;
        }
        else
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not initialize evpoll");
        }
    }
    else
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not initialize libhal %s", VERSION);
    }

    return err;
}

gboolean hal_is_initialized()
{
    return initialized;
}

hal_error hal_deinit()
{
    hal_error err, err_store;
    err_store = HAL_E_OK;
     //deinitialize all other modules one by one in order to avoid dependency issues	

    //blockdevice_deinit
    err = hal_blockdevice_deinit();
    if ( err != HAL_E_OK && err != HAL_E_NOT_INITIALIZED && err != HAL_E_NOFEATURE)
    {
    err_store = err;
    }

    //device_deinit
    err = hal_device_deinit(); 
    if ( err != HAL_E_OK && err != HAL_E_NOT_INITIALIZED && err != HAL_E_NOFEATURE)
    {
    err_store = err;
    }

    //display_deinit
    err = hal_display_deinit(); 
    if ( err != HAL_E_OK && err != HAL_E_NOT_INITIALIZED && err != HAL_E_NOFEATURE)
    {
    err_store = err;
    }

     //encoder_deinit
    err = hal_encoder_deinit(); 
    if ( err != HAL_E_OK && err != HAL_E_NOT_INITIALIZED && err != HAL_E_NOFEATURE)
    {
    err_store = err;
    }

    //gpio_deinit
    err = hal_gpio_deinit(); 
    if ( err != HAL_E_OK && err != HAL_E_NOT_INITIALIZED && err != HAL_E_NOFEATURE)
    {
    err_store = err;
    }

    //gps_deinit
    err = hal_gps_deinit(); 
    if ( err != HAL_E_OK && err != HAL_E_NOT_INITIALIZED && err != HAL_E_NOFEATURE)
    {
    err_store = err;
    }

    //buzzer_deinit
    err = hal_buzzer_deinit(); 
    if ( err != HAL_E_OK && err != HAL_E_NOT_INITIALIZED && err != HAL_E_NOFEATURE)
    {
    err_store = err;
    }
    
    //can_deinit
    err = hal_can_deinit(); 
    if ( err != HAL_E_OK && err != HAL_E_NOT_INITIALIZED && err != HAL_E_NOFEATURE)
    {
    err_store = err;
    }
    
    //dbus_deinit the function is void so no return and no check on fail, should not be able to fail
    (void)hal_dbus_deinit(); 


    //camera_deinit the function is void so no return and no check on fail, should not be able to fail
    (void)hal_camera_deinit();

    
    //keyboard_deinit
    err = hal_keyboard_deinit(); 
    if ( err != HAL_E_OK && err != HAL_E_NOT_INITIALIZED && err != HAL_E_NOFEATURE)
    {
    err_store = err;
    } 
    
    //led_deinit
    err = hal_led_deinit(); 
    if ( err != HAL_E_OK && err != HAL_E_NOT_INITIALIZED && err != HAL_E_NOFEATURE)
    {
    err_store = err;
    } 
    
    //modem_deinit -here 
    err = hal_modem_deinit(); 
    if ( err != HAL_E_OK && err != HAL_E_NOT_INITIALIZED && err != HAL_E_NOFEATURE)
    {
    err_store = err;
    } 
    
    //nvram_deinit 
    err = hal_nvram_deinit(); 
    if ( err != HAL_E_OK && err != HAL_E_NOT_INITIALIZED && err != HAL_E_NOFEATURE)
    {
    err_store = err;
    } 
    
    //power_deinit 
    err = hal_power_deinit(); 
    if ( err != HAL_E_OK && err != HAL_E_NOT_INITIALIZED && err != HAL_E_NOFEATURE)
    {
    err_store = err;
    } 
    
    //sensor_deinit 
    err = hal_sensor_deinit(); 
    if ( err != HAL_E_OK && err != HAL_E_NOT_INITIALIZED && err != HAL_E_NOFEATURE)
    {
    err_store = err;
    } 
    
    //touch_deinit 
    err = hal_touch_deinit(); 
    if ( err != HAL_E_OK && err != HAL_E_NOT_INITIALIZED && err != HAL_E_NOFEATURE)
    {
    err_store = err;
    } 

    //udev_deinit the function is void so no return and no check on fail, should not be able to fail 
    (void)hal_udev_deinit(); 
    
    //wlan_deinit the function is void so no return and no check on fail, should not be able to fail
    (void)hal_wlan_deinit();
	    
    //last deinitialize the HAL
    hal_syscfg_deinit();

    // deinitialize event-poll module..
    ev_poll_deinit();

    closelog();
    initialized = FALSE;

    if (err_store != HAL_E_OK)
    {	
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "libhal %s deinit failed", VERSION );
        return HAL_E_FAIL;
    }
    else
    {
        return HAL_E_OK;
    }
}

hal_error hal_get_version( hal_version3 * const version )
{
    CHECK_POINTER_PARAMETER( version );

    sscanf(VERSION, "%d.%d.%d", &(version->major), &(version->minor), &(version->patch));

    return HAL_E_OK;
}

void hal_set_log_func( hal_log_func const f )
{
    logger = ( f == NULL ) ? hal_log_default_func : f;
}

void hal_log_default_func( hal_log_severity const severity,
    uint8_t const * const message, va_list args )
{
    int priority;
    
    switch ( severity )
    {
        case HAL_LOG_SEVERITY_INFO:
            priority = LOG_INFO;
            break;
        case HAL_LOG_SEVERITY_WARNING:
            priority = LOG_WARNING;
            break;
        case HAL_LOG_SEVERITY_ERROR:
            priority = LOG_ERR;
            break;
        default:
            priority = LOG_DEBUG;
            break;
    }
    
    vsyslog( priority, message, args );
}

void hal_log( hal_log_severity const severity, uint8_t const * const message,
    ... )
{
    va_list args;
    
    va_start( args, message );
    logger( severity, message, args );
    va_end( args );
}
