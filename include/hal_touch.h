/**************************************************************************
 * Copyright (c) 2013 TTTControl. All rights reserved. Confidential proprietary
 * Schoenbrunnerstrasse 7, A-1040 Wien, Austria. office[at]ttcontrol.com
 **************************************************************************/
/**********************************************************************//**
 * \file hal_touch.h
 *
 * \brief Hardware Abstraction Layer for touchscreen.
 *
 * The device path which must be used is obtained through
 * hal_touch_get_device().
 *
 * The touchscreen of the HY-eVision² can be accessed directly with Tslib
 * (see tslib.h for details) or alternatively with Linux input event
 * functions (see linux/input.h for details).
 *
 * By default the touchscreen is activated and drag-support
 * is deactivated.
 *
 **************************************************************************/

#ifndef _HAL_TOUCH_H_
#define _HAL_TOUCH_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "hal_types.h"

/** \defgroup touch Touchscreen
 *  \brief Functions and datastructures for Touchscreen
 *  \{
 */

/** Available modes for the touchscreen */
typedef enum
{
    HAL_TOUCH_MODE_DRAG_ENABLE,     /**< activates dragging support, i.e. if the
                                         finger pressing the touchscreen is moved the reported
                                         coordinates are updated with the new finger position.*/
    HAL_TOUCH_MODE_DRAG_DISABLE,    /**< deactivates dragging support, i.e. if the
                                         finger pressing the touchscreen is moved the reported
                                         coordinates are not updated but remain the same as until
                                         the touch is released. */
} hal_touch_mode;

/** \brief Initialize touch module
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_FAIL               Could not initialize module
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 *
 *  \remarks
 *  This function must be called before any other function (except
 *  #hal_touch_get_device) of this module is called.
 *
 */
hal_error hal_touch_init();

/** \brief Deinitialize touch module.
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
hal_error hal_touch_deinit();


/**
 * \brief Returns the touchscreens device file
 *
 * Use this function to get the correct device file for the touchscreen.
 *
 * \param device[out] Pointer to string that contains the device file path
 *
 * \return #hal_error
 * \retval #HAL_E_OK                  Everything fine
 * \retval #HAL_E_NULL_POINTER        A NULL pointer has been passed
 *
 * \remarks
 * This function can be called without prior call of
 * #hal_touch_init
 */
hal_error hal_touch_get_device(uint8_t ** device);


/**
 * \brief Enables or disables the touchscreen
 *
 * This function activates or deactivates the touchscreen.
 * If deactivated, the touchscreen does not report any
 * coordinates.
 *
 * \param enable[in]    TRUE to enable, FALSE to disable
 *
 * \return #hal_error
 * \retval #HAL_E_OK                  Everything fine
 * \retval #HAL_E_FAIL                Could not enable touchscreen
 * \retval #HAL_E_NOT_INITIALIZED     Module not initialized
 *
 * \remark
 * This function shall not be called periodically
 */
hal_error hal_touch_set_enable( bool enable );

/**
 * \brief Sets the operating mode of the touchscreen.
 *
 * \param[in] mode      Mode to set; one of:
 *           - \c #HAL_TOUCH_MODE_DRAG_ENABLE: enable dragging support
 *           - \c #HAL_TOUCH_MODE_DRAG_DISABLE: disable dragging support
 *
 * \return #hal_error
 * \retval #HAL_E_OK                  Everything fine
 * \retval #HAL_E_FAIL                Could not set mode
 * \retval #HAL_E_NOT_INITIALIZED     Module not initialized
 *
 * \remark
 * This function shall not be called periodically
 */
hal_error hal_touch_set_mode( hal_touch_mode mode );

/** \} */

#ifdef __cplusplus
}  /* End of the 'extern "C"' block */
#endif

#endif /* _HAL_TOUCH_H_ */
