#ifdef HAVE_CONFIG_H
    #include "config.h"
#endif

#include "hal.h"
#include "hal_types.h"
#include "tools.h"

#ifdef HAVE_CAMERA

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <syslog.h>
#include <gst/gst.h>
#include <glib.h>

#define MODULE_NAME "[camera] "
#define NUM_VIEWPORTS 2

#define CHECK_VIEWPORT(vp) \
    if (vp > NUM_VIEWPORTS - 1) { return HAL_E_INVALID_PARAMETER; } \
    if (pipeline[vp] == NULL) { return HAL_E_NOT_INITIALIZED; }

static gboolean initialized = FALSE;
static gboolean cap_has_camera = FALSE;

static GstElement * pipeline[NUM_VIEWPORTS] = { NULL, NULL };
static GstElement * source[NUM_VIEWPORTS] = { NULL, NULL };
static GstElement * sink[NUM_VIEWPORTS] = { NULL, NULL };

static uint32_t axis_left[NUM_VIEWPORTS] = { 0, 0 };
static uint32_t axis_top[NUM_VIEWPORTS] = { 0, 0 };
static uint32_t disp_width[NUM_VIEWPORTS] = { 720, 720 };
static uint32_t disp_height[NUM_VIEWPORTS] = { 288, 288 };
static hal_camera_channel cam_channel[NUM_VIEWPORTS] =
    { HAL_CAMERA_CHANNEL_1, HAL_CAMERA_CHANNEL_1 };
//static hal_camera_rotation cam_rotation[NUM_VIEWPORTS] =
//    { HAL_CAMERA_ROTATION_NONE, HAL_CAMERA_ROTATION_NONE };

static hal_error check_camera_caps()
{
    hal_error err;
    uint32_t camera_cap;

    cap_has_camera = FALSE;
    err = hal_syscfg_get_capability( HAL_SYSCAP_CAMERA, &camera_cap );
    if ( err != HAL_E_OK )
    {
        return HAL_E_FAIL;
    }

    cap_has_camera = ( camera_cap != SYSCFG_FIELD_NONE );
    err = ( cap_has_camera ) ? HAL_E_OK : HAL_E_NOFEATURE;

    return err;
}

hal_error hal_camera_init()
{
    uint32_t viewport = 0;
    hal_error err = HAL_E_OK;

    CHECK_HAL_INITIALIZED();
    CHECK_MODULE_ALREADY_INITIALIZED( initialized );

    err = check_camera_caps();
    if ( err != HAL_E_OK)
    {
        return err;
    }

    if (pipeline[viewport] != NULL)
    {
        return HAL_E_ALREADY_INITIALIZED;
    }

    if (!gst_init_check(NULL, NULL, NULL))
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not initialize GStreamer." );
        return HAL_E_FAIL;
    }

    pipeline[viewport] = gst_pipeline_new(NULL);
    if (pipeline[viewport] == NULL)
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not create pipeline." );
        goto fail;
    }

    source[viewport] = gst_element_factory_make("imxv4l2src", NULL);
    if (source[viewport] == NULL)
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not create source." );
        goto fail;
    }

    sink[viewport] = gst_element_factory_make("imxv4l2sink", NULL);
    if (sink[viewport] == NULL)
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not create sink." );
        goto fail;
    }

    gst_bin_add_many(GST_BIN(pipeline[viewport]), source[viewport],
                     sink[viewport], NULL);
    gst_element_link(source[viewport], sink[viewport]);

    // Set VideoStandard to PAL (1)
    g_object_set(G_OBJECT(source[viewport]), "std", 1, NULL);
    hal_camera_set_x(viewport, axis_left[viewport]);
    hal_camera_set_y(viewport, axis_top[viewport]);
    hal_camera_set_width(viewport, disp_width[viewport]);
    hal_camera_set_height(viewport, disp_height[viewport]);
    initialized = TRUE;

    return HAL_E_OK;

fail:
    gst_object_unref(pipeline[viewport]);
    gst_object_unref(source[viewport]);
    gst_object_unref(sink[viewport]);

    pipeline[viewport] = NULL;
    source[viewport] = NULL;
    sink[viewport] = NULL;

    return HAL_E_FAIL;
}

void hal_camera_deinit()
{
    //CHECK_MODULE_INITIALIZED( initialized );

    if (pipeline[HAL_CAMERA_VIEWPORT_1] != NULL)
    {
        gst_object_unref(GST_OBJECT(pipeline[HAL_CAMERA_VIEWPORT_1]));
        pipeline[HAL_CAMERA_VIEWPORT_1] = NULL;
    }

    if (pipeline[HAL_CAMERA_VIEWPORT_2] != NULL)
    {
        gst_object_unref(GST_OBJECT(pipeline[HAL_CAMERA_VIEWPORT_2]));
        pipeline[HAL_CAMERA_VIEWPORT_2] = NULL;
    }

    initialized = FALSE;
}

hal_error hal_camera_play(hal_camera_viewport const viewport)
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_camera );
    CHECK_VIEWPORT(viewport)

    gst_element_set_state(pipeline[viewport], GST_STATE_PLAYING);
    hal_camera_set_channel(viewport, cam_channel[viewport]);
    return HAL_E_OK;
}

hal_error hal_camera_stop(hal_camera_viewport const viewport)
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_camera );
    CHECK_VIEWPORT(viewport)

    gst_element_set_state(pipeline[viewport], GST_STATE_NULL);
    return HAL_E_OK;
}

hal_error hal_camera_get_x(hal_camera_viewport const viewport, uint32_t * const x)
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_camera );
    CHECK_VIEWPORT(viewport)
    CHECK_POINTER_PARAMETER(x);

//    g_object_get(G_OBJECT(sink[viewport]), "axis-left", x, NULL);
    *x = axis_left[viewport];
    return HAL_E_OK;
}

hal_error hal_camera_set_x(hal_camera_viewport const viewport, uint32_t const x)
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_camera );
    CHECK_VIEWPORT(viewport)

    g_object_set(G_OBJECT(sink[viewport]), "axis-left", x, NULL);
    axis_left[viewport] = x;
    return HAL_E_OK;
}

hal_error hal_camera_get_y(hal_camera_viewport const viewport, uint32_t * const y)
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_camera );
    CHECK_VIEWPORT(viewport)
    CHECK_POINTER_PARAMETER(y);

//    g_object_get(G_OBJECT(sink[viewport]), "axis-top", y, NULL);
    *y = axis_top[viewport];
    return HAL_E_OK;
}

hal_error hal_camera_set_y(hal_camera_viewport const viewport, uint32_t const y)
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_camera );
    CHECK_VIEWPORT(viewport)

    g_object_set(G_OBJECT(sink[viewport]), "axis-top", y, NULL);
    axis_top[viewport] = y;
    return HAL_E_OK;
}

hal_error hal_camera_get_width(hal_camera_viewport const viewport,
    uint32_t * const width)
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_camera );
    CHECK_VIEWPORT(viewport)
    CHECK_POINTER_PARAMETER(width);

//    g_object_get(G_OBJECT(sink[viewport]), "disp-width", width, NULL);
    *width = disp_width[viewport];
    return HAL_E_OK;
}

hal_error hal_camera_set_width(hal_camera_viewport const viewport,
    uint32_t const width)
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_camera );
    CHECK_VIEWPORT(viewport)

    g_object_set(G_OBJECT(sink[viewport]), "disp-width", width, NULL);
    disp_width[viewport] = width;
    return HAL_E_OK;
}

hal_error hal_camera_get_height(hal_camera_viewport const viewport,
    uint32_t * const height)
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_camera );
    CHECK_VIEWPORT(viewport)
    CHECK_POINTER_PARAMETER(height);

//    g_object_get(G_OBJECT(sink[viewport]), "disp-height", height, NULL);
    *height = disp_height[viewport];
    return HAL_E_OK;
}

hal_error hal_camera_set_height(hal_camera_viewport const viewport,
    uint32_t const height)
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_camera );
    CHECK_VIEWPORT(viewport)

    g_object_set(G_OBJECT(sink[viewport]), "disp-height", height, NULL);
    disp_height[viewport] = height;
    return HAL_E_OK;
}

hal_error hal_camera_get_position(hal_camera_viewport const viewport,
    uint32_t * const x, uint32_t * const y, uint32_t * const width,
    uint32_t * const height)
{
    uint8_t err = HAL_E_OK;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_camera );

    err  = hal_camera_get_x(viewport, x);
    err |= hal_camera_get_y(viewport, y);
    err |= hal_camera_get_width(viewport, width);
    err |= hal_camera_get_height(viewport, height);

    return err;
}

hal_error hal_camera_set_position(hal_camera_viewport const viewport,
    uint32_t const x, uint32_t const y, uint32_t const width,
    uint32_t const height)
{
    uint8_t err = HAL_E_OK;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_camera );

    err  = hal_camera_set_x(viewport, x);
    err |= hal_camera_set_y(viewport, y);
    err |= hal_camera_set_width(viewport, width);
    err |= hal_camera_set_height(viewport, height);

    return err;
}

hal_error hal_camera_get_channel(hal_camera_viewport const viewport,
    hal_camera_channel * const channel)
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_camera );
    CHECK_VIEWPORT(viewport)
    CHECK_POINTER_PARAMETER(channel);

//    g_object_get(G_OBJECT(source[viewport]), "channel", channel, NULL);
    *channel = cam_channel[viewport];
    return HAL_E_OK;
}

hal_error hal_camera_set_channel(hal_camera_viewport const viewport,
    hal_camera_channel const channel)
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_camera );
    CHECK_VIEWPORT(viewport)

    g_object_set(G_OBJECT(source[viewport]), "channel", channel, NULL);
    cam_channel[viewport] = channel;
    return HAL_E_OK;
}

/*uint8_t hal_camera_get_rotation(hal_camera_viewport const viewport, hal_camera_rotation * const rotation)
{
    CHECK_VIEWPORT(viewport);
    CHECK_POINTER_PARAMETER(rotation);

    *rotation = cam_rotation[viewport];
    return HAL_E_OK;
}

uint8_t hal_camera_set_rotation(hal_camera_viewport const viewport, hal_camera_rotation const rotation)
{
    CHECK_VIEWPORT(viewport);

    g_object_set(G_OBJECT(source[viewport]), "rotate", rotation, NULL);
    cam_rotation[viewport] = rotation;
    return HAL_E_OK;
}*/

#else

hal_error hal_camera_init() { return HAL_E_UNSUPPORTED; }
void hal_camera_deinit() { }
hal_error hal_camera_play(hal_camera_viewport const viewport) { return HAL_E_UNSUPPORTED; }
hal_error hal_camera_stop(hal_camera_viewport const viewport) { return HAL_E_UNSUPPORTED; }
hal_error hal_camera_get_x(hal_camera_viewport const viewport, uint32_t * const x) { return HAL_E_UNSUPPORTED; }
hal_error hal_camera_set_x(hal_camera_viewport const viewport, uint32_t const x) { return HAL_E_UNSUPPORTED; }
hal_error hal_camera_get_y(hal_camera_viewport const viewport, uint32_t * const y) { return HAL_E_UNSUPPORTED; }
hal_error hal_camera_set_y(hal_camera_viewport const viewport, uint32_t const y) { return HAL_E_UNSUPPORTED; }
hal_error hal_camera_get_width(hal_camera_viewport const viewport,
    uint32_t * const width) { return HAL_E_UNSUPPORTED; }
hal_error hal_camera_set_width(hal_camera_viewport const viewport,
    uint32_t const width) { return HAL_E_UNSUPPORTED; }
hal_error hal_camera_get_height(hal_camera_viewport const viewport,
    uint32_t * const height) { return HAL_E_UNSUPPORTED; }
hal_error hal_camera_set_height(hal_camera_viewport const viewport,
    uint32_t const height) { return HAL_E_UNSUPPORTED; }
hal_error hal_camera_get_position(hal_camera_viewport const viewport,
    uint32_t * const x, uint32_t * const y, uint32_t * const width,
    uint32_t * const height) { return HAL_E_UNSUPPORTED; }
hal_error hal_camera_set_position(hal_camera_viewport const viewport,
    uint32_t const x, uint32_t const y, uint32_t const width,
    uint32_t const height) { return HAL_E_UNSUPPORTED; }
hal_error hal_camera_get_channel(hal_camera_viewport const viewport,
    hal_camera_channel * const channel) { return HAL_E_UNSUPPORTED; }
hal_error hal_camera_set_channel(hal_camera_viewport const viewport,
    hal_camera_channel const channel) { return HAL_E_UNSUPPORTED; }

#endif
