#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>
#include <string.h>

#include <glib.h>

#include "libhal.h"

static struct option options[] =
{
    {"help", no_argument, 0, '?'},
    {"ssid", required_argument, 0, 's'},
    {"pass", required_argument, 0, 'p'},
    {"async", no_argument, 0, 'a'},
    {0, 0, 0, 0}
};

#define NETWORK_COUNT 128
static struct hal_wlan_network networks[NETWORK_COUNT];
static struct hal_wlan_network_data data[NETWORK_COUNT];
static uint8_t network_count;
static uint8_t exit_app = FALSE;
static uint8_t async = FALSE;

void print_help()
{
    printf("HAL WLAN test\n" \
           "  -? --help       This message\n" \
           "  -s --ssid       SSID to connect to\n" \
           "  -p --pass       Passphrase\n" \
           "  -a --async      Use asynchronous API" \
           "\n");
}

void leave(int sig)
{
    if (sig != 0)
    {
        printf("SIGINT caught. Exiting.\n");
    }
    
    exit_app = TRUE;
}

void get_networks(uint8_t current)
{
    network_count = (current) ? 0 : NETWORK_COUNT;
    hal_wlan_get_networks2( networks, &network_count, data );
    
    printf("Got %d networks...\n", network_count);

    for ( int i = 0; i < network_count; i++ )
    {
        printf( "SSID: %s (Strength: %d  State: %d  Error: %d  AC: %d  Encryption: %d)\n" \
                "  IP: %s  Netmask: %s  Gateway %s\n\n",
            networks[i].ssid, networks[i].strength, networks[i].state,
            networks[i].error, networks[i].auto_connect, data[i].security,
            data[i].ip, data[i].netmask, data[i].gateway );
    }
}

int main(int argc, char **argv)
{
    hal_error err;
    int option_index = 0;
    int c = 0;
    int i = 0;
    uint8_t val;
    int network_found = -1;
    int connected = 0;
    char ssid[128];
    char pass[128];

    while (c != -1)
    {
        option_index = 0;
        c = getopt_long(argc, argv, "?s:p:a", options,
                        &option_index);

        switch (c)
        {
            case '?':
                print_help();
                exit(0);
                break;
            case 's':
                strncpy(ssid, optarg, 127);
                break;
            case 'p':
                strncpy(pass, optarg, 127);
                break;
            case 'a':
                async = TRUE;
                break;
        }
    }

    signal(SIGINT, leave);
    
    exit_app = 0;
    connected = 0;

    err = hal_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize HAL (%d)\n", err);
        exit(-1);
    }

    err = hal_dbus_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize DBUS module (%d)\n", err);
        exit(-1);
    }

    err = hal_wlan_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize WLAN module (%d)\n", err);
        exit(-1);
    }

    hal_wlan_enable( TRUE );
    printf("Enabled WLAN\n");

    printf("Trying to connect to %s (passphrase: %s)\n", ssid, pass);

    while ( !exit_app && network_found == -1 )
    {
        get_networks( FALSE );
        if ( network_count != 0)
        {
            for (i = 0; i < network_count; i++)
            {
                if (strcmp(ssid, networks[i].ssid) == 0)
                {
                    printf("Found network %d\n", i);
                    network_found = i;
                }
            }
        }
        sleep(1);
    }
    
    if (exit_app)
    {
        goto done;
    }

    printf("Connecting to network %d/%s (%s)\n", network_found,
        networks[network_found].ssid, networks[network_found].dbus_path);
    
    if ( async )
    {
        err = HAL_E_PENDING;
        while ( err == HAL_E_PENDING )
        {
            err = hal_wlan_connect_async(&networks[network_found], pass);
            get_networks(TRUE);
            sleep(1);
        }
        if ( err == HAL_E_OK )
        {
            connected = TRUE;
            exit_app = FALSE;
        }
        else
        {
            printf("Could not connect.\n");
            exit_app = TRUE;
        }
    }
    else
    {
        err = hal_wlan_connect( &networks[network_found], pass );
        if ( err == HAL_E_FAIL )
        {
            printf("Could not connect.\n");
            exit_app = TRUE;
        }
    }
    
    while (!exit_app && !connected)
    {
        get_networks(TRUE);

        if (networks[0].state == HAL_WLAN_STATE_READY ||
            networks[0].state == HAL_WLAN_STATE_ONLINE)
        {
            printf("switch off autoconnect\n");
            hal_wlan_set_autoconnect( &networks[0], 0 );
            connected = TRUE;
        }
        else if (networks[0].state == HAL_WLAN_STATE_FAILURE ||
                 networks[0].error != HAL_WLAN_ERROR_OK)
        {
            printf("Could not connect; failure or error (%d)\n", networks[0].error);
            goto done;
        }
        
        sleep(1);
    }
    
    c = 0;
    while (!exit_app)
    {
        if (c++ % 10 == 0)
        {
            printf("Scanning for networks...");
            if (async)
            {
                err = HAL_E_PENDING;
                while (err == HAL_E_PENDING)
                {
                    err = hal_wlan_scan_async();
                    printf("pending\n");
                    usleep(100000);
                }
                if (err != HAL_E_OK)
                {
                    printf("Failed\n");
                }
            }
            else
            {
                hal_wlan_scan();
            }
            printf("Done\n");
        }
        get_networks( FALSE );

        sleep(2);
    }

    printf("Removing network %s\n", networks[0].dbus_path);
    hal_wlan_remove( &networks[0] );

done:
    hal_wlan_enable( FALSE );
    hal_wlan_deinit();
    hal_dbus_deinit();
    hal_deinit();
    exit(0);
}
