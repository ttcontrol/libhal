/* Code (c) ldx taken from:
 * http://stackoverflow.com/questions/9378593/dbuswatch-and-dbustimeout-examples
 *
 * Copyright (c) 2015 TTControl. All rights reserved. Confidential proprietary
 * Schönbrunnerstraße 7, A-1040 Wien, Austria. office@ttcontrol.com
 */
 /**********************************************************************//**
 * \file hal_dbus.c
 *
 *  \brief Functions and datastructures for DBUS support
 *
 **************************************************************************/

#include <unistd.h>
#include <stdlib.h>
#include <dbus/dbus.h>
#include <pthread.h>
#include <event.h>
#include <event2/thread.h>

#include "hal.h"
#include "hal_dbus.h"
#include "tools.h"

#define MODULE_NAME "[dbus] "

struct hal_dbus_context {
    DBusConnection *conn;
    struct event_base *evbase;
    struct event dispatch_ev;
    void *extra;
};

static gboolean initialized = FALSE;
static struct hal_dbus_context system_ctx;
static pthread_t dbus_thread;

static void* dbus_loop( void* data )
{
    sigset_t sigmask;
    struct hal_dbus_context* ctx = ( struct hal_dbus_context* ) data;

    // Set up a sigset which blocks all signals
    sigfillset( &sigmask );
    sigprocmask( SIG_BLOCK, &sigmask, NULL );

    event_base_dispatch( ctx->evbase );
    return NULL;
}

static void dispatch( int fd, short ev, void *x )
{
    struct hal_dbus_context *ctx = x;
    DBusConnection *c = ctx->conn;
    
    while ( dbus_connection_get_dispatch_status( c ) == DBUS_DISPATCH_DATA_REMAINS )
        dbus_connection_dispatch( c );
}

static void handle_dispatch_status( DBusConnection *c,
    DBusDispatchStatus status, void *data )
{
    struct hal_dbus_context *ctx = data;
    
    if ( status == DBUS_DISPATCH_DATA_REMAINS )
    {
        struct timeval tv =
        {
            .tv_sec = 0,
            .tv_usec = 0,
        };
        event_add( &ctx->dispatch_ev, &tv );
    }
}

static void handle_watch( int fd, short events, void *x )
{
    struct hal_dbus_context *ctx = x;
    struct DBusWatch *watch = ctx->extra;
    
    unsigned int flags = 0;
    if ( events & EV_READ )
        flags |= DBUS_WATCH_READABLE;
    if ( events & EV_WRITE )
        flags |= DBUS_WATCH_WRITABLE;
        
    if ( dbus_watch_handle( watch, flags ) == FALSE )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "dbus_watch_handle() failed");
    }

    handle_dispatch_status( ctx->conn, DBUS_DISPATCH_DATA_REMAINS, ctx );
}

static dbus_bool_t add_watch( DBusWatch *w, void *data )
{
    if ( !dbus_watch_get_enabled( w ) )
        return TRUE;

    struct hal_dbus_context *ctx = data;
    ctx->extra = w;

    int fd = dbus_watch_get_unix_fd( w );
    unsigned int flags = dbus_watch_get_flags( w );
    short cond = EV_PERSIST;
    if ( flags & DBUS_WATCH_READABLE )
        cond |= EV_READ;
    if ( flags & DBUS_WATCH_WRITABLE )
        cond |= EV_WRITE;

    struct event *event = event_new( ctx->evbase, fd, cond, handle_watch, ctx );
    if ( !event )
    {
        return FALSE;
    }

    event_add( event, NULL );
    dbus_watch_set_data( w, event, NULL );

    return TRUE;
}

static void remove_watch( DBusWatch *w, void *data )
{
    struct event *event = dbus_watch_get_data( w );

    if ( event )
        event_free( event );

    dbus_watch_set_data( w, NULL, NULL );
}

static void toggle_watch(DBusWatch *w, void *data)
{
    if ( dbus_watch_get_enabled( w ) )
    {
        add_watch(w, data);
    }
    else
    {
        remove_watch(w, data);
    }
}

static void handle_timeout( int fd, short ev, void *x )
{
    DBusTimeout *t = x;
    dbus_timeout_handle( t );
}

static dbus_bool_t add_timeout( DBusTimeout *t, void *data )
{
    struct hal_dbus_context *ctx = data;

    if ( !dbus_timeout_get_enabled(t) )
    {
        return TRUE;
    }

    struct event *event = event_new( ctx->evbase, -1, EV_TIMEOUT|EV_PERSIST,
        handle_timeout, t );
    if (!event)
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "failed to allocate new event for timeout\n" );
        return FALSE;
    }

    int ms = dbus_timeout_get_interval( t );
    struct timeval tv =
    {
        .tv_sec = ms / 1000,
        .tv_usec = (ms % 1000) * 1000,
    };
    event_add( event, &tv );

    dbus_timeout_set_data( t, event, NULL );

    return TRUE;
}

static void remove_timeout( DBusTimeout *t, void *data )
{
    struct event *event = dbus_timeout_get_data( t );

    event_free( event );
    dbus_timeout_set_data( t, NULL, NULL );
}

static void toggle_timeout( DBusTimeout *t, void *data )
{
    if ( dbus_timeout_get_enabled(t) )
    {
        add_timeout( t, data );
    }
    else
    {
        remove_timeout( t, data );
    }
}

static DBusHandlerResult handle_nameownerchanged( DBusMessage *message,
    void *data )
{
    struct hal_dbus_context *ctx = data;
    char *name, *old, *new;

    if ( dbus_message_get_args( message, NULL,
        DBUS_TYPE_STRING, &name,
        DBUS_TYPE_STRING, &old,
        DBUS_TYPE_STRING, &new,
        DBUS_TYPE_INVALID ) == FALSE )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "spurious NameOwnerChanged signal\n" );
        return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
    }

    if (new[0] != '\0')
        return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;

    /* XXX handle disconnecting clients */

    return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
}

static DBusHandlerResult msg_filter( DBusConnection *connection,
    DBusMessage *message, void *data )
{
    if ( dbus_message_is_signal( message, DBUS_INTERFACE_DBUS,
        "NameOwnerChanged"))
    {
        return handle_nameownerchanged( message, data );
    }

    return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
}

hal_error hal_dbus_init()
{
    DBusConnection *conn = NULL;
    DBusError err;

    CHECK_HAL_INITIALIZED();
    CHECK_MODULE_ALREADY_INITIALIZED( initialized );

    // evil things will happen if not launched like this...
    fakesystem("/usr/bin/dbus-daemon --system --nofork &");

    usleep(500000);

    dbus_error_init( &err );
    conn = dbus_bus_get_private( DBUS_BUS_SYSTEM, &err );
    if ( conn == NULL ) {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Failed to get bus. %s\n", err.message );
        goto out;
    }

    dbus_connection_set_exit_on_disconnect( conn, FALSE );
    system_ctx.conn = conn;

    evthread_use_pthreads();
    system_ctx.evbase = event_base_new();
    pthread_create( &dbus_thread, NULL, dbus_loop, &system_ctx );

    event_assign( &system_ctx.dispatch_ev, system_ctx.evbase, -1, EV_TIMEOUT,
        dispatch, &system_ctx );

    if (!dbus_connection_set_watch_functions( conn, add_watch, remove_watch,
        toggle_watch, &system_ctx, NULL ) )
    {
        hal_log(HAL_LOG_SEVERITY_ERROR, MODULE_NAME "dbus_connection_set_watch_functions() failed\n");
        goto out;
    }

    if (!dbus_connection_set_timeout_functions( conn, add_timeout,
        remove_timeout, toggle_timeout, &system_ctx, NULL ) )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "dbus_connection_set_timeout_functions() failed\n" );
        goto out;
    }

    if ( dbus_connection_add_filter( conn, msg_filter, &system_ctx, NULL ) == FALSE ) {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "dbus_connection_add_filter() failed\n" );
        goto out;
    }

    dbus_connection_set_dispatch_status_function( conn, handle_dispatch_status,
        &system_ctx, NULL );

    usleep(500000);
    dbus_error_init( &err );
    dbus_bus_add_match( conn, "type='signal',interface='" DBUS_INTERFACE_DBUS
        "',member='NameOwnerChanged'", &err );

    if ( dbus_error_is_set( &err ) )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "dbus_bus_add_match() NameOwnerChanged failed: %s\n",
            err.message );
        dbus_error_free( &err );
        goto out;
    }

    initialized = TRUE;
    return HAL_E_OK;

out:
    if ( conn ) {
        dbus_connection_close( conn );
        dbus_connection_unref( conn );
    }

    return HAL_E_FAIL;
}

void hal_dbus_deinit()
{
    CHECK_MODULE_INITIALIZED_RETURN_VOID( initialized );

    if ( system_ctx.conn ) {
        dbus_bus_remove_match( system_ctx.conn, "type='signal',interface='"
            DBUS_INTERFACE_DBUS "',member='NameOwnerChanged'", NULL );
        dbus_connection_flush( system_ctx.conn );
        dbus_connection_close( system_ctx.conn );
        dbus_connection_unref( system_ctx.conn );
        event_del( &system_ctx.dispatch_ev );
    }
    if ( system_ctx.evbase )
    {
        event_base_loopbreak( system_ctx.evbase );
        pthread_join( dbus_thread, NULL );
        event_base_free( system_ctx.evbase );
    }

    initialized = FALSE;
}

hal_error hal_dbus_get_system_bus( DBusConnection ** conn )
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( conn );

    *conn = system_ctx.conn;
    return HAL_E_OK;
}
