#include "hal_can.h"
#include "hal.h"
#include "hal_types.h"
#include "tools.h"

#include <linux/input.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <syslog.h>
#include <glib.h>

#define MODULE_NAME "[can] "

#define IFNAME_CAN0     "can0"
#define IFNAME_CAN1     "can1"
#define IFNAME_CAN2     "can2"
#define IFNAME_CAN3     "can3"

static gboolean initialized = FALSE;

static guint cap_num_channels = 0;

static gboolean check_channel( hal_can_channel c )
{
    return !( ( cap_num_channels == 0 ) || ( cap_num_channels <= (guint)c ) );
}

static uint8_t * get_baudrate ( hal_can_baudrate b )
{
    switch ( b )
    {
        case HAL_CAN_BAUD_125K:
            return ("125");
            break;
        case HAL_CAN_BAUD_250K:
            return ("250");
            break;
        case HAL_CAN_BAUD_500K:
            return("500");
            break;
        case HAL_CAN_BAUD_800K:
            return ("800");
            break;
        case HAL_CAN_BAUD_1000K:
            return ("1000");
            break;
        default:
            return NULL;
            break;
    }
}

static hal_error check_can_caps()
{
    hal_error err = HAL_E_OK;
    uint32_t cap;

    cap_num_channels = 0;
    err = hal_syscfg_get_capability( HAL_SYSCAP_CAN, &cap );
    if ( err != HAL_E_OK )
    {
        return HAL_E_FAIL;
    }
    
    switch ( cap )
    {
        case SYSCFG_CAN_XC_2X:
            cap_num_channels = 2;
            break;
        case SYSCFG_CAN_XC_4X:
        case SYSCFG_ALL_FEATURES:
            cap_num_channels = 4;
            break;
        default:
            err = HAL_E_NOFEATURE;
            break;
    }
    
    return err;
}

hal_error hal_can_init()
{
    hal_error err = HAL_E_OK;

    CHECK_HAL_INITIALIZED();
    CHECK_MODULE_ALREADY_INITIALIZED( initialized );

    err = check_can_caps();
    if (err == HAL_E_FAIL)
    {
        return HAL_E_FAIL;
    }

    initialized = TRUE;
    return err;
}

hal_error hal_can_deinit()
{
    CHECK_MODULE_INITIALIZED( initialized );

    for ( int i = HAL_CAN_CHANNEL_0; i <= HAL_CAN_CHANNEL_3; i++ )
    {
        hal_can_deinit_channel( i );
    }

    initialized = FALSE;

    return HAL_E_OK;
}

hal_error hal_can_init_channel( hal_can_channel ch, hal_can_baudrate baudrate )
{
    hal_error err;
    uint8_t *if_name;
    uint8_t *baud;
    uint8_t *cmd;
    int ret;

    CHECK_MODULE_INITIALIZED( initialized );

    if ( !check_channel( ch ) )
    {
        return HAL_E_NOFEATURE;
    }

    err = hal_can_get_ifname( ch, &if_name );
    if ( err != HAL_E_OK  )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not get interface name for CAN channel %u", ch );
        return HAL_E_INVALID_PARAMETER;
    }

    baud = get_baudrate( baudrate );
    if ( baud == NULL )
    {
        return HAL_E_INVALID_PARAMETER;
    }

    cmd = g_strdup_printf("cansetup %s %s", if_name, baud );
    ret = fakesystem( cmd );
    g_free( cmd );

    if ( ret == 0 )
    {
        return HAL_E_OK;
    }
    else
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not setup %s",
            if_name );
        return HAL_E_FAIL;
    }
}

hal_error hal_can_deinit_channel( hal_can_channel ch )
{
    uint8_t *if_name;
    char *cmd;
    int ret;
    hal_error err;

    CHECK_MODULE_INITIALIZED( initialized );
    
    if ( !check_channel( ch ) )
    {
        return HAL_E_NOFEATURE;
    }

    err = hal_can_get_ifname( ch, &if_name );
    if ( err != HAL_E_OK )
    {
        return HAL_E_INVALID_PARAMETER;
    }

    cmd = g_strdup_printf( "cansetup -d %s", if_name );
    ret = fakesystem( cmd );

    g_free( cmd );

    if ( ret == 0 )
    {
        return HAL_E_OK;
    }
    else
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not deinitialize %s", if_name );
        return HAL_E_FAIL;
    }
}

hal_error hal_can_get_ifname( hal_can_channel ch
                            , uint8_t ** ifname )
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( ifname );

    if ( !check_channel( ch ) )
    {
        return HAL_E_NOFEATURE;
    }

    switch ( ch )
    {
        case HAL_CAN_CHANNEL_0:
            *ifname = IFNAME_CAN0;
            break;
        case HAL_CAN_CHANNEL_1:
            *ifname = IFNAME_CAN1;
            break;
        case HAL_CAN_CHANNEL_2:
            *ifname = IFNAME_CAN2;
            break;
        case HAL_CAN_CHANNEL_3:
            *ifname = IFNAME_CAN3;
            break;
        default:
            return HAL_E_INVALID_PARAMETER;
    }

    return HAL_E_OK;
}
