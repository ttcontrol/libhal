/**************************************************************************
 * Copyright (c) 2013 TTTControl. All rights reserved. Confidential proprietary
 * Schoenbrunnerstrasse 7, A-1040 Wien, Austria. office[at]ttcontrol.com
 **************************************************************************/
/**********************************************************************//**
 * \file hal_rtc.h
 *
 * \brief Hardware Abstraction Layer for realtime clock.
 *
 * The device path which must be used is obtained through
 * hal_rtc_get_device().
 * The real-time clock of the HY-eVision² (backed up by a Supercapacitor) can be
 * accessed directly with Linux rtc functions (see linux/rtc.h for details).
 *
 **************************************************************************/

#ifndef _HAL_RTC_H_
#define _HAL_RTC_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "hal_types.h"

/** \defgroup rtc RTC
 *  \brief Functions and datastructures for RTC
 *  \{
 */

/**
 * \brief Returns the RTC's device file
 *
 * Use this function to get the correct device file for the realtime clock.
 *
 * \param[out] device Pointer to string that contains the device file path
 *
 * \return #hal_error
 * \retval #HAL_E_OK                  Everything fine
 * \retval #HAL_E_NULL_POINTER        A NULL pointer has been passed
 */
hal_error hal_rtc_get_device(uint8_t const ** device);

/** \} */

#ifdef __cplusplus
}  /* End of the 'extern "C"' block */
#endif

#endif /* _HAL_RTC_H_ */
