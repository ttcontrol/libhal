#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>
#include <string.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/netlink.h>
#include <sys/socket.h>

#include <linux/can.h>


#include <glib.h>

#include "libhal.h"

#define TEST_CAN_SEND            0
#define TEST_CAN_READ            1
#define TEST_CAN_READ2           2

static struct option options[] =
{
    {"help", no_argument, 0, '?'},
    {"channel", required_argument, 0, 'c'},
    {"baud", required_argument, 0, 'b'},
    {"test", required_argument, 0, 't'},
    {0, 0, 0, 0}
};

static struct sockaddr_can addr;
static int sock;

void print_help()
{
    printf("HAL CAN test\n" \
           "  -? --help        This message\n" \
           "  -c --channel=<n> CAN channel [0, 3] \n" \
           "  -b --baud=<n>    Baudrate:\n" \
           "                   0 ...  125 kbit/s\n" \
           "                   1 ...  250 kbit/s\n" \
           "                   2 ...  500 kbit/s\n" \
           "                   3 ...  800 kbit/s\n" \
           "                   4 ... 1000 kbit/s\n" \
           "  -t --test=<n>    Test program to run\n" \
           "                   0 ... Send a single message with ID 0 \n" \
           "                         and content 01 02 03 04 05 06 07 08\n" \
           "                         on specified CAN channel\n" \
           "                   1 ... Wait for a single message and print it\n" \
           "                   2 ... Poll and print all incoming messages\n" \
           "\n");
}

void leave(int sig)
{
    if (sig != 0)
    {
        printf("SIGINT caught. Exiting.\n");
    }
    hal_can_deinit();
    hal_deinit();
    exit(sig);
}

static void open_socket ( int channel )
{
    struct ifreq ifr;
    uint8_t *ifname;
    hal_error err;

    sock = socket(PF_CAN, SOCK_RAW, CAN_RAW);

    err = hal_can_get_ifname( channel, &ifname );

    if ( err != HAL_E_OK )
    {
        printf("Could not get interface name for channel %u\n", channel );
    }
    else
    {
        strcpy( ifr.ifr_name, ifname );
        ioctl(sock, SIOCGIFINDEX, &ifr);

        addr.can_family = AF_CAN;
        addr.can_ifindex = ifr.ifr_ifindex;

        bind(sock, (struct sockaddr *)&addr, sizeof(addr));
    }
}

static void test_can_write( int channel )
{
    int nbytes;
    struct can_frame frame;

    open_socket( channel );

    frame.can_id = 0;
    frame.can_dlc = 8;
    frame.data[0] = 1;
    frame.data[1] = 2;
    frame.data[2] = 3;
    frame.data[3] = 4;
    frame.data[4] = 5;
    frame.data[5] = 6;
    frame.data[6] = 7;
    frame.data[7] = 8;

    nbytes = sendto(sock, &frame, sizeof(struct can_frame),
                    0, (struct sockaddr*)&addr, sizeof(addr));

}

static void test_can_read2(int channel)
{
    int ret;
    int nbytes;
    struct can_frame frame;

    open_socket( channel );
        

    printf("Waiting for messages....\n");
    while (TRUE)
    {       
        nbytes = read(sock, &frame, sizeof(struct can_frame));

        if (nbytes < 0) {
            perror("can raw socket read");
            return 1;
        }

        /* paranoid check ... */
        if (nbytes < sizeof(struct can_frame)) {
            fprintf(stderr, "read: incomplete CAN frame\n");
            return 1;
        }
        
        printf("%X %X %X %X %X %X %X %X\n"
              , frame.can_id
              , frame.data[0]
              , frame.data[1]
              , frame.data[2]
              , frame.data[3]
              , frame.data[4]
              , frame.data[5]
              , frame.data[6]
              , frame.data[7] );    
    }
}


static void test_can_read( int channel )
{
    int ret;
    int nbytes;
    struct can_frame frame;

    open_socket( channel );

    memset ( frame.data, 0, 8 );
    ret = recvfrom( sock, &frame, sizeof(frame), MSG_WAITFORONE, NULL, NULL);

    if ( ret > 0 )
    {
        printf("Received message with ID %X and content:\n%X %X %X %X %X %X %X %X\n"
              , frame.can_id
              , frame.data[0]
              , frame.data[1]
              , frame.data[2]
              , frame.data[3]
              , frame.data[4]
              , frame.data[5]
              , frame.data[6]
              , frame.data[7] );
    }
}


int main(int argc, char **argv)
{
    hal_error err;
    int option_index = 0;
    int c = 0;
    uint8_t val;
    int channel;
    int baud;
    int test_program;

    while (c != -1)
    {
        option_index = 0;
        c = getopt_long(argc, argv, "?c:b:t:", options,
                        &option_index);

        switch (c)
        {
            case '?':
                print_help();
                exit(0);
                break;
            case 't':
                if (sscanf(optarg, "%d", &test_program) == 0)
                {
                    printf("Missing: Test program missing.");
                    exit(-1);
                }
                break;
            case 'c':
                if (sscanf(optarg, "%d", &channel) == 0)
                {
                    printf("Missing: CAN Channel");
                    exit(-1);
                }
                break;
            case 'b':
                if (sscanf(optarg, "%d", &baud) == 0)
                {
                    printf("Missing: Baudrate");
                    exit(-1);
                }
                break;
        }
    }

    signal(SIGINT, leave);

    err = hal_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize HAL (%d)\n", err);
        exit(-1);
    }

    err = hal_can_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize CAN module (%d)\n", err);
        exit(-1);
    }

    err = hal_can_init_channel( channel, baud );
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize CAN channel %u with baudrate %u (%u)\n", channel, baud, err );
        exit(-1);
    }

    switch (test_program)
    {
        case TEST_CAN_SEND:
            test_can_write( channel );
            break;
        case TEST_CAN_READ:
            test_can_read( channel );
            break;
        case TEST_CAN_READ2:
            test_can_read2( channel );
            break;        
        default:
            printf("Undefined test program! Abort.");
            break;
    }
    leave(0);
}
