/**************************************************************************
 * Copyright (c) 2013 TTControl. All rights reserved. Confidential proprietary
 * Schoenbrunnerstrasse 7, A-1040 Wien, Austria. office[at]ttcontrol.com
 **************************************************************************/
/**********************************************************************//**
 * \file hal_block.h
 *
 * \brief Hardware Abstraction Layer for block device support.
 *
 **************************************************************************/

#ifndef HAL_blockdevice_H
#define HAL_blockdevice_H

#ifdef __cplusplus
extern "C" {
#endif

#include "hal_types.h"

/** \defgroup block Block devices
 *  \brief Functions and datastructures for block devices
 *  \{
 */

 /** \brief Callback prototype for block devices add events
 *
 *  Called if a block device is added. Use
 *  #hal_blockdevice_register_callback_device_add() to subscribe
 *  to event.
 *
 *  \param[in] uuid Pointer to unique identifier for device. Save
 *                  somewhere, all other functions take this as an
 *                  input.
 */
typedef void (*hal_blockdevice_device_add_func)(uint8_t const * const uuid);

 /** \brief Callback prototype for block devices remove events
 *
 *  Called if a block device is removed. Use
 *  #hal_blockdevice_register_callback_device_remove() to subscribe
 *  to event.
 *
 *  \param[in] uuid Pointer to unique identifier for device.
 */
typedef void (*hal_blockdevice_device_remove_func)(uint8_t const * const uuid);

/** \brief Initialize block devices module
 *
 *  Before using this module, hal_udev_init() must be called
 *  successfully.
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
hal_error hal_blockdevice_init();

/** \brief Deinitialize block devics module
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
hal_error hal_blockdevice_deinit();

/**
 * \deprecated This function has been deprecated.
 *             Use #hal_blockdevice_register_callback_device_add() instead.
 */
hal_error hal_blockdevice_register_callback_device_new(
    hal_blockdevice_device_add_func func);

/**
 * \brief Registers a callback if a block device is added
 *
 * The registered function is called if a mountable block device
 * is added to the system. If access to the device is desired,
 * #hal_blockdevice_mount() must be called.
 *
 * \param[in] device Pointer to the callback function
 *
 * \return #hal_error
 * \retval #HAL_E_OK                  Everything fine
 * \retval #HAL_E_NOT_INITIALIZED     Module not initialized
 */
hal_error hal_blockdevice_register_callback_device_add(
    hal_blockdevice_device_add_func func);

/**
 * \brief Registers a callback if a block device is removed
 *
 * The registered function is called if a block device
 * is removed from the system. If the device was not unmounted, this could
 * have caused data loss.
 *
 * \param[in] device Pointer to the callback function
 *
 * \return #hal_error
 * \retval #HAL_E_OK                  Everything fine
 * \retval #HAL_E_NOT_INITIALIZED     Module not initialized
 */
hal_error hal_blockdevice_register_callback_device_remove(
    hal_blockdevice_device_remove_func func);

/**
 * \deprecated This function has been deprecated.
               Use #hal_blockdevice_unregister_callback_device_add() instead.
 */

hal_error hal_blockdevice_unregister_callback_device_new(
    hal_blockdevice_device_add_func func);

/**
 * \brief Unregisters a callback if a block device is added
 *
 * \param[in] device Pointer to the registered callback function
 *
 * \return #hal_error
 * \retval #HAL_E_OK                  Everything fine
 * \retval #HAL_E_NOT_INITIALIZED     Module not initialized
 * \retval #HAL_E_FAIL                Could not unregister
 */
hal_error hal_blockdevice_unregister_callback_device_add(
    hal_blockdevice_device_add_func func);

/**
 * \brief Unregisters a callback if a block device is removed
 *
 * \param[in] device Pointer to the registered callback function
 *
 * \return #hal_error
 * \retval #HAL_E_OK                  Everything fine
 * \retval #HAL_E_NOT_INITIALIZED     Module not initialized
 * \retval #HAL_E_FAIL                Could not unregister
 */
hal_error hal_blockdevice_unregister_callback_device_remove(
    hal_blockdevice_device_add_func func);

/**
 * \deprecated Use hal_blockdevice_mount2 instead!
 * \brief Mounts a block device
 *
 * Mounts a block device, thus making its data available in the
 * filesystem. The directory where the data can be accessed is
 * returned.
 *
 * \param[in]  uuid        Pointer to unique device identification
 * \param[out] mountpoint  Pointer to string where the location in
 *                         filesystem shall be stored.
 *
 * \return #hal_error
 * \retval #HAL_E_OK                  Everything fine
 * \retval #HAL_E_NOT_INITIALIZED     Module not initialized
 * \retval #HAL_E_FAIL                Could not mount
 */
hal_error hal_blockdevice_mount(uint8_t const * const uuid,
    uint8_t const ** mountpoint);

/**
 * \brief Mounts a block device
 *
 * Mounts a block device, thus making its data available in the
 * filesystem. The directory where the data can be accessed is
 * returned.
 *
 * \param[in]  uuid                Pointer to unique device identification
 * \param[out] mountpoint          Pointer to buffer where the location in
 *                                 filesystem shall be stored.
 * \param[in,out] mountpoint_size  The size of the buffer mountpoint points
 *                                 to; returns the actual size of mountpoint
 *
 * \return #hal_error
 * \retval #HAL_E_OK                  Everything fine
 * \retval #HAL_E_NOT_INITIALIZED     Module not initialized
 * \retval #HAL_E_FAIL                Could not mount
 */
hal_error hal_blockdevice_mount2(uint8_t const * const uuid,
    uint8_t * const mountpoint, uint32_t * const mountpoint_size);
/**
 * \brief Mounts a block device
 *
 * Mounts a block device, thus making its data available in the
 * filesystem. The directory where the data can be accessed is
 * returned. This function allow mount options (in the form of FLAGS).
 * 
 *
 * \param[in]  mount_flags         Pointer to mount flags, Ex. MS_RDONLY
 * \param[in]  uuid                Pointer to unique device identification
 * \param[out] mountpoint          Pointer to buffer where the location in
 *                                 filesystem shall be stored.
 * \param[in,out] mountpoint_size  The size of the buffer mountpoint points
 *                                 to; returns the actual size of mountpoint
 *
 * \return #hal_error
 * \retval #HAL_E_OK                  Everything fine
 * \retval #HAL_E_NOT_INITIALIZED     Module not initialized
 * \retval #HAL_E_FAIL                Could not mount
 * \retval #HAL_E_INVALID_PARAMETER
 */
hal_error hal_blockdevice_mount3(uint8_t const * const uuid, uint8_t * const mountpoint, 
                                 uint32_t * const mountpoint_size, unsigned long int mount_flags);
    
/**
 * \brief Ejects (unmounts) a block device
 *
 * Unmounts a block device that has previously been mounted by a call
 * to #hal_blockdevice_mount(). After this call it is safe to remove
 * the device hardware from the system.
 *
 * \param[in]  uuid        Pointer to unique device identification
 *
 * \return #hal_error
 * \retval #HAL_E_OK                  Everything fine
 * \retval #HAL_E_NOT_INITIALIZED     Module not initialized
 * \retval #HAL_E_FAIL                Could not unmount
 */
hal_error hal_blockdevice_eject(uint8_t const * const uuid);

/**
 * \brief Gets the capacity of a mounted block device
 *
 * Returns total and free storage size in bytes of a mounted block
 * device.
 *
 * \param[in]  uuid        Pointer to unique device identification
 * \param[out] capacity    Pointer to value where total capacity
 *                         shall be stored
 * \param[out] free        Pointer to value where total free space
 *                         shall be stored
 *
 * \return #hal_error
 * \retval #HAL_E_OK                  Everything fine
 * \retval #HAL_E_NOT_INITIALIZED     Module not initialized
 * \retval #HAL_E_FAIL                Could not get values
 */
hal_error hal_blockdevice_get_capacity(uint8_t const * const uuid,
    uint64_t * const capacity, uint64_t * const free);

/**
 * \brief Queries information about a block device
 *
 * Returns some information related to the block device. All output pointers
 * can be NULL if the value is not needed.
 *
 * \param[in]  uuid                 Pointer to unique device identification
 * \param[out] device_node          Pointer to value where the device node
 *                                  path shall be stored
 * \param[in,out] device_node_size  The size of the buffer device_node points
 *                                  to; returns the actual size of device_node
 * \param[out] fs_type              Pointer to value where the filesystem type
 *                                  path shall be stored
 * \param[in,out] fs_type__size     The size of the buffer fs_type points
 *                                  to; returns the actual size of fs_type
 * \param[out] mountpoint           Pointer to value where the mount point
 *                                  path shall be stored
 * \param[in,out] mountpoint_size   The size of the buffer mountpoint points
 *                                  to; returns the actual size of mountpoint
 *
 * \return #hal_error
 * \retval #HAL_E_OK                  Everything fine
 * \retval #HAL_E_NOT_INITIALIZED     Module not initialized
 * \retval #HAL_E_FAIL                Could not get values
 */
hal_error hal_blockdevice_query(uint8_t const * const uuid,
    uint8_t * const device_node, uint32_t * const device_node_size,
    uint8_t * const fs_type, uint32_t * const fs_type_size,
    uint8_t * const mountpoint, uint32_t * const mountpoint_size);

/**
 * \brief Get UUID of SD card
 *
 * Gets the UUID if an SD card is present. Returns an empty string if
 * no SD card is present.
 *
 * \param[in]  uuid                 Pointer to buffer where unique device
 *                                  identification shall be stored
 * \param[in,out] uuid_size         The size of the buffer uuid points
 *                                  to; returns the actual size of uuid
 *
 * \return #hal_error
 * \retval #HAL_E_OK                  Everything fine
 * \retval #HAL_E_NOT_INITIALIZED     Module not initialized
 * \retval #HAL_E_FAIL                Could not get values
 */
hal_error hal_blockdevice_get_mmc_uuid(uint8_t * const uuid,
    uint32_t * const uuid_size);

/** \} */

#ifdef __cplusplus
}  /* End of the 'extern "C"' block */
#endif

#endif /* HAL_blockdevice_H */
