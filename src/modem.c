

#include "hal.h"
#include "hal_types.h"
#include "tools.h"
#include "scom.h"


#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <syslog.h>
#include <poll.h>
#include <termios.h>
#include <string.h>
#include <glib.h>
#include <glib/gprintf.h>


//#define DEBUG
//#define DEBUG_SMS

#define MODULE_NAME               "[modem] "
#define BUF_SIZE                  1024
#define NEWLINE                  "\r\n"
#define SHORT_TIMEOUT             5000 /* to avoid problems with the EHS8 increase the timeout on AT commands*/
#define MEDIUM_TIMEOUT            7000
#define LONG_TIMEOUT              10000


static gboolean initialized = FALSE;

static int fd_tty = -1;

hal_error hal_modem_scom_init() {
    hal_error err;
    uint32_t cap;
    hal_modem_result_code res;

    err = hal_scom_init();

    if (err != HAL_E_OK)
    {
        return err;
    }

    err = hal_syscfg_get_capability(HAL_SYSCAP_MODEM, &cap);
    if (err != HAL_E_OK)
    {
        syslog(LOG_ERR, MODULE_NAME "Could not get system capability: %d", err);
        return err;
    }

    syslog(LOG_ERR, MODULE_NAME "cap: %d", cap);

    if (cap == SYSCFG_MODEM_INTERNAL_EHS8)
    {
        // Switch modem to GSM only to enable data calls
        err = hal_scom_transfer("AT^SXRAT=0", '\r', 10000, NULL, 0, &res);
        if (err != HAL_E_OK || res != HAL_MODEM_RES_OK)
        {
            syslog(LOG_ERR, MODULE_NAME "Could not switch to GSM mode");
            return err;
        }
    }

    return err;
}
hal_error hal_modem_scom_deinit() {
    hal_error err;
    err = hal_scom_deinit();
    return err;
}

hal_error hal_modem_init()
{
    hal_modem_result_code res;
    char buf[BUF_SIZE];
    hal_error err;

    CHECK_HAL_INITIALIZED();
    CHECK_MODULE_ALREADY_INITIALIZED(initialized);

    err = hal_modem_scom_init();

    if ( (err == HAL_E_OK) || (err == HAL_E_ALREADY_INITIALIZED)  )
    {
        // set factory defaults
        hal_scom_transfer("AT&F", '\r', LONG_TIMEOUT, buf, BUF_SIZE, &res);
        // set to text mode
        err = hal_scom_transfer("AT+CMGF=1", '\r', SHORT_TIMEOUT, buf, BUF_SIZE, &res);

        if (res != HAL_MODEM_RES_OK || err != HAL_E_OK)
        {
            err = HAL_E_FAIL;
            hal_log(HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not switch to text mode");
            initialized = FALSE;
            return err;
        }
        else
        {
            err = HAL_E_OK;
        }
        initialized = TRUE;
    }
    else
    {
        err = HAL_E_FAIL;
        hal_log(HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not initialize SCOM");
        initialized = FALSE;
    }
    return err;
}

hal_error hal_modem_deinit()
{

    hal_error err;
    CHECK_MODULE_INITIALIZED( initialized );

    err = hal_modem_scom_deinit();

    if ( err != HAL_E_OK && err != HAL_E_NOT_INITIALIZED && err != HAL_E_NOFEATURE)
    {
        initialized = TRUE;
    }
    else {
        initialized = FALSE;
    }
    return err;
}

hal_error hal_modem_get_registration( hal_modem_registration * const reg
                                    , hal_modem_result_code * const result )
{
    hal_modem_result_code res;
    char response[BUF_SIZE];
    hal_error err;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( reg );

    err = hal_scom_transfer("AT+CREG?", '\r', SHORT_TIMEOUT, response, BUF_SIZE, &res );

    SET_IF_NOT_NULL( result, res );

    if ( err == HAL_E_OK )
    {
        err = HAL_E_OK;
        if ( g_strcmp0(response, "+CREG: 0,0") == 0 )
        {
            *reg = HAL_MODEM_NOT_REGISTERED;
        }
        else if ( g_strcmp0(response, "+CREG: 0,1") == 0 )
        {
            *reg = HAL_MODEM_HOME_NETWORK;
        }
        else if ( g_strcmp0(response, "+CREG: 0,2") == 0 )
        {
            *reg = HAL_MODEM_SEARCHING;
        }
        else if ( g_strcmp0(response, "+CREG: 0,3") == 0 )
        {
            *reg = HAL_MODEM_DENIED;
        }
        else if ( g_strcmp0(response, "+CREG: 0,4") == 0 )
        {
            *reg = HAL_MODEM_UNKNOWN;
        }
        else if ( g_strcmp0(response, "+CREG: 0,5") == 0 )
        {
            *reg = HAL_MODEM_ROAMING;
        }
        else
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Unknown response for AT+CREG?: %s", response );
            err = HAL_E_FAIL;
        }
    }
    else
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not retrieve network registration" );
        err =  HAL_E_FAIL;
    }

    return err;
}

hal_error hal_modem_get_operator_name( uint8_t * const operator_name
                                     , hal_modem_result_code * const result )
{
    hal_modem_result_code res;
    char response[BUF_SIZE];
    hal_error err;
    char *tmp;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( operator_name );

    err = hal_scom_transfer("AT+COPS?", '\r',SHORT_TIMEOUT, response, BUF_SIZE, &res );

    SET_IF_NOT_NULL( result, res );

    if ( err == HAL_E_OK )
    {
        tmp = g_strrstr( response, "\"" );
        if ( tmp != NULL )
        {
            *tmp = '\0';
            tmp = g_strrstr( response, "\"" );
            if ( tmp != NULL )
            {
                g_stpcpy( operator_name, (tmp+1) );
                err = HAL_E_OK;
            }
            else
            {
                hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                    "Could not parse provider name response: %s", response );
                err = HAL_E_FAIL;
            }
        }
        else
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Could not parse provider name response: %s", response );
            err = HAL_E_FAIL;
        }
    }
    else
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not retrieve provider name" );
        err =  HAL_E_FAIL;
    }

    return err;
}

hal_error hal_modem_get_operator_name2( uint8_t * const operator_name
                                      , uint32_t * const operator_name_size
                                      , hal_modem_result_code * const result )
{
    hal_modem_result_code res;
    char response[BUF_SIZE];
    hal_error err;
    char *tmp;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( operator_name );
    CHECK_POINTER_PARAMETER( operator_name_size );

    err = hal_scom_transfer("AT+COPS?", '\r',SHORT_TIMEOUT, response, BUF_SIZE, &res );

    SET_IF_NOT_NULL( result, res );

    if ( err == HAL_E_OK )
    {
        tmp = g_strrstr( response, "\"" );
        if ( tmp != NULL )
        {
            *tmp = '\0';
            tmp = g_strrstr( response, "\"" );
            if ( tmp != NULL )
            {
                *operator_name_size = g_strlcpy(operator_name, (tmp+1), *operator_name_size);
                err = HAL_E_OK;
            }
            else
            {
                hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                    "Could not parse provider name response: %s", response );
                err = HAL_E_FAIL;
            }
        }
        else
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Could not parse provider name response: %s", response );
            err = HAL_E_FAIL;
        }
    }
    else
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not retrieve provider name" );
        err =  HAL_E_FAIL;
    }

    return err;
}

hal_error hal_modem_get_imei( uint8_t * const imei
                            , hal_modem_result_code * const result )
{
    hal_modem_result_code res;
    char response[BUF_SIZE];
    hal_error err;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( imei );

    err = hal_scom_transfer("AT+CGSN", '\r',SHORT_TIMEOUT, response, BUF_SIZE, &res );
    SET_IF_NOT_NULL( result, res );

    if ( err == HAL_E_OK )
    {
        g_stpcpy( imei, response );
    }
    else
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not retrieve IMEI" );
        err = HAL_E_FAIL;
    }

    return err;
}

hal_error hal_modem_get_imei2( uint8_t * const imei
                             , uint32_t * const imei_size
                             , hal_modem_result_code * const result )
{
    hal_modem_result_code res;
    char response[BUF_SIZE];
    hal_error err;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( imei );
    CHECK_POINTER_PARAMETER( imei_size );

    err = hal_scom_transfer("AT+CGSN", '\r',SHORT_TIMEOUT, response, BUF_SIZE, &res );
    SET_IF_NOT_NULL( result, res );

    if ( err == HAL_E_OK )
    {
        *imei_size = g_strlcpy(imei, response, *imei_size);
    }
    else
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not retrieve IMEI" );
        err = HAL_E_FAIL;
    }

    return err;
}

hal_error hal_modem_get_imsi( uint8_t * const imsi
                            , hal_modem_result_code * const result )
{
    hal_modem_result_code res;
    char response[BUF_SIZE];
    hal_error err;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( imsi );

    err = hal_scom_transfer("AT+CIMI", '\r',SHORT_TIMEOUT, response, BUF_SIZE, &res );
    SET_IF_NOT_NULL( result, res );

    if ( err == HAL_E_OK )
    {
        g_stpcpy( imsi, response );
    }
    else
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not retrieve IMSI" );
        err = HAL_E_FAIL;
    }

    return err;
}

hal_error hal_modem_get_imsi2( uint8_t * const imsi
                             , uint32_t * const imsi_size
                             , hal_modem_result_code * const result )
{
    hal_modem_result_code res;
    char response[BUF_SIZE];
    hal_error err;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( imsi );
    CHECK_POINTER_PARAMETER( imsi_size );

    err = hal_scom_transfer("AT+CIMI", '\r', SHORT_TIMEOUT, response, BUF_SIZE, &res );
    SET_IF_NOT_NULL( result, res );

    if ( err == HAL_E_OK )
    {
        *imsi_size = g_strlcpy(imsi, response, *imsi_size);
    }
    else
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not retrieve IMSI" );
        err = HAL_E_FAIL;
    }

    return err;
}

hal_error hal_modem_get_signal_quality( uint8_t * const quality
                                      , hal_modem_result_code * const result )
{
    hal_modem_result_code res;
    char response[BUF_SIZE];
    char *tmp;
    uint8_t csq;
    hal_error err;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( quality );

    err = hal_scom_transfer("AT+CSQ", '\r', SHORT_TIMEOUT, response, BUF_SIZE, &res );
    SET_IF_NOT_NULL( result, res );

    if ( err == HAL_E_OK )
    {
        tmp = strstr( response, "+CSQ: ");

        if ( tmp != NULL )
        {
            tmp += strlen("+CSQ: ");
            if ( sscanf( tmp, "%u", &csq) == 1 )
            {
                *quality = csq;
                err = HAL_E_OK;
            }
            else
            {
                hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                    "Could not parse signal quality" );
                err = HAL_E_FAIL;
            }
        }
        else
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Could not parse signal quality" );
            err = HAL_E_FAIL;
        }
    }
    else
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not retrieve signal quality" );
        err = HAL_E_FAIL;
    }

    return err;
}


hal_error hal_modem_get_pin_status( hal_modem_pin_status * const status
                                  , hal_modem_result_code * const result )
{
    hal_modem_result_code res;
    char response[BUF_SIZE];
    char *tmp;
    uint8_t csq;
    hal_error err;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( status );

    err = hal_scom_transfer("AT+CPIN?", '\r', SHORT_TIMEOUT, response, BUF_SIZE, &res );
    SET_IF_NOT_NULL( result, res );

    if ( err == HAL_E_OK )
    {
        if ( g_strcmp0(response, "+CPIN: READY") == 0 )
        {
            *status = HAL_MODEM_PIN_STATUS_READY;
        }
        else if ( g_strcmp0(response, "+CPIN: SIM PIN") == 0 )
        {
            *status = HAL_MODEM_PIN_STATUS_WAITING_PIN;
        }
        else
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Unknown PIN status (%s)", response);
            *status = HAL_MODEM_PIN_STATUS_UNKNOWN;
            err = HAL_E_FAIL;
        }
    }
    else
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not retrieve PIN status" );
    }

    return err;
}

hal_error hal_modem_enter_pin( const uint8_t * const pin
                             , hal_modem_result_code * const result )
{
    hal_modem_result_code res;
    char response[BUF_SIZE];

    char *tmp;
    hal_error err;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( pin );

    tmp = g_strdup_printf( "AT+CPIN=%s", pin );
    err = hal_scom_transfer( tmp, '\r', LONG_TIMEOUT, response, BUF_SIZE, &res );
    g_free( tmp );

    SET_IF_NOT_NULL( result, res );

    if ( err != HAL_E_OK )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "PIN (%s) not accepted", pin );
    }

    return err;
}

hal_error hal_modem_send_sms( const uint8_t * const number
                            , const uint8_t * const text
                            , hal_modem_result_code * const result )
{
    hal_modem_result_code res;
    char buf[BUF_SIZE];
    char response[BUF_SIZE];

    char *tmp;
    char termination;
    hal_error err;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( number );
    CHECK_POINTER_PARAMETER( text );

    hal_scom_lock();

    // Send command for tel number and wait for ">"
    tmp = g_strdup_printf( "AT+CMGS=\"%s\"", number );
    err = hal_scom_transfer_and_wait_no_lock(tmp, '\r', MEDIUM_TIMEOUT, ">");
    g_free( tmp );

    if ( err == HAL_E_OK )
    {
#ifdef DEBUG_SMS
        // Attach 0x1B instead of 0x1A => SMS gets not sent
        termination = 0x1B;
#else
        termination = 0x1A;
#endif
        // Send text and wait for result code.
        // Timeout is set very high, as sending a SMS can take considerable amount of time.
        err = hal_scom_transfer_no_lock((char*)text, termination, 10000, response, BUF_SIZE, &res);
        SET_IF_NOT_NULL( result, res );

        if ( err != HAL_E_OK )
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "SMS send failed - step 2");
        }
    }
    else
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME"SMS send failed - step 1");
        err = HAL_E_FAIL;
    }

    hal_scom_unlock();

    return err;
}
