/**************************************************************************
 * Copyright (c) 2013 TTTControl. All rights reserved. Confidential proprietary
 * Schoenbrunnerstrasse 7, A-1040 Wien, Austria. office[at]ttcontrol.com
 **************************************************************************/
/**********************************************************************//**
 * \file hal_rs232.h
 *
 * \brief Hardware Abstraction Layer for RS232 interface.
 *
 *  The device path which must be used is obtained through
 *  hal_rs232_get_device().
 *
 *  The RS232 interface of the HY-eVision² can be accessed directly with
 *  Linux functions (see termios.h for details).
 *
 **************************************************************************/

#ifndef _HAL_RS232_H_
#define _HAL_RS232_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "hal_types.h"

/** \defgroup rs232 RS232
 *  \brief Functions for RS232
 *  \{
 */

/**
 * \brief Returns the RS232's device file
 *
 * Use this function to get the correct device file for the RS232 interface.
 *
 * \param[out] device Pointer to string that contains the device file path
 *
 * \return #hal_error
 * \retval #HAL_E_OK                  Everything fine
 * \retval #HAL_E_NOFEATURE           Current hardware has no RS232
 * \retval #HAL_E_NOT_INITIALIZED     HAL has not been initialized
 * \retval #HAL_E_NULL_POINTER        A NULL pointer has been passed
 */
hal_error hal_rs232_get_device(uint8_t const ** device);

/** \} */

#ifdef __cplusplus
}  /* End of the 'extern "C"' block */
#endif

#endif /* _HAL_RTC_H_ */
