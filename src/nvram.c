#include "hal_nvram.h"
#include "hal.h"
#include "hal_types.h"
#include "tools.h"

#include <linux/input.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <syslog.h>
#include <glib.h>

#define MODULE_NAME                 "[nvram] "
#define NVRAM_FILE                  "/var/links/eeprom"
#define NVRAM_WRITE_PROT_GPIO       "/var/links/gpio/eeprom-write-enable/value"
#define NVRAM_SIZE                  4096

static int fd_nvram = -1;
static int fd_nvram_write_prot = -1;

static gboolean cap_has_nvram = FALSE;

static gboolean initialized = FALSE;
static GMutex mutex;
static gboolean current_write_protection;

static hal_error check_nvram_caps()
{
    hal_error err = HAL_E_OK;
    uint32_t cap;

    cap_has_nvram = FALSE;
    err = hal_syscfg_get_capability( HAL_SYSCAP_NVRAM, &cap );
    if ( err != HAL_E_OK )
    {
        return HAL_E_FAIL;
    }
    
    switch ( cap )
    {
        case SYSCFG_NVRAM_RAMTRON_32KBIT:
        case SYSCFG_NVRAM_RAMTRON_64KBIT:
        case SYSCFG_ALL_FEATURES:
            cap_has_nvram = TRUE;
            break;
        default:
            err = HAL_E_NOFEATURE;
            break;
    }
    
    return err;
}

hal_error hal_nvram_init()
{
    hal_error err = HAL_E_OK;
    
    CHECK_HAL_INITIALIZED();
    CHECK_MODULE_ALREADY_INITIALIZED( initialized );

    err = check_nvram_caps();
    if (err == HAL_E_FAIL)
    {
        return HAL_E_FAIL;
    }
    else if (err == HAL_E_NOFEATURE)
    {
        initialized = TRUE;
        return HAL_E_NOFEATURE;
    }

    fd_nvram = open( NVRAM_FILE, O_RDWR );
    if ( fd_nvram == -1 )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not open NVRAM device node " NVRAM_FILE ": %d", errno );
        return HAL_E_FAIL;
    }

    fd_nvram_write_prot = open( NVRAM_WRITE_PROT_GPIO, O_RDWR );
    if ( fd_nvram_write_prot == -1 )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not open NVRAM write protection gpio "
            NVRAM_WRITE_PROT_GPIO ": %d", errno );
        return HAL_E_FAIL;
    }

    initialized = TRUE;

    current_write_protection = FALSE;
    hal_nvram_set_writeprotection(TRUE);

    return HAL_E_OK;
}

hal_error hal_nvram_deinit()
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_nvram );

    /* Set write protection deinit */
    hal_nvram_set_writeprotection( TRUE );

    if ( fd_nvram != -1 )
    {
        close( fd_nvram );
        fd_nvram = -1;
    }

    if ( fd_nvram_write_prot != -1 )
    {
        close( fd_nvram_write_prot );
        fd_nvram_write_prot = -1;
    }

    initialized = FALSE;

    return HAL_E_OK;
}

hal_error hal_nvram_get_size( uint32_t * const size)
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_nvram );
    CHECK_POINTER_PARAMETER( size );

    *size = NVRAM_SIZE;

    return HAL_E_OK;
}

hal_error hal_nvram_set_writeprotection( bool prot )
{
    hal_error err;
    
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_nvram );

    if (current_write_protection == prot)
    {
        return HAL_E_OK;
    }

    if ( prot == TRUE )
    {
        g_mutex_lock( &mutex );
        err = file_set_value_int( fd_nvram_write_prot, 0 );
        g_mutex_unlock( &mutex );
    }
    else
    {
        g_mutex_lock( &mutex );
        err = file_set_value_int( fd_nvram_write_prot, 1 );
        g_mutex_unlock( &mutex );
    }

    if ( err != HAL_E_OK )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not set write protection: %u", prot );
    }
    else
    {
        current_write_protection = prot;
    }

    return err;
}

hal_error hal_nvram_write( uint32_t offset
                         , uint32_t length
                         , const uint8_t * const data
                         , uint32_t * const bytes_written )
{
    hal_error err;
    int nvram_size;
    int res;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_nvram );
    CHECK_POINTER_PARAMETER( data );
    CHECK_POINTER_PARAMETER( bytes_written );

    err = hal_nvram_get_size( &nvram_size );

    if ( err == HAL_E_OK )
    {
        if ( offset + length > nvram_size )
        {
            err = HAL_E_INVALID_PARAMETER;
        }
        else
        {
            g_mutex_lock( &mutex );
            res = pwrite( fd_nvram, data, length, offset );
            g_mutex_unlock( &mutex );

            if ( res < 0 )
            {
                err = HAL_E_FAIL;
                hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                    "Error during write: %d", errno );

            }
            else if ( res != length )
            {
                err = HAL_E_NVRAM_INCOMPLETE_WRITE;
                *bytes_written = res;
                hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                    "Could only write %d bytes; requested %u bytes", res, length );
            }
            else
            {
                *bytes_written = res;
                err = HAL_E_OK;
            }
        }
    }

    return err;
}

hal_error hal_nvram_read ( uint32_t offset
                         , uint32_t length
                         , uint8_t * const data
                         , uint32_t * const bytes_read )
{
    hal_error err;
    int nvram_size;
    int res;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_nvram );
    CHECK_POINTER_PARAMETER( data );
    CHECK_POINTER_PARAMETER( bytes_read );

    err = hal_nvram_get_size( &nvram_size );

    if ( err == HAL_E_OK )
    {
        if ( offset + length > nvram_size )
        {
            err = HAL_E_INVALID_PARAMETER;
        }
        else
        {
            g_mutex_lock( &mutex );
            res = pread( fd_nvram, data, length, offset );
            g_mutex_unlock( &mutex );

            if ( res < 0 )
            {
                err = HAL_E_FAIL;
                hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                    "Error during read: %d", errno );
            }
            else if ( res != length )
            {
                err = HAL_E_NVRAM_INCOMPLETE_READ;
                *bytes_read = res;
                hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                    "Could only read %d bytes; requested %u bytes", res, length );
            }
            else
            {
                *bytes_read = res;
                err = HAL_E_OK;
            }
        }
    }

    return err;
}
