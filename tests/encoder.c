#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>

#include <glib.h>

#include "libhal.h"

static struct option options[] =
{
    {"help", no_argument, 0, '?'},
    {"ignore-syscfg", no_argument, 0, 'S'},
    {0, 0, 0, 0}
};

void print_help()
{
    printf("HAL encoder test\n" \
           "  -? --help           This message\n" \
           "  -S --ignore-syscfg  Ignore system config (act as test device)\n" \
           "\n");
}

void leave(int sig)
{
    if (sig != 0)
    {
        printf("SIGINT caught. Exiting.\n");
    }
    hal_encoder_deinit();
    hal_deinit();
    exit(sig);
}

int main(int argc, char **argv)
{
    hal_error err;
    int option_index = 0;
    int c = 0;
    int bl;
    uint8_t const * encoder_device;
    bool ignore_syscfg = FALSE;
    
    bool pressed = FALSE;
    uint16_t count = 0;

    while (c != -1)
    {
        option_index = 0;
        c = getopt_long(argc, argv, "?S", options,
                        &option_index);

        switch (c)
        {
            case '?':
                print_help();
                exit(0);
                break;
            case 'S':
                ignore_syscfg = TRUE;
                break;
        }
    }

    signal(SIGINT, leave);
    
    err = hal_init_internal( ignore_syscfg );
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize HAL (%d)\n", err);
        exit(-1);
    }

    err = hal_encoder_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize encoder module (%d)\n", err);
        exit(-1);
    }
    
    hal_encoder_get_device(&encoder_device);
    printf("Using encoder: %s\n", encoder_device);

    while( TRUE )
    {
        for ( int i = 0; i < 3; i++ )
        {
            hal_encoder_get_key_state( i, &pressed, &count );
            printf("Key%u: pressed: %u count: %u\n", i, pressed, count );
        }
        g_usleep( 1 * G_USEC_PER_SEC );
    }

    leave(0);
}
