#include "hal_led.h"
#include "hal.h"
#include "hal_types.h"
#include "tools.h"

#include <linux/input.h>
#include <sys/types.h>
#include <sys/sysctl.h>
#include <sys/stat.h>
#include <sys/sysinfo.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <syslog.h>
#include <glib.h>
#include <sys/statvfs.h>
#include <ifaddrs.h>
#include <netdb.h>

#define MODULE_NAME "[device] "
#define APP_PARTITION_MOUNT_POINT       "/app"
#define DEVICE_SERIAL_FILE              "/proc/vision2/serial/device"
#define DEVICE_RELEASE_VERSION_FILE     "/proc/vision2/release"
#define DEVICE_ID_FILE                  "/proc/vision2/id"

#define IF_ETHERNET                     "eth0"
#define IF_WLAN                         "wlan0"

static gboolean initialized = FALSE;
static gchar *serial = NULL;
static guint32 *release_version = 0;
static guint16 device_id = 0;

static guint8 eth_mac_address[HAL_DEVICE_MAC_SIZE];
static guint8 wlan_mac_address[HAL_DEVICE_MAC_SIZE];

static hal_error get_ethernet_address( gchar * if_name, guint8 * mac_addr )
{
    int sock;
    struct ifreq buf;

    CHECK_POINTER_PARAMETER( if_name );
    CHECK_POINTER_PARAMETER( mac_addr );

    sock = socket( PF_INET, SOCK_DGRAM, 0 );

    if ( sock == -1 )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not get ethernet"
            " address for %s: %d", if_name, errno );
        return HAL_E_FAIL;
    }
    memset( &buf, 0, sizeof(buf) );
    strcpy( buf.ifr_name, if_name );
    ioctl( sock, SIOCGIFHWADDR, &buf );
    close( sock );

    snprintf( mac_addr, HAL_DEVICE_MAC_SIZE, "%02x:%02x:%02x:%02x:%02x:%02x",
    buf.ifr_hwaddr.sa_data[0], buf.ifr_hwaddr.sa_data[1],
    buf.ifr_hwaddr.sa_data[2], buf.ifr_hwaddr.sa_data[3],
    buf.ifr_hwaddr.sa_data[4], buf.ifr_hwaddr.sa_data[5] );

    return HAL_E_OK;
}

static hal_error get_interface_addrs( gchar * if_name, uint8_t * ip_addr,
    uint8_t * netmask)
{
    struct ifaddrs *ifa, *e;
    int s;

    CHECK_POINTER_PARAMETER( if_name );
    CHECK_POINTER_PARAMETER( ip_addr );
    CHECK_POINTER_PARAMETER( netmask );

    if ( getifaddrs( &ifa ) == -1 )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not get interface addresses." );
        return HAL_E_FAIL;
    }

    for ( e = ifa; e != NULL; e = e->ifa_next )
    {
        if ( e->ifa_addr != NULL && e->ifa_addr->sa_family == AF_INET )
        {
            if ( strcmp( e->ifa_name, if_name ) == 0 )
            {
                s = getnameinfo( e->ifa_addr, sizeof( struct sockaddr_in ),
                    ip_addr, HAL_DEVICE_IP_SIZE, NULL, 0, NI_NUMERICHOST );
                if ( s != 0 )
                {
                    hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "ip: getnameinfo() failed: %s",
                        gai_strerror( s ) );
                    e = NULL;
                }

                s = getnameinfo(e->ifa_netmask, sizeof( struct sockaddr_in ),
                    netmask, HAL_DEVICE_IP_SIZE, NULL, 0, NI_NUMERICHOST );
                if ( s != 0 )
                {
                    hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "netmask: getnameinfo() failed: %s",
                        gai_strerror( s ) );
                    e = NULL;
                }

                break;
            }
        }
    }

    freeifaddrs( ifa );

    if ( e == NULL )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not find interface %s",
            if_name );
        return HAL_E_FAIL;
    }
    else
    {
        return HAL_E_OK;
    }
}

hal_error hal_device_init()
{
    GError *error = NULL;
    hal_error err = HAL_E_OK;
    gchar *buf;

    CHECK_HAL_INITIALIZED();
    CHECK_MODULE_ALREADY_INITIALIZED( initialized );

    initialized = FALSE;

    g_file_get_contents( DEVICE_SERIAL_FILE
                       , &serial
                       , NULL
                       , &error );

    if ( error != NULL )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not read device serial number: %s", error->message );

        err = HAL_E_FAIL;
        error = NULL;
    }

    g_file_get_contents( DEVICE_RELEASE_VERSION_FILE
                       , &buf
                       , NULL
                       , &error );

    if ( error == NULL )
    {
        if ( sscanf( buf, "%x", &release_version ) != 1 )
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Could not get release version. Got \"%s\"", buf );
        }
        g_free( buf );
    }
    else
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not read release version: %s", error->message );

        err = HAL_E_FAIL;
        error = NULL;
    }

    g_file_get_contents( DEVICE_ID_FILE
                       , &buf
                       , NULL
                       , &error );

    if ( error == NULL )
    {
        if ( sscanf( buf, "%hx", &device_id ) != 1 )
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Could not get device ID. Got \"%s\"", buf );
            err = HAL_E_FAIL;
        }
        g_free( buf );
    }
    else
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not read device id: %s", error->message );

        err = HAL_E_FAIL;
        error = NULL;
    }

    if ( err == HAL_E_OK )
    {
        err = get_ethernet_address( IF_ETHERNET, eth_mac_address );
    }

    if ( err == HAL_E_OK )
    {
        err = get_ethernet_address( IF_WLAN, wlan_mac_address );
    }

    if ( err == HAL_E_OK )
    {
        initialized = TRUE;
    }

    return err;
}

hal_error hal_device_deinit()
{
    CHECK_MODULE_INITIALIZED( initialized );

    initialized = FALSE;

    g_free( serial );

    return HAL_E_OK;
}

hal_error hal_device_get_ram_info( uint64_t * const capacity, uint64_t * const free )
{
    int ret;
    hal_error err;
    struct sysinfo info;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( capacity );
    CHECK_POINTER_PARAMETER( free );

    ret = sysinfo(&info);

    if ( ret == 0 )
    {
        *capacity = (uint64_t)info.totalram;
        *free = (uint64_t)info.freeram;
        err = HAL_E_OK;
    }
    else
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not get RAM info" );
        err = HAL_E_FAIL;
    }

    return err;
}

hal_error hal_device_get_flash_info( uint64_t * const capacity, uint64_t * const free )
{
    struct statvfs s;
    int ret;
    hal_error err;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( capacity );
    CHECK_POINTER_PARAMETER( free );

    ret = statvfs(APP_PARTITION_MOUNT_POINT, &s);

    if ( ret == 0 )
    {
        *capacity = (uint64_t)s.f_blocks * (uint64_t)s.f_bsize;
        *free = (uint64_t)s.f_bfree * (uint64_t)s.f_bsize;
        err = HAL_E_OK;
    }
    else
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not get free flash memory");
        err = HAL_E_FAIL;
    }

    return err;

}

hal_error hal_device_get_serial_number( uint8_t ** sn )
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( sn );

    *sn = serial;

    return HAL_E_OK;
}

hal_error hal_device_get_release_version( uint32_t * version )
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( version );

    *version = (uint32_t)release_version;

    return HAL_E_OK;
}

hal_error hal_device_get_device_id( uint16_t * id )
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( id );

    *id = device_id;

    return HAL_E_OK;
}

hal_error hal_device_get_eth_details( uint8_t * mac_addr, uint8_t * ip_addr,
    uint8_t * netmask )
{
    hal_error err = HAL_E_OK;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( mac_addr );
    CHECK_POINTER_PARAMETER( ip_addr );
    CHECK_POINTER_PARAMETER( netmask );

    err = get_interface_addrs( IF_ETHERNET, ip_addr, netmask );

    if ( err == HAL_E_OK )
    {
        memcpy( mac_addr, eth_mac_address, HAL_DEVICE_MAC_SIZE );
    }

    return err;
}

hal_error hal_device_get_wlan_details( uint8_t * mac_addr, uint8_t * ip_addr,
    uint8_t * netmask )
{
    hal_error err = HAL_E_OK;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( mac_addr );
    CHECK_POINTER_PARAMETER( ip_addr );
    CHECK_POINTER_PARAMETER( netmask );

    err = get_interface_addrs( IF_WLAN, ip_addr, netmask );

    if ( err == HAL_E_OK )
    {
        memcpy( mac_addr, eth_mac_address, HAL_DEVICE_MAC_SIZE );
    }

    return err;
}
