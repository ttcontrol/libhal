/**************************************************************************
 * Copyright (c) 2013 TTTControl. All rights reserved. Confidential proprietary
 * Schoenbrunnerstrasse 7, A-1040 Wien, Austria. office[at]ttcontrol.com
 **************************************************************************/
/**********************************************************************//**
 * \file hal_power.h
 *
 * \brief Hardware Abstraction Layer for power related functions.
 *
 **************************************************************************/

#ifndef _HAL_POWER_H_
#define _HAL_POWER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "hal_types.h"
#include <stdbool.h>
#include <linux/rtc.h>

/** \defgroup power Power
 *  \brief Functions and datastructures for various power related functionality
 *  \{
 */

/** Valid power states */
typedef enum
{
    HAL_PWR_STATE_LOW,      /**< low-level */
    HAL_PWR_STATE_HIGH,     /**< high-level */
} hal_power_state;

/** Specifies a wakeup source of the device */
typedef enum
{
    HAL_POWER_WAKEUP_POWERON,		/**< power on, i.e. complete system start via terminal 30 */
    HAL_POWER_WAKEUP_RESUME,		/**< resume from suspend */
    HAL_POWER_WAKEUP_ALARM_RESUME,	/**< resume from suspend because a RTC alarm fired */
    HAL_POWER_WAKEUP_ALARM_POWERON,	/**< power on because a RTC alarm fired */
} hal_power_wakeup_source;

/**
 * Type for function-pointer which can be registered for state-changes
 * of terminal 15 with #hal_power_reg_terminal15
 *
 * \param state new power state
 *
 */
typedef void (*hal_power_callback)( hal_power_state state );

/** \brief Initialize power module
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_FAIL               Could not initialize module
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 *
 *  \remarks
 *  This function must be called before any other function of this module is called.
 */
hal_error hal_power_init();

/** \brief Deinitialize power module.
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_NOT_INITIALIZED    Module was not initialized
 */
hal_error hal_power_deinit();
 
 /** \brief Switch camera power
  *  \param[in]   state      Set to #HAL_PWR_STATE_HIGH to enable power,
  *                          set to #HAL_PWR_STATE_LOW to  disable power
  */
hal_error hal_power_set_camera( hal_power_state const state );

 /** \brief Get camera power
  *  \param[out]  state      #HAL_PWR_STATE_HIGH if camera power enabled,
  *                          #HAL_PWR_STATE_LOW if camera power disabled
  */
hal_error hal_power_get_camera( hal_power_state * const state );

/**
 * \brief Get the current state of the terminal 15.
 *
 * \param[out] state Returned state; one of :
 *                   - \c #HAL_PWR_STATE_LOW:  low-level detected on terminal 15 pin
 *                   - \c #HAL_PWR_STATE_HIGH: high-level detected on terminal 15 pin
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_power_get_terminal15( hal_power_state * const state );

/**
 * \brief Registers a function callback for terminal 15 state change
 *
 * The registered function is called automatically on a state change
 * of terminal 15.
 *
 * \param[out] cb   Function pointer to callback;
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Could not register
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 *
 * \warning
 * The callback registered with this function is called within
 * an internal thread of the HAL. Do not perform any long
 * operations in the registered callback or otherwise
 * you may block internal functionality.\n
 * If long operations are necessary, it is suggested to
 * create a thread.
 */
hal_error hal_power_register_callback_terminal15( hal_power_callback cb );

/**
 * \brief Unregisters a callback for terminal 15.
 *
 * \param[in] device Pointer to the registered callback function
 *
 * \return #hal_error
 * \retval #HAL_E_OK                  Everything fine
 * \retval #HAL_E_NOT_INITIALIZED     Module not initialized
 * \retval #HAL_E_FAIL                Could not unregister
 */
hal_error hal_power_unregister_callback_terminal15( hal_power_callback cb );

/**
 * \brief Get the current state of the wakeup-pin (only available on HY-eVision� 10.4).
 *
 * \param[out] state Returned state; one of :
 *                   - \c #HAL_PWR_STATE_LOW:  low-level detected on terminal 15 pin
 *                   - \c #HAL_PWR_STATE_HIGH: high-level detected on terminal 15 pin
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_power_get_wakeup( hal_power_state * const state );

/**
 * \brief Registers a function callback for wakeup-pin state change
 *
 * The registered function is called automatically on a state change
 * of the wakeup-pin.
 *
 * \param[out] cb   Function pointer to callback;
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Could not register
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 *
 * \warning
 * The callback registered with this function is called within
 * an internal thread of the HAL. Do not perform any long
 * operations in the registered callback or otherwise
 * you may block internal functionality.\n
 * If long operations are necessary, it is suggested to
 * create a thread.
 */
hal_error hal_power_register_callback_wakeup( hal_power_callback cb );


/**
 * \brief Unregisters a callback for the wakeup-pin.
 *
 * \param[in] device Pointer to the registered callback function
 *
 * \return #hal_error
 * \retval #HAL_E_OK                  Everything fine
 * \retval #HAL_E_NOT_INITIALIZED     Module not initialized
 * \retval #HAL_E_FAIL                Could not unregister
 */
hal_error hal_power_unregister_callback_wakeup( hal_power_callback cb );

/**
 * \brief Enter suspend-mode.
 *
 *
 * \return #hal_error
 * \retval #HAL_E_OK                  Everything fine
 * \retval #HAL_E_NOT_INITIALIZED     Module not initialized
 * \retval #HAL_E_FAIL                Could not enter suspend mode
 */
hal_error hal_power_suspend();

/**
 * \brief Enables or disables the wakeup from suspend with a
 * key press.
 *
 * \param[in] enable 	TRUE to enable, FALSE to disable
 *
 * \return #hal_error
 * \retval #HAL_E_OK                  Everything fine
 * \retval #HAL_E_NOT_INITIALIZED     Module not initialized
 * \retval #HAL_E_FAIL                Could not set keyboard wakeup
 */
hal_error hal_power_set_keyboard_wakeup( bool enable );

/**
 * \brief Gets the devices last wakeup source.
 *
 * \param[out] source    last wakeup source; one of :
 *                   - \c #HAL_POWER_WAKEUP_POWERON:
 *                        last wakeup source was a power-on-reset,
 *                        which means that the device never entered
 *                        the suspend mode during this power-cycle.
 *
 *                   - \c #HAL_POWER_WAKEUP_RESUME:
 *                        device was resumed from suspend.
 *
 *                   - \c #HAL_POWER_WAKEUP_ALARM_POWERON:
 *                        device woke up from power-off because of an RTC alarm
 *
 *                   - \c #HAL_POWER_WAKEUP_ALARM_RESUME:
 *                        device woke up from suspend because of an RTC alarm
 *
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 *
 */
hal_error hal_power_get_wakeup_source( hal_power_wakeup_source * const source);

/**
 * \brief Set a new wake alarm at the specified RTC time.
 *
 * Wakes the device up from suspend or power-off when the time specified is
 * reached. The granularity of wakeup time is currently minutes so member
 * #tm_sec of #alarm will be ignored.
 *
 * \param[in] alarm Wake alarm structure which contains the RTC
 *                  time when the wake alarm is generated
 *
 * \return #hal_error
 * \retval #HAL_E_OK                 Everything fine
 * \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 * \retval #HAL_E_NULL_POINTER       A NULL pointer has been passed
 * \retval #HAL_E_FAIL               IOCTL failed
 * \retval #HAL_E_INVALID_PARAMETER  Alarm time is more than a month from now.
 *                                   Alarm can not be setup more than one month
 *                                   and less than two minute in advance.
 *
 */
hal_error hal_power_set_wakeup_alarm( struct rtc_wkalrm * alarm );

/**
 * \brief Set a new wake alarm the specified number of seconds in the future.
 *
 * Wakes the device up from suspend or power-off when the number of seconds
 * specified elapsed. Note that the granularity of wakeup time is currently
 * minutes.
 *
 * When the minimum sleep time of 120 seconds is used, the performed sleep
 * duration will be between 60 and 120 seconds, depending on the exact time
 * the alarm was set.
 * | Current Time | Sleep duration |
 * | ------------ | -------------- |
 * | 0 min 0 sec  | 120 sec        |
 * | 0 min 59 sec | 60 sec         |
 *
 * \param[in] seconds Seconds until the wake alarm is generated
 * \param[in] alarm   Pointer to an structt where the configured alarm will be stored.
 *                    If user passes a NULL pointer information about the alarm will not
 *                    be returned.
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_FAIL             IOCTL failed
 * \retval #HAL_E_INVALID_PARAMETER  Alarm time is more than a month from now.
 *                                   Alarm can not be setup more than one month
 *                                   and less than 120 seconds in advance.\
 * 
 * 
 * \warning
 * This function uses an absolute representation of time. Absolute in relation to the
 * actual RTC time on the device. This function assumes that the correct time has been set 
 * on the device beforehand, and provides no checks on this assumption. Always ensure you
 * have set the correct time on the device before calling this function.
 * 
 * 
 * 
 *
 */
hal_error hal_power_set_wakeup_alarm_from_now( uint32_t seconds, struct rtc_wkalrm * alarm );

/**
 * \brief Returns the current wake alarm.
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 * \retval #HAL_E_FAIL             IOCTL failed
 *
 */
hal_error hal_power_get_wakeup_alarm(struct rtc_wkalrm * alarm);

/**
 * \brief Disables the alarm interrupt.
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_FAIL             IOCTL failed
 *
 */
hal_error hal_power_clear_wakeup_alarm(void);

/**
 * \brief Power-off the device.
 *
 * Initiates a power-off in an asynchronous way, i.e. the
 * function returns after initiation of the power-off.
 * The actual power-off may take up to several seconds after
 * calling this function.
 *
 * \attention
 * If terminal15 is still connected to supply voltage after
 * the power-off is complete, the device will perform a restart.
 * To power-off the device, terminal 15 must have a low-level.
 *
 */
void hal_power_off();

/** \} */

#ifdef __cplusplus
}  /* End of the 'extern "C"' block */
#endif

#endif
