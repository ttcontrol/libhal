#include "hal_display.h"
#include "hal.h"
#include "hal_types.h"
#include "tools.h"

#include <linux/input.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <syslog.h>
#include <glib.h>

#define MODULE_NAME "[display] "

#define DISPLAY_BACKLIGHT_GPIO    "/var/links/gpio/display-backlight/value"
#define DISPLAY_BRIGHTNESS_FILE   "/var/links/pwm-backlight/brightness"

static int fd_bl_gpio = -1;
static int fd_pwm_bright = -1;

static GMutex mutex;
static gboolean initialized = FALSE;
static hal_display_bl_state current_state;
static uint8_t current_value;

static gboolean cap_has_display = FALSE;
static gboolean cap_bl_inverted = FALSE;

static hal_error set_bl_state( hal_display_bl_state const state );
static hal_error set_brightness( uint8_t const value );

static hal_error get_bl_state( hal_display_bl_state * const state );
static hal_error get_brightness( uint8_t * const value );


static hal_error get_brightness( uint8_t * const value )
{
    hal_error err;
    int val;

    CHECK_POINTER_PARAMETER( value );
    CHECK_FEATURE ( cap_has_display );
    CHECK_MODULE_INITIALIZED( initialized );

    g_mutex_lock( &mutex );
    err = file_get_value_int( fd_pwm_bright, &val );
    g_mutex_unlock( &mutex );

    if ( err == HAL_E_OK )
    {
        *value = ( cap_bl_inverted ) ? 255 - val : val;
    }
    else
    {
        syslog(LOG_ERR, "[display] - Could not get display brightness");
    }

    return err;
}

static hal_error get_bl_state( hal_display_bl_state * const state )
{
    hal_error err;
    int val;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE ( cap_has_display );
    CHECK_POINTER_PARAMETER( state );

    g_mutex_lock( &mutex );
    err = file_get_value_int( fd_bl_gpio, &val );
    g_mutex_unlock( &mutex );

    if ( err == HAL_E_OK )
    {
        switch ( val )
        {
            case 0:
                *state = HAL_DISP_BL_STATE_OFF;
                break;
            case 1:
                *state = HAL_DISP_BL_STATE_ON;
                break;
            default:
                syslog(LOG_ERR, "[display] - Got invalid state for display backlight: %u", val);
                err = HAL_E_FAIL;
                break;
        }
    }

    return err;
}

static hal_error check_display_caps()
{
    hal_error err;
    uint32_t cap;

    cap_has_display = FALSE;
    err = hal_syscfg_get_capability( HAL_SYSCAP_DISPLAY, &cap );
    if ( err != HAL_E_OK )
    {
        return HAL_E_FAIL;
    }

    switch ( cap )
    {
        case SYSCFG_DISPLAY_G070Y2L01:
        case SYSCFG_DISPLAY_G070Y2T02:
            cap_bl_inverted = FALSE;
            break;
        case SYSCFG_DISPLAY_NL6448BC2609C:
        case SYSCFG_DISPLAY_NL6448BC2626C:
            cap_bl_inverted = TRUE;
            break;
        case SYSCFG_DISPLAY_G104X1L04:
        case SYSCFG_ALL_FEATURES:
            cap_bl_inverted = FALSE;
            break;
        default:
            syslog(LOG_ERR, "[display] - Unknown display capability: %u", cap);
            // Unknown display type
            return HAL_E_NOFEATURE;
            break;
    }
    
    cap_has_display = TRUE;
    return HAL_E_OK;
}

hal_error hal_display_init()
{
    hal_display_init_internal( TRUE );
}

hal_error hal_display_init_internal(uint8_t set_default)
{
    hal_error err;
    hal_display_bl_state bl_state;
    uint8_t bl_value;
    
    CHECK_HAL_INITIALIZED();
    CHECK_MODULE_ALREADY_INITIALIZED( initialized );
    
    err = check_display_caps();
    if ( err == HAL_E_FAIL)
    {
        return HAL_E_FAIL;
    }
    else if ( err == HAL_E_NOFEATURE )
    {
        initialized = TRUE;
        return HAL_E_NOFEATURE;
    }

    fd_bl_gpio = open( DISPLAY_BACKLIGHT_GPIO, O_RDWR );
    if ( fd_bl_gpio == -1 )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not open display backlight gpio " DISPLAY_BACKLIGHT_GPIO
            ": %d", errno );
        return HAL_E_FAIL;
    }

    fd_pwm_bright = open( DISPLAY_BRIGHTNESS_FILE, O_RDWR );
    if ( fd_pwm_bright == -1 )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not open display brightness file " DISPLAY_BRIGHTNESS_FILE
            ": %d", errno );
        return HAL_E_FAIL;
    }

    initialized = TRUE;

    if (set_default)
    {    
        /* Set defined display backlight state on init */
        set_bl_state( HAL_DISP_BL_STATE_ON );
        set_brightness( 255 );
    }
    else
    {
        // No default values shall be set.
        // Get current value of brightness and state from files and store them
        err = get_bl_state( &bl_state );
        if ( err == HAL_E_OK )
        {
            err = get_brightness( &bl_value );
            if ( err == HAL_E_OK )
            {
                current_state = bl_state;
                current_value = bl_value;
            }
            else
            {
                return err;
            }           
        }
        else
        {
            return err;
        }
    }
    return HAL_E_OK;
}

hal_error hal_display_deinit()
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE ( cap_has_display );

    if ( fd_bl_gpio != -1 )
    {
        close( fd_bl_gpio );
        fd_bl_gpio = -1;
    }

    if ( fd_pwm_bright != -1 )
    {
        close( fd_pwm_bright );
        fd_pwm_bright = -1;
    }

    initialized = FALSE;

    return HAL_E_OK;
}

static hal_error set_bl_state( hal_display_bl_state const state )
{
    hal_error err;
    int val;

    switch ( state )
    {
        case HAL_DISP_BL_STATE_OFF:
            val = 0;
            break;
        case HAL_DISP_BL_STATE_ON:
            val = 1;
            break;
        default:
            return HAL_E_INVALID_PARAMETER;
    }

    g_mutex_lock( &mutex );
    err = file_set_value_int( fd_bl_gpio, val );
    g_mutex_unlock( &mutex );

    if (err == HAL_E_OK)
    {
        current_state = state;
    }
    return err;
}

hal_error hal_display_set_bl_state( hal_display_bl_state const state )
{
    hal_error err;
    int val;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE ( cap_has_display );

    if (current_state == state)
    {
        // State has not changed; do nothing and return OK;
        return HAL_E_OK;
    }

    err = set_bl_state( state );
    
    return err;
}

hal_error hal_display_get_bl_state( hal_display_bl_state * const state )
{
    hal_error err;
    int val;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE ( cap_has_display );
    CHECK_POINTER_PARAMETER( state );

    *state = current_state;
    return HAL_E_OK;
}

static hal_error set_brightness( uint8_t const value )
{
    hal_error err;
    uint8_t brightness = value;

    if ( cap_bl_inverted )
    {
        brightness = 255 - value;
    }

    g_mutex_lock( &mutex );
    err = file_set_value_int( fd_pwm_bright, (int)brightness );
    g_mutex_unlock( &mutex );

    if ( err != HAL_E_OK )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not set display brightness to %u", value );
    }
    else
    {
        current_value = value;
    }

    return err;
}

hal_error hal_display_set_brightness( uint8_t const value )
{
    hal_error err;
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE ( cap_has_display );

    if ( current_value == value )
    {
        // State has not changed; do nothing and return OK;
        return HAL_E_OK;
    }

    err = set_brightness( value );
    
    return err;
}

hal_error hal_display_get_brightness( uint8_t * const value )
{
    hal_error err;
    int val;

    CHECK_POINTER_PARAMETER( value );
    CHECK_FEATURE ( cap_has_display );
    CHECK_MODULE_INITIALIZED( initialized );

    *value = current_value;
    return HAL_E_OK;
}

