/*
 * Copyright (c) 2015 TTControl. All rights reserved. Confidential proprietary
 * Schoenbrunnerstrasse 7, A-1040 Wien, Austria. office@ttcontrol.com
 */
 /**********************************************************************//**
 * \file hal_wlan.c
 *
 *  \brief Functions and datastructures for WLAN support
 *
 **************************************************************************/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>
#include <string.h>
#include <glib.h>
#include <dbus/dbus.h>
#include <pthread.h>

#include "hal.h"
#include "hal_types.h"
#include "tools.h"

#define MODULE_NAME "[wlan] "
#define TIMEOUT -1
#define PASSPHRASE_LEN 128

static gboolean cap_has_wlan = FALSE;

static gboolean initialized = FALSE;
static DBusConnection *system_bus = NULL;
static char agent_path[PATH_LEN];
static char wlan_passphrase[PASSPHRASE_LEN];
static hal_wlan_error connect_error;
static gboolean password_requested;
static pthread_mutex_t connect_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t  connect_condition = PTHREAD_COND_INITIALIZER;
static char *current_network_path = NULL;

static struct hal_async_result scan_state = { HAL_E_OK, HAL_ASYNC_STATE_IDLE };
static struct hal_async_result connect_state = { HAL_E_OK, HAL_ASYNC_STATE_IDLE };

static const char const * states[] = {
    "idle",
    "failure",
    "association",
    "configuration",
    "ready",
    "online",
    "disconnect"
};

static char* errors[] = {
    "ok",
    "out-of-range",
    "pin-missing",
    "dhcp-failed",
    "connect-failed",
    "login-failed",
    "auth-failed",
    "invalid-key"
};

static char* security_methods[] = {
    "none",
    "wep",
    "psk",
    "ieee8021x",
    "wps"
};

static char* config_methods[] = {
    "off",
    "dhcp",
    "manual",
    "fixed"
};

static hal_wlan_state parse_service_state( char * state )
{
    hal_wlan_state result;

    for (result = HAL_WLAN_STATE_IDLE; result != HAL_WLAN_STATE_0; result++)
    {
        if ( strcmp( state, states[result] ) == 0)
        {
            break;
        }
    }

    return result;
}

static hal_wlan_error parse_service_error( char * error )
{
    hal_wlan_error result;
    
    for (result = HAL_WLAN_ERROR_OK; result != HAL_WLAN_ERROR_0; result++)
    {
        if ( strcmp( error, errors[result] ) == 0)
        {
            break;
        }
    }

    return result;
}

static hal_wlan_security parse_security_method( char * method )
{
    hal_wlan_security result;
    
    for (result = HAL_WLAN_SECURITY_NONE; result != HAL_WLAN_SECURITY_0; result++)
    {
        if ( strcmp( method, security_methods[result] ) == 0)
        {
            break;
        }
    }

    return result;
}

static hal_wlan_config parse_config_method( char * method )
{
    hal_wlan_config result;
    
    for (result = HAL_WLAN_CONFIG_OFF; result != HAL_WLAN_CONFIG_0; result++)
    {
        if ( strcmp( method, config_methods[result] ) == 0)
        {
            break;
        }
    }

    return result;
}

static hal_error register_agent( char * path )
{
    DBusMessage* msg;
    DBusMessageIter iter;
    DBusPendingCall* pending;
    hal_error result = HAL_E_OK;

    msg = dbus_message_new_method_call("net.connman", "/", "net.connman.Manager",
        "RegisterAgent");

    if (msg == NULL)
    {
        return HAL_E_FAIL;
    }

    dbus_message_iter_init_append(msg, &iter);
    dbus_message_iter_append_basic(&iter, DBUS_TYPE_OBJECT_PATH, &path);

    if (!dbus_connection_send_with_reply(system_bus, msg, &pending, TIMEOUT))
    {
        result = HAL_E_FAIL;
        goto end;
    }

    if (pending == NULL)
    {
        result = HAL_E_FAIL;
        goto end;
    }

    dbus_pending_call_block(pending);
    dbus_message_unref(msg);
    msg = dbus_pending_call_steal_reply(pending);
    dbus_pending_call_unref(pending);

    if (dbus_message_get_type(msg) == DBUS_MESSAGE_TYPE_ERROR)
    {
        DBusError err;
        dbus_error_init(&err);
        dbus_set_error_from_message(&err, msg);
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "register_agent: %s", err.message);
        dbus_error_free(&err);
        result = HAL_E_FAIL;
        goto end;
    }

end:
    dbus_message_unref(msg);
    return result;
}

static void wlan_request_input( DBusMessage *msg, DBusMessage *reply )
{
    DBusMessageIter iter, dict, entry, value;
    char * p = "Passphrase";
    char * pp = wlan_passphrase;
    
    dbus_message_iter_init_append( reply, &iter );
    dbus_message_iter_open_container( &iter, DBUS_TYPE_ARRAY,
        DBUS_DICT_ENTRY_BEGIN_CHAR_AS_STRING DBUS_TYPE_STRING_AS_STRING
        DBUS_TYPE_VARIANT_AS_STRING DBUS_DICT_ENTRY_END_CHAR_AS_STRING,
        &dict);
    dbus_message_iter_open_container( &dict, DBUS_TYPE_DICT_ENTRY, NULL, &entry );
    dbus_message_iter_append_basic( &entry, DBUS_TYPE_STRING, &p );
    dbus_message_iter_open_container( &entry, DBUS_TYPE_VARIANT,
        DBUS_TYPE_STRING_AS_STRING, &value );
    dbus_message_iter_append_basic( &value, DBUS_TYPE_STRING, &pp );
    dbus_message_iter_close_container( &entry, &value );
    dbus_message_iter_close_container( &dict, &entry );
    dbus_message_iter_close_container( &iter, &dict );
    password_requested = TRUE;
}

static void wlan_report_error( DBusMessage *msg, DBusMessage **reply )
{
    DBusMessageIter iter;
    char * str;
    
    dbus_message_iter_init( msg, &iter );
    dbus_message_iter_next( &iter );
    dbus_message_iter_get_basic( &iter, &str );
    connect_error = parse_service_error( str );
    
    if ( password_requested )
    {
        *reply = dbus_message_new_method_return( msg );
        dbus_message_append_args( *reply, DBUS_TYPE_INVALID );
    }
    else
    {
        connect_error = HAL_WLAN_ERROR_OK;
        *reply = dbus_message_new_error( msg, "net.connman.Agent.Error.Retry",
            NULL );
    }
}

static DBusHandlerResult wlan_message_func( DBusConnection *connection,
    DBusMessage *msg, void *data )
{
    DBusMessage *reply = NULL;
    dbus_uint32_t serial = 0;
    
    if ( dbus_message_is_method_call( msg, "net.connman.Agent", "RequestInput" ) )
    {
        reply = dbus_message_new_method_return( msg );
        wlan_request_input( msg, reply );
    }
    else if ( dbus_message_is_method_call( msg, "net.connman.Agent", "ReportError" ) )
    {
        wlan_report_error( msg, &reply );
    }

    if ( reply != NULL )
    {
        dbus_connection_send( system_bus, reply, &serial );
        dbus_message_unref( reply );
    }
    
    dbus_message_unref(msg);

    return DBUS_HANDLER_RESULT_HANDLED;
}

static void wlan_unregister_func(DBusConnection *connection, void *data)
{
}

static DBusObjectPathVTable wlan_dbus_vtable = {
    .unregister_function = wlan_unregister_func,
    .message_function = wlan_message_func,
};

static hal_error set_property(const char *path, const char *interface,
    const char *property, int type, void *value)
{
    DBusMessage* msg;
    DBusMessageIter iter, var;
    DBusPendingCall* pending;
    char * type_str;
    dbus_bool_t b;
    hal_error result = HAL_E_OK;

    msg = dbus_message_new_method_call("net.connman", path, interface,
        "SetProperty");

    if (msg == NULL)
    {
        return HAL_E_FAIL;
    }

    switch ( type )
    {
        case DBUS_TYPE_BOOLEAN:
            type_str = DBUS_TYPE_BOOLEAN_AS_STRING;
            b = *((uint8_t*)value);
            break;
        default:
            result = HAL_E_FAIL;
            goto end;
    }

    dbus_message_iter_init_append(msg, &iter);
    dbus_message_iter_append_basic(&iter, DBUS_TYPE_STRING, &property);
    dbus_message_iter_open_container(&iter, DBUS_TYPE_VARIANT, type_str, &var);
    dbus_message_iter_append_basic(&var, type, &b);
    dbus_message_iter_close_container(&iter, &var);

    if (!dbus_connection_send_with_reply(system_bus, msg, &pending, TIMEOUT))
    {
        result = HAL_E_FAIL;
        goto end;
    }

    if (pending == NULL)
    {
        result = HAL_E_FAIL;
        goto end;
    }

    dbus_pending_call_block(pending);
    dbus_message_unref(msg);
    msg = dbus_pending_call_steal_reply(pending);
    dbus_pending_call_unref(pending);

    if (dbus_message_get_type(msg) == DBUS_MESSAGE_TYPE_ERROR)
    {
        DBusError err;
        dbus_error_init(&err);
        dbus_set_error_from_message(&err, msg);
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "set_property: %s", err.message);
        if ( strcmp( err.name, "net.connman.Error.AlreadyEnabled" ) == 0 )
        {
            result = HAL_E_OK;
        }
        else
        {
            result = HAL_E_FAIL;
        }
        dbus_error_free(&err);
        
        goto end;
    }

end:
    dbus_message_unref(msg);
    return result;
}

static hal_error check_wlan_caps()
{
    hal_error err = HAL_E_OK;
    uint32_t cap;

    cap_has_wlan = FALSE;
    err = hal_syscfg_get_capability( HAL_SYSCAP_WLAN, &cap );
    if ( err != HAL_E_OK )
    {
        return HAL_E_FAIL;
    }
    
    switch ( cap )
    {
        case SYSCFG_WLAN_TIWI_BLE_EXT_ANT:
        case SYSCFG_WLAN_TIWI_BLE_INT_ANT:
        case SYSCFG_ALL_FEATURES:
            cap_has_wlan = TRUE;
            break;
        default:
            err = HAL_E_NOFEATURE;
            break;
    }
    
    return err;
}

hal_error hal_wlan_init()
{
    hal_error result = HAL_E_OK;
    DBusError dbus_err;

    CHECK_HAL_INITIALIZED();
    CHECK_MODULE_ALREADY_INITIALIZED( initialized );
    
    scan_state.state = HAL_ASYNC_STATE_IDLE;
    scan_state.result = HAL_E_OK;
    connect_state.state = HAL_ASYNC_STATE_IDLE;
    connect_state.result = HAL_E_OK;
    
/*    result = check_wlan_caps();
    if (result == HAL_E_FAIL)
    {
        return HAL_E_FAIL;
    }
    else if (result == HAL_E_NOFEATURE)
    {
        initialized = TRUE;
        return HAL_E_NOFEATURE;
    }*/
    
    cap_has_wlan = TRUE;

    result = hal_dbus_get_system_bus( &system_bus );
    if ( result != HAL_E_OK )
    {
        hal_log(HAL_LOG_SEVERITY_ERROR, "hal_wlan_init: Could not get bus\n");
        return result;
    }

    snprintf( agent_path, PATH_LEN, "/net/connman/libhal%d", getpid() );

    if ( !dbus_connection_register_object_path( system_bus, agent_path,
        &wlan_dbus_vtable, NULL ) )
    {
        return HAL_E_FAIL;
    }

    result = register_agent( agent_path );
    if ( result != HAL_E_OK )
    {
        hal_log(HAL_LOG_SEVERITY_ERROR, "hal_wlan_init: Could not register agent\n");
        return result;
    }

    usleep(300000);
    initialized = TRUE;
    return result;
}

void hal_wlan_deinit()
{
    CHECK_MODULE_INITIALIZED_RETURN_VOID( initialized );

    if (current_network_path != NULL)
    {
        g_free( current_network_path );
        current_network_path = NULL;
    }

    initialized = FALSE;
}

hal_error hal_wlan_enable( uint8_t enable )
{
    hal_error err;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_wlan );

    err = set_property( "/net/connman/technology/wifi", "net.connman.Technology",
        "Powered", DBUS_TYPE_BOOLEAN, &enable );
    
    if ( enable )
    {
        usleep(300000);
    }
    
    return err;
}

static void scan_reply( DBusPendingCall *call, void *user )
{
    DBusMessage *msg;
    struct hal_async_result *res = (struct hal_async_result *)user;
    
    res->result = HAL_E_OK;
    msg = dbus_pending_call_steal_reply( call );
    dbus_pending_call_unref( call );
    if ( dbus_message_get_type(msg) == DBUS_MESSAGE_TYPE_ERROR )
    {
        DBusError err;
        dbus_error_init( &err );
        dbus_set_error_from_message( &err, msg );
        
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Scan: %s", err.message );
        
        if (strcmp(err.name, "org.freedesktop.DBus.Error.NoReply") == 0)
        {
            res->result = HAL_E_DBUS_NOREPLY;
        }
        else if (strcmp(err.name, "net.connman.Error.NoCarrier") == 0)
        {
            res->result = HAL_E_WLAN_NOCARRIER;
        }
        else
        {
            res->result = HAL_E_FAIL;
        }
        
        dbus_error_free( &err );
    }
    dbus_message_unref(msg);
    
    __atomic_store_n( &res->state, HAL_ASYNC_STATE_FINISHED, __ATOMIC_SEQ_CST );
}

static hal_error hal_wlan_scan_internal( uint8_t blocking )
{
    DBusMessage *msg;
    DBusPendingCall *pending;
    hal_error result = HAL_E_OK;
    hal_async_state local_state;
    
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_wlan );
    
    local_state = __atomic_load_n( &scan_state.state, __ATOMIC_SEQ_CST );
    if ( local_state == HAL_ASYNC_STATE_PENDING )
    {
        return HAL_E_PENDING;
    }
    else if ( local_state == HAL_ASYNC_STATE_FINISHED )
    {
        scan_state.state = HAL_ASYNC_STATE_IDLE;
        return scan_state.result;
    }
    
    hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Initiate network scan" );
    
    msg = dbus_message_new_method_call( "net.connman", "/net/connman/technology/wifi",
        "net.connman.Technology", "Scan" );

    if ( msg == NULL )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Scan: Could not create command" );
        return HAL_E_FAIL;
    }

    if ( !dbus_connection_send_with_reply(system_bus, msg, &pending, TIMEOUT ) )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Scan: Could not send command" );
        result = HAL_E_FAIL;
        goto end;
    }

    if ( pending == NULL )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Scan: pending NULL" );
        result = HAL_E_FAIL;
        goto end;
    }
    
    if ( blocking )
    {
        dbus_pending_call_block( pending );
        dbus_message_unref( msg );
        msg = dbus_pending_call_steal_reply( pending );
        dbus_pending_call_unref( pending );

        if ( dbus_message_get_type(msg) == DBUS_MESSAGE_TYPE_ERROR )
        {
            DBusError err;
            dbus_error_init( &err );
            dbus_set_error_from_message( &err, msg );
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Scan: %s", err.message );
            dbus_error_free( &err );
            result = HAL_E_FAIL;
        }
    }
    else
    {
        scan_state.state = HAL_ASYNC_STATE_PENDING;
        dbus_pending_call_set_notify( pending, scan_reply, &scan_state, NULL );
        result = HAL_E_PENDING;
    }

end:
    dbus_message_unref( msg );
    return result;
}

hal_error hal_wlan_scan()
{
    return hal_wlan_scan_internal(TRUE);
}

hal_error hal_wlan_scan_async()
{
    return hal_wlan_scan_internal(FALSE);
}

hal_error hal_wlan_get_networks2( struct hal_wlan_network * networks,
    uint8_t * count, struct hal_wlan_network_data * data )
{
    DBusMessage *msg;
    DBusMessageIter iter, array, array2, struc, struc2, dict, dict2, entry, entry2, val, val2;
    DBusPendingCall* pending;
    hal_error result = HAL_E_OK;
    char *property, *str;
    uint8_t i = 0;
    gboolean b;
    struct hal_wlan_network n;
    struct hal_wlan_network_data d;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_wlan );
    CHECK_POINTER_PARAMETER( networks );
    CHECK_POINTER_PARAMETER( count );
    
    msg = dbus_message_new_method_call("net.connman", "/", "net.connman.Manager",
        "GetServices");
        
    if (msg == NULL)
    {
        return HAL_E_FAIL;
    }

    if (!dbus_connection_send_with_reply(system_bus, msg, &pending, 2000))
    {
        result = HAL_E_FAIL;
        goto end;
    }
    
    if (pending == NULL)
    {
        result = HAL_E_FAIL;
        goto end;
    }

    dbus_pending_call_block(pending);
    dbus_message_unref(msg);
    msg = dbus_pending_call_steal_reply(pending);
    dbus_pending_call_unref(pending);

    if (dbus_message_get_type(msg) == DBUS_MESSAGE_TYPE_ERROR)
    {
        DBusError err;
        dbus_error_init(&err);
        dbus_set_error_from_message(&err, msg);
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "get_networks: %s", err.message);
        dbus_error_free(&err);
        result = HAL_E_FAIL;
        goto end;
    }
    
    dbus_message_iter_init(msg, &iter);
    if (dbus_message_iter_get_arg_type(&iter) != DBUS_TYPE_ARRAY)
    {
        result = HAL_E_FAIL;
        *count = 0;
        goto end;
    }

    dbus_message_iter_recurse(&iter, &array);
    while (dbus_message_iter_get_arg_type(&array) == DBUS_TYPE_STRUCT)
    {
        memset( &n, 0, sizeof(n));
        memset( &d, 0, sizeof(d));
        
        dbus_message_iter_recurse(&array, &struc);
        dbus_message_iter_get_basic(&struc, &str);
        strncpy( n.dbus_path, str, PATH_LEN );

        dbus_message_iter_next(&struc);
        dbus_message_iter_recurse(&struc, &dict);
        while (dbus_message_iter_get_arg_type(&dict) != DBUS_TYPE_INVALID)
        {
            str = NULL;
            b = FALSE;

            dbus_message_iter_recurse(&dict, &entry);
            dbus_message_iter_get_basic(&entry, &property);
            dbus_message_iter_next(&entry);

            if (strcmp(property, "Name") == 0)
            {
                dbus_message_iter_recurse(&entry, &val);
                dbus_message_iter_get_basic(&val, &str);
                strncpy( n.ssid, str, SSID_LEN );
            }
            else if (strcmp(property, "Strength") == 0)
            {
                dbus_message_iter_recurse(&entry, &val);
                dbus_message_iter_get_basic(&val, &n.strength);
            }
            else if (strcmp(property, "State") == 0)
            {
                dbus_message_iter_recurse(&entry, &val);
                dbus_message_iter_get_basic(&val, &str);
                n.state = parse_service_state( str );
                
                if (current_network_path == NULL)
                {
                    if (n.state == HAL_WLAN_STATE_READY ||
                        n.state == HAL_WLAN_STATE_ONLINE)
                    {
                        current_network_path = g_strdup( n.dbus_path );
                    }
                }
            }
            else if (strcmp(property, "Error") == 0)
            {
                dbus_message_iter_recurse(&entry, &val);
                dbus_message_iter_get_basic(&val, &str);
                n.error = parse_service_error( str );
            }
            else if (strcmp(property, "AutoConnect") == 0)
            {
                dbus_message_iter_recurse(&entry, &val);
                dbus_message_iter_get_basic(&val, &n.auto_connect);
            }
            else if (strcmp(property, "Security") == 0)
            {
                dbus_message_iter_recurse(&entry, &val);
                dbus_message_iter_recurse(&val, &val2);
                if ( dbus_message_iter_get_arg_type(&val2) == DBUS_TYPE_STRING )
                {
                    dbus_message_iter_get_basic(&val2, &str);
                    d.security = parse_security_method(str);
                }
            }
            else if (strcmp(property, "IPv4") == 0)
            {
                dbus_message_iter_recurse(&entry, &array2);

                dbus_message_iter_recurse(&array2, &struc2);

                while (dbus_message_iter_get_arg_type(&struc2) == DBUS_TYPE_DICT_ENTRY)
                {
                    dbus_message_iter_recurse(&struc2, &entry2);

                    dbus_message_iter_get_basic(&entry2, &property);
                    dbus_message_iter_next(&entry2);

                if (strcmp(property, "Method") == 0)
                    {
                        dbus_message_iter_recurse(&entry2, &val);
                        if ( dbus_message_iter_get_arg_type(&val) == DBUS_TYPE_STRING )
                        {
                            dbus_message_iter_get_basic(&val, &str);
                            d.config_method = parse_config_method(str);
                        }
                    }
                    else if (strcmp(property, "Address") == 0)
                    {
                        dbus_message_iter_recurse(&entry2, &val);
                        if ( dbus_message_iter_get_arg_type(&val) == DBUS_TYPE_STRING )
                        {
                            dbus_message_iter_get_basic(&val, &str);
                            strncpy( d.ip, str, IP_LEN );
                        }
                    }
                    else if (strcmp(property, "Netmask") == 0)
                    {
                        dbus_message_iter_recurse(&entry2, &val);
                        if ( dbus_message_iter_get_arg_type(&val) == DBUS_TYPE_STRING )
                        {
                            dbus_message_iter_get_basic(&val, &str);
                            strncpy( d.netmask, str, IP_LEN );
                        }
                    }
                    else if (strcmp(property, "Gateway") == 0)
                    {
                        dbus_message_iter_recurse(&entry2, &val);
                        if ( dbus_message_iter_get_arg_type(&val) == DBUS_TYPE_STRING )
                        {
                            dbus_message_iter_get_basic(&val, &str);
                            strncpy( d.gateway, str, IP_LEN );
                        }
                    }
                    dbus_message_iter_next(&struc2);
                }
            }
            dbus_message_iter_next(&dict);
        }
        
        if ( *count == 0 )
        {
            if ( (current_network_path != NULL) && (strcmp( n.dbus_path, current_network_path ) == 0) )
            {
                i = 1;
                memcpy( &networks[0], &n, sizeof( n ) );
                if (data != NULL)
                {
                    memcpy( &data[0], &d, sizeof( d ) );
                }
                break;
            }
        }
        else if ( i == *count )
        {
            hal_log( HAL_LOG_SEVERITY_WARNING, "Too many networks" );
            break;
        }
        else if ( n.ssid[0] != '\0' )
        {
            memcpy( &networks[i], &n, sizeof( n ) );
            if (data != NULL)
            {
                memcpy( &data[i], &d, sizeof( d ) );
            }
            i++;
        }
        
        dbus_message_iter_next(&array);
    }

    *count = i;

end:
    dbus_message_unref(msg);

    return result;
}

hal_error hal_wlan_get_networks( struct hal_wlan_network * networks, uint8_t * count )
{
    return hal_wlan_get_networks2( networks, count, NULL );
}

static void connect_reply( DBusPendingCall *call, void *user )
{
    DBusMessage *msg = NULL;
    struct hal_async_result *res = (struct hal_async_result *)user;
    
    res->result = HAL_E_OK;

    msg = dbus_pending_call_steal_reply( call );
    dbus_pending_call_unref( call );
    if ( connect_error != HAL_WLAN_ERROR_OK )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Connect: error %d", connect_error);
    
        switch (connect_error)
        {
            case HAL_WLAN_ERROR_INVALID_KEY:
                res->result = HAL_E_WLAN_PASSPHRASE; break;
            default:
                res->result = HAL_E_FAIL; break;
        }
    }
    else if ( dbus_message_get_type(msg) == DBUS_MESSAGE_TYPE_ERROR )
    {
        DBusError err;
        dbus_error_init( &err );
        dbus_set_error_from_message( &err, msg );
        
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Connect: %s", err.name );
        
        if (strcmp(err.name, "org.freedesktop.DBus.Error.NoReply") == 0)
        {
            res->result = HAL_E_DBUS_NOREPLY;
        }
        else if (strcmp(err.name, "net.connman.Error.NoCarrier") == 0)
        {
            res->result = HAL_E_WLAN_NOCARRIER;
        }
        else if (strcmp(err.name, "net.connman.Error.AlreadyConnected") == 0)
        {
            res->result = HAL_E_WLAN_ALREADY_CONNECTED;
        }
        else
        {
            res->result = HAL_E_FAIL;
        }
        
        dbus_error_free( &err );
    }
    
    if ( res->state == HAL_ASYNC_STATE_IDLE )
    {
        pthread_mutex_lock( &connect_mutex );
        pthread_cond_signal( &connect_condition );
        pthread_mutex_unlock( &connect_mutex );
    }
    else
    {
        __atomic_store_n( &res->state, HAL_ASYNC_STATE_FINISHED, __ATOMIC_SEQ_CST );
    }
    dbus_message_unref(msg);
}

static hal_error hal_wlan_connect_internal( struct hal_wlan_network * network,
    char * passphrase, uint8_t blocking )
{
    DBusMessage* msg = NULL;
    DBusPendingCall* pending;
    hal_error result = HAL_E_OK;
    hal_async_state local_state;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_wlan );
    CHECK_POINTER_PARAMETER( network );
    CHECK_POINTER_PARAMETER( passphrase );
    
    local_state = __atomic_load_n( &connect_state.state, __ATOMIC_SEQ_CST );
    
    if ( local_state == HAL_ASYNC_STATE_PENDING )
    {
        return HAL_E_PENDING;
    }
    else if ( local_state == HAL_ASYNC_STATE_FINISHED )
    {
        connect_state.state = HAL_ASYNC_STATE_IDLE;
        result = connect_state.result;
        
        connect_error = HAL_WLAN_ERROR_OK;
        wlan_passphrase[0] = '\0';
    }
    else
    {
        CHECK_POINTER_PARAMETER(network->dbus_path);

        strncpy( wlan_passphrase, passphrase, PASSPHRASE_LEN );

        hal_log(HAL_LOG_SEVERITY_INFO, "Connecting to %s (%s)\n", network->ssid,
            network->dbus_path);

        msg = dbus_message_new_method_call("net.connman", network->dbus_path,
            "net.connman.Service", "Connect");

        if (msg == NULL)
        {
            return HAL_E_FAIL;
        }

        if (!dbus_connection_send_with_reply(system_bus, msg, &pending, 25000))
        {
            hal_log(HAL_LOG_SEVERITY_ERROR, "connect: no send\n");
            result = HAL_E_FAIL;
            goto end;
        }

        if (pending == NULL)
        {
            hal_log(HAL_LOG_SEVERITY_ERROR, "connect: not pending\n");
            result = HAL_E_FAIL;
            goto end;
        }
        
        if ( blocking )
        {
            connect_state.state = HAL_ASYNC_STATE_IDLE;
            pthread_mutex_lock( &connect_mutex );
            dbus_pending_call_set_notify( pending, connect_reply, &connect_state, NULL );
            pthread_cond_wait( &connect_condition , &connect_mutex );
            pthread_mutex_unlock( &connect_mutex );
            result = connect_state.result;
        }
        else
        {
            connect_state.state = HAL_ASYNC_STATE_PENDING;
            dbus_pending_call_set_notify( pending, connect_reply, &connect_state, NULL );
            result = HAL_E_PENDING;
            if ( dbus_pending_call_get_completed( pending ) )
            {
                connect_reply( pending, &connect_state );
            }
        }
    }

    g_free( current_network_path );
    current_network_path = g_strdup( network->dbus_path );

end:
    if ( msg != NULL)
    {
        dbus_message_unref(msg);
    }
    return result;
}

hal_error hal_wlan_connect( struct hal_wlan_network * network, char * passphrase )
{
    return hal_wlan_connect_internal( network, passphrase, TRUE );
}

hal_error hal_wlan_connect_async( struct hal_wlan_network * network,
    char * passphrase )
{
    return hal_wlan_connect_internal( network, passphrase, FALSE );
}

hal_error hal_wlan_disconnect( )
{
    DBusMessage* msg;
    DBusPendingCall* pending;
    hal_error result = HAL_E_OK;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_wlan );
    
    if (current_network_path == NULL)
        return result;

    msg = dbus_message_new_method_call("net.connman", current_network_path,
        "net.connman.Service", "Disconnect");

    if (msg == NULL)
    {
        return HAL_E_FAIL;
    }

    if (!dbus_connection_send_with_reply(system_bus, msg, &pending, TIMEOUT))
    {
        result = HAL_E_FAIL;
        goto end;
    }

    if (pending == NULL)
    {
        result = HAL_E_FAIL;
        goto end;
    }

    dbus_pending_call_block(pending);
    dbus_pending_call_unref(pending);
    
    g_free( current_network_path );
    current_network_path = NULL;

end:
    dbus_message_unref(msg);
    return result;
}

hal_error hal_wlan_remove( struct hal_wlan_network * network )
{
    DBusMessage* msg;
    DBusPendingCall* pending;
    hal_error result = HAL_E_OK;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_wlan );
    CHECK_POINTER_PARAMETER( network );
    CHECK_POINTER_PARAMETER( network->dbus_path );
    
    msg = dbus_message_new_method_call("net.connman", network->dbus_path,
        "net.connman.Service", "Remove");

    if (msg == NULL)
    {
        return HAL_E_FAIL;
    }

    if (!dbus_connection_send_with_reply(system_bus, msg, &pending, TIMEOUT))
    {
        result = HAL_E_FAIL;
        goto end;
    }

    if (pending == NULL)
    {
        result = HAL_E_FAIL;
        goto end;
    }

    dbus_pending_call_block(pending);
    dbus_pending_call_unref(pending);

end:
    dbus_message_unref(msg);
    return result;
}

hal_error hal_wlan_set_autoconnect( struct hal_wlan_network * network,
    uint8_t enable )
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_wlan );
    CHECK_POINTER_PARAMETER( network );
    CHECK_POINTER_PARAMETER( network->dbus_path );

    return set_property( network->dbus_path, "net.connman.Service",
        "AutoConnect", DBUS_TYPE_BOOLEAN, &enable );
}
