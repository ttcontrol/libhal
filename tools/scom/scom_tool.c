/*
 * ----------------------------------------------------------------------------
 */
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <getopt.h>
#include <libhal.h>
#include <glib.h>
#include <syslog.h>
#include <hal_syscfg.h>
#include <hal_types.h>

// Import scom.h, which is necessary for the internal function hal_scom_transfer
#include "../../src/scom.h"

#include "scom_tool.h"

#define HAL_INIT_TIMEOUT 500000U    // Unit: micro seconds
#define MODULE_NAME "[scom_tool] "
#define BUF_SIZE 256

#define GPS_SHORT_TIMEOUT       8000
#define GPS_MEDIUM_TIMEOUT      10000
#define GPS_LONG_TIMEOUT        13000


#define ARRAY_SIZE(array) \
    (sizeof(array) / sizeof(*array))

static struct option options[] =
{
    { "production", no_argument, 0, 'p' },
    { "factory", no_argument, 0, 'f' },
    { 0, 0, 0, 0 }
};


static hal_error config_and_verify(char *get_cmd, char *set_cmd, char *verify_str, uint32_t timeout)
{
    hal_modem_result_code res;
    char response[BUF_SIZE];
    hal_error err;

    response[0] = '\0';

    err = hal_scom_transfer(get_cmd, '\r', timeout, response, ARRAY_SIZE(response), &res);
    if (err != HAL_E_OK || res != HAL_MODEM_RES_OK)
    {
        printf("Could not execute '%s' (err: %d, res: %d)\n", get_cmd, err, res);
        return HAL_E_FAIL;
    }

    printf("%s (before): %s\n", get_cmd, response);

    if (verify_str != NULL)
    {
        if (strstr(response, verify_str) != NULL)
        {
            printf("Setting ('%s') already correct", verify_str);
            return HAL_E_OK;
        }
    }

    err = hal_scom_transfer(set_cmd, '\r', timeout, response, ARRAY_SIZE(response), &res);
    if (err != HAL_E_OK || res != HAL_MODEM_RES_OK)
    {
        printf("Could not set %s (err: %d, res: %d):%s\n", set_cmd, err, res, response);
        return HAL_E_FAIL;
    }

    hal_scom_transfer(get_cmd, '\r', timeout, response, ARRAY_SIZE(response), &res);
    printf("%s (after): %s\n", get_cmd, response);

    return HAL_E_OK;
}

static hal_error reset_module()
{
    hal_error err;

    printf("Reseting module...\n");
    hal_scom_reset();
    sleep(20);
    printf("DONE!\n");

    err = hal_modem_scom_init();

    if (err != HAL_E_OK && err != HAL_E_ALREADY_INITIALIZED)
    {
        return HAL_E_FAIL;
    }
    else
    {
        return HAL_E_OK;
    }
}

/* Configure the GPS module. For Cinterion EHS8, some settings (e.g. GPS antenna), must be
 * done during production. These settings are persistent.
 * For the u-Bloxx module, no special configuration required.
 * If paramter "factory_reset" is true, the function will revert all the changes back to
 * factory defaults and also uninstall the MIDlet, if present.
 */
hal_error configure_module(gboolean factory_reset)
{
    int val;

    int i;
    hal_modem_result_code res;
    char response[BUF_SIZE];
    hal_error err;
    uint32_t cap;
    uint8_t mod_ready[2];
    uint8_t ehs8_ready;

    ehs8_ready = 0;
    cap = 0;
    val = 0;

    char *mode_antenna_cmd, *power_antenna_cmd, *nmea_if_cmd;
    char *mode_antenna_verify, *power_antenna_verify, *nmea_if_verify;


    err = hal_modem_scom_init();
    if (err != HAL_E_OK && err != HAL_E_ALREADY_INITIALIZED)
    {
        printf("ERROR: Could not initialize SCOM: %i\n", err);
        goto error;
    }


    /* check from HAL on module type - HAL should already been initialized */
    err = hal_syscfg_get_capability(HAL_SYSCAP_GPS, &cap);
    if (err != HAL_E_OK)
    {
        printf("\nSCOM - HAL sysconfig get capability failed! %d", err);
        cap = 0;
    }
    switch (cap)
    {

    /*Cinterion EES3 - */
    case SYSCFG_GPS_INTERNAL:
        printf("Modem is Cinterion EES3 - GPS is u-Bloxx\n");
        break;
        /* Cinterion EHS8 - */
    case SYSCFG_GPS_EHS8:
        printf("Modem and GPS are Cinterion EHS8\n");

        if (factory_reset)
        {
            // default settings according to document "EHS8 AT Command Set"
            mode_antenna_cmd = "AT^SGPSC=\"Mode/Antenna\",\"0\"";
            mode_antenna_verify = "\"0\"";
            power_antenna_cmd = "AT^SGPSC=\"Power/Antenna\",\"auto\"";
            power_antenna_verify = "\"auto\"";
            nmea_if_cmd = "AT^SGPSC=\"Nmea/Interface\",\"local\"";
            nmea_if_verify = "\"local\"";
        }
        else
        {
            mode_antenna_cmd = "AT^SGPSC=\"Mode/Antenna\",\"1\"";
            mode_antenna_verify = "\"1\"";
            power_antenna_cmd = "AT^SGPSC=\"Power/Antenna\",\"on\"";
            power_antenna_verify = "\"on\"";
            nmea_if_cmd = "AT^SGPSC=\"Nmea/Interface\",\"usb3\"";
            nmea_if_verify = "\"usb3\"";
        }

        if (factory_reset)
        {
            err = hal_scom_transfer("AT&F", '\r', GPS_SHORT_TIMEOUT, response, ARRAY_SIZE(response), &res);
            if (err != HAL_E_OK || res != HAL_MODEM_RES_OK)
            {
                printf("ERROR: Could not reset to factory defaults (err: %d, res: %d)\n", err, res);
                goto error;
            }
        }

        // Configure antenna gpio
        err = config_and_verify("AT^SCFG=\"GPIO/mode/GPSANT\"", "AT^SCFG=\"GPIO/mode/GPSANT\",\"std\"", "\"std\"", GPS_SHORT_TIMEOUT);
        if (err != HAL_E_OK)
        {
            printf("ERROR: Could not configure GPSANT GPIO\n");
            goto error;
        }

        // GPSANT setting only takes effect after a reset, so reset the module
        err = reset_module();
        if (err != HAL_E_OK)
        {
            printf("ERROR: Could not reset module: %i\n", err);
            goto error;
        }

        // Stop GPSService MIDlet. This is necessary as we need to disable the GPS engine to be able to
        // set some configurations (e.g. Antenna Mode/Power). This is not permanent: after the next module start,
        // the MIDlet will be automatically started again. If the MIDlet is not installed this will fail.
        printf("Stopping MIDlet...\n");
        err = hal_scom_transfer("AT^SJAM=2,\"a:/GPSService-1.0.1.jad\",\"\"", '\r', 15000, response, ARRAY_SIZE(response), &res);
        if (err != HAL_E_OK || res != HAL_MODEM_RES_OK)
        {
            printf("Could not stop MIDlet (err: %d, res: %d). Continuing...\n", err, res);
        }


        if (factory_reset)
        {
            // Uninstall MIDlet
            err = hal_scom_transfer("AT^SJAM=3,\"a:/GPSService-1.0.1.jad\",\"\"", '\r', GPS_SHORT_TIMEOUT, response, ARRAY_SIZE(response), &res);
            if (err != HAL_E_OK || res != HAL_MODEM_RES_OK)
            {
                printf("Could not uninstall MIDlet (err: %d, res: %d). Continuing...\n", err, res);
            }
        }

        // Disable GPS Engine.
        err = config_and_verify("AT^SGPSC=\"Engine\"", "AT^SGPSC=\"Engine\",\"0\"", "\"0\"", GPS_SHORT_TIMEOUT);
        if (err != HAL_E_OK)
        {
            // Don't abort if this fails: it fails if the GPS Engine is already turned off.
            printf("Could not disable GPS Engine. Continuing...\n");
        }

        // Configure antenna mode
        err = config_and_verify("AT^SGPSC=\"Mode/Antenna\"", mode_antenna_cmd, mode_antenna_verify, GPS_SHORT_TIMEOUT);
        if (err != HAL_E_OK)
        {
            printf("ERROR: Could not configure Antenna Mode\n");
            goto error;
        }

        // Configure antenna power
        err = config_and_verify("AT^SGPSC=\"Power/Antenna\"", power_antenna_cmd, power_antenna_verify, GPS_SHORT_TIMEOUT);
        if (err != HAL_E_OK)
        {
            printf("ERROR: Could not configure Antenna Power\n");
            goto error;
        }

        // Configure NMEA interface
        err = config_and_verify("AT^SGPSC=\"Nmea/Interface\"", nmea_if_cmd, nmea_if_verify, GPS_LONG_TIMEOUT);
        if (err != HAL_E_OK)
        {
            printf("ERROR: Could not configure Nmea interface\n");
            goto error;
        }

        if (factory_reset)
        {
            // Configure GPS antenna pin to default ("gpio"). This must be done after
            // all other antenna configurations (Power/Mode Antenna). Commands for Power/Mode
            // fail if the pin is not set to "std".
            err = config_and_verify("AT^SCFG=\"GPIO/mode/GPSANT\"",  "AT^SCFG=\"GPIO/mode/GPSANT\",\"gpio\"", "\"gpio\"", GPS_SHORT_TIMEOUT);
            if (err != HAL_E_OK)
            {
                printf("ERROR: Could not configure GPSANT GPIO\n");
                goto error;
            }
        }

        err = reset_module();
        if (err != HAL_E_OK)
        {
            printf("ERROR: Could not reset module: %i\n", err);
            goto error;
        }

        break;
        /*Unknown modem - or not present*/
    default:
        printf("\nUnknown GPS capability: %u - not supported\n", cap);
        /* Unknown modem type */
        break;
    }

    hal_modem_scom_deinit();
    return HAL_E_OK;

error:
    hal_modem_scom_deinit();
    return HAL_E_FAIL;
}

int main(int argc, char **argv)
{
    int res;
    int i;
    hal_error err;
    uint32_t cap;
    int c;
    int option_index;
    option_index = 0;
    err = HAL_E_FAIL;
    gint64 ts_start, ts_current;

    c = 0;
    option_index = 0;
    c = getopt_long(argc, argv, "pf", options, &option_index);

    openlog("scom_tool", 0, LOG_USER);

    err = hal_init();
    if (err != HAL_E_OK)
    {
        syslog(LOG_ERR, MODULE_NAME"Could not initialize HAL!");
        exit(EXIT_FAILURE);
    }

    switch (c)
    {
    case 'p':
        syslog(LOG_INFO, MODULE_NAME"Production Mode");

        // TODO: The MIDlet name and version are hardcoded in the function hal_gps_production_internal.
        // If the name/version ever changes, it must be extended to make it more flexible.
        err = configure_module(false);
        if (err == HAL_E_OK)
        {
            syslog(LOG_INFO, MODULE_NAME"Production initialization successful");
        }
        else
        {
            syslog(LOG_ERR, MODULE_NAME"Production initialization failed: %d", err);
        }
        break;
    case 'f':
        syslog(LOG_INFO, MODULE_NAME"Factory reset");

        // TODO: The MIDlet name and version are hardcoded in the function hal_gps_production_internal.
        // If the name/version ever changes, it must be extended to make it more flexible.
        err = configure_module(true);
        if (err == HAL_E_OK)
        {
            syslog(LOG_INFO, MODULE_NAME"Factory reset successful");
        }
        else
        {
            syslog(LOG_ERR, MODULE_NAME"Factory reset failed: %d", err);
        }
        break;
    default:
        cap = 0;
        syslog(LOG_INFO, "Serial Communications Tool v1 is initializing...");
        /* initialize modem - this will link the modem and configure
         *  based on the proper modem type, since the HAL is already up
         */
        err = hal_modem_scom_init();
        if (err != HAL_E_OK)
        {
            syslog(LOG_ERR, MODULE_NAME"Could not initialize scom: %d", err);
        }

        hal_modem_scom_deinit();
        break;
    }

    hal_deinit();

    if (err == HAL_E_OK)
    {
        exit(EXIT_SUCCESS);
    }
    else
    {
        exit(EXIT_FAILURE);
    }
}

/* eof --------------------------------------------------------------------- */

