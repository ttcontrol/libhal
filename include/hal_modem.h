/**************************************************************************
 * Copyright (c) 2013 TTTControl. All rights reserved. Confidential proprietary
 * Schoenbrunnerstrasse 7, A-1040 Wien, Austria. office[at]ttcontrol.com
 **************************************************************************/
/**********************************************************************//**
 * \file hal_modem.h
 *
 * \brief Hardware Abstraction Layer for Modem.
 *
 * Contains functions to get various information about registered network
 * and device.
 *
 * \remarks
 * All functions within this module are blocking. As some operations (e.g.
 * sending text messages) may take some time, please consider this within
 * your application.
 *
 **************************************************************************/

#ifndef _HAL_MODEM_H_
#define _HAL_MODEM_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "hal_types.h"
#include <glib.h>
#include <stdint.h>

/** \defgroup modem Modem
 *  \brief Functions and datastructures for modem
 *
 *  \{
 */

/** Result codes of a command */
typedef enum
{
    HAL_MODEM_RES_OK = 0,           /**< Everything OK */
    HAL_MODEM_RES_ERROR,            /**< Generic Error */
    HAL_MODEM_RES_CONNECT,          /**< No connection possible */
    HAL_MODEM_RES_RING,             /**< Ringing */
    HAL_MODEM_RES_NO_CARRIER,       /**< No carrier */
    HAL_MODEM_RES_NO_DIALTONE,      /**< No dialtone */
    HAL_MODEM_RES_BUSY,             /**< Modem busy */
    HAL_MODEM_RES_NO_ANSWER,        /**< No answer */
    HAL_MODEM_RES_UNKNOWN,          /**< Unknown result */
    HAL_MODEM_RES_MAX,
} hal_modem_result_code;

/** Network registration states */
typedef enum
{
    HAL_MODEM_NOT_REGISTERED,       /**< Not registered, not searching */
    HAL_MODEM_HOME_NETWORK,         /**< Registered to home network */
    HAL_MODEM_SEARCHING,            /**< Not registered, searching for network */
    HAL_MODEM_DENIED,               /**< Registration denied  */
    HAL_MODEM_UNKNOWN,              /**< Unknown status */
    HAL_MODEM_ROAMING,              /**< Registered, roaming */
} hal_modem_registration;

/** PIN states */
typedef enum
{
    HAL_MODEM_PIN_STATUS_READY,         /**< No PIN request pending; ready to use */
    HAL_MODEM_PIN_STATUS_WAITING_PIN,   /**< Waiting for SIM PIN */
    HAL_MODEM_PIN_STATUS_UNKNOWN        /**< Unknown state */
} hal_modem_pin_status;


/** \brief Initialize Modem module
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_FAIL               Could not initialize module
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 *
 *  \remarks
 *  This function must be called before any other function of this module is called.
 */
hal_error hal_modem_init( );

/** \brief Deinitialize Modem module.
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 *
 */
hal_error hal_modem_deinit();

/**
 * \brief Gets information on current network registration
 *
 * \param[out] reg     current network registration status
 * \param[out] result  (optional) result of sent AT command
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Error
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_modem_get_registration( hal_modem_registration * const reg
                                    , hal_modem_result_code * const result );

/**
 * \brief Gets the current signal strength.
 *
 * Possible values are:\n
 * - 0: -113 dBm or less
 * - 1: -111 dBm
 * - 2...30: -109...-53 dBm
 * - 31: -51 dBm or greater
 * - 99: not known or not detectable
 *
 * \param[out] quality  signal strength
 * \param[out] result   (optional) result of sent AT command
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Error
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_modem_get_signal_quality( uint8_t * const quality
                                      , hal_modem_result_code * const result );

/**
 * \deprecated This function has been deprecated.
 *             Use #hal_modem_get_operator_name2() instead.
 * \brief Gets the name of currently registered operator
 *
 * \param[out] operator_name  operator name
 * \param[out] result         (optional) result of sent AT command
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Error
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_modem_get_operator_name( uint8_t * const operator_name
                                     , hal_modem_result_code * const result );

/**
 * \brief Gets the name of currently registered operator
 *
 * \param[out]    operator_name      operator name
 * \param[in,out] operator_name_size The size of the buffer operator_name points
 *                                   to; returns the actual size of operator_name
 * \param[out]    result             (optional) result of sent AT command
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Error
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_modem_get_operator_name2( uint8_t * const operator_name
                                      , uint32_t * const operator_name_size
                                      , hal_modem_result_code * const result );

/**
 * \deprecated This function has been deprecated.
 *             Use #hal_modem_get_imsi2() instead.
 * \brief Gets the IMSI (International Mobile Subscriber Identity)
 *  of the currently inserted SIM card
 *
 * \param[out] imsi     IMSI
 * \param[out] result   (optional) result of sent AT command
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Error
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_modem_get_imsi( uint8_t * const imsi
                            , hal_modem_result_code * const result );

/**
 * \brief Gets the IMSI (International Mobile Subscriber Identity)
 *  of the currently inserted SIM card
 *
 * \param[out]    imsi       IMSI
 * \param[in,out] imsi_size  The size of the buffer imsi points
 *                           to; returns the actual size of imsi
 * \param[out]    result     (optional) result of sent AT command
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Error
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_modem_get_imsi2( uint8_t * const imsi
                             , uint32_t * const imsi_size
                             , hal_modem_result_code * const result );

/**
 * \deprecated This function has been deprecated.
 *             Use #hal_modem_get_imei2() instead.
 * \brief Gets the IMEI (International Mobile Equipment Identity)
 *  of the GSM/GPRS Modem
 *
 * \param[out] imei     IMEI
 * \param[out] result   (optional) result of sent AT command
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Error
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_modem_get_imei( uint8_t * const imei
                            , hal_modem_result_code * const result );

/**
 * \brief Gets the IMEI (International Mobile Equipment Identity)
 *  of the GSM/GPRS Modem
 *
 * \param[out]    imei       IMEI
 * \param[in,out] imei_size  The size of the buffer imei points
 *                           to; returns the actual size of imei
 * \param[out]    result     (optional) result of sent AT command
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Error
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_modem_get_imei2( uint8_t * const imei
                             , uint32_t * const imei_size
                             , hal_modem_result_code * const result );

/**
 * \brief Gets the current PIN status, i.e. if PIN must be entered or
 * if PIN was entered correctly or is not needed.
 *
 * \param[out] status   PIN status
 * \param[out] result   (optional) result of sent AT command
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Error
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_modem_get_pin_status( hal_modem_pin_status * const status
                                  , hal_modem_result_code * const result );

/**
 * \brief Enter the PIN (Personal identification number).
 * Check with #hal_modem_get_pin_status beforehand if a PIN is needed.
 *
 * \param[out] pin      PIN which shall be entered
 * \param[out] result   (optional) result of sent AT command
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Error
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_modem_enter_pin( const uint8_t * const pin
                             , hal_modem_result_code * const result );

/**
 * \brief Send a text message to specified number.
 *
 * \param[in] number   telephone number of receiver
 * \param[in] text     message text
 * \param[out] result   (optional) result of sent AT command
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Error
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 *
 * \remarks
 * Sending a text message may take some time. As all other functions
 * of hal_modem.h, this function is blocking.
 */
hal_error hal_modem_send_sms( const uint8_t * const number
                            , const uint8_t * const text
                            , hal_modem_result_code * const result );


/** \internal
 * \brief Initialize the SCOM module from inside the HAL
 */
hal_error hal_modem_scom_init();

/** \internal
 * \brief Deinitialize the SCOM module from inside the HAL
 */
hal_error hal_modem_scom_deinit();

/** \} */

#ifdef __cplusplus
}  /* End of the 'extern "C"' block */
#endif

#endif /* _HAL_MODEM_H_ */
