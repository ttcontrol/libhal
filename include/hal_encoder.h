/**************************************************************************
 * Copyright (c) 2013 TTTControl. All rights reserved. Confidential proprietary
 * Schoenbrunnerstrasse 7, A-1040 Wien, Austria. office[at]ttcontrol.com
 **************************************************************************/
/**********************************************************************//**
 * \file hal_encoder.h
 *
 * \brief Hardware Abstraction Layer for encoder.
 *
 * Key events can be retrieved directly via Linux input events
 * (see linux/input.h for details).
 * The device path which must be used is obtained through
 * hal_encoder_get_device().
 *
 **************************************************************************/

#ifndef _HAL_ENCODER_H_
#define _HAL_ENCODER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "hal_types.h"
#include <stdbool.h>

/** Available keys for incremental rotary encoder */
typedef enum
{
    HAL_ENCODER_LEFT,           /**< Encoder turned left, i.e. counter clockwise */
    HAL_ENCODER_RIGHT,          /**< Encoder turned right, i.e. clockwise */
    HAL_ENCODER_ENTER,          /**< Encoder button pressed */
} hal_encoder_key;

/** \defgroup encoder Encoder
 *  \brief Functions and datastructures for Encoder
 *  \{
 */

/** \brief Initialize keyboard module
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 *  \retval #HAL_E_NOFEATURE          Current hardware has no encoder
 */
hal_error hal_encoder_init();

/** \brief Deinitialize keyboard module.
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
hal_error hal_encoder_deinit();

/**
 * \brief Returns the encoder's device file
 *
 * Use this function to get the correct device file for the encoder. This file
 * is needed for accessing encoder events.
 *
 * \param[out] device Pointer to string that contains the device file path
 *
 * \return #hal_error
 * \retval #HAL_E_OK                  Everything fine
 * \retval #HAL_E_NOT_INITIALIZED     Module not initialized 
 * \retval #HAL_E_NULL_POINTER        A NULL pointer has been passed
 */
hal_error hal_encoder_get_device(uint8_t const ** device);

/**
 * \brief Gets the state of a encoder key
 *
 * The function returns if the key is currently pressed and how many times
 * the key was pressed since the last call of this function.
 *
 * \param[in]  key     Key to query
 * \param[out] pressed TRUE if key is currently pressed, FALSE otherwise
 * \param[out] count   Contains the number of presses and releases since last
 *                     call; the value gets incremented on each release event.
  *
 * \return #hal_error
 * \retval #HAL_E_OK                 Everything fine
 * \retval #HAL_E_FAIL               Could not read encoder key state
 * \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 * \retval #HAL_E_INVALID_PARAMETER  Invalid parameter passed to the function
 * \retval #HAL_E_NULL_POINTER       A NULL pointer has been passed
 *
 */
hal_error hal_encoder_get_key_state( hal_encoder_key key, bool * const pressed, uint16_t * const count );

/** \} */

#ifdef __cplusplus
}  /* End of the 'extern "C"' block */
#endif

#endif /* _HAL_ENCODER_H_ */
