#include <unistd.h>
#include <stdio.h>
#include <syslog.h>
#include <glib.h>
#include <libudev.h>

#include "hal.h"
#include "hal_types.h"
#include "tools.h"
#include "ev_poll.h"


static gboolean initialized = FALSE;
static struct udev * udev = NULL;
static struct udev_monitor * udev_mon = NULL;
//static GIOChannel * udev_channel = NULL;
//static guint udev_event_source = 0;
static GPtrArray * callbacks = NULL;

static void do_callback(gpointer data, gpointer user_data)
{
    struct udev_device * dev = (struct udev_device *)user_data;
    
    (*(hal_device_event_func)(data))(dev);
}

static void process_device(struct udev_device * const dev)
{
    if (dev)
    {
        g_ptr_array_foreach(callbacks, &do_callback, dev);
    }
}

static gpointer udev_event(gpointer data)
{
    struct udev_monitor * mon = (struct udev_monitor *)data;
    struct udev_device * dev = NULL;
    
    dev = udev_monitor_receive_device(mon);
    process_device(dev);
    udev_device_unref(dev);
}

hal_error hal_udev_init()
{
    CHECK_HAL_INITIALIZED();
    CHECK_MODULE_ALREADY_INITIALIZED(initialized);
    
    udev = udev_new();
    if (udev == NULL)
    {
        printf("Could not create udev instance.\n");
        return HAL_E_FAIL;
    }
    
    udev_mon = udev_monitor_new_from_netlink(udev, "udev");
    udev_monitor_filter_add_match_subsystem_devtype(udev_mon, "block", NULL);
    udev_monitor_filter_add_match_subsystem_devtype(udev_mon, "usb", NULL);
    
/*    udev_channel = g_io_channel_unix_new(udev_monitor_get_fd(udev_mon));
    udev_event_source = g_io_add_watch(udev_channel, G_IO_IN | G_IO_HUP,
        channel_event_udev, udev_mon);*/
    ev_poll_add( udev_monitor_get_fd(udev_mon)
               , POLLIN
               , udev_event
               , udev_mon);
        
    callbacks = g_ptr_array_new();
    
    initialized = TRUE;
    return HAL_E_OK;
}

void hal_udev_deinit()
{
    CHECK_MODULE_INITIALIZED_RETURN_VOID(initialized);

    g_ptr_array_free(callbacks, TRUE);

    ev_poll_remove(udev_monitor_get_fd(udev_mon));

/*    g_source_remove(udev_event_source);
    udev_event_source = 0;
    
    g_io_channel_unref(udev_channel);
    udev_channel = NULL;*/
    
    udev_monitor_unref(udev_mon);
    udev_mon = NULL;
    
    udev_unref(udev);
    udev = NULL;
    initialized = FALSE;
}

gboolean hal_udev_is_initialized()
{
    return initialized;
}

hal_error hal_udev_start(uint8_t const replay)
{
    struct udev_enumerate * enumerate = NULL;
    struct udev_list_entry * devices = NULL;
    struct udev_list_entry * dev_list_entry = NULL;
    struct udev_device * dev = NULL;

    CHECK_MODULE_INITIALIZED(initialized);

    udev_monitor_enable_receiving(udev_mon);
    
    if (replay)
    {
        enumerate = udev_enumerate_new(udev);
    
        udev_enumerate_add_match_subsystem(enumerate, "block");
        udev_enumerate_add_match_subsystem(enumerate, "ubi");
        udev_enumerate_add_match_subsystem(enumerate, "usb");
        udev_enumerate_scan_devices(enumerate);
        
        devices = udev_enumerate_get_list_entry(enumerate);
        udev_list_entry_foreach(dev_list_entry, devices)
        {
            dev = udev_device_new_from_syspath(udev,
                udev_list_entry_get_name(dev_list_entry));
            process_device(dev);
            udev_device_unref(dev);
        }
        udev_enumerate_unref(enumerate);
    }
    
    return HAL_E_OK;
}

hal_error hal_udev_register_callback(hal_device_event_func func)
{
    CHECK_MODULE_INITIALIZED(initialized);
    
    g_ptr_array_add(callbacks, func);

    return HAL_E_OK;
}

// removes first
hal_error hal_udev_unregister_callback(hal_device_event_func func)
{
    CHECK_MODULE_INITIALIZED(initialized);

    return (g_ptr_array_remove(callbacks, func)) ? HAL_E_OK : HAL_E_FAIL;
}
