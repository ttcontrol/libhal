#ifndef __SYSCFG_V1_H
#define __SYSCFG_V1_H

#ifdef __cplusplus
extern "C" {
#endif

#define CHECK_FEATURE(feature) \
    do { \
        if ( !feature ) { return HAL_E_NOFEATURE; } \
    } while (0)

/*! @brief this is an enum
 *  more detailed description...
 */
typedef enum
{
    /*! enum value 1 */
    HAL_SYSCAP_FLASH_NOR,
    /*! enum value 2 */
    HAL_SYSCAP_FLASH_NAND,
    HAL_SYSCAP_RAM,
    HAL_SYSCAP_NVRAM,
    HAL_SYSCAP_DISPLAY,
    HAL_SYSCAP_TOUCHCONTROLLER,
    HAL_SYSCAP_TOUCHSCREEN,
    HAL_SYSCAP_KEYBOARD,
    HAL_SYSCAP_KEYLIGHT,
    HAL_SYSCAP_ENCODER,
    HAL_SYSCAP_LEDS,
    HAL_SYSCAP_AMBIENT_SENSOR,
    HAL_SYSCAP_CAN,
    HAL_SYSCAP_RS232,
    HAL_SYSCAP_CONSOLE,
    HAL_SYSCAP_USB,
    HAL_SYSCAP_SD,
    HAL_SYSCAP_CAMERA,
    HAL_SYSCAP_MODEM,
    HAL_SYSCAP_GPS,
    HAL_SYSCAP_BUZZER,
    HAL_SYSCAP_GPIO,
    HAL_SYSCAP_WAKEUP_RTC,
    HAL_SYSCAP_WLAN,
    HAL_SYSCAP_LAST
} hal_system_caps;

hal_error hal_syscfg_init();
void hal_syscfg_deinit();

/** \internal
 */
hal_error hal_syscfg_init_internal( bool const ignore_config );

hal_error hal_syscfg_get_capability( hal_system_caps const cap_id,
    uint32_t * const value );

// GENERAL
// That's the newest configuration version this library version can parse
#define SYSCFG_NEWEST_VERSION           6
// Feature not available
#define SYSCFG_FIELD_NONE               '0'
#define SYSCFG_ALL_FEATURES             0xFFFFFFFF

// VERSION 1
#define SYSCFG_FIELDS_V1                22
#define SYSCFG_FIELDID_NOR_FLASH        0
#define SYSCFG_NOR_FLASH_ST_32MBIT      'A'

#define SYSCFG_FIELDID_NAND_FLASH       1
#define SYSCFG_NAND_FLASH_MICRON_4GBIT  'A'
#define SYSCFG_NAND_FLASH_MICRON_8GBIT  'B'

#define SYSCFG_FIELDID_RAM              2
#define SYSCFG_RAM_HYNIX_2x1GBIT        'A'
#define SYSCFG_RAM_NANYA_2x1GBIT        'B'
#define SYSCFG_RAM_NANYA_4X1GBIT        'C'

#define SYSCFG_FIELDID_NVRAM            3
#define SYSCFG_NVRAM_RAMTRON_32KBIT     'A'
#define SYSCFG_NVRAM_RAMTRON_64KBIT     'B'

#define SYSCFG_FIELDID_DISPLAY          4
#define SYSCFG_DISPLAY_G070Y2L01        'A'
#define SYSCFG_DISPLAY_NL6448BC2609C    'B'
#define SYSCFG_DISPLAY_G104X1L04        'C'

#define SYSCFG_FIELDID_TOUCHCONTROLLER  5
#define SYSCFG_TOUCH_RESISTIVE_PMIC     'A'

#define SYSCFG_FIELDID_TOUCHSCREEN      6
#define SYSCFG_TOUCH_RESISTIVE_HAN      'A'

#define SYSCFG_FIELDID_KEYBOARD         7
#define SYSCFG_KEYBOARD_A               'A'
#define SYSCFG_KEYBOARD_B               'B'

#define SYSCFG_FIELDID_KEY_LIGHT        8
#define SYSCFG_KEYLIGHT_EL              'A'

#define SYSCFG_FIELDID_ENCODER          9
#define SYSCFG_ENCODER_QUAD             'A'

#define SYSCFG_FIELDID_LEDS             10
#define SYSCFG_LEDS_2xPWM               'A'

#define SYSCFG_FIELDID_AMBSENS          11
#define SYSCFG_AMBSENS_BH1840           'A'

#define SYSCFG_FIELDID_CAN              12
#define SYSCFG_CAN_XC_2X                'A'
#define SYSCFG_CAN_XC_4X                'B'

#define SYSCFG_FIELDID_RS232            13
#define SYSCFG_RS232                    'A'

#define SYSCFG_FIELDID_CONSOLE          14
#define SYSCFG_CONSOLE                  'A'

#define SYSCFG_FIELDID_USB              15
#define SYSCFG_USB_INVALID              'A'
#define SYSCFG_USB_HOST_PHY             'B'
#define SYSCFG_USB_OTG_PHY              'C'
#define SYSCFG_USB_HOST_NOPHY           'D'
#define SYSCFG_USB_OTG_NOPHY            'E'

#define SYSCFG_FIELDID_SD               16
#define SYSCFG_SD                       'A'

#define SYSCFG_FIELDID_CAMERA           17
#define SYSCFG_CAMERA_1CSI_2CAM         'A'
#define SYSCFG_CAMERA_2CSI_2CAM         'B'
#define SYSCFG_CAMERA_2CSI_4CAM         'C'
#define SYSCFG_CAMERA_1CSI_2CAM_MAX9526 SYSCFG_CAMERA_1CSI_2CAM
#define SYSCFG_CAMERA_2CSI_2CAM_MAX9526 SYSCFG_CAMERA_2CSI_2CAM
#define SYSCFG_CAMERA_2CSI_4CAM_MAX9526 SYSCFG_CAMERA_2CSI_4CAM

#define SYSCFG_FIELDID_MODEM            18
#define SYSCFG_MODEM_INTERNAL_GPRS      'A'
#define SYSCFG_MODEM_INTERNAL_EHS8      'B'

#define SYSCFG_FIELDID_GPS              19
#define SYSCFG_GPS_INTERNAL             'A'
#define SYSCFG_GPS_EHS8                 'B'

#define SYSCFG_FIELDID_BUZZER           20
#define SYSCFG_BUZZER_RESONANCE         'A'

#define SYSCFG_FIELDID_GPIO             21
#define SYSCFG_GPIO_3x                  'A'

// VERSION 2
#define SYSCFG_FIELDS_V2                1
#define SYSCFG_FIELDID2_CAMERA          0
#define SYSCFG_CAMERA_1CSI_2CAM_TVP5150 'D'

// VERSION 3
#define SYSCFG_FIELDS_V3                1
#define SYSCFG_FIELDID3_KEYBOARD        0
#define SYSCFG_KEYBOARD_C               'C'

// VERSION 4
#define SYSCFG_FIELDS_V4                3

#define SYSCFG_FIELDID4_WAKEUP_RTC      0
#define SYSCFG_WAKEUP_RTC_EXT           'A'

#define SYSCFG_FIELDID4_WLAN            1
#define SYSCFG_WLAN_TIWI_BLE_EXT_ANT    'A'
#define SYSCFG_WLAN_TIWI_BLE_INT_ANT    'B'

#define SYSCFG_FIELDID4_DISPLAY         2
#define SYSCFG_DISPLAY_G070Y2T02        'D'

// VERSION 5
#define SYSCFG_FIELDS_V5                1

#define SYSCFG_FIELDID5_RAM             0
#define SYSCFG_RAM_NANYA2_4X1GBIT       'C'

// VERSION 6
#define SYSCFG_FIELDS_V6                1

#define SYSCFG_FIELDID6_DISPLAY         0
#define SYSCFG_DISPLAY_NL6448BC2626C    'E'


#ifdef __cplusplus
}  /* End of the 'extern "C"' block */
#endif

#endif
