/**************************************************************************
 * Copyright (c) 2013 TTTControl. All rights reserved. Confidential proprietary
 * Schoenbrunnerstrasse 7, A-1040 Wien, Austria. office[at]ttcontrol.com
 **************************************************************************/
/**********************************************************************//**
 * \file hal_led.h
 *
 * \brief Hardware Abstraction Layer for LEDs.
 *
 **************************************************************************/

#ifndef _HAL_LEDS_H_
#define _HAL_LEDS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "hal_types.h"
#include <glib.h>
#include <stdint.h>

/** \defgroup leds LEDs
 *  \brief Functions and datastructures for LEDs
 *  \{
 */

/** Available LEDs */
typedef enum
{
    HAL_LED_0,  /**< LED 0; only available on HY-eVision² 10.4 (green LED) */
    HAL_LED_1,  /**< LED 1; only available on HY-eVision² 10.4 (red LED)*/
} hal_led;

/** \brief Initialize LEDs module
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_FAIL               Could not initialize module
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 *
 *  \remarks
 *  This function must be called before any other function of this module is called.
 */
hal_error hal_led_init();

/** \brief Deinitialize LEDs module.
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 *
 *  \remarks
 *  All leds are disabled automatically when #hal_led_deinit is called.
 */
hal_error hal_led_deinit();

/**
 * \brief Sets the value of a LED
 *
 * \param[in] value Brightness ( 0 .. 255);\n
 *               maximum brightness: 255\n
 *               minimum brightness: 0
 *
 * \return #hal_error
 * \retval #HAL_E_OK                 Everything fine
 * \retval #HAL_E_FAIL               Could not set state
 * \retval #HAL_E_INVALID_PARAMETER  Invalid parameter passed to the function
 * \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
hal_error hal_led_set_value( hal_led led, uint8_t value );

/**
 * \brief Get the current set value of a LED
 *
 * \param[out] value Current LED brightness (Range: 0..255)
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Could not read LED brightness
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_led_get_value( hal_led led, uint8_t * const value );

/** \} */

#ifdef __cplusplus
}  /* End of the 'extern "C"' block */
#endif

#endif /* _HAL_LEDS_H_ */
