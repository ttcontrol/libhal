/**************************************************************************
 * Copyright (c) 2013 TTTControl. All rights reserved. Confidential proprietary
 * Schoenbrunnerstrasse 7, A-1040 Wien, Austria. office[at]ttcontrol.com
 **************************************************************************/
/**********************************************************************//**
 * \file hal_can.h
 *
 * \brief Hardware Abstraction Layer for CAN.
 *
 * This module only contains functions to initialize and deinitialize a CAN
 * channel with given baudrate. To send/receive CAN frames, please use
 * standard SocketCAN interface (see linux/can.h or https://www.kernel.org/doc/Documentation/networking/can.txt
 * for details).
 *
 * \attention
 * The interface name of a specific CAN channel must be retrieved via
 * #hal_can_get_ifname.
 *
 **************************************************************************/

#ifndef _HAL_CAN_H_
#define _HAL_CAN_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "hal_types.h"
#include <glib.h>
#include <stdint.h>

/** \defgroup can CAN
 *  \brief Functions and datastructures for CAN
 *  \{
 */

/** Available CAN channels */
typedef enum
{
    HAL_CAN_CHANNEL_0,  /**< CAN channel 0 */
    HAL_CAN_CHANNEL_1,  /**< CAN channel 1 */
    HAL_CAN_CHANNEL_2,  /**< CAN channel 2; only available on HY-eVision² 10.4 and HY-eVision² 7.0, 4xCAN */
    HAL_CAN_CHANNEL_3,  /**< CAN channel 3; only available on HY-eVision² 10.4 and HY-eVision² 7.0, 4xCAN */
} hal_can_channel;

/** Available CAN baudrates */
typedef enum
{
    HAL_CAN_BAUD_125K,  /**< 125 kbit/s */
    HAL_CAN_BAUD_250K,  /**< 250 kbit/s */
    HAL_CAN_BAUD_500K,  /**< 500 kbit/s */
    HAL_CAN_BAUD_800K,  /**< 800 kbit/s */
    HAL_CAN_BAUD_1000K, /**< 1000 kbit/s */
} hal_can_baudrate;


/** \brief Initialize CAN module
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_FAIL               Could not initialize module
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 *
 *  \remarks
 *  This function must be called before any other function of this module is called.
 */
hal_error hal_can_init();

/** \brief Deinitialize CAN module.
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 *
 *  \remarks
 *  This function deinitializes all CAN channels automatically.
 */
hal_error hal_can_deinit();

/**
 * \brief Initialize a CAN channel with specified baudrate
 *
 * \param[in] ch        CAN channel which shall be initialized\n
 * \param[in] baudrate  Baudrate with which the specified CAN channel shall
 *                      be initialized.
 *
 * \return #hal_error
 * \retval #HAL_E_OK                 Everything fine
 * \retval #HAL_E_FAIL               Could not set state
 * \retval #HAL_E_INVALID_PARAMETER  Invalid parameter passed to the function
 * \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 *
 * \attention
 * If #hal_can_init_channel is called on a CAN channel which was already
 * initialized, the channel is reinitialized with the given baudrate.
 */
hal_error hal_can_init_channel( hal_can_channel ch, hal_can_baudrate baudrate );

/**
 * \brief Deinitialize a CAN channel
 *
 * \param[in]  ch        CAN channel which shall be deinitialized
 *
 * \return #hal_error
 * \retval #HAL_E_OK                 Everything fine
 * \retval #HAL_E_FAIL               Could not set state
 * \retval #HAL_E_INVALID_PARAMETER  Invalid parameter passed to the function
 * \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 *
 */
hal_error hal_can_deinit_channel( hal_can_channel ch );

/**
 * \brief Gets the interface name of the given CAN channel.
 *
 * Only the interface name returned by this function shall be used for
 * further handling (e.g. read/write via SocketCAN)
 *
 * \param[in] ch        CAN channel
 * \param[out] ifname   Interface name (e.g. "can0" for HAL_CAN_CHANNEL_0);
 *
 * \return #hal_error
 * \retval #HAL_E_OK                 Everything fine
 * \retval #HAL_E_FAIL               Could not set state
 * \retval #HAL_E_INVALID_PARAMETER  Invalid parameter passed to the function
 * \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 *
 */
hal_error hal_can_get_ifname( hal_can_channel ch
                            , uint8_t ** ifname );

/** \} */

#ifdef __cplusplus
}  /* End of the 'extern "C"' block */
#endif

#endif /* _HAL_CAN_H_ */
