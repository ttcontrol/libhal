#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>

#include <glib.h>

#include "libhal.h"

static struct option options[] =
{
    {"help", no_argument, 0, '?'},
    {"led0", required_argument, 0, '0'},
    {"led1", required_argument, 0, '1'},
    {0, 0, 0, 0}
};

void print_help()
{
    printf("HAL LEDs test\n" \
           "  -? --help       This message\n" \
           "  -0 --led0=<n>   switch LED0 off/on\n" \
           "  -1 --led1=<n>   switch LED0 off/on\n" \
           "\n");
}

void leave(int sig)
{
    if (sig != 0)
    {
        printf("SIGINT caught. Exiting.\n");
    }
    hal_device_deinit();
    hal_deinit();
    exit(sig);
}



int main(int argc, char **argv)
{
    hal_error err;
    int option_index = 0;
    int c = 0;
    uint8_t val;
    int led0;
    int led1;
    uint64_t free_ram, total_ram, free_flash, total_flash;
    uint8_t *sn;
    uint32_t release_version;
    uint16_t device_id;
    uint8_t eth_mac_addr[HAL_DEVICE_MAC_SIZE], wlan_mac_addr[HAL_DEVICE_MAC_SIZE];
    uint8_t eth_ip_addr[HAL_DEVICE_IP_SIZE], eth_netmask[HAL_DEVICE_IP_SIZE];
    uint8_t wlan_ip_addr[HAL_DEVICE_IP_SIZE], wlan_netmask[HAL_DEVICE_IP_SIZE];

    while (c != -1)
    {
        option_index = 0;
        c = getopt_long(argc, argv, "?0:1:", options,
                        &option_index);

        switch (c)
        {
            case '?':
                print_help();
                exit(0);
                break;
            case '0':
                if (sscanf(optarg, "%d", &led0) == 0)
                {
                    printf("Missing: LED0");
                    exit(-1);
                }
                break;
            case '1':
                if (sscanf(optarg, "%d", &led1) == 0)
                {
                    printf("Missing: LED1");
                    exit(-1);
                }
                break;
        }
    }

    signal(SIGINT, leave);

    err = hal_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize HAL (%d)\n", err);
        exit(-1);
    }

    err = hal_device_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize Device module (%d)\n", err);
        exit(-1);
    }

    err = hal_device_get_serial_number( &sn );
    if ( err != HAL_E_OK )
    {
        printf("Could not get serial number (%d)\n", err);
        exit(-1);
    }
    else
    {
        printf("Serial number: %s\n", sn);
    }

    err = hal_device_get_release_version( &release_version );
    if ( err != HAL_E_OK )
    {
        printf("Could not get release version (%d)\n", err);
        exit(-1);
    }
    else
    {
        printf("Release version: %x\n", release_version);
    }

    err = hal_device_get_device_id( &device_id );
    if ( err != HAL_E_OK )
    {
        printf("Could not get device id (%d)\n", err);
        exit(-1);
    }
    else
    {
        printf("Release version: %x\n", device_id);
    }

    err = hal_device_get_eth_details( eth_mac_addr, eth_ip_addr, eth_netmask );
    if ( err != HAL_E_OK )
    {
        printf( "Could not get ethernet details (%d)\n", err );
        exit( -1 );
    }
    else
    {
        printf( "ETH: mac=%s\n", eth_mac_addr );
        printf( "ETH: ip=%s\n", eth_ip_addr );
        printf( "ETH: netmask=%s\n", eth_netmask );
    }

    err = hal_device_get_wlan_details( wlan_mac_addr, wlan_ip_addr, wlan_netmask );
    if ( err != HAL_E_OK )
    {
        printf( "Could not get wlan details (%d)\n", err );
    }
    else
    {
        printf( "WLAN: mac=%s\n", wlan_mac_addr );
        printf( "WLAN: ip=%s\n", wlan_ip_addr );
        printf( "WLAN: netmask=%s\n", wlan_netmask );
    }

    err = hal_device_get_ram_info( &total_ram, &free_ram );
    if ( err != HAL_E_OK )
    {
        printf("Could not get free RAM (%d)\n", err);
        exit(-1);
    }
    else
    {
        printf("Free RAM: %llu/%llu kB\n", free_ram / 1024, total_ram / 1024);
    }

    err = hal_device_get_flash_info( &total_flash, &free_flash);
    if ( err != HAL_E_OK )
    {
        printf("Could not get free Flash (%d)\n", err);
        exit(-1);
    }
    else
    {
        printf("Free Flash: %llu/%llu kB\n", free_flash / 1024, total_flash / 1024);
    }

    leave(0);
}
