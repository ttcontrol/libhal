/**************************************************************************
 * Copyright (c) 2013 TTTControl. All rights reserved. Confidential proprietary
 * Schoenbrunnerstrasse 7, A-1040 Wien, Austria. office[at]ttcontrol.com
 **************************************************************************/
/**********************************************************************//**
 * \file hal_keyboard.h
 *
 * \brief Hardware Abstraction Layer for keyboard.
 *
 * This module contains only functions to access non-standard keyboard featuers
 * like key illumination. Key events can be retrieved directly via Linux input events
 * (see linux/input.h for details).
 *
 * The device path which must be used is obtained through
 * hal_keyboard_get_device().
 *
 * ## Keyboard layouts
 * ### HY-eVision2 7.0 and 7.0 touch
 * ![](keyboard-layout-7.png)
 *
 * ### HY-eVision2 10.4
 * ![](keyboard-layout-10.png)
 *
 **************************************************************************/

#ifndef _HAL_KEYBOARD_H_
#define _HAL_KEYBOARD_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "hal_types.h"
#include <glib.h>
#include <stdint.h>
#include <stdbool.h>

/** \defgroup keyboard Keyboard
 *  \brief Functions and datastructures for Keyboard
 *  \{
 */

/** Available states of keyboard backlight */
typedef enum
{
    HAL_KEYB_BL_STATE_OFF,     /**< Keyboard backlight off */
    HAL_KEYB_BL_STATE_ON,      /**< Keyboard backlight on */
} hal_keyboard_bl_state;


/** Available keys for keyboard */
typedef enum
{
    HAL_KEYB_KEY_F1,
    HAL_KEYB_KEY_F2,
    HAL_KEYB_KEY_F3,
    HAL_KEYB_KEY_F4,
    HAL_KEYB_KEY_F5,
    HAL_KEYB_KEY_F6,
    HAL_KEYB_KEY_F7,
    HAL_KEYB_KEY_F8,
    HAL_KEYB_KEY_F9,
    HAL_KEYB_KEY_F10,
} hal_keyboard_key;

/** \brief Initialize keyboard module
 *
 *  Initializes this module and loads the correct keymapping.
 *  If this function is not called, the keyboard layout may not correspond
 *  to the mapping shown in the section "Keyboard layouts".
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_FAIL               Could not initialize module
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
hal_error hal_keyboard_init();

/** \brief Deinitialize keyboard module.
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
hal_error hal_keyboard_deinit();

/**
 * \brief Sets the state (on/off) of the keyboard backlight
 *
 * \param[in] state One of:
 *              - \c #HAL_KEYB_BL_STATE_OFF: Switch the keyboard backlight off
 *              - \c #HAL_KEYB_BL_STATE_ON:  Switch the keyboard backlight on
 *
 * \return #hal_error
 * \retval #HAL_E_OK                 Everything fine
 * \retval #HAL_E_FAIL               Could not set state
 * \retval #HAL_E_INVALID_PARAMETER  Invalid parameter passed to the function
 * \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
hal_error hal_keyboard_set_bl_state( hal_keyboard_bl_state const state );

/**
 * \brief Get the current state of the keyboard backlight
 *
 * \param[out] state Current state
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Could not read keyboard backlight state
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_keyboard_get_bl_state( hal_keyboard_bl_state * const state );

/**
 * \brief Gets the state of a key
 *
 * The function returns if the key is currently pressed and how many times
 * the key was pressed since the last call of this function.
 *
 * \param[in]  key     Key to query
 * \param[out] pressed TRUE if key is currently pressed, FALSE otherwise
 * \param[out] count   Contains the number of presses and releases since last
 *                     call; the value gets incremented on each release event.
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_FAIL             Could not read keyboard key state
 * \retval #HAL_E_NOT_INITIALIZED  Module not initialized
 * \retval #HAL_E_INVALID_PARAMETER  Invalid parameter passed to the function
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_keyboard_get_key_state( hal_keyboard_key key, bool * const pressed, uint16_t * const count );

/**
 * \brief Returns the keyboard's device file
 *
 * Use this function to get the correct device file for the keyboard. This file
 * is needed for accessing keyboard events.
 *
 * \param[out] device Pointer to string that contains the device file path
 *
 * \return #hal_error
 * \retval #HAL_E_OK               Everything fine
 * \retval #HAL_E_NULL_POINTER     A NULL pointer has been passed
 */
hal_error hal_keyboard_get_device(uint8_t const ** device);

/** \} */

#ifdef __cplusplus
}  /* End of the 'extern "C"' block */
#endif

#endif /* _HAL_KEYBOARD_H_ */
