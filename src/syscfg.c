#include "libhal.h"
#include "tools.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <syslog.h>
#include <assert.h>
#include <glib/gprintf.h>


#define MTD_SYSTEM_CONFIG           "sys_cfg"
#define MTD_PROC_FILENAME           "/proc/mtd"
#define SYSCFG_FIELD_SEPARATOR      ';'
#define SYSCFG_FIELD_MAGIC          'O'
#define MODULE_NAME                 "[syscfg] "

#define ASSIGN_FIELD(original, new) \
    do { original = ((original != SYSCFG_FIELD_NONE) && (new == SYSCFG_FIELD_NONE)) ? original : new; } while (0)

static uint32_t version = 0;

static uint32_t nor_flash;
static uint32_t nand_flash;
static uint32_t ram;
static uint32_t nvram;
static uint32_t display;
static uint32_t touchcontroller;
static uint32_t touchscreen;
static uint32_t keyboard;
static uint32_t keyboard_light;
static uint32_t encoder;
static uint32_t leds;
static uint32_t light_sensor;
static uint32_t can;
static uint32_t rs232;
static uint32_t console;
static uint32_t usb;
static uint32_t sd;
static uint32_t camera;
static uint32_t modem;
static uint32_t gps;
static uint32_t buzzer;
static uint32_t gpio;
static uint32_t wakeup_rtc;
static uint32_t wlan;

static gboolean initialized = FALSE;
static bool ignore_syscfg = FALSE;

static GPtrArray *mtd_parts = NULL;

typedef struct
{
    guint8 mtdNum;
    gchar *mtdName;
    guint32 mtdSize;
} MtdPartition;


/* Read one field terminated with SYSCFG_FIELD_TERMINATOR from buf and copy it
 * to field (NULL-terminated). Return value is strlen(field).
 */
static ssize_t syscfg_read_field(unsigned char const * const buf,
    unsigned char * const field)
{
    unsigned char const * field_end = buf;
    int len = 0;

    if ((buf == NULL) || (field == NULL))
    {
        return -1;
    }

    while ((*field_end != 0xFF) && (*field_end != 0x10))
    {
        //printf("%c\n", *field_end);
        if (*field_end == SYSCFG_FIELD_SEPARATOR)
        {
            //printf("separator found\n");
            len = field_end - buf;
            //printf("len=%d\n", len);
            memcpy(field, buf, len);
            field[len] = '\0';
            //printf("field=%s\n\n", field);
            return len;
        }
        field_end++;
    }

    return -1;
}

static int syscfg_read_version(unsigned char ** buf, int * version)
{
    unsigned char field[8];
    int len;

    len = syscfg_read_field(*buf, field);
    if (len < 1)
    {
        return -1;
    }
    else
    {
        if (sscanf(field, "%d", version) == 0)
        {
            return 0;
        }
    }

    *buf = *buf + len + 1;
    return len;
}

static void initialize_caps()
{
    version = 0;
    nor_flash = SYSCFG_FIELD_NONE;
    nand_flash = SYSCFG_FIELD_NONE;
    ram = SYSCFG_FIELD_NONE;
    nvram = SYSCFG_FIELD_NONE;
    display = SYSCFG_FIELD_NONE;
    touchcontroller = SYSCFG_FIELD_NONE;
    touchscreen = SYSCFG_FIELD_NONE;
    keyboard = SYSCFG_FIELD_NONE;
    keyboard_light = SYSCFG_FIELD_NONE;
    encoder = SYSCFG_FIELD_NONE;
    leds = SYSCFG_FIELD_NONE;
    light_sensor = SYSCFG_FIELD_NONE;
    can = SYSCFG_FIELD_NONE;
    rs232 = SYSCFG_FIELD_NONE;
    console = SYSCFG_FIELD_NONE;
    usb = SYSCFG_FIELD_NONE;
    sd = SYSCFG_FIELD_NONE;
    camera = SYSCFG_FIELD_NONE;
    modem = SYSCFG_FIELD_NONE;
    gps = SYSCFG_FIELD_NONE;
    buzzer = SYSCFG_FIELD_NONE;
    gpio = SYSCFG_FIELD_NONE;
    wakeup_rtc = SYSCFG_FIELD_NONE;
    wlan = SYSCFG_FIELD_NONE;
}

void mtd_destroy(gpointer data)
{
    MtdPartition *part = (MtdPartition*)data;

    if (part != NULL)
    {
        g_free(part->mtdName);
    }
}

static gboolean syscfg_parse_mtd()
{
    gchar *buf = NULL;
    gchar *bufp = NULL;
    gsize size;
    GError *err = NULL;
    gboolean retBool;
    int matches;

    int mtdnum;
    int mtdsize;
    int mtderasesize;
    gchar mtdname[64];

    retBool = g_file_get_contents( MTD_PROC_FILENAME, &buf, &size, &err);

    if (retBool == TRUE && size > 0)
    {
        if (mtd_parts != NULL)
        {
            g_ptr_array_unref(mtd_parts);
        }

        mtd_parts = g_ptr_array_new_with_free_func(mtd_destroy);

        bufp = buf;

        while (size > 0)
        {
            matches = sscanf(bufp, "mtd%d: %x %x \"%63[^\"]", &mtdnum, &mtdsize,
                    &mtderasesize, mtdname);

            // Ignore the first line
            if (matches == 4)
            {
                MtdPartition *part = g_new0(MtdPartition, 1);
                part->mtdNum = mtdnum;
                part->mtdName = g_strdup(mtdname);
                part->mtdSize = mtdsize;
                g_ptr_array_add(mtd_parts, part);
            }

            // Discard the reset of the line
            while (size > 0 && *bufp != '\n')
            {
                bufp++;
                size--;
            }

            if (size > 0)
            {
                bufp++;
                size--;
            }
        }

        hal_log(HAL_LOG_SEVERITY_DEBUG, MODULE_NAME
                "Parsed MTD devices:");

        for (int i = 0; i < mtd_parts->len; i++)
        {
            MtdPartition *part = g_ptr_array_index(mtd_parts, i);
            hal_log(HAL_LOG_SEVERITY_DEBUG, MODULE_NAME
                    "Name: %s\t\tNumber: %u\tSize: 0x%x", part->mtdName,
                    part->mtdNum, part->mtdSize);
        }
    }
    else
    {
        hal_log(HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Could not get content of %s:\n%s", MTD_PROC_FILENAME, err->message);
    }

    return retBool;
}

static MtdPartition* syscfg_get_mtd(gchar *name)
{
    if (mtd_parts != NULL)
    {
        for (int i = 0; i < mtd_parts->len; i++)
        {
            MtdPartition *part = g_ptr_array_index(mtd_parts, i);
            if (g_strcmp0(part->mtdName, name) == 0)
            {
                return part;
            }
        }
    }

    return NULL;
}

hal_error syscfg_parse_v1(char const * const field, int field_id, int len)
{
    switch (field_id)
    {
        case SYSCFG_FIELDID_NOR_FLASH:
            nor_flash = field[0];
            break;
        case SYSCFG_FIELDID_NAND_FLASH:
            nand_flash = field[0];
            break;
        case SYSCFG_FIELDID_RAM:
            ram = field[0];
            break;
        case SYSCFG_FIELDID_NVRAM:
            nvram = field[0];
            break;
        case SYSCFG_FIELDID_DISPLAY:
            display = field[0];
            break;
        case SYSCFG_FIELDID_TOUCHCONTROLLER:
            touchcontroller = field[0];
            break;
        case SYSCFG_FIELDID_TOUCHSCREEN:
            touchscreen = field[0];
            break;
        case SYSCFG_FIELDID_KEYBOARD:
            keyboard = field[0];
            break;
        case SYSCFG_FIELDID_KEY_LIGHT:
            keyboard_light = field[0];
            break;
        case SYSCFG_FIELDID_ENCODER:
            encoder = field[0];
            break;
        case SYSCFG_FIELDID_LEDS:
            leds = field[0];
            break;
        case SYSCFG_FIELDID_AMBSENS:
            light_sensor = field[0];
            break;
        case SYSCFG_FIELDID_CAN:
            can = field[0];
            break;
        case SYSCFG_FIELDID_RS232:
            rs232 = field[0];
            break;
        case SYSCFG_FIELDID_CONSOLE:
            console = field[0];
            break;
        case SYSCFG_FIELDID_USB:
            usb = field[0];
            break;
        case SYSCFG_FIELDID_SD:
            sd = field[0];
            break;
        case SYSCFG_FIELDID_CAMERA:
            camera = field[0];
            break;
        case SYSCFG_FIELDID_MODEM:
            modem = field[0];
            break;
        case SYSCFG_FIELDID_GPS:
            gps = field[0];
            break;
        case SYSCFG_FIELDID_BUZZER:
            buzzer = field[0];
            break;
        case SYSCFG_FIELDID_GPIO:
            gpio = field[0];
            break;
        default:
            return HAL_E_FAIL;
            break;
    }
    return HAL_E_OK;
}

hal_error syscfg_parse_v2(char const * const field, int field_id, int len)
{
    switch (field_id)
    {
        case SYSCFG_FIELDID2_CAMERA:
            ASSIGN_FIELD(camera, field[0]);
            break;
        default:
            return HAL_E_FAIL;
            break;
    }
    return HAL_E_OK;
}

hal_error syscfg_parse_v3(char const * const field, int field_id, int len)
{
    switch (field_id)
    {
        case SYSCFG_FIELDID3_KEYBOARD:
            ASSIGN_FIELD(keyboard, field[0]);
            break;
        default:
            return HAL_E_FAIL;
            break;
    }
    return HAL_E_OK;
}

hal_error syscfg_parse_v4(char const * const field, int field_id, int len)
{
    switch (field_id)
    {
        case SYSCFG_FIELDID4_WAKEUP_RTC:
            ASSIGN_FIELD(wakeup_rtc, field[0]);
            break;
        case SYSCFG_FIELDID4_WLAN:
            ASSIGN_FIELD(wlan, field[0]);
            break;
        case SYSCFG_FIELDID4_DISPLAY:
            ASSIGN_FIELD(display, field[0]);
            break;
        default:
            return HAL_E_FAIL;
            break;
    }
    return HAL_E_OK;
}

hal_error syscfg_parse_v5(char const * const field, int field_id, int len)
{
    switch (field_id)
    {
        case SYSCFG_FIELDID5_RAM:
            ASSIGN_FIELD(ram, field[0]);
            break;
        default:
            return HAL_E_FAIL;
            break;
    }
    return HAL_E_OK;
}

hal_error syscfg_parse_v6(char const * const field, int field_id, int len)
{
    switch (field_id)
    {
        case SYSCFG_FIELDID6_DISPLAY:
            ASSIGN_FIELD(display, field[0]);
            break;
        default:
            return HAL_E_FAIL;
            break;
    }
    return HAL_E_OK;
}

hal_error hal_syscfg_init()
{
    return hal_syscfg_init_internal( FALSE );
}

hal_error hal_syscfg_init_internal( bool const ignore_config )
{
    int fd;
    unsigned char system_config[128], field[8];
    ssize_t len = 0;
    int version, f, fields;
    unsigned char *p;
    hal_error err;
    gboolean retBool;
    char mtd_path[80];
    MtdPartition *mtd_part;

    CHECK_MODULE_ALREADY_INITIALIZED( initialized);

    initialize_caps();

    if (!ignore_config )
    {
        retBool = syscfg_parse_mtd();
        if (retBool == FALSE)
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Could not parse mtd partition information");
            return HAL_E_FAIL;
        }

        mtd_part = syscfg_get_mtd(MTD_SYSTEM_CONFIG);
        if (mtd_part == NULL)
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME "Could not get syscfg mtd partition");
            return HAL_E_FAIL;
        }

        g_sprintf(mtd_path, "/dev/mtd%d", mtd_part->mtdNum);
        // Get system config data from MTD partition (NOR flash, normally)
        fd = open( mtd_path, O_RDONLY );
        if ( fd == -1 )
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Could not open system config device " MTD_SYSTEM_CONFIG ": %d",
                errno );
            return HAL_E_FAIL;
        }
        len = read( fd, system_config, 128 );
        if ( len == -1 )
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Could not read system configuration (%d)", errno );
            close( fd );
            return HAL_E_FAIL;
        }
        close( fd );

        p = system_config;
        // First byte must be magic
        if ( *p++ != SYSCFG_FIELD_MAGIC )
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Could not find magic byte in system config device" );
            return HAL_E_FAIL;
        }

        // Loop while there is either
        // - a system config version to read (end of config not reached)
        // - the previous round did not find an unknown version
        while (fields > -1)
        {
            int version_len = syscfg_read_version( &p, &version );
            // At end of system configuration
            if ( version_len == -1 )
            {
                fields = -1;
            }
            // Could not parse version number
            else if ( version_len == 0 )
            {
                hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                    "Could not read version information" );
                return HAL_E_FAIL;
            }
            else
            {
                hal_log( HAL_LOG_SEVERITY_INFO, MODULE_NAME
                    "Parsing system config version %d", version);
                // If system config version is known set the number of fields expected
                // If the version is unknown, exit the loop
                switch ( version )
                {
                    case 1:
                        fields = SYSCFG_FIELDS_V1;
                        break;
                    case 2:
                        fields = SYSCFG_FIELDS_V2;
                        break;
                    case 3:
                        fields = SYSCFG_FIELDS_V3;
                        break;
                    case 4:
                        fields = SYSCFG_FIELDS_V4;
                        break;
                    case 5:
                        fields = SYSCFG_FIELDS_V5;
                        break;
                    case 6:
                        fields = SYSCFG_FIELDS_V6;
                        break;
                    default:
                        hal_log( HAL_LOG_SEVERITY_INFO, MODULE_NAME
                            "System config version unknown");
                        fields = -1;
                        break;
                }

                // Loop through all fields for version and parse value
                for ( f = 0; f < fields; f++ )
                {
                    len = syscfg_read_field( p, field );

                    hal_log( HAL_LOG_SEVERITY_INFO, MODULE_NAME
                        "Field %d = %c", f, field[0]);

                    if ( len < 0 )
                    {
                        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                            "Not enough fields (%d != %d)", f, fields );
                        return HAL_E_FAIL;
                    }

                    switch ( version )
                    {
                        case 1:
                            err = syscfg_parse_v1( p, f, len );
                            break;
                        case 2:
                            err = syscfg_parse_v2( p, f, len );
                            break;
                        case 3:
                            err = syscfg_parse_v3( p, f, len );
                            break;
                        case 4:
                            err = syscfg_parse_v4( p, f, len );
                            break;
                        case 5:
                            err = syscfg_parse_v5( p, f, len );
                            break;
                        case 6:
                            err = syscfg_parse_v6( p, f, len );
                            break;
                        default:
                            // This should never happen; if version is unknown
                            // fields is -1, so we do not enter this loop
                            assert( "Want to parse unknown version" );
                            break;
                    }

                    if ( err != HAL_E_OK )
                    {
                        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                            "Error while parsing parameter #%d (version %d)",
                            f, version );
                        return err;
                    }

                    p = p + len + 1;
                }
            }
        }
    }

    ignore_syscfg = ignore_config;
    initialized = TRUE;

    return HAL_E_OK;
}

void hal_syscfg_deinit()
{
    initialize_caps();
    initialized = FALSE;
    ignore_syscfg = FALSE;

    if (mtd_parts != NULL)
    {
        g_ptr_array_unref(mtd_parts);
        mtd_parts = NULL;
    }
}

hal_error hal_syscfg_get_capability(hal_system_caps const cap_id,
    uint32_t * const value)
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_POINTER_PARAMETER( value );

    if ( ignore_syscfg )
    {
        *value = 0xFFFFFFFF;
        return HAL_E_OK;
    }

    switch ( cap_id )
    {
        case HAL_SYSCAP_FLASH_NOR:
            *value = nor_flash;
            break;
        case HAL_SYSCAP_FLASH_NAND:
            *value = nand_flash;
            break;
        case HAL_SYSCAP_RAM:
            *value = ram;
            break;
        case HAL_SYSCAP_NVRAM:
            *value = nvram;
            break;
        case HAL_SYSCAP_DISPLAY:
            *value = display;
            break;
        case HAL_SYSCAP_TOUCHCONTROLLER:
            *value = touchcontroller;
            break;
        case HAL_SYSCAP_TOUCHSCREEN:
            *value = touchscreen;
            break;
        case HAL_SYSCAP_KEYBOARD:
            *value = keyboard;
            break;
        case HAL_SYSCAP_KEYLIGHT:
            *value = keyboard_light;
            break;
        case HAL_SYSCAP_ENCODER:
            *value = encoder;
            break;
        case HAL_SYSCAP_LEDS:
            *value = leds;
            break;
        case HAL_SYSCAP_AMBIENT_SENSOR:
            *value = light_sensor;
            break;
        case HAL_SYSCAP_CAN:
            *value = can;
            break;
        case HAL_SYSCAP_RS232:
            *value = rs232;
            break;
        case HAL_SYSCAP_CONSOLE:
            *value = console;
            break;
        case HAL_SYSCAP_USB:
            *value = usb;
            break;
        case HAL_SYSCAP_SD:
            *value = sd;
            break;
        case HAL_SYSCAP_CAMERA:
            *value = camera;
            break;
        case HAL_SYSCAP_MODEM:
            *value = modem;
            break;
        case HAL_SYSCAP_GPS:
            *value = gps;
            break;
        case HAL_SYSCAP_BUZZER:
            *value = buzzer;
            break;
        case HAL_SYSCAP_GPIO:
            *value = gpio;
            break;
        case HAL_SYSCAP_WAKEUP_RTC:
            *value = wakeup_rtc;
            break;
        case HAL_SYSCAP_WLAN:
            *value = wlan;
            break;

        default:
            return HAL_E_INVALID_PARAMETER;
    }

    return HAL_E_OK;
}
