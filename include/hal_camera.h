/**************************************************************************
 * Copyright (c) 2013 TTTControl. All rights reserved. Confidential proprietary
 * Schoenbrunnerstrasse 7, A-1040 Wien, Austria. office[at]ttcontrol.com
 **************************************************************************/
/**********************************************************************//**
 * \file hal_camera.h
 *
 * \brief Hardware Abstraction Layer for camera device.
 *
 * The hardware abstraction layer currently only supports viewport 1. This
 * means that for all devices with two camera inputs both channels can be
 * shown. For variants with 4 camera inputs only channels
 * one and two can be displayed. For that reason only value
 * #HAL_CAMERA_VIEWPORT_1 may be used as value for parameter viewport.
 *
 * \image html camera-channels.png
 *
 * Please note that the dual-camera feature available in CODESYS is
 * currently also not supported.

 **************************************************************************/

#ifndef _HAL_CAMERA_H_
#define _HAL_CAMERA_H_

#ifdef __cplusplus
extern "C" {
#endif

/** \defgroup camera Camera
 *  \brief Functions and datastructures for Camera
 *  \{
 */

/** \brief Viewport identifiers
 */
typedef enum 
{
    /** \brief Viewport 1 */
    HAL_CAMERA_VIEWPORT_1 = 0,
    /** \brief Viewport 2 - not supported! */
    HAL_CAMERA_VIEWPORT_2
} hal_camera_viewport;
 
/** \brief Camera channels
 *  Channel|Connector HY-eVision² 10.4
 *  -------|--------------------------
 *       1 | C5
 *       2 | C4
 */
typedef enum
{
    /** Select camera channel 1 */
    HAL_CAMERA_CHANNEL_1,
    /** Select camera channel 2 */
    HAL_CAMERA_CHANNEL_2,
} hal_camera_channel;

#if 0
/** \brief Camera rotation
 */
typedef enum
{
    /** No rotation */
    HAL_CAMERA_ROTATION_NONE = 0,
    /** Flip vertically */
    HAL_CAMERA_ROTATION_VFLIP = 1,
    /** Flip horizontally */
    HAL_CAMERA_ROTATION_HFLIP = 2,
    HAL_CAMERA_ROTATION_180 = 3,
    HAL_CAMERA_ROTATION_90CW = 4,
    HAL_CAMERA_ROTATION_90CW_VFLIP = 5,
    HAL_CAMERA_ROTATION_90CW_HFLIP = 6,
    HAL_CAMERA_ROTATION_90CCW = 7
} hal_camera_rotation;

#endif
 
/** \brief Initialize camera subsytem
 *  \remarks
 *  This function must be called before any other function of this module is called.*
 */
hal_error hal_camera_init();

/** \brief Finalize camera subsytem
 */
void hal_camera_deinit();

/** \brief Start camera playback
 *  \param[in]   viewport  Identifier of viewport
 */
hal_error hal_camera_play(hal_camera_viewport const viewport);

/** \brief Stop camera playback
 *  \param[in]   viewport  Identifier of viewport
 */
hal_error hal_camera_stop(hal_camera_viewport const viewport);

/** \brief Get X position of camera picture
 *  \param[in]   viewport  Identifier of viewport
 *  \param[out]  x         X coordinate in pixels from upper left screen corner
 */
hal_error hal_camera_get_x(hal_camera_viewport const viewport,
    uint32_t * const x);

/** \brief Set X position of camera picture
 *  \param[in]   viewport  Identifier of viewport
 *  \param[in]   x         X coordinate in pixels from upper left screen corner
 */
hal_error hal_camera_set_x(hal_camera_viewport const viewport,
    uint32_t const x);

/** \brief Get Y position of camera picture
 *  \param[in]   viewport  Identifier of viewport
 *  \param[out]  y         Y coordinate in pixels from upper left screen corner
 */
hal_error hal_camera_get_y(hal_camera_viewport const viewport,
    uint32_t * const y);

/** \brief Set Y position of camera picture
 *  \param[in]   viewport  Identifier of viewport
 *  \param[in]   y         Y coordinate in pixels from upper left screen corner
 */
hal_error hal_camera_set_y(hal_camera_viewport const viewport,
    uint32_t const y);

/** \brief Get width of camera picture
 *  \param[in]   viewport  Identifier of viewport
 *  \param[out]  width     Width of displayed camera image in pixel
 */
hal_error hal_camera_get_width(hal_camera_viewport const viewport,
    uint32_t * const width);

/** \brief Set width of camera picture
 *  \param[in]   viewport  Identifier of viewport
 *  \param[in]   width     Width of displayed camera image in pixel
 */
hal_error hal_camera_set_width(hal_camera_viewport const viewport,
    uint32_t const width);

/** \brief Get height of camera picture
 *  \param[in]   viewport  Identifier of viewport
 *  \param[out]  height    Height of displayed camera image in pixel
 */
hal_error hal_camera_get_height(hal_camera_viewport const viewport,
    uint32_t * const height);

/** \brief Set height of camera picture
 *  \param[in]   viewport  Identifier of viewport
 *  \param[in]   height    Height of displayed camera image in pixel
 */
hal_error hal_camera_set_height(hal_camera_viewport const viewport,
    uint32_t const height);

/** \brief Sets position of camera picture
 *  \param[in]   viewport  Identifier of viewport
 *  \param[out]  x         X coordinate in pixels from upper left screen corner
 *  \param[out]  y         Y coordinate in pixels from upper left screen corner
 *  \param[out]  width     Width of displayed camera image in pixel
 *  \param[out]  height    Height of displayed camera image in pixel
 */
hal_error hal_camera_get_position(hal_camera_viewport const viewport,
    uint32_t * const x, uint32_t * const y, uint32_t * const width,
    uint32_t * const height);

/** \brief Sets position of camera picture
 *  \param[in]   viewport  Identifier of viewport
 *  \param[in]   x         X coordinate in pixels from upper left screen corner
 *  \param[in]   y         Y coordinate in pixels from upper left screen corner
 *  \param[in]   width     Width of displayed camera image in pixel
 *  \param[in]   height    Height of displayed camera image in pixel
 */
hal_error hal_camera_set_position(hal_camera_viewport const viewport,
    uint32_t const x, uint32_t const y, uint32_t const width,
    uint32_t const height);
    
/** \brief Get camera channel
 *  \param[in]   viewport  Identifier of viewport
 *  \param[out]  channel   Currently viewed channel
 */
hal_error hal_camera_get_channel(hal_camera_viewport const viewport,
    hal_camera_channel * const channel);

/** \brief Set camera channel
 *  \param[in]   viewport  Identifier of viewport
 *  \param[in]   channel   Channel to view
 */
hal_error hal_camera_set_channel(hal_camera_viewport const viewport,
    hal_camera_channel const channel);

/** \brief Get camera orientation
 *  \param[in]   viewport  Identifier of viewport
 *  \param[out]  rotation  Current orientation
 */
//uint8_t hal_camera_get_rotation(hal_camera_viewport const viewport,
//    hal_camera_rotation * const rotation);

/** \brief Set camera orientation
 *  \param[in]   viewport  Identifier of viewport
 *  \param[in]   rotation  New orientation
 */
//uint8_t hal_camera_set_rotation(hal_camera_viewport const viewport,
//    hal_camera_rotation const rotation);

/** \} */

#ifdef __cplusplus
}  /* End of the 'extern "C"' block */
#endif

#endif
