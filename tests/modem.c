#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>

#include <glib.h>
#include <gps.h>

#include "libhal.h"
#include "hal_modem.h"

#define BUF_SIZE                1024

#define TEST_MODEM_GET_INFO     0
#define TEST_MODEM_SEND_SMS     1

static struct option options[] =
{
    {"help", no_argument, 0, '?'},
    {"test", required_argument, 0, 't'},
    {0, 0, 0, 0}
};

static int error_cnt = 0;

void print_help()
{
    printf("HAL Modem test\n" \
           "  -? --help       This message\n" \
           "  -t --test=<n>   Test program to run\n" \
           "                  0 ... Show various information;\n" \
           "                        PIN must be deactivated beforehand\n" \
           "\n");
}

void leave(int sig)
{
    if (sig != 0)
    {
        printf("SIGINT caught. Exiting.\n");
    }
    printf("Error counter: %d\n", error_cnt);

    hal_modem_deinit();
    hal_deinit();
    exit(sig);
}

static void test_modem_get_info()
{
    hal_modem_result_code res;
    hal_modem_registration reg;
    uint8_t quality;
    uint8_t buf[BUF_SIZE];
    uint32_t size;
    hal_error err;
    hal_modem_pin_status pin_status;

    while (1) {
    err = hal_modem_get_registration( &reg, &res );
    if ( err == HAL_E_OK )
    {
        printf("Network registration (result: %u): %u\n", res, reg);
    }
    else
    {
        printf("Could not get network registration (result: %u)\n", res);
        error_cnt++;
    }
    
    size = BUF_SIZE;
    err = hal_modem_get_operator_name2( buf, &size, &res );
    if ( err == HAL_E_OK )
    {
        printf("Network operator (result: %u): %s (size: %u)\n", res, buf, size);
    }
    else
    {
        printf("Could not get network operator (result: %u)\n", res);
        error_cnt++;
    } 

    err = hal_modem_get_operator_name( buf, &res );
    if ( err == HAL_E_OK )
    {
        printf("Network operator (result: %u): %s\n", res, buf);
    }
    else
    {
        printf("Could not get network operator (result: %u)\n", res);
        error_cnt++;
    } 
    
    size = BUF_SIZE;
    err = hal_modem_get_imsi2( buf, &size, &res );
    if ( err == HAL_E_OK )
    {
        printf("IMSI (result: %u): %s (size: %u)\n", res, buf, size);
    }
    else
    {
        printf("Could not get IMSI (result: %u)\n", res);
        error_cnt++;
    } 

    err = hal_modem_get_imsi( buf, &res );
    if ( err == HAL_E_OK )
    {
        printf("IMSI (result: %u): %s\n", res, buf);
    }
    else
    {
        printf("Could not get IMSI (result: %u)\n", res);
        error_cnt++;
    } 

    size = BUF_SIZE;
    err = hal_modem_get_imei2( buf, &size, &res);
    if ( err == HAL_E_OK )
    {
        printf("IMEI (result: %u): %s (size: %u)\n", res, buf, size);
    }
    else
    {
        printf("Could not get IMEI (result: %u)\n", res);
        error_cnt++;
    }

    err = hal_modem_get_imei( buf, &res);
    if ( err == HAL_E_OK )
    {
        printf("IMEI (result: %u): %s\n", res, buf);
    }
    else
    {
        printf("Could not get IMEI (result: %u)\n", res);
        error_cnt++;
    } 

     err = hal_modem_get_signal_quality( &quality, &res );
    if ( err == HAL_E_OK )
    {
        printf("Signal quality (result: %u): %u\n", res, quality );
    } 
    else
    {
        printf("Could not get signal quality (result: %u)\n", res);
        error_cnt++;
    } 
    } 
}

static void test_modem_send_sms()
{
    // Example how a SMS can be sent
    hal_modem_send_sms("+0123456789", "Hello World", NULL );
}


int main(int argc, char **argv)
{
    hal_error err;
    int ret;
    int option_index = 0;
    int c = 0;
    int test_program = 0;

    while (c != -1)
    {
        option_index = 0;
        c = getopt_long(argc, argv, "?t:", options,
                        &option_index);

        switch (c)
        {
            case '?':
                print_help();
                exit(0);
                break;
            case 't':
                if (sscanf(optarg, "%d", &test_program) == 0)
                {
                    printf("Missing: Test program missing.");
                    exit(-1);
                }
                break;
        }
    }


    signal(SIGINT, leave);

    err = hal_init();
    if ( err != HAL_E_OK )
    {
	hal_deinit();
        printf("Could not initialize HAL (%d)\n", err);
        exit(-1);
    }

    err = hal_modem_init( );
    if ( err != HAL_E_OK )
    {
	hal_deinit();
        printf("Could not initialize modem module (%d)\n", err);
        exit(-1);
    }

    switch (test_program)
    {
        case TEST_MODEM_GET_INFO:
            test_modem_get_info();
            break;
        case TEST_MODEM_SEND_SMS:
            test_modem_send_sms();
            break;
        default:
            printf("Undefined test program! Abort.");
            break;
    } 

    leave(0);
}
