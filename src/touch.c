#include "hal.h"
#include "hal_types.h"
#include "tools.h"
#include <syslog.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#define MODULE_NAME             "[touchscreen] "
#define DEV_TOUCHSCREEN         "/dev/input/touchscreen"
#define DEV_TOUCHSCREEN_PCT     "/dev/input/touchscreen-pct"

#define SYSFS_TOUCH_ROOT         "/sys/devices/platform/mxc_ts"
#define SYSFS_TOUCH_ROOT_PCT     "/sys/devices/platform/imx-i2c.1/i2c-1/1-004a"
#define SYSFS_TOUCH_DISABLE_FILE "disable"
#define SYSFS_DRAG_ENABLE_FILE   "drag_enable"

static gboolean initialized = FALSE;

static hal_error touch_set_file(gchar *file, bool enable, bool optional)
{
    int fd;
    hal_error err;

    fd = open( file, O_RDWR );
    if ( fd == -1 )
    {
        if (!optional)
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Could not open touch file '%s': %d", file, errno );
            err = HAL_E_FAIL;
        }
        else
        {
            err = HAL_E_OK;
        }
    }
    else
    {
        if ( enable )
        {
            err = file_set_value_str(fd, "1");
        }
        else
        {
            err = file_set_value_str(fd, "0");
        }

        if ( err != HAL_E_OK )
        {
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Could not write to file '%s': %d", file, errno );
        }

        close(fd);
    }

    return err;
}


hal_error hal_touch_init()
{
    CHECK_HAL_INITIALIZED();
    CHECK_MODULE_ALREADY_INITIALIZED( initialized );

    initialized = TRUE;
    return HAL_E_OK;
}

hal_error hal_touch_deinit()
{
    CHECK_MODULE_INITIALIZED( initialized );

    initialized = FALSE;
    return HAL_E_OK;
}


hal_error hal_touch_get_device(uint8_t ** device)
{
    if (device == NULL)
    {
        return HAL_E_NULL_POINTER;
    }

    if (g_file_test(DEV_TOUCHSCREEN_PCT, G_FILE_TEST_EXISTS))
    {
        *device = DEV_TOUCHSCREEN_PCT;
    }
    else
    {
        *device = DEV_TOUCHSCREEN;
    }
    return HAL_E_OK;
}

hal_error hal_touch_set_enable( bool enable )
{
    hal_error err;

    CHECK_MODULE_INITIALIZED( initialized );

    err = touch_set_file(SYSFS_TOUCH_ROOT "/" SYSFS_TOUCH_DISABLE_FILE, !enable, false);
    if ( err == HAL_E_OK )
    {
        err = touch_set_file(SYSFS_TOUCH_ROOT_PCT "/" SYSFS_TOUCH_DISABLE_FILE, !enable, true);
    }

    return err;
}

hal_error hal_touch_set_mode( hal_touch_mode mode )
{
    CHECK_MODULE_INITIALIZED( initialized );

    int fd;
    hal_error err;
    bool enable;

    switch (mode)
    {
        case HAL_TOUCH_MODE_DRAG_ENABLE:
            enable = true;
            break;
        case HAL_TOUCH_MODE_DRAG_DISABLE:
            enable = false;
            break;
        default:
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Invalid value for 'mode' parameter: %u", mode);
            return HAL_E_INVALID_PARAMETER;
    }

    err = touch_set_file(SYSFS_TOUCH_ROOT "/" SYSFS_DRAG_ENABLE_FILE, enable, false);
    if ( err == HAL_E_OK )
    {
        err = touch_set_file(SYSFS_TOUCH_ROOT_PCT "/" SYSFS_DRAG_ENABLE_FILE, enable, true);
    }

    return err;
}
