#include "hal_keyboard.h"
#include "hal.h"
#include "hal_types.h"
#include "ev_poll.h"
#include "tools.h"

#include <linux/input.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <syslog.h>
#include <glib.h>

#define MODULE_NAME         "[keyboard] "
#define DEV_KEYBOARD        "/dev/input/keypad"
#define DEV_KEYBOARD_BL     "/var/links/gpio/keyboard-backlight/value"
#define BIN_KEYMAP          "/lib/udev/keymap"

#define KEYMAP_TYPE_A       "/lib/udev/keymaps/vision2_a"
#define KEYMAP_TYPE_B       "/lib/udev/keymaps/vision2_b"
#define MAX_KEYS            10

static int fd_keyboard_bl = -1;
static int fd_keyboard_input = -1;

static gboolean initialized = FALSE;
static GMutex mutex;
static hal_keyboard_bl_state current_state;

static gboolean cap_has_keyboard = FALSE;
static gboolean cap_has_bl = FALSE;

static key_state_t key_state[MAX_KEYS];

static hal_error load_keymap(uint32_t keyboard_type)
{
    char * keymap = NULL;
    char keymap_cmd[64];
    int result = 0;

    switch (keyboard_type)
    {
        case SYSCFG_KEYBOARD_A:
            keymap = KEYMAP_TYPE_A;
            break;
            
        case SYSCFG_KEYBOARD_B:
        case SYSCFG_KEYBOARD_C:
            keymap = KEYMAP_TYPE_B;
            break;
            
        default:
            hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
                "Could not identify keyboard (type %d)\n", keyboard_type );
            return HAL_E_FAIL;
    }
    
    snprintf(keymap_cmd, 64, BIN_KEYMAP " " DEV_KEYBOARD " %s", keymap);
    result = fakesystem(keymap_cmd);
    
    if (result != 0)
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not set keymap: %d", result );
        return HAL_E_FAIL;
    }
    
    return HAL_E_OK;
}

static hal_error check_keyboard_caps()
{
    hal_error err;
    uint32_t keyboard_cap, bl_cap;

    cap_has_keyboard = FALSE;
    cap_has_bl = FALSE;
    err = hal_syscfg_get_capability( HAL_SYSCAP_KEYBOARD, &keyboard_cap );
    err |= hal_syscfg_get_capability( SYSCFG_FIELDID_KEY_LIGHT, &bl_cap );
    if ( err != HAL_E_OK )
    {
        return HAL_E_FAIL;
    }

    err = HAL_E_OK;
    
    switch ( keyboard_cap )
    {
        case SYSCFG_KEYBOARD_A:
        case SYSCFG_KEYBOARD_B:
        case SYSCFG_KEYBOARD_C:
            load_keymap( keyboard_cap );
            cap_has_keyboard = TRUE;
            break;
        case SYSCFG_ALL_FEATURES:
            cap_has_keyboard = TRUE;
            break;
        default:
            err = HAL_E_NOFEATURE;
            break;
    }
    
    switch ( bl_cap )
    {
        case SYSCFG_KEYLIGHT_EL:
        case SYSCFG_ALL_FEATURES:
            cap_has_bl = TRUE;
            break;
    }
    
    return err;
}

static gpointer ev_poll_callback ( gpointer data )
{
    int bytes_read;
    struct input_event ev;

    if ( fd_keyboard_input == -1 )
    {
        return NULL;
    }

    if ( read(fd_keyboard_input, &ev, sizeof(ev)) == sizeof(ev) )
    {
        if ( ev.type == EV_KEY )
        {
            if ( ev.code >= KEY_F1 && ev.code <= KEY_F10 )
            {
                int key_idx = ev.code - KEY_F1;

                g_mutex_lock( &mutex );

                if ( ev.value == 1 )
                {
                    key_state[key_idx].pressed = TRUE;
                }
                else
                {
                    key_state[key_idx].pressed = FALSE;
                    if ( key_state[key_idx].count < UINT16_MAX )
                    {
                        key_state[key_idx].count++;
                    }
                }

                g_mutex_unlock( &mutex );
            }
        }
    }

    return NULL;
}

hal_error hal_keyboard_init()
{
    hal_error err = HAL_E_OK;
    
    CHECK_HAL_INITIALIZED();
    CHECK_MODULE_ALREADY_INITIALIZED( initialized );

    fd_keyboard_bl = open( DEV_KEYBOARD_BL, O_RDWR );
    if ( fd_keyboard_bl == -1 )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not open keyboard backlight device node " DEV_KEYBOARD_BL
            ": %d", errno );
        return HAL_E_FAIL;
    }
    
    fd_keyboard_input = open( DEV_KEYBOARD, O_RDONLY );
    if ( fd_keyboard_input == -1 )
    {
        hal_log( HAL_LOG_SEVERITY_ERROR, MODULE_NAME
            "Could not open keyboard input device node " DEV_KEYBOARD ": %d",
            errno );
        return HAL_E_FAIL;
    }

    err = check_keyboard_caps();
    if ( err == HAL_E_FAIL)
    {
        return HAL_E_FAIL;
    }
    else if ( err == HAL_E_NOFEATURE )
    {
        initialized = TRUE;
        err = HAL_E_NOFEATURE;
    }

    for ( int i = 0; i < MAX_KEYS; i++ )
    {
        key_state[i].count = 0;
        key_state[i].pressed = FALSE;
    }

    ev_poll_add( fd_keyboard_input
               , POLLIN
               , &ev_poll_callback
               , NULL);

    if ( err == HAL_E_OK )
    {
        initialized = TRUE;
    }
    
    // Set current state to ON so set_bl_state really sets the BL
    current_state = HAL_KEYB_BL_STATE_ON;
    hal_keyboard_set_bl_state(HAL_KEYB_BL_STATE_OFF);
    
    return err;
}

hal_error hal_keyboard_deinit()
{
    CHECK_MODULE_INITIALIZED( initialized );

    /* Switch off backlight before deinit */
    hal_keyboard_set_bl_state( HAL_DISP_BL_STATE_OFF );

    if ( fd_keyboard_bl != -1 )
    {
        close( fd_keyboard_bl );
        fd_keyboard_bl = -1;
    }

    if ( fd_keyboard_input != -1 )
    {
        ev_poll_remove( fd_keyboard_input );
        close(fd_keyboard_input);
        fd_keyboard_input = -1;
    }

    initialized = FALSE;

    return HAL_E_OK;
}

hal_error hal_keyboard_set_bl_state( hal_keyboard_bl_state state )
{
    hal_error err;
    int val;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_bl );

    if (current_state == state)
    {
        return HAL_E_OK;
    }

    switch ( state )
    {
        case HAL_KEYB_BL_STATE_OFF:
            val = 0;
            break;
        case HAL_KEYB_BL_STATE_ON:
            val = 1;
            break;
        default:
            return HAL_E_INVALID_PARAMETER;
    }

    g_mutex_lock( &mutex );
    err = file_set_value_int( fd_keyboard_bl, val );
    g_mutex_unlock( &mutex );

    if (err == HAL_E_OK)
    {
        current_state = state;
    }
    return err;
}

hal_error hal_keyboard_get_bl_state( hal_keyboard_bl_state * const state )
{
    hal_error err;
    int val;

    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_bl );
    CHECK_POINTER_PARAMETER( state );

    *state = current_state;
    return HAL_E_OK;
}

hal_error hal_keyboard_get_device(uint8_t const ** device)
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_keyboard );

    if (device == NULL)
    {
        return HAL_E_NULL_POINTER;
    }
    
    *device = DEV_KEYBOARD;
    return HAL_E_OK;
}

hal_error hal_keyboard_get_key_state( hal_keyboard_key key, bool * const pressed, uint16_t * const count )
{
    CHECK_MODULE_INITIALIZED( initialized );
    CHECK_FEATURE( cap_has_keyboard );

    if ( key >= MAX_KEYS )
    {
        return HAL_E_INVALID_PARAMETER;
    }

    g_mutex_lock( &mutex );
    SET_IF_NOT_NULL( pressed, key_state[key].pressed );
    SET_IF_NOT_NULL( count, key_state[key].count );

    key_state[key].count = 0;

    g_mutex_unlock(&mutex);

    return HAL_E_OK;
}
