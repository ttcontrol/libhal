#include <glib.h>

#include "hal.h"
#include "hal_types.h"
#include "tools.h"

#define DEV_RTC         "/dev/rtc-backup"

hal_error hal_rtc_get_device(uint8_t const ** device)
{
    if (device == NULL)
    {
        return HAL_E_NULL_POINTER;
    }
    
    *device = DEV_RTC;
    return HAL_E_OK;
}
