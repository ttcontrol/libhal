/**************************************************************************
 * Copyright (c) 2013 TTTControl. All rights reserved. Confidential proprietary
 * Schoenbrunnerstrasse 7, A-1040 Wien, Austria. office[at]ttcontrol.com
 **************************************************************************/
/**********************************************************************//**
 * \file hal_gps.h
 *
 * \brief Hardware Abstraction Layer for GPS.
 *
 **************************************************************************/

#ifndef _HAL_GPS_H_
#define _HAL_GPS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "hal_types.h"
#include <glib.h>
#include <stdint.h>

/** \defgroup gps GPS
 *  \brief Functions and datastructures for GPS
 *
 *  The data returned by "get"-functions of the GPS module are refreshed approximately every second.
 *
 *  \{
 */

/** Type of GPS-fix */
typedef enum
{
    HAL_GPS_FIX_INVALID,    /**< Invalid status; no information about the fix yet received */
    HAL_GPS_FIX_NONE,       /**< No fix available */
    HAL_GPS_FIX_2D,         /**< 2D fix; needed for determination of latitude/longitude and time */
    HAL_GPS_FIX_3D,         /**< 3D fix; needed for determination of altitude and speed */
} hal_gps_fix;


/** \brief Initialize GPS module
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_FAIL               Could not initialize module
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 *
 *  \remarks
 *  This function must be called before any other function of this module is called.
 *
 */
hal_error hal_gps_init( );

/** \brief Deinitialize GPS module.
 *
 *  \return #hal_error
 *  \retval #HAL_E_OK                 Everything fine
 *  \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 */
hal_error hal_gps_deinit();

/**
 * \brief Get the current state
 *
 * \param[out] fix       Current fix state
 * \param[out] sat_used  Number of currently used satellites
 * \param[out] sat_vis   Number of satellites in view
 *
 * \return #hal_error
 * \retval #HAL_E_OK                 Everything fine
 * \retval #HAL_E_FAIL               Could not set state
 * \retval #HAL_E_NULL_POINTER       A NULL pointer has been passed
 * \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 *
 */
hal_error hal_gps_get_state( hal_gps_fix * const fix, uint8_t * const sat_used, uint8_t * const sat_vis );

/**
 * \brief Get the current location
 *
 * \param[out] lat  Latitude in degrees
 * \param[out] lon  Longitude in degrees
 * \param[out] alt  Altitude in meters.
 *                  Parameter is optional; pass NULL if not needed
 *
 * \return #hal_error
 * \retval #HAL_E_OK                 Everything fine
 * \retval #HAL_E_FAIL               Could not set state
 * \retval #HAL_E_NULL_POINTER       A NULL pointer has been passed
 * \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 * \retval #HAL_E_GPS_NO_FIX         Values for longitude/latitude are not valid
 *
 * \remarks
 * The longitude and latitude can only be determined if there is a 2D fix. If currently there is no 2D fix
 * available, the function returns #HAL_E_GPS_NO_FIX.
 * Altitude can only be determined if there is a 3D fix. If currently there is no 3D fix available,
 * Not-a-Number (NaN) will be returned for altitude.
 */
hal_error hal_gps_get_location( double * const lat, double * const lon, double * const alt );

/**
 * \brief Get the current speed
 *
 * \param[out] ground  Speed over ground [m/s]
 * \param[out] climb   Vertical speed [m/s]
 *
 * \return #hal_error
 * \retval #HAL_E_OK                 Everything fine
 * \retval #HAL_E_FAIL               Could not set state
 * \retval #HAL_E_NULL_POINTER       A NULL pointer has been passed
 * \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 * \retval #HAL_E_GPS_NO_FIX         Speed could not be determined due to no fix
 *
 * \remarks
 * The speed over ground can only be determined if there is a 2D fix. If currently there is no 2D fix
 * available, the function returns #HAL_E_GPS_NO_FIX.
 * The vertical speed can only be determined if there is a 3D fix. If currently there is no 3D fix available,
 * Not-a-Number (NaN) will be returned for climb.
 */
hal_error hal_gps_get_speed( double * const ground, double * const climb );

/**
 * \brief Get the current UTC time in ISO 8601 format
 *
 * \param[out] str   String where time shall be stored.
 * \param[in]  size  Length of string passed as \c str.
 *
 * \return #hal_error
 * \retval #HAL_E_OK                 Everything fine
 * \retval #HAL_E_FAIL               Could not set state
 * \retval #HAL_E_NULL_POINTER       A NULL pointer has been passed
 * \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 * \retval #HAL_E_GPS_NO_FIX             UTC time could not be determined due to no fix
 *
 */
hal_error hal_gps_get_utc_iso8601 ( uint8_t * const str, uint16_t size );

/**
 * \brief Get the current UTC time as UNIX time
 *
 * \param[out] time  UNIX time
 *
 * \return #hal_error
 * \retval #HAL_E_OK                 Everything fine
 * \retval #HAL_E_FAIL               Could not set state
 * \retval #HAL_E_NULL_POINTER       A NULL pointer has been passed
 * \retval #HAL_E_NOT_INITIALIZED    Module not initialized
 * \retval #HAL_E_GPS_NO_FIX             UTC time could not be determined due to no fix
 *
 */
hal_error hal_gps_get_utc_unix ( double * const time );

/** \} */

#ifdef __cplusplus
}  /* End of the 'extern "C"' block */
#endif

#endif /* _HAL_GPS_H_ */
