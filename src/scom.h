/**************************************************************************
 * Copyright (c) 2013 TTTControl. All rights reserved. Confidential proprietary
 * Schoenbrunnerstrasse 7, A-1040 Wien, Austria. office[at]ttcontrol.com
 **************************************************************************/
/**********************************************************************//**
 * \file scom.h
 *
 * \brief Hardware Abstraction Layer tool for tty's.
 *
 * 
 * \remarks
 *
 **************************************************************************/

#ifndef _SCOM_H_
#define _SCOM_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "hal_types.h"
#include <glib.h>
#include <stdint.h>
#include <termios.h>
#include "hal_modem.h"

/* declarations */


#define SCOM_BUF_SIZE           256
#define SCOM_INIT_TIMEOUT_EES3  4000 /* ms - the EES3 is faster on the regular cases, 3s could also be ok, 4 to safe*/
#define SCOM_INIT_TIMEOUT_EHS8  20000 /* ms - the EHS8 can be slow to load ~10s */
#define SCOM_TIMEOUT            100


/* functions */

hal_error hal_scom_deinit(void); 
hal_error hal_scom_init(void); 

void scom_print_at(char * message);
int get_scom_ref_counter (void);


hal_error hal_scom_transfer(char * const cmd,
                             char cmd_termination,
                             int timeout,
                             char * const response,
                             int size,
                             hal_modem_result_code * const result);

hal_error hal_scom_transfer_no_lock(char * const cmd,
                             char cmd_termination,
                             int timeout,
                             char * const response,
                             int size,
                             hal_modem_result_code * const result);

hal_error hal_scom_transfer_and_wait_no_lock(char * const cmd,
                                            char cmd_termination,
                                            int timeout,
                                            char *str);

hal_error hal_scom_lock();
hal_error hal_scom_unlock();
hal_error hal_scom_reset();

#ifdef __cplusplus
}  /* End of the 'extern "C"' block */
#endif

#endif /* _SCOM_H_ */
