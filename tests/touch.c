#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>

#include <glib.h>

#include "libhal.h"

static struct option options[] =
{
    {"help", no_argument, 0, '?'},
    {"enable", required_argument, 0, 'e'},
    {0, 0, 0, 0}
};

void print_help()
{
    printf("HAL LEDs test\n" \
           "  -? --help       This message\n" \
           "  -e --enable=<n>   0=disabled 1=enabled\n" \
           "\n");
}

void leave(int sig)
{
    if (sig != 0)
    {
        printf("SIGINT caught. Exiting.\n");
    }
    hal_deinit();
    exit(sig);
}



int main(int argc, char **argv)
{
    hal_error err;
    int option_index = 0;
    int c = 0;
    uint8_t en;
    uint8_t *dev = NULL;
    int led0;
    int led1;

    while (c != -1)
    {
        option_index = 0;
        c = getopt_long(argc, argv, "?e:", options,
                        &option_index);

        switch (c)
        {
            case '?':
                print_help();
                exit(0);
                break;
            case 'e':
                if (sscanf(optarg, "%d", &en) == 0)
                {
                    printf("Missing: enable");
                    exit(-1);
                }
                break;
        }
    }

    signal(SIGINT, leave);

    err = hal_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize HAL (%d)\n", err);
        exit(-1);
    }

    err = hal_touch_get_device(&dev);
    if ( err != HAL_E_OK )
    {
        printf("Could not get touchscreen device (%d)\n", err);
        exit(-1);
    }

    if (en == 0)
    {
        hal_touch_set_enable(FALSE);
    }
    else
    {
        hal_touch_set_enable(TRUE);
    }

    leave(0);
}
