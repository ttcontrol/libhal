#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>

#include <glib.h>

#include "libhal.h"

static struct option options[] =
{
    {"help", no_argument, 0, '?'},
    {"led0", required_argument, 0, '0'},
    {"led1", required_argument, 0, '1'},
    {0, 0, 0, 0}
};

void print_help()
{
    printf("HAL LEDs test\n" \
           "  -? --help       This message\n" \
           "  -0 --led0=<n>   switch LED0 off/on\n" \
           "  -1 --led1=<n>   switch LED0 off/on\n" \
           "\n");
}

void leave(int sig)
{
    if (sig != 0)
    {
        printf("SIGINT caught. Exiting.\n");
    }
    hal_deinit();
    exit(sig);
}



int main(int argc, char **argv)
{
    hal_error err;
    int option_index = 0;
    int c = 0;
    uint8_t val;
    int led0;
    int led1;

    while (c != -1)
    {
        option_index = 0;
        c = getopt_long(argc, argv, "?0:1:", options,
                        &option_index);

        switch (c)
        {
            case '?':
                print_help();
                exit(0);
                break;
            case '0':
                if (sscanf(optarg, "%d", &led0) == 0)
                {
                    printf("Missing: LED0");
                    exit(-1);
                }
                break;
            case '1':
                if (sscanf(optarg, "%d", &led1) == 0)
                {
                    printf("Missing: LED1");
                    exit(-1);
                }
                break;
        }
    }

    signal(SIGINT, leave);

    err = hal_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize HAL (%d)\n", err);
        exit(-1);
    }

    err = hal_led_init();
    if ( err != HAL_E_OK )
    {
        printf("Could not initialize LED module (%d)\n", err);
        exit(-1);
    }

    err = hal_led_set_value( HAL_LED_0, led0 );
    if ( err != HAL_E_OK )
    {
        printf("Could not set LED0 value(%d)\n", err);
        exit(-1);
    }

    err = hal_led_set_value( HAL_LED_1, led1 );
    if ( err != HAL_E_OK )
    {
        printf("Could not set LED0 value(%d)\n", err);
        exit(-1);
    }

    for ( int i = 0; i < 2; i++ )
    {
        err = hal_led_get_value( i, &val );
        if ( err != HAL_E_OK )
        {
            printf("Could not read LED%u value(%d)\n", i, err);
            exit(-1);
        }

        printf("LED%u value: %u\n", i, val );
    }

    leave(0);
}
